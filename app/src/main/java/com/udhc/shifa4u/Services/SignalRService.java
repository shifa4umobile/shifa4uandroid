package com.udhc.shifa4u.Services;

/**
 * Created by Fisnik on 4/14/2016.
 */

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.udhc.shifa4u.Activities.Shifa4U;
import com.udhc.shifa4u.Utilities.Events;
import com.udhc.shifa4u.Utilities.GlobalBus;

import java.util.concurrent.ExecutionException;

import microsoft.aspnet.signalr.client.Action;
import microsoft.aspnet.signalr.client.LogLevel;
import microsoft.aspnet.signalr.client.Logger;
import microsoft.aspnet.signalr.client.Platform;
import microsoft.aspnet.signalr.client.SignalRFuture;
import microsoft.aspnet.signalr.client.http.android.AndroidPlatformComponent;
import microsoft.aspnet.signalr.client.hubs.SubscriptionHandler1;
import microsoft.aspnet.signalr.client.hubs.SubscriptionHandler3;
import microsoft.aspnet.signalr.client.transport.ClientTransport;
import microsoft.aspnet.signalr.client.transport.ServerSentEventsTransport;

public class SignalRService extends Service {

    private microsoft.aspnet.signalr.client.hubs.HubConnection mHubConnection;
    private microsoft.aspnet.signalr.client.hubs.HubProxy mHubProxy;
    private Handler mHandler; // to display Toast message
    private final IBinder mBinder = new LocalBinder(); // Binder given to clients

    public static final String BROADCAST_ACTION = "com.android.com.simplechatwithsignalr";

    public SignalRService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mHandler = new Handler(Looper.getMainLooper());
        GlobalBus.getBus().register(this);


   /*     Events.ConnectWithDoctor connectWithDoctor =
                new Events.ConnectWithDoctor("");
        GlobalBus.getBus().post(connectWithDoctor);*/
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        int result = super.onStartCommand(intent, flags, startId);
        startSignalR();
        return result;
    }

    @Override
    public void onDestroy() {
        mHubConnection.stop();
        GlobalBus.getBus().unregister(this);
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // Return the communication channel to the service.
      //  startSignalR();
        return mBinder;
    }

    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
        public SignalRService getService() {
            // Return this instance of SignalRService so clients can call public methods
            return SignalRService.this;
        }
    }

    /**
     * method for clients (activities)
     */


    private void startSignalR() {
        Platform.loadPlatformComponent(new AndroidPlatformComponent());

        String serverUrl = "https://signalr.shifa4u.com/";
       // String serverUrl = "http://192.168.0.92:5001/";
        serverUrl = serverUrl + "signalr";
     //   mHubConnection = new microsoft.aspnet.signalr.client.hubs.HubConnection(serverUrl);

        String token = "";
        try {
           // token = URLEncoder.encode("eyJhbGciOiJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGRzaWctbW9yZSNobWFjLXNoYTI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjU1MiIsImh0dHA6Ly9zY2hlbWFzLnhtbHNvYXAub3JnL3dzLzIwMDUvMDUvaWRlbnRpdHkvY2xhaW1zL25hbWUiOiJzaGFoaWQubXJkQGhvdG1haWwuY29tIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS9hY2Nlc3Njb250cm9sc2VydmljZS8yMDEwLzA3L2NsYWltcy9pZGVudGl0eXByb3ZpZGVyIjoiQVNQLk5FVCBJZGVudGl0eSIsIkFzcE5ldC5JZGVudGl0eS5TZWN1cml0eVN0YW1wIjoiNjVmNGQyNjctM2Y3Mi00MTkzLTlhYjgtODllMTBhZjgyNzk0IiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjoiQ3VzdG9tZXIiLCJ1aWQiOiI1NTIiLCJuYmYiOjE1NDUwMzM2ODYsImV4cCI6MTU0NjI0MzI4NiwiaXNzIjoiaHR0cHM6Ly9hcGkuc2hpZmE0dS5jb20iLCJhdWQiOiJhYWQ0ZGMwNzM5YjY0YzUyOWFiODZjMjEyNmVkMzQxYyJ9.VxD0QK3HoSHWr09l4Xl3PHt3QK1pWcJ5qBzDlyRSp5k", "UTF-8");
        }
        catch (Exception e)
        {
            Log.e("Yourapp", "UnsupportedEncodingException");
        }
        String CONNECTION_QUERYSTRING ="access_token="+Shifa4U.mySharePrefrence.getLogin().getAccessToken();
        mHubConnection = new microsoft.aspnet.signalr.client.hubs.HubConnection(serverUrl, CONNECTION_QUERYSTRING, false, new Logger() {
            @Override
            public void log(String message, LogLevel level) {
                System.out.println(message);
            }
        });

        mHubConnection .connected(new Runnable()
        {
            @Override public void run()
            {
                System.out.println("CONNECTED");
            }
        });
        String SERVER_HUB_CHAT = "IntegratedHUB";
        mHubProxy = mHubConnection.createHubProxy(SERVER_HUB_CHAT);
        ClientTransport clientTransport = new ServerSentEventsTransport(mHubConnection.getLogger());
        SignalRFuture<Void> signalRFuture = mHubConnection.start(clientTransport);

        try {
            signalRFuture.get();
        } catch (InterruptedException | ExecutionException e) {
            Log.e("SimpleSignalR", e.toString());
            return;
        }

        invokeOnConnectedHub("");

        mHubProxy.on("connectionSucceeded", new SubscriptionHandler1<String>() {
            @Override
            public void run(final String msg) {
               /* Intent intent = new Intent(BROADCAST_ACTION);
                intent.putExtra("message", msg);
                sendBroadcast(intent);*/

                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();

                        invokeUpdateCall("");
                    }
                });
            }
        }, String.class);



        mHubProxy.on("organizerConnected", new SubscriptionHandler3<String, String , Long>() {

            @Override
            public void run(String callId, String uniqueId, Long physicianId) {
                Log.i("", "run: ");

                Shifa4U.mySharePrefrence.setUniqueId(uniqueId);
                Shifa4U.mySharePrefrence.setMeetingId(callId);
                Shifa4U.mySharePrefrence.setPhysicianId(physicianId);




                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                      /* startActivity(new Intent(getApplicationContext(), OnlineDoctorGoingToConnectActivity.class));
                       getApplicationContext().*/
                           Events.ConnectWithDoctor connectWithDoctor =
                        new Events.ConnectWithDoctor("");
                GlobalBus.getBus().post(connectWithDoctor);
                    }
                });

            }


        }, String .class , String.class , Long.class);



        mHubProxy.on("connectionFailed", new SubscriptionHandler1<String>() {
            @Override
            public void run(final String msg) {
               /* Intent intent = new Intent(BROADCAST_ACTION);
                intent.putExtra("message", msg);
                sendBroadcast(intent);*/

                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }, String.class);
    }

    public void invokeOnConnectedHub(String message) {
        String SERVER_METHOD_SEND = "OnConnectedHub";
      //  mHubProxy.invoke(SERVER_METHOD_SEND);

        try {
            mHubProxy.invoke(SERVER_METHOD_SEND).done(new Action<Void>()
            {
                @Override public void run(Void obj) throws Exception
                {
                    System.out.println("SENT!");
                }
            });
        }
        catch (Exception e)
        {
            Log.i("", "On Connected HUB: "+e.getMessage());
        }

     /*   mHubProxy.invoke(SERVER_METHOD_SEND).done(new Action<Void>()
        {
            @Override public void run(Void obj) throws Exception
            {
                System.out.println("SENT!");
            }
        });*/

    }



  public void invokeUpdateCall(String message) {
        String SERVER_METHOD_SEND = "UpdateCallRoom";
      //  mHubProxy.invoke(SERVER_METHOD_SEND);

        try {
            mHubProxy.invoke(SERVER_METHOD_SEND).done(new Action<Void>()
            {
                @Override public void run(Void obj) throws Exception
                {
                    System.out.println("SENT!");
                }
            });
        }
        catch (Exception e)
        {
            Log.i("", "UpdateCallRoom: "+e.getMessage());
        }

     /*   mHubProxy.invoke(SERVER_METHOD_SEND).done(new Action<Void>()
        {
            @Override public void run(Void obj) throws Exception
            {
                System.out.println("SENT!");
            }
        });*/

    }


    private void connectionFailed (String msg)
    {

    }


}