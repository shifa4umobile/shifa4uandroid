
package com.udhc.shifa4u.Models.RadioLogyDetail;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Location {

    @SerializedName("VendorLocationId")
    @Expose
    private Integer vendorLocationId;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Contacts")
    @Expose
    private List<Contact> contacts = null;

    public Integer getVendorLocationId() {
        return vendorLocationId;
    }

    public void setVendorLocationId(Integer vendorLocationId) {
        this.vendorLocationId = vendorLocationId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }

}
