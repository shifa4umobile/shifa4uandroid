
package com.udhc.shifa4u.Models.MyOrdersMode;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OpenOrder {

    @SerializedName("Sr")
    @Expose
    private Integer sr;
    @SerializedName("OrderDetailId")
    @Expose
    private Integer orderDetailId;
    @SerializedName("OrderId")
    @Expose
    private String orderId;
    @SerializedName("OrderDate")
    @Expose
    private String orderDate;
    @SerializedName("CustomerContact")
    @Expose
    private Object customerContact;
    @SerializedName("Lab")
    @Expose
    private Object lab;
    @SerializedName("OrderDateTime")
    @Expose
    private String orderDateTime;
    @SerializedName("PaymentMethod")
    @Expose
    private String paymentMethod;
    @SerializedName("PaymentMethodPrefix")
    @Expose
    private String paymentMethodPrefix;
    @SerializedName("IsClosed")
    @Expose
    private Boolean isClosed;
    @SerializedName("IsPayed")
    @Expose
    private Boolean isPayed;
    @SerializedName("TotalAmount")
    @Expose
    private String totalAmount;
    @SerializedName("IsHomeSampleCollection")
    @Expose
    private Boolean isHomeSampleCollection;
    @SerializedName("Zone")
    @Expose
    private Object zone;
    @SerializedName("ZoneName")
    @Expose
    private String zoneName;
    @SerializedName("AreaId")
    @Expose
    private Integer areaId;
    @SerializedName("Area")
    @Expose
    private String area;
    @SerializedName("OrderStatusId")
    @Expose
    private Integer orderStatusId;
    @SerializedName("OrderStatus")
    @Expose
    private String orderStatus;
    @SerializedName("OrderStatusPrefix")
    @Expose
    private String orderStatusPrefix;
    @SerializedName("PaymentStatusId")
    @Expose
    private Integer paymentStatusId;
    @SerializedName("PaymentStatus")
    @Expose
    private String paymentStatus;
    @SerializedName("PaymentStatusPrefix")
    @Expose
    private String paymentStatusPrefix;
    @SerializedName("TimeSlotId")
    @Expose
    private Integer timeSlotId;
    @SerializedName("TimeSlot")
    @Expose
    private Object timeSlot;
    @SerializedName("Customer")
    @Expose
    private String customer;
    @SerializedName("CustomerId")
    @Expose
    private String customerId;
    @SerializedName("ItemId")
    @Expose
    private Integer itemId;
    @SerializedName("ItemType")
    @Expose
    private String itemType;
    @SerializedName("ItemName")
    @Expose
    private String itemName;
    @SerializedName("Details")
    @Expose
    private String details;
    @SerializedName("Price")
    @Expose
    private String price;
    @SerializedName("StatusDate")
    @Expose
    private String statusDate;
    @SerializedName("StatusTime")
    @Expose
    private String statusTime;
    @SerializedName("UserName")
    @Expose
    private String userName;
    @SerializedName("AppointmentDate")
    @Expose
    private String appointmentDate;
    @SerializedName("CustomerAddress")
    @Expose
    private String customerAddress;
    @SerializedName("CustomerMobile")
    @Expose
    private String customerMobile;
    @SerializedName("VendorName")
    @Expose
    private String vendorName;
    @SerializedName("CostPrice")
    @Expose
    private String costPrice;
    @SerializedName("SalePrice")
    @Expose
    private String salePrice;
    @SerializedName("Date")
    @Expose
    private Object date;
    @SerializedName("Category")
    @Expose
    private String category;
    @SerializedName("Shifa4UCode")
    @Expose
    private String shifa4UCode;
    @SerializedName("ProviderItemName")
    @Expose
    private String providerItemName;
    @SerializedName("ProviderItemCode")
    @Expose
    private String providerItemCode;
    @SerializedName("VendorLocationId")
    @Expose
    private Integer vendorLocationId;
    @SerializedName("OrderTime")
    @Expose
    private String orderTime;
    @SerializedName("AppointmentTime")
    @Expose
    private String appointmentTime;
    @SerializedName("Collection")
    @Expose
    private String collection;
    @SerializedName("OrderDetailsVM")
    @Expose
    private List<OrderDetailsVM> orderDetailsVM = null;

    public Integer getSr() {
        return sr;
    }

    public void setSr(Integer sr) {
        this.sr = sr;
    }

    public Integer getOrderDetailId() {
        return orderDetailId;
    }

    public void setOrderDetailId(Integer orderDetailId) {
        this.orderDetailId = orderDetailId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public Object getCustomerContact() {
        return customerContact;
    }

    public void setCustomerContact(Object customerContact) {
        this.customerContact = customerContact;
    }

    public Object getLab() {
        return lab;
    }

    public void setLab(Object lab) {
        this.lab = lab;
    }

    public String getOrderDateTime() {
        return orderDateTime;
    }

    public void setOrderDateTime(String orderDateTime) {
        this.orderDateTime = orderDateTime;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentMethodPrefix() {
        return paymentMethodPrefix;
    }

    public void setPaymentMethodPrefix(String paymentMethodPrefix) {
        this.paymentMethodPrefix = paymentMethodPrefix;
    }

    public Boolean getIsClosed() {
        return isClosed;
    }

    public void setIsClosed(Boolean isClosed) {
        this.isClosed = isClosed;
    }

    public Boolean getIsPayed() {
        return isPayed;
    }

    public void setIsPayed(Boolean isPayed) {
        this.isPayed = isPayed;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Boolean getIsHomeSampleCollection() {
        return isHomeSampleCollection;
    }

    public void setIsHomeSampleCollection(Boolean isHomeSampleCollection) {
        this.isHomeSampleCollection = isHomeSampleCollection;
    }

    public Object getZone() {
        return zone;
    }

    public void setZone(Object zone) {
        this.zone = zone;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public Integer getOrderStatusId() {
        return orderStatusId;
    }

    public void setOrderStatusId(Integer orderStatusId) {
        this.orderStatusId = orderStatusId;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderStatusPrefix() {
        return orderStatusPrefix;
    }

    public void setOrderStatusPrefix(String orderStatusPrefix) {
        this.orderStatusPrefix = orderStatusPrefix;
    }

    public Integer getPaymentStatusId() {
        return paymentStatusId;
    }

    public void setPaymentStatusId(Integer paymentStatusId) {
        this.paymentStatusId = paymentStatusId;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getPaymentStatusPrefix() {
        return paymentStatusPrefix;
    }

    public void setPaymentStatusPrefix(String paymentStatusPrefix) {
        this.paymentStatusPrefix = paymentStatusPrefix;
    }

    public Integer getTimeSlotId() {
        return timeSlotId;
    }

    public void setTimeSlotId(Integer timeSlotId) {
        this.timeSlotId = timeSlotId;
    }

    public Object getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(Object timeSlot) {
        this.timeSlot = timeSlot;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(String statusDate) {
        this.statusDate = statusDate;
    }

    public String getStatusTime() {
        return statusTime;
    }

    public void setStatusTime(String statusTime) {
        this.statusTime = statusTime;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAppointmentDate() {
        return appointmentDate;
    }

    public void setAppointmentDate(String appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCustomerMobile() {
        return customerMobile;
    }

    public void setCustomerMobile(String customerMobile) {
        this.customerMobile = customerMobile;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(String costPrice) {
        this.costPrice = costPrice;
    }

    public String getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(String salePrice) {
        this.salePrice = salePrice;
    }

    public Object getDate() {
        return date;
    }

    public void setDate(Object date) {
        this.date = date;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getShifa4UCode() {
        return shifa4UCode;
    }

    public void setShifa4UCode(String shifa4UCode) {
        this.shifa4UCode = shifa4UCode;
    }

    public String getProviderItemName() {
        return providerItemName;
    }

    public void setProviderItemName(String providerItemName) {
        this.providerItemName = providerItemName;
    }

    public String getProviderItemCode() {
        return providerItemCode;
    }

    public void setProviderItemCode(String providerItemCode) {
        this.providerItemCode = providerItemCode;
    }

    public Integer getVendorLocationId() {
        return vendorLocationId;
    }

    public void setVendorLocationId(Integer vendorLocationId) {
        this.vendorLocationId = vendorLocationId;
    }

    public String getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    public String getAppointmentTime() {
        return appointmentTime;
    }

    public void setAppointmentTime(String appointmentTime) {
        this.appointmentTime = appointmentTime;
    }

    public String getCollection() {
        return collection;
    }

    public void setCollection(String collection) {
        this.collection = collection;
    }

    public List<OrderDetailsVM> getOrderDetailsVM() {
        return orderDetailsVM;
    }

    public void setOrderDetailsVM(List<OrderDetailsVM> orderDetailsVM) {
        this.orderDetailsVM = orderDetailsVM;
    }

}
