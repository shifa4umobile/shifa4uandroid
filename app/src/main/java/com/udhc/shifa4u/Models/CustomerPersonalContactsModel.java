package com.udhc.shifa4u.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CustomerPersonalContactsModel {

@SerializedName("AddressContactId")
@Expose
private String addressContactId;
@SerializedName("PhoneNumberContactId")
@Expose
private String phoneNumberContactId;

public String getAddressContactId() {
return addressContactId;
}

public void setAddressContactId(String addressContactId) {
this.addressContactId = addressContactId;
}

public String getPhoneNumberContactId() {
return phoneNumberContactId;
}

public void setPhoneNumberContactId(String phoneNumberContactId) {
this.phoneNumberContactId = phoneNumberContactId;
}

}