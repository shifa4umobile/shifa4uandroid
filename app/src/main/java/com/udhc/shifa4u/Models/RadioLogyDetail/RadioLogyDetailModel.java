
package com.udhc.shifa4u.Models.RadioLogyDetail;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RadioLogyDetailModel {

    @SerializedName("TotalRecords")
    @Expose
    private Integer totalRecords;
    @SerializedName("Radiologies")
    @Expose
    private List<Radiology> radiologies = null;

    public Integer getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(Integer totalRecords) {
        this.totalRecords = totalRecords;
    }

    public List<Radiology> getRadiologies() {
        return radiologies;
    }

    public void setRadiologies(List<Radiology> radiologies) {
        this.radiologies = radiologies;
    }

}
