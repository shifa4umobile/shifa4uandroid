package com.udhc.shifa4u.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CustomerProfileModel {

@SerializedName("UserId")
@Expose
private Long userId;
@SerializedName("FirstName")
@Expose
private String firstName;

@SerializedName("IsVerified")
@Expose
private boolean IsVerified;
@SerializedName("LastName")
@Expose
private String lastName;
@SerializedName("FullName")
@Expose
private String fullName;
@SerializedName("FatherName")
@Expose
private Object fatherName;
@SerializedName("Gender")
@Expose
private String gender;
@SerializedName("Title")
@Expose
private Object title;
@SerializedName("CNIC")
@Expose
private Object cNIC;
@SerializedName("DateofBirth")
@Expose
private String dateofBirth;
@SerializedName("MaritalStatus")
@Expose
private Object maritalStatus;
@SerializedName("AreaId")
@Expose
private Integer areaId;
@SerializedName("CountryId")
@Expose
private Integer countryId;
@SerializedName("CityId")
@Expose
private Integer cityId;
@SerializedName("Sex")
@Expose
private Boolean sex;
@SerializedName("LoginName")
@Expose
private String loginName;
@SerializedName("Country")
@Expose
private String country;
@SerializedName("City")
@Expose
private String city;
@SerializedName("Area")
@Expose
private String area;
@SerializedName("Address")
@Expose
private String address;
@SerializedName("MobileNo")
@Expose
private String mobileNo;
@SerializedName("Email")
@Expose
private String email;

    public boolean isVerified() {
        return IsVerified;
    }

    public void setVerified(boolean verified) {
        IsVerified = verified;
    }

    public void setUserId(Long userId) {
this.userId = userId;
}

public String getFirstName() {
return firstName;
}

public void setFirstName(String firstName) {
this.firstName = firstName;
}

public String getLastName() {
return lastName;
}

public void setLastName(String lastName) {
this.lastName = lastName;
}

public String getFullName() {
return fullName;
}

public void setFullName(String fullName) {
this.fullName = fullName;
}

public Object getFatherName() {
return fatherName;
}

public void setFatherName(Object fatherName) {
this.fatherName = fatherName;
}

public String getGender() {
return gender;
}

public void setGender(String gender) {
this.gender = gender;
}

public Object getTitle() {
return title;
}

public void setTitle(Object title) {
this.title = title;
}

public Object getCNIC() {
return cNIC;
}

public void setCNIC(Object cNIC) {
this.cNIC = cNIC;
}

public String getDateofBirth() {
return dateofBirth;
}

public void setDateofBirth(String dateofBirth) {
this.dateofBirth = dateofBirth;
}

public Object getMaritalStatus() {
return maritalStatus;
}

public void setMaritalStatus(Object maritalStatus) {
this.maritalStatus = maritalStatus;
}

public Integer getAreaId() {
return areaId;
}

public void setAreaId(Integer areaId) {
this.areaId = areaId;
}

public Integer getCountryId() {
return countryId;
}

public void setCountryId(Integer countryId) {
this.countryId = countryId;
}

public Integer getCityId() {
return cityId;
}

public void setCityId(Integer cityId) {
this.cityId = cityId;
}

public Boolean getSex() {
return sex;
}

public void setSex(Boolean sex) {
this.sex = sex;
}

public String getLoginName() {
return loginName;
}

public void setLoginName(String loginName) {
this.loginName = loginName;
}

public String getCountry() {
return country;
}

public void setCountry(String country) {
this.country = country;
}

public String getCity() {
return city;
}

public void setCity(String city) {
this.city = city;
}

public String getArea() {
return area;
}

public void setArea(String area) {
this.area = area;
}

public String getAddress() {
return address;
}

public void setAddress(String address) {
this.address = address;
}

public String getMobileNo() {
return mobileNo;
}

public void setMobileNo(String mobileNo) {
this.mobileNo = mobileNo;
}

public String getEmail() {
return email;
}

public void setEmail(String email) {
this.email = email;
}

}