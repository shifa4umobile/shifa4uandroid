
package com.udhc.shifa4u.Models.MedicalPackages;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MedicalPackagesModel implements Serializable {

    @SerializedName("LabPackageId")
    @Expose
    private Integer labPackageId;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("PackageType")
    @Expose
    private String packageType;
    @SerializedName("LabPackageVendorLocationId")
    @Expose
    private Integer labPackageVendorLocationId;
    @SerializedName("Ranking")
    @Expose
    private Integer ranking;
    @SerializedName("ShortDescription")
    @Expose
    private String shortDescription;
    @SerializedName("LabTests")
    @Expose
    private List<LabTest> labTests = null;
    @SerializedName("LabPackagesComparison")
    @Expose
    private List<LabPackagesComparison> labPackagesComparison = null;

    public Integer getLabPackageId() {
        return labPackageId;
    }

    public void setLabPackageId(Integer labPackageId) {
        this.labPackageId = labPackageId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPackageType() {
        return packageType;
    }

    public void setPackageType(String packageType) {
        this.packageType = packageType;
    }

    public Integer getLabPackageVendorLocationId() {
        return labPackageVendorLocationId;
    }

    public void setLabPackageVendorLocationId(Integer labPackageVendorLocationId) {
        this.labPackageVendorLocationId = labPackageVendorLocationId;
    }

    public Integer getRanking() {
        return ranking;
    }

    public void setRanking(Integer ranking) {
        this.ranking = ranking;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public List<LabTest> getLabTests() {
        return labTests;
    }

    public void setLabTests(List<LabTest> labTests) {
        this.labTests = labTests;
    }

    public List<LabPackagesComparison> getLabPackagesComparison() {
        return labPackagesComparison;
    }

    public void setLabPackagesComparison(List<LabPackagesComparison> labPackagesComparison) {
        this.labPackagesComparison = labPackagesComparison;
    }

}
