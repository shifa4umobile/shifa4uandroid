package com.udhc.shifa4u.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CountryModel {

@SerializedName("CountryId")
@Expose
private Integer countryId;
@SerializedName("CountryName")
@Expose
private String countryName;
@SerializedName("MedicalProductCategoryPrefix")
@Expose
private Object medicalProductCategoryPrefix;
@SerializedName("SearchWord")
@Expose
private Object searchWord;
@SerializedName("Cities")
@Expose
private Object cities;

public Integer getCountryId() {
return countryId;
}

public void setCountryId(Integer countryId) {
this.countryId = countryId;
}

public String getCountryName() {
return countryName;
}

public void setCountryName(String countryName) {
this.countryName = countryName;
}

public Object getMedicalProductCategoryPrefix() {
return medicalProductCategoryPrefix;
}

public void setMedicalProductCategoryPrefix(Object medicalProductCategoryPrefix) {
this.medicalProductCategoryPrefix = medicalProductCategoryPrefix;
}

public Object getSearchWord() {
return searchWord;
}

public void setSearchWord(Object searchWord) {
this.searchWord = searchWord;
}

public Object getCities() {
return cities;
}

public void setCities(Object cities) {
this.cities = cities;
}

}