
package com.udhc.shifa4u.Models.PaymentMode;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FinalOrderDataModel {

    @SerializedName("PaymentMethodPrefix")
    @Expose
    private String paymentMethodPrefix;
    @SerializedName("CustomerAddress")
    @Expose
    private String customerAddress;
    @SerializedName("CustomerContact")
    @Expose
    private String customerContact;
    @SerializedName("OrderDetails")
    @Expose
    private List<OrderDetail> orderDetails = null;

    public String getPaymentMethodPrefix() {
        return paymentMethodPrefix;
    }

    public void setPaymentMethodPrefix(String paymentMethodPrefix) {
        this.paymentMethodPrefix = paymentMethodPrefix;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCustomerContact() {
        return customerContact;
    }

    public void setCustomerContact(String customerContact) {
        this.customerContact = customerContact;
    }

    public List<OrderDetail> getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(List<OrderDetail> orderDetails) {
        this.orderDetails = orderDetails;
    }

}
