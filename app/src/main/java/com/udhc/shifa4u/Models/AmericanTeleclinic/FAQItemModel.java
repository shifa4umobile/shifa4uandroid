package com.udhc.shifa4u.Models.AmericanTeleclinic;

import android.animation.TimeInterpolator;

public class FAQItemModel {
    public final String question;
    public final String answer;
    public final TimeInterpolator interpolator;

    public FAQItemModel(String question,String answer,  TimeInterpolator interpolator) {
        this.question = question;
        this.answer = answer;
        this.interpolator = interpolator;
    }
}