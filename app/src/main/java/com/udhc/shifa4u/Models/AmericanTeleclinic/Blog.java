package com.udhc.shifa4u.Models.AmericanTeleclinic;

import android.support.annotation.NonNull;

import java.io.Serializable;
import java.util.Comparator;

public class Blog implements Serializable
{
    private String ShortDescription;

    private String HomePageImageUrl;

    private String BlogCategory;

    private String Descrption;

    private String PublishDateTime;

    private String BlogId;

    private String Author;

    private String ImageUrl;

    private String BlogCategoryId;

    private String Title;

    private String[] BlogTags;

    public String getShortDescription ()
    {
        return ShortDescription;
    }

    public void setShortDescription (String ShortDescription)
    {
        this.ShortDescription = ShortDescription;
    }

    public String getHomePageImageUrl ()
    {
        return HomePageImageUrl;
    }

    public void setHomePageImageUrl (String HomePageImageUrl)
    {
        this.HomePageImageUrl = HomePageImageUrl;
    }

    public String getBlogCategory ()
    {
        return BlogCategory;
    }

    public void setBlogCategory (String BlogCategory)
    {
        this.BlogCategory = BlogCategory;
    }

    public String getDescrption ()
    {
        return Descrption;
    }

    public void setDescrption (String Descrption)
    {
        this.Descrption = Descrption;
    }

    public String getPublishDateTime ()
    {
        return PublishDateTime;
    }

    public void setPublishDateTime (String PublishDateTime)
    {
        this.PublishDateTime = PublishDateTime;
    }

    public String getBlogId ()
    {
        return BlogId;
    }

    public void setBlogId (String BlogId)
    {
        this.BlogId = BlogId;
    }

    public String getAuthor ()
    {
        return Author;
    }

    public void setAuthor (String Author)
    {
        this.Author = Author;
    }

    public String getImageUrl ()
    {
        return ImageUrl;
    }

    public void setImageUrl (String ImageUrl)
    {
        this.ImageUrl = ImageUrl;
    }

    public String getBlogCategoryId ()
    {
        return BlogCategoryId;
    }

    public void setBlogCategoryId (String BlogCategoryId)
    {
        this.BlogCategoryId = BlogCategoryId;
    }

    public String getTitle ()
    {
        return Title;
    }

    public void setTitle (String Title)
    {
        this.Title = Title;
    }

    public String[] getBlogTags ()
    {
        return BlogTags;
    }

    public void setBlogTags (String[] BlogTags)
    {
        this.BlogTags = BlogTags;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [ShortDescription = "+ShortDescription+", HomePageImageUrl = "+HomePageImageUrl+", BlogCategory = "+BlogCategory+", Descrption = "+Descrption+", PublishDateTime = "+PublishDateTime+", BlogId = "+BlogId+", Author = "+Author+", ImageUrl = "+ImageUrl+", BlogCategoryId = "+BlogCategoryId+", Title = "+Title+", BlogTags = "+BlogTags+"]";
    }

    public static Comparator<Blog> BlogDateComparator = new Comparator<Blog>() {

        public int compare(Blog s1, Blog s2) {
            String StudentName1 = s1.getPublishDateTime().toUpperCase();
            String StudentName2 = s2.getPublishDateTime().toUpperCase();

            //ascending order
           // return StudentName1.compareTo(StudentName2);

            //descending order
            return StudentName2.compareTo(StudentName1);
        }};
}