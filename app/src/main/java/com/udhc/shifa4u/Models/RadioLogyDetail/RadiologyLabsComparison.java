
package com.udhc.shifa4u.Models.RadioLogyDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RadiologyLabsComparison {

    @SerializedName("ImagingId")
    @Expose
    private Integer imagingId;
    @SerializedName("ProductLineId")
    @Expose
    private Integer productLineId;
    @SerializedName("LabName")
    @Expose
    private String labName;
    @SerializedName("LabId")
    @Expose
    private Integer labId;
    @SerializedName("YourPrice")
    @Expose
    private Double yourPrice;
    @SerializedName("VendorPrice")
    @Expose
    private Double vendorPrice;
    @SerializedName("Discount")
    @Expose
    private Double discount;
    @SerializedName("ImageUrlWeb")
    @Expose
    private String imageUrlWeb;

    public Integer getImagingId() {
        return imagingId;
    }

    public void setImagingId(Integer imagingId) {
        this.imagingId = imagingId;
    }

    public Integer getProductLineId() {
        return productLineId;
    }

    public void setProductLineId(Integer productLineId) {
        this.productLineId = productLineId;
    }

    public String getLabName() {
        return labName;
    }

    public void setLabName(String labName) {
        this.labName = labName;
    }

    public Integer getLabId() {
        return labId;
    }

    public void setLabId(Integer labId) {
        this.labId = labId;
    }

    public Double getYourPrice() {
        return yourPrice;
    }

    public void setYourPrice(Double yourPrice) {
        this.yourPrice = yourPrice;
    }

    public Double getVendorPrice() {
        return vendorPrice;
    }

    public void setVendorPrice(Double vendorPrice) {
        this.vendorPrice = vendorPrice;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public String getImageUrlWeb() {
        return imageUrlWeb;
    }

    public void setImageUrlWeb(String imageUrlWeb) {
        this.imageUrlWeb = imageUrlWeb;
    }

}
