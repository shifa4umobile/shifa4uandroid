package com.udhc.shifa4u.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LabDetailModel {

@SerializedName("LabCenterProductId")
@Expose
private Integer labCenterProductId;
@SerializedName("LabName")
@Expose
private String labName;
@SerializedName("LabId")
@Expose
private Integer labId;
@SerializedName("ShortDescription")
@Expose
private String shortDescription;
@SerializedName("OtherNames")
@Expose
private String otherNames;
@SerializedName("ImageUrlWeb")
@Expose
private String imageUrlWeb;
@SerializedName("TestName")
@Expose
private String testName;
@SerializedName("TestId")
@Expose
private Integer testId;
@SerializedName("YourPrice")
@Expose
private Double yourPrice;
@SerializedName("VendorPrice")
@Expose
private Double vendorPrice;
@SerializedName("ImageId")
@Expose
private Integer imageId;

public Integer getLabCenterProductId() {
return labCenterProductId;
}

public void setLabCenterProductId(Integer labCenterProductId) {
this.labCenterProductId = labCenterProductId;
}

public String getLabName() {
return labName;
}

public void setLabName(String labName) {
this.labName = labName;
}

public Integer getLabId() {
return labId;
}

public void setLabId(Integer labId) {
this.labId = labId;
}

public String getShortDescription() {
return shortDescription;
}

public void setShortDescription(String shortDescription) {
this.shortDescription = shortDescription;
}

public String getOtherNames() {
return otherNames;
}

public void setOtherNames(String otherNames) {
this.otherNames = otherNames;
}

public String getImageUrlWeb() {
return imageUrlWeb;
}

public void setImageUrlWeb(String imageUrlWeb) {
this.imageUrlWeb = imageUrlWeb;
}

public String getTestName() {
return testName;
}

public void setTestName(String testName) {
this.testName = testName;
}

public Integer getTestId() {
return testId;
}

public void setTestId(Integer testId) {
this.testId = testId;
}

public Double getYourPrice() {
return yourPrice;
}

public void setYourPrice(Double yourPrice) {
this.yourPrice = yourPrice;
}

public Double getVendorPrice() {
return vendorPrice;
}

public void setVendorPrice(Double vendorPrice) {
this.vendorPrice = vendorPrice;
}

public Integer getImageId() {
return imageId;
}

public void setImageId(Integer imageId) {
this.imageId = imageId;
}

}