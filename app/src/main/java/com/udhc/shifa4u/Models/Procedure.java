
package com.udhc.shifa4u.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Procedure {

    @SerializedName("Vendor")
    @Expose
    private Object vendor;
    @SerializedName("MedicalSpecialty")
    @Expose
    private Object medicalSpecialty;
    @SerializedName("MedicalBranch")
    @Expose
    private Object medicalBranch;
    @SerializedName("Disorder")
    @Expose
    private Object disorder;
    @SerializedName("Symptom")
    @Expose
    private Object symptom;
    @SerializedName("Countries_Cities_Areas")
    @Expose
    private Object countriesCitiesAreas;
    @SerializedName("PhysiciansSchedules")
    @Expose
    private Object physiciansSchedules;
    @SerializedName("PhysicianGenders")
    @Expose
    private Object physicianGenders;
    @SerializedName("Countries")
    @Expose
    private Object countries;
    @SerializedName("ProductLine_OrderId")
    @Expose
    private Integer productLineOrderId;
    @SerializedName("ProductLine_ProductName")
    @Expose
    private String productLineProductName;
    @SerializedName("ProductLine_CostPrice")
    @Expose
    private Double productLineCostPrice;
    @SerializedName("ProductLine_SalePrice")
    @Expose
    private Double productLineSalePrice;
    @SerializedName("ProductLine_DiscountedPrice")
    @Expose
    private Double productLineDiscountedPrice;
    @SerializedName("ProductLine_ConsultantFee")
    @Expose
    private Double productLineConsultantFee;
    @SerializedName("MedicalProductSearchId")
    @Expose
    private Integer medicalProductSearchId;
    @SerializedName("MedicalProductSearchTag")
    @Expose
    private Object medicalProductSearchTag;
    @SerializedName("MedicalProductPrefix")
    @Expose
    private Object medicalProductPrefix;
    @SerializedName("MedicalProductId")
    @Expose
    private Integer medicalProductId;
    @SerializedName("ProductLineId")
    @Expose
    private Integer productLineId;
    @SerializedName("MedicalProductName")
    @Expose
    private String medicalProductName;
    @SerializedName("MedicalProductDescription")
    @Expose
    private String medicalProductDescription;
    @SerializedName("MedicalProductUrduName")
    @Expose
    private String medicalProductUrduName;
    @SerializedName("MedicalProductUrduDescription")
    @Expose
    private String medicalProductUrduDescription;
    @SerializedName("MedicalProductOtherNames")
    @Expose
    private String medicalProductOtherNames;
    @SerializedName("MedicalProductImageUrlWeb")
    @Expose
    private String medicalProductImageUrlWeb;
    @SerializedName("MedicalProductImageUrlMob")
    @Expose
    private String medicalProductImageUrlMob;
    @SerializedName("MedicalProductSpecialities")
    @Expose
    private String medicalProductSpecialities;
    @SerializedName("MedicalProductSymptoms")
    @Expose
    private Object medicalProductSymptoms;
    @SerializedName("MedicalProductDisorders")
    @Expose
    private Object medicalProductDisorders;
    @SerializedName("MedicalProductPrice")
    @Expose
    private Double medicalProductPrice;
    @SerializedName("VendorId")
    @Expose
    private Integer vendorId;
    @SerializedName("VendorName")
    @Expose
    private Object vendorName;
    @SerializedName("VendorAddress")
    @Expose
    private Object vendorAddress;
    @SerializedName("VendorMobileContact")
    @Expose
    private Object vendorMobileContact;
    @SerializedName("VendorLandlineContact")
    @Expose
    private Object vendorLandlineContact;
    @SerializedName("VendorUANContact")
    @Expose
    private Object vendorUANContact;
    @SerializedName("VendorImageUrlWeb")
    @Expose
    private Object vendorImageUrlWeb;
    @SerializedName("VendorGoogleMapImage")
    @Expose
    private Object vendorGoogleMapImage;
    @SerializedName("VendorGoogleMapImageLink")
    @Expose
    private Object vendorGoogleMapImageLink;
    @SerializedName("VendorLocationId")
    @Expose
    private Integer vendorLocationId;
    @SerializedName("PhysicianId")
    @Expose
    private Integer physicianId;
    @SerializedName("PhysicianName")
    @Expose
    private Object physicianName;
    @SerializedName("PhysicianSpeciality")
    @Expose
    private Object physicianSpeciality;
    @SerializedName("PhysicianBoardCertification")
    @Expose
    private Object physicianBoardCertification;
    @SerializedName("PhysicianEducation")
    @Expose
    private Object physicianEducation;
    @SerializedName("PhysicianExperience")
    @Expose
    private Object physicianExperience;
    @SerializedName("PhysicianTraining")
    @Expose
    private Object physicianTraining;
    @SerializedName("PhysicianAreasOfInterest")
    @Expose
    private Object physicianAreasOfInterest;
    @SerializedName("PhysicianLanguagesSpoken")
    @Expose
    private Object physicianLanguagesSpoken;
    @SerializedName("PhysicianImageUrlWeb")
    @Expose
    private Object physicianImageUrlWeb;
    @SerializedName("PhysicianCurrentPracticeLocation")
    @Expose
    private Object physicianCurrentPracticeLocation;
    @SerializedName("PhysicianSpecialties")
    @Expose
    private Object physicianSpecialties;
    @SerializedName("PhysicianScheduleId")
    @Expose
    private Integer physicianScheduleId;
    @SerializedName("PhysicianScheduleWeekDay")
    @Expose
    private Object physicianScheduleWeekDay;
    @SerializedName("PhysicianScheduleStartTime")
    @Expose
    private String physicianScheduleStartTime;
    @SerializedName("PhysicianScheduleEndTime")
    @Expose
    private String physicianScheduleEndTime;
    @SerializedName("PhysicianScheduleNotes")
    @Expose
    private Object physicianScheduleNotes;
    @SerializedName("MedicalSpecialityId")
    @Expose
    private Integer medicalSpecialityId;
    @SerializedName("SpecialityCategoryId")
    @Expose
    private Object specialityCategoryId;
    @SerializedName("SpecialityName")
    @Expose
    private Object specialityName;
    @SerializedName("SpecialityDescription")
    @Expose
    private Object specialityDescription;
    @SerializedName("Specialist")
    @Expose
    private Object specialist;
    @SerializedName("SpecialityRanking")
    @Expose
    private Object specialityRanking;
    @SerializedName("SpecialityUrduName")
    @Expose
    private Object specialityUrduName;
    @SerializedName("SpecialityUrduDescription")
    @Expose
    private Object specialityUrduDescription;
    @SerializedName("SpecialityImageUrlWeb")
    @Expose
    private Object specialityImageUrlWeb;
    @SerializedName("PhysicianProfileDetail")
    @Expose
    private Object physicianProfileDetail;
    @SerializedName("PysicanProfileDetailShifa4u")
    @Expose
    private Object pysicanProfileDetailShifa4u;
    @SerializedName("PhysicianAffiliations")
    @Expose
    private Object physicianAffiliations;
    @SerializedName("ConditionTreated")
    @Expose
    private Object conditionTreated;
    @SerializedName("ImageWebId")
    @Expose
    private Integer imageWebId;
    @SerializedName("ImageMobileId")
    @Expose
    private Integer imageMobileId;
    @SerializedName("FlagId")
    @Expose
    private Integer flagId;

    public Object getVendor() {
        return vendor;
    }

    public void setVendor(Object vendor) {
        this.vendor = vendor;
    }

    public Object getMedicalSpecialty() {
        return medicalSpecialty;
    }

    public void setMedicalSpecialty(Object medicalSpecialty) {
        this.medicalSpecialty = medicalSpecialty;
    }

    public Object getMedicalBranch() {
        return medicalBranch;
    }

    public void setMedicalBranch(Object medicalBranch) {
        this.medicalBranch = medicalBranch;
    }

    public Object getDisorder() {
        return disorder;
    }

    public void setDisorder(Object disorder) {
        this.disorder = disorder;
    }

    public Object getSymptom() {
        return symptom;
    }

    public void setSymptom(Object symptom) {
        this.symptom = symptom;
    }

    public Object getCountriesCitiesAreas() {
        return countriesCitiesAreas;
    }

    public void setCountriesCitiesAreas(Object countriesCitiesAreas) {
        this.countriesCitiesAreas = countriesCitiesAreas;
    }

    public Object getPhysiciansSchedules() {
        return physiciansSchedules;
    }

    public void setPhysiciansSchedules(Object physiciansSchedules) {
        this.physiciansSchedules = physiciansSchedules;
    }

    public Object getPhysicianGenders() {
        return physicianGenders;
    }

    public void setPhysicianGenders(Object physicianGenders) {
        this.physicianGenders = physicianGenders;
    }

    public Object getCountries() {
        return countries;
    }

    public void setCountries(Object countries) {
        this.countries = countries;
    }

    public Integer getProductLineOrderId() {
        return productLineOrderId;
    }

    public void setProductLineOrderId(Integer productLineOrderId) {
        this.productLineOrderId = productLineOrderId;
    }

    public String getProductLineProductName() {
        return productLineProductName;
    }

    public void setProductLineProductName(String productLineProductName) {
        this.productLineProductName = productLineProductName;
    }

    public Double getProductLineCostPrice() {
        return productLineCostPrice;
    }

    public void setProductLineCostPrice(Double productLineCostPrice) {
        this.productLineCostPrice = productLineCostPrice;
    }

    public Double getProductLineSalePrice() {
        return productLineSalePrice;
    }

    public void setProductLineSalePrice(Double productLineSalePrice) {
        this.productLineSalePrice = productLineSalePrice;
    }

    public Double getProductLineDiscountedPrice() {
        return productLineDiscountedPrice;
    }

    public void setProductLineDiscountedPrice(Double productLineDiscountedPrice) {
        this.productLineDiscountedPrice = productLineDiscountedPrice;
    }

    public Double getProductLineConsultantFee() {
        return productLineConsultantFee;
    }

    public void setProductLineConsultantFee(Double productLineConsultantFee) {
        this.productLineConsultantFee = productLineConsultantFee;
    }

    public Integer getMedicalProductSearchId() {
        return medicalProductSearchId;
    }

    public void setMedicalProductSearchId(Integer medicalProductSearchId) {
        this.medicalProductSearchId = medicalProductSearchId;
    }

    public Object getMedicalProductSearchTag() {
        return medicalProductSearchTag;
    }

    public void setMedicalProductSearchTag(Object medicalProductSearchTag) {
        this.medicalProductSearchTag = medicalProductSearchTag;
    }

    public Object getMedicalProductPrefix() {
        return medicalProductPrefix;
    }

    public void setMedicalProductPrefix(Object medicalProductPrefix) {
        this.medicalProductPrefix = medicalProductPrefix;
    }

    public Integer getMedicalProductId() {
        return medicalProductId;
    }

    public void setMedicalProductId(Integer medicalProductId) {
        this.medicalProductId = medicalProductId;
    }

    public Integer getProductLineId() {
        return productLineId;
    }

    public void setProductLineId(Integer productLineId) {
        this.productLineId = productLineId;
    }

    public String getMedicalProductName() {
        return medicalProductName;
    }

    public void setMedicalProductName(String medicalProductName) {
        this.medicalProductName = medicalProductName;
    }

    public String getMedicalProductDescription() {
        return medicalProductDescription;
    }

    public void setMedicalProductDescription(String medicalProductDescription) {
        this.medicalProductDescription = medicalProductDescription;
    }

    public String getMedicalProductUrduName() {
        return medicalProductUrduName;
    }

    public void setMedicalProductUrduName(String medicalProductUrduName) {
        this.medicalProductUrduName = medicalProductUrduName;
    }

    public String getMedicalProductUrduDescription() {
        return medicalProductUrduDescription;
    }

    public void setMedicalProductUrduDescription(String medicalProductUrduDescription) {
        this.medicalProductUrduDescription = medicalProductUrduDescription;
    }

    public String getMedicalProductOtherNames() {
        return medicalProductOtherNames;
    }

    public void setMedicalProductOtherNames(String medicalProductOtherNames) {
        this.medicalProductOtherNames = medicalProductOtherNames;
    }

    public String getMedicalProductImageUrlWeb() {
        return medicalProductImageUrlWeb;
    }

    public void setMedicalProductImageUrlWeb(String medicalProductImageUrlWeb) {
        this.medicalProductImageUrlWeb = medicalProductImageUrlWeb;
    }

    public String getMedicalProductImageUrlMob() {
        return medicalProductImageUrlMob;
    }

    public void setMedicalProductImageUrlMob(String medicalProductImageUrlMob) {
        this.medicalProductImageUrlMob = medicalProductImageUrlMob;
    }

    public String getMedicalProductSpecialities() {
        return medicalProductSpecialities;
    }

    public void setMedicalProductSpecialities(String medicalProductSpecialities) {
        this.medicalProductSpecialities = medicalProductSpecialities;
    }

    public Object getMedicalProductSymptoms() {
        return medicalProductSymptoms;
    }

    public void setMedicalProductSymptoms(Object medicalProductSymptoms) {
        this.medicalProductSymptoms = medicalProductSymptoms;
    }

    public Object getMedicalProductDisorders() {
        return medicalProductDisorders;
    }

    public void setMedicalProductDisorders(Object medicalProductDisorders) {
        this.medicalProductDisorders = medicalProductDisorders;
    }

    public Double getMedicalProductPrice() {
        return medicalProductPrice;
    }

    public void setMedicalProductPrice(Double medicalProductPrice) {
        this.medicalProductPrice = medicalProductPrice;
    }

    public Integer getVendorId() {
        return vendorId;
    }

    public void setVendorId(Integer vendorId) {
        this.vendorId = vendorId;
    }

    public Object getVendorName() {
        return vendorName;
    }

    public void setVendorName(Object vendorName) {
        this.vendorName = vendorName;
    }

    public Object getVendorAddress() {
        return vendorAddress;
    }

    public void setVendorAddress(Object vendorAddress) {
        this.vendorAddress = vendorAddress;
    }

    public Object getVendorMobileContact() {
        return vendorMobileContact;
    }

    public void setVendorMobileContact(Object vendorMobileContact) {
        this.vendorMobileContact = vendorMobileContact;
    }

    public Object getVendorLandlineContact() {
        return vendorLandlineContact;
    }

    public void setVendorLandlineContact(Object vendorLandlineContact) {
        this.vendorLandlineContact = vendorLandlineContact;
    }

    public Object getVendorUANContact() {
        return vendorUANContact;
    }

    public void setVendorUANContact(Object vendorUANContact) {
        this.vendorUANContact = vendorUANContact;
    }

    public Object getVendorImageUrlWeb() {
        return vendorImageUrlWeb;
    }

    public void setVendorImageUrlWeb(Object vendorImageUrlWeb) {
        this.vendorImageUrlWeb = vendorImageUrlWeb;
    }

    public Object getVendorGoogleMapImage() {
        return vendorGoogleMapImage;
    }

    public void setVendorGoogleMapImage(Object vendorGoogleMapImage) {
        this.vendorGoogleMapImage = vendorGoogleMapImage;
    }

    public Object getVendorGoogleMapImageLink() {
        return vendorGoogleMapImageLink;
    }

    public void setVendorGoogleMapImageLink(Object vendorGoogleMapImageLink) {
        this.vendorGoogleMapImageLink = vendorGoogleMapImageLink;
    }

    public Integer getVendorLocationId() {
        return vendorLocationId;
    }

    public void setVendorLocationId(Integer vendorLocationId) {
        this.vendorLocationId = vendorLocationId;
    }

    public Integer getPhysicianId() {
        return physicianId;
    }

    public void setPhysicianId(Integer physicianId) {
        this.physicianId = physicianId;
    }

    public Object getPhysicianName() {
        return physicianName;
    }

    public void setPhysicianName(Object physicianName) {
        this.physicianName = physicianName;
    }

    public Object getPhysicianSpeciality() {
        return physicianSpeciality;
    }

    public void setPhysicianSpeciality(Object physicianSpeciality) {
        this.physicianSpeciality = physicianSpeciality;
    }

    public Object getPhysicianBoardCertification() {
        return physicianBoardCertification;
    }

    public void setPhysicianBoardCertification(Object physicianBoardCertification) {
        this.physicianBoardCertification = physicianBoardCertification;
    }

    public Object getPhysicianEducation() {
        return physicianEducation;
    }

    public void setPhysicianEducation(Object physicianEducation) {
        this.physicianEducation = physicianEducation;
    }

    public Object getPhysicianExperience() {
        return physicianExperience;
    }

    public void setPhysicianExperience(Object physicianExperience) {
        this.physicianExperience = physicianExperience;
    }

    public Object getPhysicianTraining() {
        return physicianTraining;
    }

    public void setPhysicianTraining(Object physicianTraining) {
        this.physicianTraining = physicianTraining;
    }

    public Object getPhysicianAreasOfInterest() {
        return physicianAreasOfInterest;
    }

    public void setPhysicianAreasOfInterest(Object physicianAreasOfInterest) {
        this.physicianAreasOfInterest = physicianAreasOfInterest;
    }

    public Object getPhysicianLanguagesSpoken() {
        return physicianLanguagesSpoken;
    }

    public void setPhysicianLanguagesSpoken(Object physicianLanguagesSpoken) {
        this.physicianLanguagesSpoken = physicianLanguagesSpoken;
    }

    public Object getPhysicianImageUrlWeb() {
        return physicianImageUrlWeb;
    }

    public void setPhysicianImageUrlWeb(Object physicianImageUrlWeb) {
        this.physicianImageUrlWeb = physicianImageUrlWeb;
    }

    public Object getPhysicianCurrentPracticeLocation() {
        return physicianCurrentPracticeLocation;
    }

    public void setPhysicianCurrentPracticeLocation(Object physicianCurrentPracticeLocation) {
        this.physicianCurrentPracticeLocation = physicianCurrentPracticeLocation;
    }

    public Object getPhysicianSpecialties() {
        return physicianSpecialties;
    }

    public void setPhysicianSpecialties(Object physicianSpecialties) {
        this.physicianSpecialties = physicianSpecialties;
    }

    public Integer getPhysicianScheduleId() {
        return physicianScheduleId;
    }

    public void setPhysicianScheduleId(Integer physicianScheduleId) {
        this.physicianScheduleId = physicianScheduleId;
    }

    public Object getPhysicianScheduleWeekDay() {
        return physicianScheduleWeekDay;
    }

    public void setPhysicianScheduleWeekDay(Object physicianScheduleWeekDay) {
        this.physicianScheduleWeekDay = physicianScheduleWeekDay;
    }

    public String getPhysicianScheduleStartTime() {
        return physicianScheduleStartTime;
    }

    public void setPhysicianScheduleStartTime(String physicianScheduleStartTime) {
        this.physicianScheduleStartTime = physicianScheduleStartTime;
    }

    public String getPhysicianScheduleEndTime() {
        return physicianScheduleEndTime;
    }

    public void setPhysicianScheduleEndTime(String physicianScheduleEndTime) {
        this.physicianScheduleEndTime = physicianScheduleEndTime;
    }

    public Object getPhysicianScheduleNotes() {
        return physicianScheduleNotes;
    }

    public void setPhysicianScheduleNotes(Object physicianScheduleNotes) {
        this.physicianScheduleNotes = physicianScheduleNotes;
    }

    public Integer getMedicalSpecialityId() {
        return medicalSpecialityId;
    }

    public void setMedicalSpecialityId(Integer medicalSpecialityId) {
        this.medicalSpecialityId = medicalSpecialityId;
    }

    public Object getSpecialityCategoryId() {
        return specialityCategoryId;
    }

    public void setSpecialityCategoryId(Object specialityCategoryId) {
        this.specialityCategoryId = specialityCategoryId;
    }

    public Object getSpecialityName() {
        return specialityName;
    }

    public void setSpecialityName(Object specialityName) {
        this.specialityName = specialityName;
    }

    public Object getSpecialityDescription() {
        return specialityDescription;
    }

    public void setSpecialityDescription(Object specialityDescription) {
        this.specialityDescription = specialityDescription;
    }

    public Object getSpecialist() {
        return specialist;
    }

    public void setSpecialist(Object specialist) {
        this.specialist = specialist;
    }

    public Object getSpecialityRanking() {
        return specialityRanking;
    }

    public void setSpecialityRanking(Object specialityRanking) {
        this.specialityRanking = specialityRanking;
    }

    public Object getSpecialityUrduName() {
        return specialityUrduName;
    }

    public void setSpecialityUrduName(Object specialityUrduName) {
        this.specialityUrduName = specialityUrduName;
    }

    public Object getSpecialityUrduDescription() {
        return specialityUrduDescription;
    }

    public void setSpecialityUrduDescription(Object specialityUrduDescription) {
        this.specialityUrduDescription = specialityUrduDescription;
    }

    public Object getSpecialityImageUrlWeb() {
        return specialityImageUrlWeb;
    }

    public void setSpecialityImageUrlWeb(Object specialityImageUrlWeb) {
        this.specialityImageUrlWeb = specialityImageUrlWeb;
    }

    public Object getPhysicianProfileDetail() {
        return physicianProfileDetail;
    }

    public void setPhysicianProfileDetail(Object physicianProfileDetail) {
        this.physicianProfileDetail = physicianProfileDetail;
    }

    public Object getPysicanProfileDetailShifa4u() {
        return pysicanProfileDetailShifa4u;
    }

    public void setPysicanProfileDetailShifa4u(Object pysicanProfileDetailShifa4u) {
        this.pysicanProfileDetailShifa4u = pysicanProfileDetailShifa4u;
    }

    public Object getPhysicianAffiliations() {
        return physicianAffiliations;
    }

    public void setPhysicianAffiliations(Object physicianAffiliations) {
        this.physicianAffiliations = physicianAffiliations;
    }

    public Object getConditionTreated() {
        return conditionTreated;
    }

    public void setConditionTreated(Object conditionTreated) {
        this.conditionTreated = conditionTreated;
    }

    public Integer getImageWebId() {
        return imageWebId;
    }

    public void setImageWebId(Integer imageWebId) {
        this.imageWebId = imageWebId;
    }

    public Integer getImageMobileId() {
        return imageMobileId;
    }

    public void setImageMobileId(Integer imageMobileId) {
        this.imageMobileId = imageMobileId;
    }

    public Integer getFlagId() {
        return flagId;
    }

    public void setFlagId(Integer flagId) {
        this.flagId = flagId;
    }

}
