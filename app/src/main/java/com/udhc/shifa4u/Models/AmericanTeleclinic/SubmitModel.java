package com.udhc.shifa4u.Models.AmericanTeleclinic;

public class SubmitModel {
    private String patientName;
    private String contactNo;
    private int specialitySelected = -1;
    private int doctorSelected = -1;
    private String medialIsuues;
    private String speciality;
    private String doctorName;
    private String PhysicianId;
    private String MedicalSpecialityId;
    private String ProductLineId;

    public String getPhysicianId() {
        return PhysicianId;
    }

    public void setPhysicianId(String physicianId) {
        PhysicianId = physicianId;
    }

    public String getMedicalSpecialityId() {
        return MedicalSpecialityId;
    }

    public void setMedicalSpecialityId(String medicalSpecialityId) {
        MedicalSpecialityId = medicalSpecialityId;
    }

    public String getProductLineId() {
        return ProductLineId;
    }

    public void setProductLineId(String productLineId) {
        ProductLineId = productLineId;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public int getSpecialitySelected() {
        return specialitySelected;
    }

    public void setSpecialitySelected(int specialitySelected) {
        this.specialitySelected = specialitySelected;
    }

    public int getDoctorSelected() {
        return doctorSelected;
    }

    public void setDoctorSelected(int doctorSelected) {
        this.doctorSelected = doctorSelected;
    }

    public String getMedialIsuues() {
        return medialIsuues;
    }

    public void setMedialIsuues(String medialIsuues) {
        this.medialIsuues = medialIsuues;
    }
}
