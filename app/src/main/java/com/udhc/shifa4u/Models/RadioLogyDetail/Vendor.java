
package com.udhc.shifa4u.Models.RadioLogyDetail;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Vendor {

    @SerializedName("ImagingId")
    @Expose
    private Integer imagingId;
    @SerializedName("ImagingName")
    @Expose
    private String imagingName;
    @SerializedName("LabId")
    @Expose
    private Integer labId;
    @SerializedName("LabName")
    @Expose
    private String labName;
    @SerializedName("Locations")
    @Expose
    private List<Location> locations = null;

    public Integer getImagingId() {
        return imagingId;
    }

    public void setImagingId(Integer imagingId) {
        this.imagingId = imagingId;
    }

    public String getImagingName() {
        return imagingName;
    }

    public void setImagingName(String imagingName) {
        this.imagingName = imagingName;
    }

    public Integer getLabId() {
        return labId;
    }

    public void setLabId(Integer labId) {
        this.labId = labId;
    }

    public String getLabName() {
        return labName;
    }

    public void setLabName(String labName) {
        this.labName = labName;
    }

    public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }

}
