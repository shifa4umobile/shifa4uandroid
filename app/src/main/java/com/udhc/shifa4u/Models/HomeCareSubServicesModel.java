
package com.udhc.shifa4u.Models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomeCareSubServicesModel {

    @SerializedName("Status")
    @Expose
    private Boolean status;
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("Alphabet")
    @Expose
    private List<String> alphabet = null;
    @SerializedName("ProceduresLetters")
    @Expose
    private List<String> proceduresLetters = null;
    @SerializedName("Procedures")
    @Expose
    private List<Procedure> procedures = null;
    @SerializedName("TotalPages")
    @Expose
    private Integer totalPages;
    @SerializedName("TotalRecords")
    @Expose
    private Integer totalRecords;
    @SerializedName("Description")
    @Expose
    private Object description;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<String> getAlphabet() {
        return alphabet;
    }

    public void setAlphabet(List<String> alphabet) {
        this.alphabet = alphabet;
    }

    public List<String> getProceduresLetters() {
        return proceduresLetters;
    }

    public void setProceduresLetters(List<String> proceduresLetters) {
        this.proceduresLetters = proceduresLetters;
    }

    public List<Procedure> getProcedures() {
        return procedures;
    }

    public void setProcedures(List<Procedure> procedures) {
        this.procedures = procedures;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public Integer getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(Integer totalRecords) {
        this.totalRecords = totalRecords;
    }

    public Object getDescription() {
        return description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

}
