package com.udhc.shifa4u.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CityModel {

@SerializedName("CityId")
@Expose
private Integer cityId;
@SerializedName("CityName")
@Expose
private String cityName;
@SerializedName("MedicalProductCategoryPrefix")
@Expose
private Object medicalProductCategoryPrefix;
@SerializedName("SearchWord")
@Expose
private Object searchWord;
@SerializedName("Areas")
@Expose
private Object areas;

public Integer getCityId() {
return cityId;
}

public void setCityId(Integer cityId) {
this.cityId = cityId;
}

public String getCityName() {
return cityName;
}

public void setCityName(String cityName) {
this.cityName = cityName;
}

public Object getMedicalProductCategoryPrefix() {
return medicalProductCategoryPrefix;
}

public void setMedicalProductCategoryPrefix(Object medicalProductCategoryPrefix) {
this.medicalProductCategoryPrefix = medicalProductCategoryPrefix;
}

public Object getSearchWord() {
return searchWord;
}

public void setSearchWord(Object searchWord) {
this.searchWord = searchWord;
}

public Object getAreas() {
return areas;
}

public void setAreas(Object areas) {
this.areas = areas;
}

}