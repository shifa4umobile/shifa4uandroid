
package com.udhc.shifa4u.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LabTestList implements Serializable {

    @SerializedName("LabTestId")
    @Expose
    private Integer labTestId;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("ShortName")
    @Expose
    private Object shortName;
    @SerializedName("ShortDescription")
    @Expose
    private String shortDescription;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("TestPopularityProfileTypeId")
    @Expose
    private Integer testPopularityProfileTypeId;
    @SerializedName("TestChemistryProfileId")
    @Expose
    private Integer testChemistryProfileId;
    @SerializedName("SpecimenRequired")
    @Expose
    private Object specimenRequired;
    @SerializedName("SpecimenVolume")
    @Expose
    private String specimenVolume;
    @SerializedName("Container")
    @Expose
    private String container;
    @SerializedName("ContainerColor")
    @Expose
    private String containerColor;
    @SerializedName("Quantity")
    @Expose
    private Integer quantity;
    @SerializedName("Temperature")
    @Expose
    private Object temperature;
    @SerializedName("FastingNeed")
    @Expose
    private Object fastingNeed;
    @SerializedName("ProvocationNeed")
    @Expose
    private Object provocationNeed;
    @SerializedName("TestResults")
    @Expose
    private String testResults;
    @SerializedName("IsActive")
    @Expose
    private Boolean isActive;
    @SerializedName("Notes")
    @Expose
    private Object notes;
    @SerializedName("MedicalProductCategoryProfilePrefix")
    @Expose
    private Object medicalProductCategoryProfilePrefix;
    @SerializedName("SampleType")
    @Expose
    private String sampleType;
    @SerializedName("SpecialInstructions")
    @Expose
    private Object specialInstructions;
    @SerializedName("TestPreparation")
    @Expose
    private String testPreparation;
    @SerializedName("OtherNames")
    @Expose
    private String otherNames;
    @SerializedName("YourPrice")
    @Expose
    private Double yourPrice;
    @SerializedName("OriginalPrice")
    @Expose
    private Integer originalPrice;
    @SerializedName("Ranking")
    @Expose
    private Integer ranking;
    @SerializedName("Labs")
    @Expose
    private Object labs;

    public Integer getLabTestId() {
        return labTestId;
    }

    public void setLabTestId(Integer labTestId) {
        this.labTestId = labTestId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getShortName() {
        return shortName;
    }

    public void setShortName(Object shortName) {
        this.shortName = shortName;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getTestPopularityProfileTypeId() {
        return testPopularityProfileTypeId;
    }

    public void setTestPopularityProfileTypeId(Integer testPopularityProfileTypeId) {
        this.testPopularityProfileTypeId = testPopularityProfileTypeId;
    }

    public Integer getTestChemistryProfileId() {
        return testChemistryProfileId;
    }

    public void setTestChemistryProfileId(Integer testChemistryProfileId) {
        this.testChemistryProfileId = testChemistryProfileId;
    }

    public Object getSpecimenRequired() {
        return specimenRequired;
    }

    public void setSpecimenRequired(Object specimenRequired) {
        this.specimenRequired = specimenRequired;
    }

    public String getSpecimenVolume() {
        return specimenVolume;
    }

    public void setSpecimenVolume(String specimenVolume) {
        this.specimenVolume = specimenVolume;
    }

    public String getContainer() {
        return container;
    }

    public void setContainer(String container) {
        this.container = container;
    }

    public String getContainerColor() {
        return containerColor;
    }

    public void setContainerColor(String containerColor) {
        this.containerColor = containerColor;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Object getTemperature() {
        return temperature;
    }

    public void setTemperature(Object temperature) {
        this.temperature = temperature;
    }

    public Object getFastingNeed() {
        return fastingNeed;
    }

    public void setFastingNeed(Object fastingNeed) {
        this.fastingNeed = fastingNeed;
    }

    public Object getProvocationNeed() {
        return provocationNeed;
    }

    public void setProvocationNeed(Object provocationNeed) {
        this.provocationNeed = provocationNeed;
    }

    public String getTestResults() {
        return testResults;
    }

    public void setTestResults(String testResults) {
        this.testResults = testResults;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Object getNotes() {
        return notes;
    }

    public void setNotes(Object notes) {
        this.notes = notes;
    }

    public Object getMedicalProductCategoryProfilePrefix() {
        return medicalProductCategoryProfilePrefix;
    }

    public void setMedicalProductCategoryProfilePrefix(Object medicalProductCategoryProfilePrefix) {
        this.medicalProductCategoryProfilePrefix = medicalProductCategoryProfilePrefix;
    }

    public String getSampleType() {
        return sampleType;
    }

    public void setSampleType(String sampleType) {
        this.sampleType = sampleType;
    }

    public Object getSpecialInstructions() {
        return specialInstructions;
    }

    public void setSpecialInstructions(Object specialInstructions) {
        this.specialInstructions = specialInstructions;
    }

    public String getTestPreparation() {
        return testPreparation;
    }

    public void setTestPreparation(String testPreparation) {
        this.testPreparation = testPreparation;
    }

    public String getOtherNames() {
        return otherNames;
    }

    public void setOtherNames(String otherNames) {
        this.otherNames = otherNames;
    }

    public Double getYourPrice() {
        return yourPrice;
    }

    public void setYourPrice(Double yourPrice) {
        this.yourPrice = yourPrice;
    }

    public Integer getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(Integer originalPrice) {
        this.originalPrice = originalPrice;
    }

    public Integer getRanking() {
        return ranking;
    }

    public void setRanking(Integer ranking) {
        this.ranking = ranking;
    }

    public Object getLabs() {
        return labs;
    }

    public void setLabs(Object labs) {
        this.labs = labs;
    }

}
