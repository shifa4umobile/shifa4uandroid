package com.udhc.shifa4u.Models;

public class OnlineDoctorMeetingModel {
    private String ReasonForCall;
    private String Temperature;
    private String HeartRate;
    private String Systolic;
    private String Diastolic;


    public String getReasonForCall() {
        return ReasonForCall;
    }

    public void setReasonForCall(String reasonForCall) {
        ReasonForCall = reasonForCall;
    }

    public String getTemperature() {
        return Temperature;
    }

    public void setTemperature(String temperature) {
        Temperature = temperature;
    }

    public String getHeartRate() {
        return HeartRate;
    }

    public void setHeartRate(String heartRate) {
        HeartRate = heartRate;
    }

    public String getSystolic() {
        return Systolic;
    }

    public void setSystolic(String systolic) {
        Systolic = systolic;
    }

    public String getDiastolic() {
        return Diastolic;
    }

    public void setDiastolic(String diastolic) {
        Diastolic = diastolic;
    }
}
