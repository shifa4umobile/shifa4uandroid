
package com.udhc.shifa4u.Models.RadiologyCategories;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BodyLocation {

    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("BodyLocationId")
    @Expose
    private Integer bodyLocationId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getBodyLocationId() {
        return bodyLocationId;
    }

    public void setBodyLocationId(Integer bodyLocationId) {
        this.bodyLocationId = bodyLocationId;
    }

}
