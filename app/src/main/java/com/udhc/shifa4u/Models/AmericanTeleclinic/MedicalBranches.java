package com.udhc.shifa4u.Models.AmericanTeleclinic;

public class MedicalBranches
{
    private String Name;

    private String Description;

    private String MedicalSpecialityId;

    private String Ranking;

    private String MedicalBranchId;

    private String ImageWebUrl;

    private String DetailDescription;

    private String ImageMobileUrl;

    public String getName ()
    {
        return Name;
    }

    public void setName (String Name)
    {
        this.Name = Name;
    }

    public String getDescription ()
    {
        return Description;
    }

    public void setDescription (String Description)
    {
        this.Description = Description;
    }

    public String getMedicalSpecialityId ()
    {
        return MedicalSpecialityId;
    }

    public void setMedicalSpecialityId (String MedicalSpecialityId)
    {
        this.MedicalSpecialityId = MedicalSpecialityId;
    }

    public String getRanking ()
    {
        return Ranking;
    }

    public void setRanking (String Ranking)
    {
        this.Ranking = Ranking;
    }

    public String getMedicalBranchId ()
    {
        return MedicalBranchId;
    }

    public void setMedicalBranchId (String MedicalBranchId)
    {
        this.MedicalBranchId = MedicalBranchId;
    }

    public String getImageWebUrl ()
    {
        return ImageWebUrl;
    }

    public void setImageWebUrl (String ImageWebUrl)
    {
        this.ImageWebUrl = ImageWebUrl;
    }

    public String getDetailDescription ()
    {
        return DetailDescription;
    }

    public void setDetailDescription (String DetailDescription)
    {
        this.DetailDescription = DetailDescription;
    }

    public String getImageMobileUrl ()
    {
        return ImageMobileUrl;
    }

    public void setImageMobileUrl (String ImageMobileUrl)
    {
        this.ImageMobileUrl = ImageMobileUrl;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Name = "+Name+", Description = "+Description+", MedicalSpecialityId = "+MedicalSpecialityId+", Ranking = "+Ranking+", MedicalBranchId = "+MedicalBranchId+", ImageWebUrl = "+ImageWebUrl+", DetailDescription = "+DetailDescription+", ImageMobileUrl = "+ImageMobileUrl+"]";
    }
}
