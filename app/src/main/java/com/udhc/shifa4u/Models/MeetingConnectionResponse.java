package com.udhc.shifa4u.Models;

public class MeetingConnectionResponse {

    private String UrgentCallId;
    private String CallerId;
    private String ReasonForCall;
    private String JoinCallUrl;
    private String MeetingId;


    public String getMeetingId() {
        return MeetingId;
    }

    public void setMeetingId(String meetingId) {
        MeetingId = meetingId;
    }

    public String getUrgentCallId() {
        return UrgentCallId;
    }

    public void setUrgentCallId(String urgentCallId) {
        UrgentCallId = urgentCallId;
    }

    public String getCallerId() {
        return CallerId;
    }

    public void setCallerId(String callerId) {
        CallerId = callerId;
    }

    public String getReasonForCall() {
        return ReasonForCall;
    }

    public void setReasonForCall(String reasonForCall) {
        ReasonForCall = reasonForCall;
    }

    public String getJoinCallUrl() {
        return JoinCallUrl;
    }

    public void setJoinCallUrl(String joinCallUrl) {
        JoinCallUrl = joinCallUrl;
    }
}
