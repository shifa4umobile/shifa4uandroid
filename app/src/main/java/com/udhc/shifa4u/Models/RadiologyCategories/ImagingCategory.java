
package com.udhc.shifa4u.Models.RadiologyCategories;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ImagingCategory {

    @SerializedName("ImagingCategoryId")
    @Expose
    private Integer imagingCategoryId;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("MedicalProductCategoryProfilePrefix")
    @Expose
    private Object medicalProductCategoryProfilePrefix;
    @SerializedName("ImagingId")
    @Expose
    private Integer imagingId;
    @SerializedName("CityId")
    @Expose
    private Integer cityId;
    @SerializedName("BodyLocationId")
    @Expose
    private Integer bodyLocationId;
    @SerializedName("BodyLocationPartId")
    @Expose
    private Integer bodyLocationPartId;
    @SerializedName("BodyLocationPartTypeId")
    @Expose
    private Integer bodyLocationPartTypeId;
    @SerializedName("Skip")
    @Expose
    private Integer skip;
    @SerializedName("Take")
    @Expose
    private Integer take;
    @SerializedName("IsActive")
    @Expose
    private Boolean isActive;

    public Integer getImagingCategoryId() {
        return imagingCategoryId;
    }

    public void setImagingCategoryId(Integer imagingCategoryId) {
        this.imagingCategoryId = imagingCategoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getMedicalProductCategoryProfilePrefix() {
        return medicalProductCategoryProfilePrefix;
    }

    public void setMedicalProductCategoryProfilePrefix(Object medicalProductCategoryProfilePrefix) {
        this.medicalProductCategoryProfilePrefix = medicalProductCategoryProfilePrefix;
    }

    public Integer getImagingId() {
        return imagingId;
    }

    public void setImagingId(Integer imagingId) {
        this.imagingId = imagingId;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public Integer getBodyLocationId() {
        return bodyLocationId;
    }

    public void setBodyLocationId(Integer bodyLocationId) {
        this.bodyLocationId = bodyLocationId;
    }

    public Integer getBodyLocationPartId() {
        return bodyLocationPartId;
    }

    public void setBodyLocationPartId(Integer bodyLocationPartId) {
        this.bodyLocationPartId = bodyLocationPartId;
    }

    public Integer getBodyLocationPartTypeId() {
        return bodyLocationPartTypeId;
    }

    public void setBodyLocationPartTypeId(Integer bodyLocationPartTypeId) {
        this.bodyLocationPartTypeId = bodyLocationPartTypeId;
    }

    public Integer getSkip() {
        return skip;
    }

    public void setSkip(Integer skip) {
        this.skip = skip;
    }

    public Integer getTake() {
        return take;
    }

    public void setTake(Integer take) {
        this.take = take;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

}
