package com.udhc.shifa4u.Models.AmericanTeleclinic;

public class Provider {

        String name;
        String speciality;
        String location;
        int photoId;

    Provider(String name, String speciality,String location, int photoId) {
            this.name = name;
           this.speciality = speciality;
           this.location = location;
            this.photoId = photoId;
        }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getPhotoId() {
        return photoId;
    }

    public void setPhotoId(int photoId) {
        this.photoId = photoId;
    }
}
