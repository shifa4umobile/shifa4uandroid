package com.udhc.shifa4u.Models.AmericanTeleclinic;

import java.io.Serializable;

public class Physician implements Serializable
{
    private String GenderProfileId;

    private String ImageWebId;

    private String Specialties;

    private String CurrentPracticeLocation;

    private String ProfileDetail;

    private String TotalPages;

    private String MedicalBranchId;

    private String ProfileDetailShifa4u;

    private String PhysicianName;

    private String Specialist;

    private String BranchName;

    private String PhysicianId;

    private String MedicalSpecialityId;

    private String FlagImageId;

    private String ProductLineId;

    private String Gender;

    private String PhysicianNameWithDesignation;

    private String SpecialityName;

    private String ConditionsTreated;

    private String ImageMobId;

    public String getGenderProfileId ()
    {
        return GenderProfileId;
    }

    public void setGenderProfileId (String GenderProfileId)
    {
        this.GenderProfileId = GenderProfileId;
    }

    public String getImageWebId ()
    {
        return ImageWebId;
    }

    public void setImageWebId (String ImageWebId)
    {
        this.ImageWebId = ImageWebId;
    }

    public String getSpecialties ()
    {
        return Specialties;
    }

    public void setSpecialties (String Specialties)
    {
        this.Specialties = Specialties;
    }

    public String getCurrentPracticeLocation ()
    {
        return CurrentPracticeLocation;
    }

    public void setCurrentPracticeLocation (String CurrentPracticeLocation)
    {
        this.CurrentPracticeLocation = CurrentPracticeLocation;
    }

    public String getProfileDetail ()
    {
        return ProfileDetail;
    }

    public void setProfileDetail (String ProfileDetail)
    {
        this.ProfileDetail = ProfileDetail;
    }

    public String getTotalPages ()
    {
        return TotalPages;
    }

    public void setTotalPages (String TotalPages)
    {
        this.TotalPages = TotalPages;
    }

    public String getMedicalBranchId ()
    {
        return MedicalBranchId;
    }

    public void setMedicalBranchId (String MedicalBranchId)
    {
        this.MedicalBranchId = MedicalBranchId;
    }

    public String getProfileDetailShifa4u ()
    {
        return ProfileDetailShifa4u;
    }

    public void setProfileDetailShifa4u (String ProfileDetailShifa4u)
    {
        this.ProfileDetailShifa4u = ProfileDetailShifa4u;
    }

    public String getPhysicianName ()
    {
        return PhysicianName;
    }

    public void setPhysicianName (String PhysicianName)
    {
        this.PhysicianName = PhysicianName;
    }

    public String getSpecialist ()
    {
        return Specialist;
    }

    public void setSpecialist (String Specialist)
    {
        this.Specialist = Specialist;
    }

    public String getBranchName ()
    {
        return BranchName;
    }

    public void setBranchName (String BranchName)
    {
        this.BranchName = BranchName;
    }

    public String getPhysicianId ()
    {
        return PhysicianId;
    }

    public void setPhysicianId (String PhysicianId)
    {
        this.PhysicianId = PhysicianId;
    }

    public String getMedicalSpecialityId ()
    {
        return MedicalSpecialityId;
    }

    public void setMedicalSpecialityId (String MedicalSpecialityId)
    {
        this.MedicalSpecialityId = MedicalSpecialityId;
    }

    public String getFlagImageId ()
    {
        return FlagImageId;
    }

    public void setFlagImageId (String FlagImageId)
    {
        this.FlagImageId = FlagImageId;
    }

    public String getProductLineId ()
    {
        return ProductLineId;
    }

    public void setProductLineId (String ProductLineId)
    {
        this.ProductLineId = ProductLineId;
    }

    public String getGender ()
    {
        return Gender;
    }

    public void setGender (String Gender)
    {
        this.Gender = Gender;
    }

    public String getPhysicianNameWithDesignation ()
    {
        return PhysicianNameWithDesignation;
    }

    public void setPhysicianNameWithDesignation (String PhysicianNameWithDesignation)
    {
        this.PhysicianNameWithDesignation = PhysicianNameWithDesignation;
    }

    public String getSpecialityName ()
    {
        return SpecialityName;
    }

    public void setSpecialityName (String SpecialityName)
    {
        this.SpecialityName = SpecialityName;
    }

    public String getConditionsTreated ()
    {
        return ConditionsTreated;
    }

    public void setConditionsTreated (String ConditionsTreated)
    {
        this.ConditionsTreated = ConditionsTreated;
    }

    public String getImageMobId ()
    {
        return ImageMobId;
    }

    public void setImageMobId (String ImageMobId)
    {
        this.ImageMobId = ImageMobId;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [GenderProfileId = "+GenderProfileId+", ImageWebId = "+ImageWebId+", Specialties = "+Specialties+", CurrentPracticeLocation = "+CurrentPracticeLocation+", ProfileDetail = "+ProfileDetail+", TotalPages = "+TotalPages+", MedicalBranchId = "+MedicalBranchId+", ProfileDetailShifa4u = "+ProfileDetailShifa4u+", PhysicianName = "+PhysicianName+", Specialist = "+Specialist+", BranchName = "+BranchName+", PhysicianId = "+PhysicianId+", MedicalSpecialityId = "+MedicalSpecialityId+", FlagImageId = "+FlagImageId+", ProductLineId = "+ProductLineId+", Gender = "+Gender+", PhysicianNameWithDesignation = "+PhysicianNameWithDesignation+", SpecialityName = "+SpecialityName+", ConditionsTreated = "+ConditionsTreated+", ImageMobId = "+ImageMobId+"]";
    }
}
