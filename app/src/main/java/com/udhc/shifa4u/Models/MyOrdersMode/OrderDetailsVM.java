
package com.udhc.shifa4u.Models.MyOrdersMode;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderDetailsVM {

    @SerializedName("Sr")
    @Expose
    private Integer sr;
    @SerializedName("OrderDetailId")
    @Expose
    private Integer orderDetailId;
    @SerializedName("OrderId")
    @Expose
    private String orderId;
    @SerializedName("OrderDate")
    @Expose
    private String orderDate;
    @SerializedName("Lab")
    @Expose
    private Object lab;
    @SerializedName("CustomerContact")
    @Expose
    private Object customerContact;
    @SerializedName("PaymentMethod")
    @Expose
    private Object paymentMethod;
    @SerializedName("PaymentMethodPrefix")
    @Expose
    private Object paymentMethodPrefix;
    @SerializedName("IsClosed")
    @Expose
    private Boolean isClosed;
    @SerializedName("IsPayed")
    @Expose
    private Boolean isPayed;
    @SerializedName("TotalAmount")
    @Expose
    private Object totalAmount;
    @SerializedName("IsHomeSampleCollection")
    @Expose
    private Boolean isHomeSampleCollection;
    @SerializedName("Zone")
    @Expose
    private Object zone;
    @SerializedName("ZoneName")
    @Expose
    private String zoneName;
    @SerializedName("AreaId")
    @Expose
    private Integer areaId;
    @SerializedName("Area")
    @Expose
    private Object area;
    @SerializedName("OrderStatusId")
    @Expose
    private Integer orderStatusId;
    @SerializedName("OrderStatus")
    @Expose
    private String orderStatus;
    @SerializedName("OrderStatusPrefix")
    @Expose
    private Object orderStatusPrefix;
    @SerializedName("PaymentStatusId")
    @Expose
    private Integer paymentStatusId;
    @SerializedName("PaymentStatus")
    @Expose
    private Object paymentStatus;
    @SerializedName("PaymentStatusPrefix")
    @Expose
    private Object paymentStatusPrefix;
    @SerializedName("TimeSlotId")
    @Expose
    private Integer timeSlotId;
    @SerializedName("TimeSlot")
    @Expose
    private Object timeSlot;
    @SerializedName("Customer")
    @Expose
    private Object customer;
    @SerializedName("CustomerId")
    @Expose
    private String customerId;
    @SerializedName("ItemId")
    @Expose
    private Integer itemId;
    @SerializedName("ItemType")
    @Expose
    private String itemType;
    @SerializedName("ItemName")
    @Expose
    private String itemName;
    @SerializedName("Details")
    @Expose
    private Object details;
    @SerializedName("Price")
    @Expose
    private String price;
    @SerializedName("StatusDate")
    @Expose
    private Object statusDate;
    @SerializedName("StatusTime")
    @Expose
    private Object statusTime;
    @SerializedName("UserName")
    @Expose
    private Object userName;
    @SerializedName("AppointmentDate")
    @Expose
    private String appointmentDate;
    @SerializedName("CustomerAddress")
    @Expose
    private Object customerAddress;
    @SerializedName("CustomerMobile")
    @Expose
    private Object customerMobile;
    @SerializedName("VendorName")
    @Expose
    private String vendorName;
    @SerializedName("CostPrice")
    @Expose
    private Object costPrice;
    @SerializedName("SalePrice")
    @Expose
    private Object salePrice;
    @SerializedName("Date")
    @Expose
    private Object date;
    @SerializedName("Category")
    @Expose
    private String category;
    @SerializedName("Shifa4UCode")
    @Expose
    private Object shifa4UCode;
    @SerializedName("ProviderItemName")
    @Expose
    private Object providerItemName;
    @SerializedName("ProviderItemCode")
    @Expose
    private Object providerItemCode;
    @SerializedName("VendorLocationId")
    @Expose
    private Integer vendorLocationId;
    @SerializedName("OrderTime")
    @Expose
    private Object orderTime;
    @SerializedName("AppointmentTime")
    @Expose
    private String appointmentTime;
    @SerializedName("Collection")
    @Expose
    private String collection;

    public Integer getSr() {
        return sr;
    }

    public void setSr(Integer sr) {
        this.sr = sr;
    }

    public Integer getOrderDetailId() {
        return orderDetailId;
    }

    public void setOrderDetailId(Integer orderDetailId) {
        this.orderDetailId = orderDetailId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public Object getLab() {
        return lab;
    }

    public void setLab(Object lab) {
        this.lab = lab;
    }

    public Object getCustomerContact() {
        return customerContact;
    }

    public void setCustomerContact(Object customerContact) {
        this.customerContact = customerContact;
    }

    public Object getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(Object paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Object getPaymentMethodPrefix() {
        return paymentMethodPrefix;
    }

    public void setPaymentMethodPrefix(Object paymentMethodPrefix) {
        this.paymentMethodPrefix = paymentMethodPrefix;
    }

    public Boolean getIsClosed() {
        return isClosed;
    }

    public void setIsClosed(Boolean isClosed) {
        this.isClosed = isClosed;
    }

    public Boolean getIsPayed() {
        return isPayed;
    }

    public void setIsPayed(Boolean isPayed) {
        this.isPayed = isPayed;
    }

    public Object getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Object totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Boolean getIsHomeSampleCollection() {
        return isHomeSampleCollection;
    }

    public void setIsHomeSampleCollection(Boolean isHomeSampleCollection) {
        this.isHomeSampleCollection = isHomeSampleCollection;
    }

    public Object getZone() {
        return zone;
    }

    public void setZone(Object zone) {
        this.zone = zone;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public Object getArea() {
        return area;
    }

    public void setArea(Object area) {
        this.area = area;
    }

    public Integer getOrderStatusId() {
        return orderStatusId;
    }

    public void setOrderStatusId(Integer orderStatusId) {
        this.orderStatusId = orderStatusId;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Object getOrderStatusPrefix() {
        return orderStatusPrefix;
    }

    public void setOrderStatusPrefix(Object orderStatusPrefix) {
        this.orderStatusPrefix = orderStatusPrefix;
    }

    public Integer getPaymentStatusId() {
        return paymentStatusId;
    }

    public void setPaymentStatusId(Integer paymentStatusId) {
        this.paymentStatusId = paymentStatusId;
    }

    public Object getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(Object paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public Object getPaymentStatusPrefix() {
        return paymentStatusPrefix;
    }

    public void setPaymentStatusPrefix(Object paymentStatusPrefix) {
        this.paymentStatusPrefix = paymentStatusPrefix;
    }

    public Integer getTimeSlotId() {
        return timeSlotId;
    }

    public void setTimeSlotId(Integer timeSlotId) {
        this.timeSlotId = timeSlotId;
    }

    public Object getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(Object timeSlot) {
        this.timeSlot = timeSlot;
    }

    public Object getCustomer() {
        return customer;
    }

    public void setCustomer(Object customer) {
        this.customer = customer;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Object getDetails() {
        return details;
    }

    public void setDetails(Object details) {
        this.details = details;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Object getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(Object statusDate) {
        this.statusDate = statusDate;
    }

    public Object getStatusTime() {
        return statusTime;
    }

    public void setStatusTime(Object statusTime) {
        this.statusTime = statusTime;
    }

    public Object getUserName() {
        return userName;
    }

    public void setUserName(Object userName) {
        this.userName = userName;
    }

    public String getAppointmentDate() {
        return appointmentDate;
    }

    public void setAppointmentDate(String appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    public Object getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(Object customerAddress) {
        this.customerAddress = customerAddress;
    }

    public Object getCustomerMobile() {
        return customerMobile;
    }

    public void setCustomerMobile(Object customerMobile) {
        this.customerMobile = customerMobile;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public Object getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(Object costPrice) {
        this.costPrice = costPrice;
    }

    public Object getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(Object salePrice) {
        this.salePrice = salePrice;
    }

    public Object getDate() {
        return date;
    }

    public void setDate(Object date) {
        this.date = date;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Object getShifa4UCode() {
        return shifa4UCode;
    }

    public void setShifa4UCode(Object shifa4UCode) {
        this.shifa4UCode = shifa4UCode;
    }

    public Object getProviderItemName() {
        return providerItemName;
    }

    public void setProviderItemName(Object providerItemName) {
        this.providerItemName = providerItemName;
    }

    public Object getProviderItemCode() {
        return providerItemCode;
    }

    public void setProviderItemCode(Object providerItemCode) {
        this.providerItemCode = providerItemCode;
    }

    public Integer getVendorLocationId() {
        return vendorLocationId;
    }

    public void setVendorLocationId(Integer vendorLocationId) {
        this.vendorLocationId = vendorLocationId;
    }

    public Object getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Object orderTime) {
        this.orderTime = orderTime;
    }

    public String getAppointmentTime() {
        return appointmentTime;
    }

    public void setAppointmentTime(String appointmentTime) {
        this.appointmentTime = appointmentTime;
    }

    public String getCollection() {
        return collection;
    }

    public void setCollection(String collection) {
        this.collection = collection;
    }

}
