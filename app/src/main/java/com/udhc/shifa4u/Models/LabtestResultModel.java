package com.udhc.shifa4u.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LabtestResultModel {

@SerializedName("Sr")
@Expose
private Integer sr;
@SerializedName("OrderId")
@Expose
private String orderId;
@SerializedName("OrderDate")
@Expose
private String orderDate;
@SerializedName("ReportDetail")
@Expose
private String reportDetail;
@SerializedName("ReportName")
@Expose
private String reportName;
@SerializedName("ReportHeading")
@Expose
private String reportHeading;
@SerializedName("Paths")
@Expose
private String paths;
@SerializedName("VendorName")
@Expose
private String vendorName;
@SerializedName("PatientResultId")
@Expose
private Integer patientResultId;
@SerializedName("IsRecommended")
@Expose
private Boolean isRecommended;
@SerializedName("AssistiveRecommendationsStatusId")
@Expose
private Object assistiveRecommendationsStatusId;

public Integer getSr() {
return sr;
}

public void setSr(Integer sr) {
this.sr = sr;
}

public String getOrderId() {
return orderId;
}

public void setOrderId(String orderId) {
this.orderId = orderId;
}

public String getOrderDate() {
return orderDate;
}

public void setOrderDate(String orderDate) {
this.orderDate = orderDate;
}

public String getReportDetail() {
return reportDetail;
}

public void setReportDetail(String reportDetail) {
this.reportDetail = reportDetail;
}

public String getReportName() {
return reportName;
}

public void setReportName(String reportName) {
this.reportName = reportName;
}

public String getReportHeading() {
return reportHeading;
}

public void setReportHeading(String reportHeading) {
this.reportHeading = reportHeading;
}

public String getPaths() {
return paths;
}

public void setPaths(String paths) {
this.paths = paths;
}

public String getVendorName() {
return vendorName;
}

public void setVendorName(String vendorName) {
this.vendorName = vendorName;
}

public Integer getPatientResultId() {
return patientResultId;
}

public void setPatientResultId(Integer patientResultId) {
this.patientResultId = patientResultId;
}

public Boolean getIsRecommended() {
return isRecommended;
}

public void setIsRecommended(Boolean isRecommended) {
this.isRecommended = isRecommended;
}

public Object getAssistiveRecommendationsStatusId() {
return assistiveRecommendationsStatusId;
}

public void setAssistiveRecommendationsStatusId(Object assistiveRecommendationsStatusId) {
this.assistiveRecommendationsStatusId = assistiveRecommendationsStatusId;
}

}