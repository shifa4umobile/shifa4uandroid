package com.udhc.shifa4u.Models;

public class AreasModel
{
    private String AreaId;

    private String MedicalProductCategoryPrefix;

    private String AreaName;

    private String SearchWord;

    public String getAreaId ()
    {
        return AreaId;
    }

    public void setAreaId (String AreaId)
    {
        this.AreaId = AreaId;
    }

    public String getMedicalProductCategoryPrefix ()
{
    return MedicalProductCategoryPrefix;
}

    public void setMedicalProductCategoryPrefix (String MedicalProductCategoryPrefix)
    {
        this.MedicalProductCategoryPrefix = MedicalProductCategoryPrefix;
    }

    public String getAreaName ()
    {
        return AreaName;
    }

    public void setAreaName (String AreaName)
    {
        this.AreaName = AreaName;
    }

    public String getSearchWord ()
{
    return SearchWord;
}

    public void setSearchWord (String SearchWord)
    {
        this.SearchWord = SearchWord;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [AreaId = "+AreaId+", MedicalProductCategoryPrefix = "+MedicalProductCategoryPrefix+", AreaName = "+AreaName+", SearchWord = "+SearchWord+"]";
    }
}
