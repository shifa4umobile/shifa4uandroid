
package com.udhc.shifa4u.Models.RadioLogyDetail;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Radiology {

    @SerializedName("ImagingId")
    @Expose
    private Integer imagingId;
    @SerializedName("ImagingName")
    @Expose
    private String imagingName;
    @SerializedName("ImagingCategoryId")
    @Expose
    private Integer imagingCategoryId;
    @SerializedName("ImagingCategoryName")
    @Expose
    private String imagingCategoryName;
    @SerializedName("BodyLocationId")
    @Expose
    private Integer bodyLocationId;
    @SerializedName("BodyLocationName")
    @Expose
    private String bodyLocationName;
    @SerializedName("Ranking")
    @Expose
    private Integer ranking;
    @SerializedName("IsActive")
    @Expose
    private Boolean isActive;
    @SerializedName("BodyLocationPartId")
    @Expose
    private Integer bodyLocationPartId;
    @SerializedName("BodyLocationPartTypeId")
    @Expose
    private Integer bodyLocationPartTypeId;
    @SerializedName("BodyLocationPartTypeName")
    @Expose
    private String bodyLocationPartTypeName;
    @SerializedName("BodyLocationPartName")
    @Expose
    private String bodyLocationPartName;
    @SerializedName("ShortDescription")
    @Expose
    private String shortDescription;
    @SerializedName("ImagUrl")
    @Expose
    private String imagUrl;
    @SerializedName("RadiologyLabsComparison")
    @Expose
    private List<RadiologyLabsComparison> radiologyLabsComparison = null;
    @SerializedName("Vendors")
    @Expose
    private List<Vendor> vendors = null;
    @SerializedName("Name")
    @Expose
    private Object name;

    public Integer getImagingId() {
        return imagingId;
    }

    public void setImagingId(Integer imagingId) {
        this.imagingId = imagingId;
    }

    public String getImagingName() {
        return imagingName;
    }

    public void setImagingName(String imagingName) {
        this.imagingName = imagingName;
    }

    public Integer getImagingCategoryId() {
        return imagingCategoryId;
    }

    public void setImagingCategoryId(Integer imagingCategoryId) {
        this.imagingCategoryId = imagingCategoryId;
    }

    public String getImagingCategoryName() {
        return imagingCategoryName;
    }

    public void setImagingCategoryName(String imagingCategoryName) {
        this.imagingCategoryName = imagingCategoryName;
    }

    public Integer getBodyLocationId() {
        return bodyLocationId;
    }

    public void setBodyLocationId(Integer bodyLocationId) {
        this.bodyLocationId = bodyLocationId;
    }

    public String getBodyLocationName() {
        return bodyLocationName;
    }

    public void setBodyLocationName(String bodyLocationName) {
        this.bodyLocationName = bodyLocationName;
    }

    public Integer getRanking() {
        return ranking;
    }

    public void setRanking(Integer ranking) {
        this.ranking = ranking;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Integer getBodyLocationPartId() {
        return bodyLocationPartId;
    }

    public void setBodyLocationPartId(Integer bodyLocationPartId) {
        this.bodyLocationPartId = bodyLocationPartId;
    }

    public Integer getBodyLocationPartTypeId() {
        return bodyLocationPartTypeId;
    }

    public void setBodyLocationPartTypeId(Integer bodyLocationPartTypeId) {
        this.bodyLocationPartTypeId = bodyLocationPartTypeId;
    }

    public String getBodyLocationPartTypeName() {
        return bodyLocationPartTypeName;
    }

    public void setBodyLocationPartTypeName(String bodyLocationPartTypeName) {
        this.bodyLocationPartTypeName = bodyLocationPartTypeName;
    }

    public String getBodyLocationPartName() {
        return bodyLocationPartName;
    }

    public void setBodyLocationPartName(String bodyLocationPartName) {
        this.bodyLocationPartName = bodyLocationPartName;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getImagUrl() {
        return imagUrl;
    }

    public void setImagUrl(String imagUrl) {
        this.imagUrl = imagUrl;
    }

    public List<RadiologyLabsComparison> getRadiologyLabsComparison() {
        return radiologyLabsComparison;
    }

    public void setRadiologyLabsComparison(List<RadiologyLabsComparison> radiologyLabsComparison) {
        this.radiologyLabsComparison = radiologyLabsComparison;
    }

    public List<Vendor> getVendors() {
        return vendors;
    }

    public void setVendors(List<Vendor> vendors) {
        this.vendors = vendors;
    }

    public Object getName() {
        return name;
    }

    public void setName(Object name) {
        this.name = name;
    }

}
