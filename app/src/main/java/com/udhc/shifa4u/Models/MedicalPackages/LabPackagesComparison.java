
package com.udhc.shifa4u.Models.MedicalPackages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LabPackagesComparison implements Serializable {

    @SerializedName("LabPackageVendorLocationId")
    @Expose
    private Integer labPackageVendorLocationId;
    @SerializedName("LabName")
    @Expose
    private String labName;
    @SerializedName("labId")
    @Expose
    private Integer labId;
    @SerializedName("PackageName")
    @Expose
    private String packageName;
    @SerializedName("PackageId")
    @Expose
    private Integer packageId;
    @SerializedName("VendorPrice")
    @Expose
    private Integer vendorPrice;
    @SerializedName("YourPrice")
    @Expose
    private Integer yourPrice;
    @SerializedName("IsHeadOffice")
    @Expose
    private Boolean isHeadOffice;
    @SerializedName("Discount")
    @Expose
    private Integer discount;

    public Integer getLabPackageVendorLocationId() {
        return labPackageVendorLocationId;
    }

    public void setLabPackageVendorLocationId(Integer labPackageVendorLocationId) {
        this.labPackageVendorLocationId = labPackageVendorLocationId;
    }

    public String getLabName() {
        return labName;
    }

    public void setLabName(String labName) {
        this.labName = labName;
    }

    public Integer getLabId() {
        return labId;
    }

    public void setLabId(Integer labId) {
        this.labId = labId;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public Integer getPackageId() {
        return packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    public Integer getVendorPrice() {
        return vendorPrice;
    }

    public void setVendorPrice(Integer vendorPrice) {
        this.vendorPrice = vendorPrice;
    }

    public Integer getYourPrice() {
        return yourPrice;
    }

    public void setYourPrice(Integer yourPrice) {
        this.yourPrice = yourPrice;
    }

    public Boolean getIsHeadOffice() {
        return isHeadOffice;
    }

    public void setIsHeadOffice(Boolean isHeadOffice) {
        this.isHeadOffice = isHeadOffice;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

}
