
package com.udhc.shifa4u.Models.PaymentMode;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderDetail {

    @SerializedName("ProductLineId")
    @Expose
    private String productLineId;
    @SerializedName("LabId")
    @Expose
    private String labId;
    @SerializedName("VendorLocationId")
    @Expose
    private String vendorLocationId;
    @SerializedName("IsHomeSampleCollection")
    @Expose
    private String isHomeSampleCollection;

    public String getProductLineId() {
        return productLineId;
    }

    public void setProductLineId(String productLineId) {
        this.productLineId = productLineId;
    }

    public String getLabId() {
        return labId;
    }

    public void setLabId(String labId) {
        this.labId = labId;
    }

    public String getVendorLocationId() {
        return vendorLocationId;
    }

    public void setVendorLocationId(String vendorLocationId) {
        this.vendorLocationId = vendorLocationId;
    }

    public String getIsHomeSampleCollection() {
        return isHomeSampleCollection;
    }

    public void setIsHomeSampleCollection(String isHomeSampleCollection) {
        this.isHomeSampleCollection = isHomeSampleCollection;
    }

}
