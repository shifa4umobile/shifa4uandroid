package com.udhc.shifa4u.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfileModel {

@SerializedName("FirstName")
@Expose
private String firstName;
@SerializedName("LastName")
@Expose
private String lastName;
@SerializedName("FullName")
@Expose
private String fullName;
@SerializedName("DateofBirth")
@Expose
private String dateofBirth;
@SerializedName("CountryId")
@Expose
private String countryId;
@SerializedName("CityId")
@Expose
private String cityId;

@SerializedName("AreaId")
@Expose
private String areaId;
@SerializedName("Address")
@Expose
private String address;
@SerializedName("MobileNo")
@Expose
private String mobileNo;
@SerializedName("Email")
@Expose
private String email;
@SerializedName("Gender")
@Expose
private String gender;

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getFirstName() {
return firstName;
}

public void setFirstName(String firstName) {
this.firstName = firstName;
}

public String getLastName() {
return lastName;
}

public void setLastName(String lastName) {
this.lastName = lastName;
}

public String getFullName() {
return fullName;
}

public void setFullName(String fullName) {
this.fullName = fullName;
}

public String getDateofBirth() {
return dateofBirth;
}

public void setDateofBirth(String dateofBirth) {
this.dateofBirth = dateofBirth;
}

public String getCountryId() {
return countryId;
}

public void setCountryId(String countryId) {
this.countryId = countryId;
}

public String getCityId() {
return cityId;
}

public void setCityId(String cityId) {
this.cityId = cityId;
}

public String getAddress() {
return address;
}

public void setAddress(String address) {
this.address = address;
}

public String getMobileNo() {
return mobileNo;
}

public void setMobileNo(String mobileNo) {
this.mobileNo = mobileNo;
}

public String getEmail() {
return email;
}

public void setEmail(String email) {
this.email = email;
}

public String getGender() {
return gender;
}

public void setGender(String gender) {
this.gender = gender;
}

}