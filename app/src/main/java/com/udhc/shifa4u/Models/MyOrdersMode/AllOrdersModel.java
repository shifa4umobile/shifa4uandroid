
package com.udhc.shifa4u.Models.MyOrdersMode;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AllOrdersModel {

    @SerializedName("OpenOrders")
    @Expose
    private List<OpenOrder> openOrders = null;
    @SerializedName("CanceledOrders")
    @Expose
    private List<CanceledOrder> canceledOrders = null;
    @SerializedName("ClosedOrders")
    @Expose
    private List<ClosedOrders> closedOrders = null;

    public List<OpenOrder> getOpenOrders() {
        return openOrders;
    }

    public void setOpenOrders(List<OpenOrder> openOrders) {
        this.openOrders = openOrders;
    }

    public List<CanceledOrder> getCanceledOrders() {
        return canceledOrders;
    }

    public void setCanceledOrders(List<CanceledOrder> canceledOrders) {
        this.canceledOrders = canceledOrders;
    }

    public List<ClosedOrders> getClosedOrders() {
        return closedOrders;
    }

    public void setClosedOrders(List<ClosedOrders> closedOrders) {
        this.closedOrders = closedOrders;
    }

}
