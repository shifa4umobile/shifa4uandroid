package com.udhc.shifa4u.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PhysiotherapyServiceTypesModel {

@SerializedName("MedicalSpecialityName")
@Expose
private String medicalSpecialityName;
@SerializedName("MedicalSpecialityId")
@Expose
private Integer medicalSpecialityId;
@SerializedName("ImageUrlWeb")
@Expose
private String imageUrlWeb;
@SerializedName("Description")
@Expose
private String description;

public String getMedicalSpecialityName() {
return medicalSpecialityName;
}

public void setMedicalSpecialityName(String medicalSpecialityName) {
this.medicalSpecialityName = medicalSpecialityName;
}

public Integer getMedicalSpecialityId() {
return medicalSpecialityId;
}

public void setMedicalSpecialityId(Integer medicalSpecialityId) {
this.medicalSpecialityId = medicalSpecialityId;
}

public String getImageUrlWeb() {
return imageUrlWeb;
}

public void setImageUrlWeb(String imageUrlWeb) {
this.imageUrlWeb = imageUrlWeb;
}

public String getDescription() {
return description;
}

public void setDescription(String description) {
this.description = description;
}

}