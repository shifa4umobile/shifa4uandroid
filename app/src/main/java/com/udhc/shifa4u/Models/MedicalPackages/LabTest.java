
package com.udhc.shifa4u.Models.MedicalPackages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LabTest implements Serializable{

    @SerializedName("LabTestId")
    @Expose
    private Integer labTestId;
    @SerializedName("LabTestName")
    @Expose
    private String labTestName;

    public Integer getLabTestId() {
        return labTestId;
    }

    public void setLabTestId(Integer labTestId) {
        this.labTestId = labTestId;
    }

    public String getLabTestName() {
        return labTestName;
    }

    public void setLabTestName(String labTestName) {
        this.labTestName = labTestName;
    }

}
