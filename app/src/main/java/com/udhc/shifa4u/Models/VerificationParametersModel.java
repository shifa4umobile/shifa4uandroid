package com.udhc.shifa4u.Models;

public class VerificationParametersModel {

    private String VerificationCode;
    private String LoginName;

    public String getVerificationCode() {
        return VerificationCode;
    }


    public void setVerificationCode(String verificationCode) {
        VerificationCode = verificationCode;
    }

    public String getLoginName() {
        return LoginName;
    }

    public void setLoginName(String loginName) {
        LoginName = loginName;
    }
}
