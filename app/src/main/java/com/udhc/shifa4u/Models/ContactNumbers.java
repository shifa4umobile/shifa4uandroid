package com.udhc.shifa4u.Models;

public class ContactNumbers
{
    private String Description;

    private String ContactId;

    public String getDescription ()
    {
        return Description;
    }

    public void setDescription (String Description)
    {
        this.Description = Description;
    }

    public String getContactId ()
    {
        return ContactId;
    }

    public void setContactId (String ContactId)
    {
        this.ContactId = ContactId;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Description = "+Description+", ContactId = "+ContactId+"]";
    }
}