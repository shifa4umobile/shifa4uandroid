package com.udhc.shifa4u.Models;

import com.udhc.shifa4u.Models.RadioLogyDetail.Contact;
import com.udhc.shifa4u.Models.RadioLogyDetail.Location;

import java.io.Serializable;

/**
 * Created by Ali Imran Bangash on 7/31/2018.
 */

public class CartModel implements Serializable {

    public static enum product {
        LAB_TEST, RADIOLOGY, MEDICAL_PACKAGE, HOME_CARE;
    }


    int productId;
    String productName;
    String productDiscription;
    product productTypoe;
    String productShortDescription;
    int selectedLabId = -1;
    String selectedLabName;
    String Location;
    String collectionType;

    Object originalObject;

    String ProductLineId = "";


    Double priice;
    Double originalPriice;

    Contact selectedContact=null;

    public Contact getSelectedContact() {
        return selectedContact;
    }

    public void setSelectedContact(Contact selectedContact) {
        this.selectedContact = selectedContact;
    }

    public String getProductLineId() {
        return ProductLineId;
    }

    public void setProductLineId(String productLineId) {
        ProductLineId = productLineId;
    }

    public Double getOriginalPriice() {
        return originalPriice;
    }

    public void setOriginalPriice(Double originalPriice) {
        this.originalPriice = originalPriice;
    }

    public Double getPriice() {
        return priice;
    }

    public void setPriice(Double priice) {
        this.priice = priice;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDiscription() {
        return productDiscription;
    }

    public void setProductDiscription(String productDiscription) {
        this.productDiscription = productDiscription;
    }

    public product getProductType() {
        return productTypoe;
    }

    public void setProductType(product productTypoe) {
        this.productTypoe = productTypoe;
    }

    public String getProductShortDescription() {
        return productShortDescription;
    }

    public void setProductShortDescription(String productShortDescription) {
        this.productShortDescription = productShortDescription;
    }

    public int getSelectedLabId() {
        return selectedLabId;
    }

    public void setSelectedLabId(int selectedLabId) {
        this.selectedLabId = selectedLabId;
    }

    public String getSelectedLabName() {
        return selectedLabName;
    }

    public void setSelectedLabName(String selectedLabName) {
        this.selectedLabName = selectedLabName;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public String getCollectionType() {
        return collectionType;
    }

    public void setCollectionType(String collectionType) {
        this.collectionType = collectionType;
    }

    public Object getOriginalObject() {
        return originalObject;
    }

    public void setOriginalObject(Object originalObject) {
        this.originalObject = originalObject;
    }
}
