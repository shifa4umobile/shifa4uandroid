package com.udhc.shifa4u.Models;

/**
 * Created by Ali Imran Bangash on 7/2/2018.
 */

public class MenuModel {

    String name;
    int Drawable;

    public MenuModel(String name, int drawable)
    {
        this.name=name;
        this.Drawable=drawable;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDrawable() {
        return Drawable;
    }

    public void setDrawable(int drawable) {
        Drawable = drawable;
    }
}
