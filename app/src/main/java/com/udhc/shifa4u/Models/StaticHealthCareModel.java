package com.udhc.shifa4u.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StaticHealthCareModel {

@SerializedName("title")
@Expose
private String title;
@SerializedName("sortDesc")
@Expose
private String sortDesc;
@SerializedName("image")
@Expose
private String image;

public String getTitle() {
return title;
}

public void setTitle(String title) {
this.title = title;
}

public String getSortDesc() {
return sortDesc;
}

public void setSortDesc(String sortDesc) {
this.sortDesc = sortDesc;
}

public String getImage() {
return image;
}

public void setImage(String image) {
this.image = image;
}

}