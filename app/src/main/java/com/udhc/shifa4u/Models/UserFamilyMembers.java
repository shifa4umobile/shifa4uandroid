package com.udhc.shifa4u.Models;

public class UserFamilyMembers
{
    private String Name;

    private String RelationName;
    private String RelationshipPrefix ;

    private int CustomerId;

    private String DateofBirth;

    private String Gender;

    private String FamilyMemberId;

    private int RelationshipProfileId;

    public String getName ()
    {
        return Name;
    }

    public void setName (String Name)
    {
        this.Name = Name;
    }

    public String getRelationshipPrefix() {
        return RelationshipPrefix;
    }

    public void setRelationshipPrefix(String relationshipPrefix) {
        RelationshipPrefix = relationshipPrefix;
    }

    public String getRelationName ()
    {
        return RelationName;
    }

    public void setRelationName (String RelationName)
    {
        this.RelationName = RelationName;
    }

    public int getCustomerId ()
{
    return CustomerId;
}

    public void setCustomerId (int CustomerId)
    {
        this.CustomerId = CustomerId;
    }

    public String getDateofBirth ()
    {
        return DateofBirth;
    }

    public void setDateofBirth (String DateofBirth)
    {
        this.DateofBirth = DateofBirth;
    }

    public String getGender ()
{
    return Gender;
}

    public void setGender (String Gender)
    {
        this.Gender = Gender;
    }

    public String getFamilyMemberId ()
    {
        return FamilyMemberId;
    }

    public void setFamilyMemberId (String FamilyMemberId)
    {
        this.FamilyMemberId = FamilyMemberId;
    }

    public int getRelationshipProfileId ()
{
    return RelationshipProfileId;
}

    public void setRelationshipProfileId (int RelationshipProfileId)
    {
        this.RelationshipProfileId = RelationshipProfileId;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Name = "+Name+", RelationName = "+RelationName+", CustomerId = "+CustomerId+", DateofBirth = "+DateofBirth+", Gender = "+Gender+", FamilyMemberId = "+FamilyMemberId+", RelationshipProfileId = "+RelationshipProfileId+"]";
    }
}