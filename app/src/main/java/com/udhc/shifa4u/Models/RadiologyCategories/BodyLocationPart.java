package com.udhc.shifa4u.Models.RadiologyCategories;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BodyLocationPart {

@SerializedName("Name")
@Expose
private String name;
@SerializedName("BodyLocationPartId")
@Expose
private Integer bodyLocationPartId;

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public Integer getBodyLocationPartId() {
return bodyLocationPartId;
}

public void setBodyLocationPartId(Integer bodyLocationPartId) {
this.bodyLocationPartId = bodyLocationPartId;
}

}