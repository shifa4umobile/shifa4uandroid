
package com.udhc.shifa4u.Models;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LabsModel implements Serializable {

    @SerializedName("TotalRecords")
    @Expose
    private Integer totalRecords;
    @SerializedName("LabTestList")
    @Expose
    private List<LabTestList> labTestList = null;

    public Integer getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(Integer totalRecords) {
        this.totalRecords = totalRecords;
    }

    public List<LabTestList> getLabTestList() {
        return labTestList;
    }

    public void setLabTestList(List<LabTestList> labTestList) {
        this.labTestList = labTestList;
    }

}
