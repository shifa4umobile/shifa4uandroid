
package com.udhc.shifa4u.Models.RadioLogyDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Contact {

    @SerializedName("VendorLocationId")
    @Expose
    private Integer vendorLocationId;
    @SerializedName("TypeId")
    @Expose
    private Integer typeId;
    @SerializedName("TypeName")
    @Expose
    private String typeName;
    @SerializedName("Description")
    @Expose
    private String description;

    public Integer getVendorLocationId() {
        return vendorLocationId;
    }

    public void setVendorLocationId(Integer vendorLocationId) {
        this.vendorLocationId = vendorLocationId;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
