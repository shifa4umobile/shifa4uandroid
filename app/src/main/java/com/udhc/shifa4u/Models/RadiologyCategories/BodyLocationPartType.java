package com.udhc.shifa4u.Models.RadiologyCategories;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BodyLocationPartType {

@SerializedName("Name")
@Expose
private String name;
@SerializedName("BodyLocationPartTypeId")
@Expose
private Integer bodyLocationPartTypeId;

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public Integer getBodyLocationPartTypeId() {
return bodyLocationPartTypeId;
}

public void setBodyLocationPartTypeId(Integer bodyLocationPartTypeId) {
this.bodyLocationPartTypeId = bodyLocationPartTypeId;
}

}