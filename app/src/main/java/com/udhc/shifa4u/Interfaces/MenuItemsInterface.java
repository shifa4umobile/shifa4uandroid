package com.udhc.shifa4u.Interfaces;

import android.view.View;

import com.udhc.shifa4u.Models.MenuModel;

/**
 * Created by Ali Imran Bangash on 7/3/2018.
 */

public interface MenuItemsInterface {

    public void onItemClick(Integer position, MenuModel menuModel);

    public void onButtonClick(Integer position, Object model,View button);

    public void onItemClick(Integer position, Object o);
}
