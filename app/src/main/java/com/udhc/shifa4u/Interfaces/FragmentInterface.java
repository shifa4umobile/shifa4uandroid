package com.udhc.shifa4u.Interfaces;

import android.app.Fragment;
import android.support.annotation.Nullable;

/**
 * Created by Ali Imran Bangash on 7/2/2018.
 */

public interface FragmentInterface {

public void onCloseFragent(@Nullable Fragment fragment);

}
