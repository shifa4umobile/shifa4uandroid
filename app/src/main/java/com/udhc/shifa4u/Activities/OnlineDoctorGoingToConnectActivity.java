package com.udhc.shifa4u.Activities;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.HeaderManager;

import us.zoom.sdk.JoinMeetingOptions;
import us.zoom.sdk.JoinMeetingParams;
import us.zoom.sdk.MeetingService;
import us.zoom.sdk.ZoomSDK;

public class OnlineDoctorGoingToConnectActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_doctor_going_to_connect);
        setHeader();


         Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                joinMeeting();
               // startActivity(new Intent(OnlineDoctorGoingToConnectActivity.this , TokBoxActivity.class));
            }
        }, 5000);




    }

    private void joinMeeting ()
    {
        String meetingIdFromIniatialResponse = Shifa4U.mySharePrefrence.getConnectionResponse().getMeetingId();
        String testMeetingId = Shifa4U.mySharePrefrence.getMeetingId();
        if (meetingIdFromIniatialResponse.equals(testMeetingId)) {
            String uniqueId = Shifa4U.mySharePrefrence.getUniqueId();
            String meetingPassword = "";

            String vanityId = "";

            if (uniqueId.length() == 0 && vanityId.length() == 0) {
                Toast.makeText(OnlineDoctorGoingToConnectActivity.this, "You need to enter a meeting number/ vanity id which you want to join.", Toast.LENGTH_LONG).show();
                return;
            }

            if (uniqueId.length() != 0 && vanityId.length() != 0) {
                Toast.makeText(OnlineDoctorGoingToConnectActivity.this, "Both meeting number and vanity id have value,  just set one of them", Toast.LENGTH_LONG).show();
                return;
            }

            ZoomSDK zoomSDK = ZoomSDK.getInstance();

            if (!zoomSDK.isInitialized()) {
                Toast.makeText(OnlineDoctorGoingToConnectActivity.this, "ZoomSDK has not been initialized successfully", Toast.LENGTH_LONG).show();
                return;
            }

            MeetingService meetingService = zoomSDK.getMeetingService();

            JoinMeetingOptions opts = new JoinMeetingOptions();
//		opts.no_driving_mode = true;
//		opts.no_invite = true;
//		opts.no_meeting_end_message = true;
//		opts.no_titlebar = true;
//		opts.no_bottom_toolbar = true;
//		opts.no_dial_in_via_phone = true;
//		opts.no_dial_out_to_phone = true;
//		opts.no_disconnect_audio = true;
//		opts.no_share = true;
//		opts.invite_options = InviteOptions.INVITE_VIA_EMAIL + InviteOptions.INVITE_VIA_SMS;
//		opts.no_audio = true;
//		opts.no_video = true;
//		opts.meeting_views_options = MeetingViewsOptions.NO_BUTTON_SHARE;
//		opts.no_meeting_error_message = true;
//		opts.participant_id = "participant id";
            JoinMeetingParams params = new JoinMeetingParams();

            params.displayName = Shifa4U.mySharePrefrence.getProfile().getFullName();
            params.password = meetingPassword;

            if (vanityId.length() != 0) {
                params.vanityID = vanityId;
            } else {
                params.meetingNo = uniqueId;
            }
            int ret = meetingService.joinMeetingWithParams(OnlineDoctorGoingToConnectActivity.this, params);
            finish();

            Log.i("", "onClickBtnJoinMeeting, ret=" + ret);
        }
    }








    void setHeader() {
        HeaderManager headerManager = new HeaderManager(OnlineDoctorGoingToConnectActivity.this, findViewById(R.id.header));
        headerManager.setMenutype(HeaderManager.BACK_MENU);
        headerManager.setShowFirstImageButton(false);
        headerManager.setShowSecoundImageButton(false);
        headerManager.setTitle("Online Doctor");
        headerManager.setShowBottomView(true);
        headerManager.setHeaderColor(getResources().getColor(R.color.header_background_color));
    }

}
