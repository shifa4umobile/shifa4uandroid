package com.udhc.shifa4u.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.udhc.shifa4u.Adapters.American_teleclinic_main_adaptor;
import com.udhc.shifa4u.Models.AmericanTeleclinic.MyData;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.HeaderManager;

import java.util.ArrayList;

public class AmericanTeleclinicActivity extends AppCompatActivity {

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.american_teleclinic);
        setHeader();
        RecyclerView recyclerView = findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this,
                DividerItemDecoration.VERTICAL));
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
               if (position ==0)
                {
                    Intent myIntent = new Intent(AmericanTeleclinicActivity.this, Main2Activity.class);
                   startActivity(myIntent);
                    overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );

                }
                else if (position ==1 )
                {
                    Intent myIntent = new Intent(AmericanTeleclinicActivity.this, HowItWorks.class);
                    startActivity(myIntent);
                    overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );

                }
                else if (position ==2 )
                {
                    Intent myIntent = new Intent(AmericanTeleclinicActivity.this, MeetOurProviders.class);

                    startActivity(myIntent);
                    overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );

                }

                else if (position ==3 )
                {
                    Intent myIntent = new Intent(AmericanTeleclinicActivity.this, PricingAcitivityFinal.class);
                    startActivity(myIntent);
                    overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );

                }

                else if (position ==4 )
                {
                    Intent myIntent = new Intent(AmericanTeleclinicActivity.this, FrequentlyAskedQuestions.class);
                    startActivity(myIntent);
                    overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );

                }

                else if (position == 5)
                {
                    Intent myIntent = new Intent(AmericanTeleclinicActivity.this, SampleRecommendation.class);
                    startActivity(myIntent);
                    overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );

                }
                else if (position == 6)
                {
                    Intent myIntent = new Intent(AmericanTeleclinicActivity.this, SubmitActivity.class);
                    startActivity(myIntent);
                    overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );

                }

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        ArrayList<String> items = new ArrayList<String>();
        for (int i = 0; i < MyData.nameArray.length; i++) {
            items.add(MyData.nameArray[i]);
        }

        RecyclerView.Adapter adapter = new American_teleclinic_main_adaptor(items , this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setHeader();
    }

    void setHeader()
    {
        HeaderManager headerManager = new HeaderManager(AmericanTeleclinicActivity.this, findViewById(R.id.header));
        headerManager.setMenutype(HeaderManager.BACK_MENU);
        headerManager.setShowFirstImageButton(false);
        headerManager.setShowSecoundImageButton(false);
        headerManager.setTitle("American Teleclinic");
        headerManager.setShowBottomView(true);
        headerManager.setHeaderColor(getResources().getColor(R.color.header_background_color));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition( R.anim.slide_in_right, R.anim.slide_out_right );
    }
}
