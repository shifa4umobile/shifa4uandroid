package com.udhc.shifa4u.Activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.udhc.shifa4u.Adapters.PhysiotherapyServiceTypesAdapter;
import com.udhc.shifa4u.Adapters.StaticHealthCareAdapter;
import com.udhc.shifa4u.Models.PhysiotherapyServiceTypesModel;
import com.udhc.shifa4u.Models.StaticHealthCareModel;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.GzipUtils;
import com.udhc.shifa4u.Utilities.HeaderManager;
import com.udhc.shifa4u.Utilities.MyLog;
import com.udhc.shifa4u.Utilities.MyUtils;
import com.udhc.shifa4u.api.RestAPIFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class phyotherapyActivity extends AppCompatActivity {

    private ListView lv_health_care_static;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phyotherapy);

       setHeader();
        lv_health_care_static = (ListView) findViewById(R.id.lv_health_care_static);


        getPhysiotherapyServiceTypes();

        Dexter.withActivity(this)
                .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {

                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {

                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {

                    }
                })
                .check();


        lv_health_care_static.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    Intent intent=new Intent(phyotherapyActivity.this,HomeCareSubTherapyActivity.class);
                    intent.putExtra("ProductTypePrefix","PHY");
                    intent.putExtra("Title","Physiotherapy");
                    intent.putExtra("spcialMedicalId",id);
                    startActivity(intent);


            }
        });

        lv_health_care_static.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        setHeader();
    }

    private void setHeader() {
        HeaderManager headerManager = new HeaderManager(phyotherapyActivity.this, findViewById(R.id.header));
        headerManager.setMenutype(HeaderManager.BACK_MENU);
        headerManager.setShowFirstImageButton(false);
        headerManager.setShowSecoundImageButton(false);
        headerManager.setTitle(getString(R.string.Physiotherapy_title));
//        headerManager.setShowBottomView(true);
        headerManager.setHeaderColor(getResources().getColor(R.color.header_background_color));


    }

    void getPhysiotherapyServiceTypes() {
        showProgressDialog("Please wait...");
        Call<ResponseBody> posts = RestAPIFactory.getApi().getPhysiotherapyServiceTypes();

        posts.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dismissProgressDislog();
                try {
                    String Json = GzipUtils.decompress(response.body().bytes());

                    Type listType = new TypeToken<List<PhysiotherapyServiceTypesModel>>() {
                    }.getType();
                    List<PhysiotherapyServiceTypesModel> physiotherapyServiceTypesModels = new Gson().fromJson(Json, listType);

//                    List<PhysiotherapyServiceTypesModel> listClass;
//                    listClass = new MyUtils<ArrayList<PhysiotherapyServiceTypesModel>>().JsonToModel(Json);


                    PhysiotherapyServiceTypesAdapter physiotherapyServiceTypesAdapter = new PhysiotherapyServiceTypesAdapter(phyotherapyActivity.this, (ArrayList<PhysiotherapyServiceTypesModel>) physiotherapyServiceTypesModels);
                    lv_health_care_static.setAdapter(physiotherapyServiceTypesAdapter);
                } catch (IOException e) {
                    e.printStackTrace();
                }


//
//                ItemModel itemModel = LoganSquare.parse(new GZIPInputStream(response.body().byteStream()), ItemModel.class);

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dismissProgressDislog();
                Log.e("", "");
            }
        });


    }

    public void showProgressDialog(String message) {
        if (progressDialog == null)
            progressDialog = new ProgressDialog(phyotherapyActivity.this);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    public void dismissProgressDislog() {
        if (progressDialog.isShowing() && progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }
}
