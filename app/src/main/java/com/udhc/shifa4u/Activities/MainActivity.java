package com.udhc.shifa4u.Activities;

import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.udhc.shifa4u.Adapters.MenuAdapter;
import com.udhc.shifa4u.Fragments.ChangeCityFragment;
import com.udhc.shifa4u.Fragments.ContactUsFragment;
import com.udhc.shifa4u.Fragments.FollowUsFragment;
import com.udhc.shifa4u.Fragments.HomeFragment;
import com.udhc.shifa4u.Fragments.LiveChatFragment;
import com.udhc.shifa4u.Fragments.MyAccountFragment;
import com.udhc.shifa4u.Fragments.StaticPagesFragment;
import com.udhc.shifa4u.Fragments.UploadPriscriptionFragment;
import com.udhc.shifa4u.Interfaces.FragmentInterface;
import com.udhc.shifa4u.Interfaces.MenuItemsInterface;
import com.udhc.shifa4u.Models.MenuModel;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.HeaderManager;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    public static DrawerLayout drawer;
    private TextView txt_name;
    private ListView menu_list;

    private long refid;
    public String fileUrl = "https://api.shifa4u.com/api/common/v1/displayfilebyfilename?filename=";

    ArrayList<Long> list = new ArrayList<>();

    private Uri Download_Uri;

    private DownloadManager downloadManager;

    List<String> myAccountActivityNames = new ArrayList<>();

    ArrayList<MenuModel> menuModels = new ArrayList<MenuModel>();
    private Fragment fragment_main;
    private HeaderManager headerManager;
    private AlertDialog alertDialog;

    MenuModel menuModelSelected = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setHeader();
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        menu_list = (ListView) findViewById(R.id.menu_list);
        txt_name = (TextView) findViewById(R.id.txt_name);



    }


    public void addmyAccountActivityName (String name)
    {
        if (myAccountActivityNames != null) {
            myAccountActivityNames.add(name);
        }
    }

    public void removemyAccountActivityName()
    {
        if (myAccountActivityNames != null)
        {
            if (myAccountActivityNames.size()>0)
            {
                myAccountActivityNames.remove(0);
            }
        }
    }
    private void setHeader() {
        headerManager = new HeaderManager(MainActivity.this, findViewById(R.id.header));
        headerManager.setMenutype(HeaderManager.MAIN_MENU);
        headerManager.setShowFirstImageButton(false);
        headerManager.setShowSecoundImageButton(false);
        headerManager.setTitle(getString(R.string.home_title));
        headerManager.setShowBottomView(true);
        headerManager.setHeaderColor(getResources().getColor(R.color.header_background_color));
    }

    @Override
    protected void onResume() {
        super.onResume();

        setHeader();

        if (Shifa4U.mySharePrefrence !=null) {
            if (Shifa4U.mySharePrefrence.getProfile() != null) {
                txt_name.setText(Shifa4U.mySharePrefrence.getProfile().getFirstName() + " " + Shifa4U.mySharePrefrence.getProfile().getLastName());
            } else {
                txt_name.setText(R.string.Guest_String);

            }
        }

            if (myAccountActivityNames.size()>0)
            {
                if (myAccountActivityNames.get(0).equals("social"))
                {
                    FollowUsFragment fragment = new FollowUsFragment();
                    loadFragment(fragment);
                    buildMenu();
                    if (menuModelSelected == null)
                        menuModelSelected = menuModels.get(1);

                    headerManager.setTitle(menuModelSelected.getName());
                    myAccountActivityNames.remove(0);
                }
                else if (myAccountActivityNames.get(0).equals("pharmacy"))
                {
                    PharmacyActivity fragment = new PharmacyActivity();
                    loadFragment(fragment);
                    buildMenu();
                    if (menuModelSelected == null)
                        menuModelSelected = menuModels.get(1);

                    headerManager.setTitle(menuModelSelected.getName());
                    myAccountActivityNames.remove(0);
                }

                else {
                    MyAccountFragment fragment = new MyAccountFragment();
                    loadFragment(fragment);
                    buildMenu();
                    if (menuModelSelected == null)
                        menuModelSelected = menuModels.get(1);

                    headerManager.setTitle(menuModelSelected.getName());
                    myAccountActivityNames.remove(0);
                }
            }


            else if (Shifa4U.mySharePrefrence.getAccountActivityNames() != null)
            {
                PharmacyActivity fragment = new PharmacyActivity();
                loadFragment(fragment);
                buildMenu();
                if (menuModelSelected == null)
                    menuModelSelected = menuModels.get(1);

                headerManager.setTitle(menuModelSelected.getName());
            }


        else {

            HomeFragment fragment = new HomeFragment();
            loadFragment(fragment);
            buildMenu();
            if (menuModelSelected == null)
                menuModelSelected = menuModels.get(0);
            headerManager.setTitle(menuModelSelected.getName());
        }
    }

    public void buildMenu() {
            if (Shifa4U.mySharePrefrence.getLogin() != null) {
                menuModels = new ArrayList<>();
                menuModels.add(new MenuModel("Home", R.drawable.menu_home));
                menuModels.add(new MenuModel("My Account", R.drawable.menu_user));
                menuModels.add(new MenuModel("Change City", R.drawable.menu_building));
                menuModels.add(new MenuModel("Latest Blog", R.drawable.menu_blog));
                menuModels.add(new MenuModel("Live Chat", R.drawable.menu_chat));
                menuModels.add(new MenuModel("Follow Us on Social Media", R.drawable.menu_users));
                menuModels.add(new MenuModel("Upload Prescription", R.drawable.upload_prescription));
                menuModels.add(new MenuModel("Pharmacy", R.drawable.online_pharmacy));
                menuModels.add(new MenuModel("Contact Us", R.drawable.menu_support));
                menuModels.add(new MenuModel("Privacy Policy", R.drawable.menu_policy));
                menuModels.add(new MenuModel("Terms and Conditions", R.drawable.menu_terms));
                menuModels.add(new MenuModel("Logout", R.drawable.menu_logout));
                menu_list.setAdapter(new MenuAdapter(MainActivity.this, menuModels, menuItemsInterface));
            } else {
                menuModels = new ArrayList<>();
                menuModels.add(new MenuModel("Home", R.drawable.menu_home));
                menuModels.add(new MenuModel("Change City", R.drawable.menu_building));
                menuModels.add(new MenuModel("Latest Blog", R.drawable.menu_blog));
                menuModels.add(new MenuModel("Live Chat", R.drawable.menu_chat));
                menuModels.add(new MenuModel("Follow Us on Social Media", R.drawable.menu_users));
                menuModels.add(new MenuModel("Upload Prescription", R.drawable.upload_prescription));
                menuModels.add(new MenuModel("Pharmacy", R.drawable.online_pharmacy));
                menuModels.add(new MenuModel("Contact Us", R.drawable.menu_support));
                menuModels.add(new MenuModel("Privacy Policy", R.drawable.menu_policy));
                menuModels.add(new MenuModel("Terms and Conditions", R.drawable.menu_terms));
                menuModels.add(new MenuModel("Login", R.drawable.menu_logout));
                menu_list.setAdapter(new MenuAdapter(MainActivity.this, menuModels, menuItemsInterface));
            }
        menu_list.setAdapter(new MenuAdapter(MainActivity.this, menuModels, menuItemsInterface));


    }


    public void loadFragment(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();

        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        fragmentTransaction.replace(R.id.fragment_main, fragment);

        fragmentTransaction.commit();

        if (menuModelSelected != null)
            headerManager.setTitle(menuModelSelected.getName());

    }


    @Override
    public void onBackPressed() {

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragment_main);

            if (f instanceof HomeFragment) {
                super.onBackPressed();
            } else {
                HomeFragment fragment = new HomeFragment();
                loadFragment(fragment);
                headerManager.setTitle(menuModels.get(0).getName());
                menuModelSelected = menuModels.get(0);
            }
        }
    }


    public MenuItemsInterface menuItemsInterface = new MenuItemsInterface() {
        @Override
        public void onItemClick(Integer position, MenuModel menuModel) {
            headerManager.setMenutype(HeaderManager.MAIN_MENU);
            drawer.closeDrawer(Gravity.START);
            Fragment fragment = null;


            if (Shifa4U.mySharePrefrence.getLogin() != null) {

                switch (position) {
                    case 0:
                        menuModelSelected = menuModel;
                        fragment = new HomeFragment();
                        loadFragment(fragment);
                        // headerManager.setTitle(menuModel.getName());
                        break;
                    case 1:
                        menuModelSelected = menuModel;
                        fragment = new MyAccountFragment();
                        loadFragment(fragment);
                        //headerManager.setTitle(menuModel.getName());
                        break;
                    case 2:
                        menuModelSelected = menuModel;
                        fragment = new ChangeCityFragment(new FragmentInterface() {
                            @Override
                            public void onCloseFragent(android.app.Fragment fragment) {

                                menuModelSelected = menuModels.get(0);
                                HomeFragment fragmentHome = new HomeFragment();
                                loadFragment(fragmentHome);
                                headerManager.setTitle(menuModels.get(0).getName());
                            }
                        });
                        loadFragment(fragment);
//                        headerManager.setTitle(menuModel.getName());
                        break;
                    case 3:
                        Intent myIntent = new Intent(MainActivity.this, BlogActivity.class);
                        startActivity(myIntent);
                        overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );
                        break;
                    case 4:
                        menuModelSelected = menuModel;
                        fragment = new LiveChatFragment();
                        loadFragment(fragment);
//                        headerManager.setTitle(menuModel.getName());
                        break;
                    case 5:
                        menuModelSelected = menuModel;
                        fragment = new FollowUsFragment();
                        loadFragment(fragment);
//                        headerManager.setTitle(menuModel.getName());
                        break;
                    case 6:
                        Intent intent = new Intent(MainActivity.this, UploadPriscriptionFragment.class);
                        startActivity(intent);
                        overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );

//                        headerManager.setTitle(menuModel.getName());
                        break;

                        case 7:
                            menuModelSelected = menuModel;
                            fragment = new PharmacyActivity();
                            loadFragment(fragment);

//                        headerManager.setTitle(menuModel.getName());
                        break;
                    case 8:
                        menuModelSelected = menuModel;
                        fragment = new ContactUsFragment();
                        loadFragment(fragment);
//                        headerManager.setTitle(menuModel.getName());
                        break;
                    case 9:
                        menuModelSelected = menuModel;
                        fragment = StaticPagesFragment.newInstance("Privacy policy", "");
                        loadFragment(fragment);
//                        headerManager.setTitle(menuModel.getName());
                        break;
                    case 10:
                        menuModelSelected = menuModel;
                        fragment = StaticPagesFragment.newInstance("Terms And Condition", "");
                        loadFragment(fragment);
//                        headerManager.setTitle(menuModel.getName());
                        break;
                    case 11:
                        logoutAllery();
                        break;

                }
            } else {
                switch (position) {
                    case 0:
                        menuModelSelected = menuModel;
                        fragment = new HomeFragment();
                        loadFragment(fragment);
//                        headerManager.setTitle(menuModel.getName());
                        break;
                    case 1:
                        menuModelSelected = menuModel;
                        fragment = new ChangeCityFragment(new FragmentInterface() {
                            @Override
                            public void onCloseFragent(android.app.Fragment fragment) {

                                menuModelSelected = menuModels.get(0);
                                HomeFragment fragmentHome = new HomeFragment();
                                loadFragment(fragmentHome);
                                headerManager.setTitle(menuModels.get(0).getName());
                            }
                        });
                        loadFragment(fragment);
//                        headerManager.setTitle(menuModel.getName());
                        break;
                    case 2:
                        Intent myIntent = new Intent(MainActivity.this, BlogActivity.class);
                        startActivity(myIntent);
                        overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );

                        break;
                    case 3:
                        menuModelSelected = menuModel;
                        fragment = new LiveChatFragment();
                        loadFragment(fragment);
//                        headerManager.setTitle(menuModel.getName());
                        break;
                    case 4:
                        menuModelSelected = menuModel;
                        fragment = new FollowUsFragment();
                        loadFragment(fragment);
//                        headerManager.setTitle(menuModel.getName());

                        break;
                    case 5:
                        Intent intent = new Intent(MainActivity.this, UploadPriscriptionFragment.class);
                        startActivity(intent);
                        overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );
//                        headerManager.setTitle(menuModel.getName());
                        break;

                    case 6:
                        menuModelSelected = menuModel;
                        fragment = new PharmacyActivity();
                        loadFragment(fragment);

//                        headerManager.setTitle(menuModel.getName());
                        break;
                    case 7:
                        menuModelSelected = menuModel;
                        fragment = new ContactUsFragment();
                        loadFragment(fragment);
//                        headerManager.setTitle(menuModel.getName());
                        break;
                    case 8:
                        menuModelSelected = menuModel;
                        fragment = StaticPagesFragment.newInstance("Privacy policy", "");
                        loadFragment(fragment);
//                        headerManager.setTitle(menuModel.getName());
                        break;
                    case 9:
                        menuModelSelected = menuModel;
                        fragment = StaticPagesFragment.newInstance("Terms And Condition", "");
                        loadFragment(fragment);
//                        headerManager.setTitle(menuModel.getName());
                        break;
                    case 10:
                        startActivity(new Intent(MainActivity.this, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
                        finish();

                        break;
                }

            }
        }

        @Override
        public void onButtonClick(Integer position, Object model, View button) {

        }

        @Override
        public void onItemClick(Integer position, Object o) {

        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment f : fragments) {
               /* if (f instanceof UploadPriscriptionFragment) {
                    f.onActivityResult(requestCode, resultCode, data);
                }*/
            }
        }
    }


    public void logoutAllery() {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
        alertDialogBuilder.setTitle("Logout");
        alertDialogBuilder.setMessage("Are you sure to logout?");
        alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Shifa4U.mySharePrefrence.clearLogin();
                Shifa4U.mySharePrefrence.clearProfile();
                Shifa4U.mySharePrefrence.clearCart();
                startActivity(new Intent(MainActivity.this, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
                finish();
            }
        });
        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });

        if (alertDialog == null) {
            alertDialog = alertDialogBuilder.create();
        }
        alertDialog.show();
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
