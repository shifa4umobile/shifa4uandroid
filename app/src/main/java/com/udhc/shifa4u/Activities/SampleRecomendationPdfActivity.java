package com.udhc.shifa4u.Activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.http.SslError;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.udhc.shifa4u.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SampleRecomendationPdfActivity extends AppCompatActivity {

    @BindView(R.id.webview)
    WebView webview;


    String pdf= "https://www.shifa4u.com/Content/images/doctors/sample-rec.pdf";
    String url = "http://drive.google.com/viewerng/viewer?embedded=true&url=" + pdf;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample_recomendation_pdf);
        ButterKnife.bind(this);

        webview.setWebViewClient(new MyBrowser(this.getBaseContext()));

        webview.getSettings().setJavaScriptEnabled(true);
        webview.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webview.loadUrl(url);
    }

    private class MyBrowser extends WebViewClient {
        Context c;
        MyBrowser(Context context)
        {
            c = context;
        }
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(c);
            builder.setMessage("There Seems to be an SSL error. Do you want to proceed?");
            builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.proceed();
                }
            });
            builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.cancel();
                }
            });
            final AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

}
