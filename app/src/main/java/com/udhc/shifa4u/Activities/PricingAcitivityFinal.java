package com.udhc.shifa4u.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.aakira.expandablelayout.ExpandableLinearLayout;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.HeaderManager;
import com.udhc.shifa4u.Utilities.Utility;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PricingAcitivityFinal extends AppCompatActivity {



    @BindView(R.id.buttonPaymentModes)
    RelativeLayout buttonPaymentModes;
    @BindView(R.id.textViewExchangeRates)
    TextView textViewExchangeRates;
    @BindView(R.id.btnPaymentModes)
    RelativeLayout btnPaymentModes;
    @BindView(R.id.trPaymentModes)
    ExpandableLinearLayout trPaymentModes;
    @BindView(R.id.buttonRefunds)
    RelativeLayout buttonRefunds;
    @BindView(R.id.textViewRefunds)
    TextView textViewRefunds;
    @BindView(R.id.btnRefunds)
    RelativeLayout btnRefunds;
    @BindView(R.id.trRefunds)
    ExpandableLinearLayout trRefunds;
    @BindView(R.id.buttonExchangeRates)
    RelativeLayout buttonExchangeRates;
    @BindView(R.id.btnExchangeRates)
    RelativeLayout btnExchangeRates;
    @BindView(R.id.trExchangeRates)
    ExpandableLinearLayout trExchangeRates;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pricing_final);
        ButterKnife.bind(this);
        setHeader();
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setHeader();
    }

    private void init() {
        Utility.setClick(btnPaymentModes , trPaymentModes);
        Utility.setRotateAnimator(trPaymentModes,buttonPaymentModes);
        Utility.setClick(btnRefunds , trRefunds);
        Utility.setRotateAnimator(trRefunds,buttonRefunds);
        Utility.setClick(btnExchangeRates , trExchangeRates);
        Utility.setRotateAnimator(trExchangeRates,buttonExchangeRates);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }


    void setHeader()
    {
        HeaderManager headerManager = new HeaderManager(PricingAcitivityFinal.this, findViewById(R.id.header));
        headerManager.setMenutype(HeaderManager.BACK_MENU);
        headerManager.setShowFirstImageButton(false);
        headerManager.setShowSecoundImageButton(false);
        headerManager.setTitle("Pricing");
        headerManager.setShowBottomView(true);
        headerManager.setHeaderColor(getResources().getColor(R.color.header_background_color));
    }
}
