package com.udhc.shifa4u.Activities;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.otto.Subscribe;
import com.udhc.shifa4u.Adapters.WaitingRoomDoctorsAdaptor;
import com.udhc.shifa4u.Interfaces.Constants;
import com.udhc.shifa4u.Models.AmericanTeleclinic.Physician;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Services.SignalRService;
import com.udhc.shifa4u.Utilities.Events;
import com.udhc.shifa4u.Utilities.GlobalBus;
import com.udhc.shifa4u.Utilities.HeaderManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import us.zoom.sdk.MeetingError;
import us.zoom.sdk.MeetingService;
import us.zoom.sdk.MeetingServiceListener;
import us.zoom.sdk.MeetingStatus;
import us.zoom.sdk.ZoomError;
import us.zoom.sdk.ZoomSDK;
import us.zoom.sdk.ZoomSDKAuthenticationListener;
import us.zoom.sdk.ZoomSDKInitializeListener;

public class WatingRoom extends Activity implements Constants, MeetingServiceListener, ZoomSDKAuthenticationListener, ZoomSDKInitializeListener, View.OnClickListener {

    TextView textView;

    long MillisecondTime, StartTime, TimeBuff, UpdateTime = 0L;
    Handler handler;

    int Seconds, Minutes, Hours;

    WaitingRoomDoctorsAdaptor adapter;

    private boolean mbPendingStartMeeting = false;
    @BindView(R.id.btnCancelCall)
    Button btnCancelCall;
    private LinearLayoutManager layoutManager;

    RecyclerView recyclerView;
    private List<Physician> providers;

    private ZoomSDK mZoomSDK;
    private final Context mContext = this;
    private SignalRService mService;
    private boolean mBound = false;
    public TextView txtMessage;
    private String message= "";

    @Override
    protected void onStart() {
        GlobalBus.getBus().register(this);
        super.onStart();
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wating_room);
        ButterKnife.bind(this);


        setHeader();

        mZoomSDK = ZoomSDK.getInstance();
        mZoomSDK.initialize(this, APP_KEY, APP_SECRET, WEB_DOMAIN, this);

        Intent intent = new Intent();
        intent.setClass(mContext, SignalRService.class);
        startService(intent);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);


        handler = new Handler();
        textView = (TextView) findViewById(R.id.tVstopWatch);
        StartTime = SystemClock.uptimeMillis();
        handler.postDelayed(runnable, 0);
        recyclerView = (RecyclerView) findViewById(R.id.waiting_room_recycler_view);
        providers = new ArrayList<Physician>();


        Physician p1, p2, p3;
        p1 = new Physician();
        p2 = new Physician();
        p3 = new Physician();

        p1.setPhysicianNameWithDesignation("Ali Imran");
        p2.setPhysicianNameWithDesignation("Shahid Khan");
        p3.setPhysicianNameWithDesignation("Mateen Bhai");

        p1.setSpecialityName("Naphrologist");
        p2.setSpecialityName("Neoroloigist");
        p3.setSpecialityName("Dematologist");


        p1.setCurrentPracticeLocation("Lahore, Pakistan");
        p2.setCurrentPracticeLocation("Karcahi, Pakistan");
        p3.setCurrentPracticeLocation("Islamabad, Pakistan");


        providers.add(p1);
        providers.add(p2);
        providers.add(p3);


        adapter = new WaitingRoomDoctorsAdaptor(providers, this);

        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);


        btnCancelCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String meetingNo = "132304527";
                startActivity(new Intent(WatingRoom.this, OnlineDoctorGoingToConnectActivity.class));
            }
        });

        if (mZoomSDK.isInitialized()) {
            registerListener();
        }
    }

    public Runnable runnable = new Runnable() {
        public void run() {
            MillisecondTime = SystemClock.uptimeMillis() - StartTime;
            UpdateTime = TimeBuff + MillisecondTime;
            Seconds = (int) (UpdateTime / 1000);
            Minutes = Seconds / 60;
            Hours = Minutes / 60;
            Seconds = Seconds % 60;

            textView.setText("" + Hours + ":"
                    + String.format("%02d", Minutes) + ":"
                    + String.format("%02d", Seconds));

            handler.postDelayed(this, 0);
        }

    };


    void setHeader() {
        HeaderManager headerManager = new HeaderManager(WatingRoom.this, findViewById(R.id.header));
        headerManager.setMenutype(HeaderManager.BACK_MENU);
        headerManager.setShowFirstImageButton(false);
        headerManager.setShowSecoundImageButton(false);
        headerManager.setTitle("Online Doctor");
        headerManager.setShowBottomView(true);
        headerManager.setHeaderColor(getResources().getColor(R.color.header_background_color));
    }

    private void registerListener() {
        ZoomSDK zoomSDK = ZoomSDK.getInstance();
        zoomSDK.addAuthenticationListener(this);
        MeetingService meetingService = zoomSDK.getMeetingService();
        if (meetingService != null) {
            meetingService.addListener(this);
        }
    }

    @Override
    protected void onDestroy() {
        ZoomSDK zoomSDK = ZoomSDK.getInstance();
        zoomSDK.removeAuthenticationListener(this);
        if (zoomSDK.isInitialized()) {
            MeetingService meetingService = zoomSDK.getMeetingService();
            meetingService.removeListener(this);
        }

        super.onDestroy();

    }

    @Override
    public void onMeetingStatusChanged(MeetingStatus meetingStatus, int errorCode, int internalErrorCode) {


        Log.i("", "onMeetingStatusChanged, meetingStatus=" + meetingStatus + ", errorCode=" + errorCode
                + ", internalErrorCode=" + internalErrorCode);

        if (meetingStatus == MeetingStatus.MEETING_STATUS_FAILED && errorCode == MeetingError.MEETING_ERROR_CLIENT_INCOMPATIBLE) {
            Toast.makeText(this, "Version of ZoomSDK is too low!", Toast.LENGTH_LONG).show();
        }

       /* if(mbPendingStartMeeting && meetingStatus == MeetingStatus.MEETING_STATUS_IDLE) {
            mbPendingStartMeeting = false;
          //  onClickBtnStartMeeting(null);
        }*/

    }

    @Override
    public void onZoomSDKLoginResult(long l) {

    }

    @Override
    public void onZoomSDKLogoutResult(long l) {
        ZoomSDK.getInstance().logoutZoom();

    }

    @Override
    public void onZoomIdentityExpired() {

    }


    @Override
    public void onZoomSDKInitializeResult(int errorCode, int internalErrorCode) {


        Log.i("", "onZoomSDKInitializeResult, errorCode=" + errorCode + ", internalErrorCode=" + internalErrorCode);

        if (errorCode != ZoomError.ZOOM_ERROR_SUCCESS) {
            Toast.makeText(this, "Failed to initialize Zoom SDK. Error: " + errorCode + ", internalErrorCode=" + internalErrorCode, Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Initialize Zoom SDK successfully.", Toast.LENGTH_LONG).show();
         /*   if(mZoomSDK.tryAutoLoginZoom() == ZoomApiError.ZOOM_API_ERROR_SUCCESS) {
                mZoomSDK.addAuthenticationListener(this);
                mBtnLogin.setVisibility(View.GONE);
                mBtnWithoutLogin.setVisibility(View.GONE);
                mProgressPanel.setVisibility(View.VISIBLE);
            } else {
                mBtnWithoutLogin.setVisibility(View.VISIBLE);
                mBtnLogin.setVisibility(View.VISIBLE);
                mProgressPanel.setVisibility(View.GONE);
            }*/
        }

    }
    public void sendMessage(View view) {
        if (mBound) {
            // Call a method from the SignalRService.
            // However, if this call were something that might hang, then this request should
            // occur in a separate thread to avoid slowing down the activity performance.
           /* EditText editText = (EditText) findViewById(R.id.edit_message);
            if (editText != null && editText.getText().length() > 0) {
                String message = editText.getText().toString();
                editText.setText("");
                mService.sendMessage(message);

            }*/
        }
    }
    private final ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to SignalRService, cast the IBinder and get SignalRService instance
            SignalRService.LocalBinder binder = (SignalRService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };



    @Subscribe
    public void goToNextActivity (Events.ConnectWithDoctor connectWithDoctor)
    {
        startActivity(new Intent(WatingRoom.this , OnlineDoctorGoingToConnectActivity.class));
        finish();
    }

    @Override
    protected void onStop() {
        // Unbind from the service
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }

        GlobalBus.getBus().unregister(this);
        super.onStop();
    }



    @Override
    public void onClick(View view) {

    }
}
