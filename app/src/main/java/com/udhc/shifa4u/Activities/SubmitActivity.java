package com.udhc.shifa4u.Activities;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.reflect.TypeToken;
import com.udhc.shifa4u.Models.AmericanTeleclinic.MedicalBranches;
import com.udhc.shifa4u.Models.AmericanTeleclinic.Physician;
import com.udhc.shifa4u.Models.AmericanTeleclinic.SubmitModel;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.CheckNetwork;
import com.udhc.shifa4u.Utilities.GZipRequest;
import com.udhc.shifa4u.Utilities.HeaderManager;
import com.udhc.shifa4u.Utilities.Utility;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SubmitActivity extends AppCompatActivity {
    @BindView(R.id.spSpeciality)
    Spinner spSpeciality;
    @BindView(R.id.edPatientName)
    EditText edPatientName;
    @BindView(R.id.edPhoneNo)
    EditText edPhoneNo;
    @BindView(R.id.tVSpecialityName)
    TextView tVSpecialityName;
    @BindView(R.id.spDoctorNames)
    Spinner spDoctorNames;
    @BindView(R.id.tVDoctorNames)
    TextView tVDoctorNames;
    @BindView(R.id.edMedicalIssues)
    EditText edMedicalIssues;
    @BindView(R.id.btnRequest)
    Button btnRequest;



    String myUrl = "https://api.shifa4u.com/api/AmericanTeleClinic/v1/GetAllMedicalBranches";
    String DoctorsUrl = "https://api.shifa4u.com/api/AmericanTeleClinic/v1/GetAllATPPhysicians";
    String SubmitUrl = "https://api.shifa4u.com/api/AmericanTeleClinic/v1/OrderOfflineConsultancy";
    CheckNetwork checkNetwork = new CheckNetwork(this);

    RequestQueue requestQueue;
    ProgressDialog progressDialog;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;
    public static final String mypreference = "mypref";

    List<MedicalBranches> medicalBranches;

    List<String> specialitiesNames = new ArrayList<>();
    List<String> doctorNames = new ArrayList<>();
    List<Physician> doctors = new ArrayList<>();
    @BindView(R.id.tVSpecialityNameLabel)
    TextView tVSpecialityNameLabel;
    @BindView(R.id.tVDoctorNamesLabel)
    TextView tVDoctorNamesLabel;
    String PhysicianId  = "";
    String specialitiyName = "";
    String MedicalSpecialityId = "";
    String ProductLineId = "";
    String ContactNumber = "";
    String PatientName = "";
    String MedicalIssues = "";
    String doctorName;
    SubmitModel submitModel;
    boolean isGoingToLoginPage = false;


    private BroadcastReceiver networkChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("app","Network connectivity change");
        }
    };


    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkChangeReceiver, intentFilter);
        setHeader();
    }

    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver(networkChangeReceiver);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submit);
        ButterKnife.bind(this);
        setHeader();
        sharedpreferences = getSharedPreferences(mypreference, Context.MODE_PRIVATE);

        requestQueue = Volley.newRequestQueue(this);

          doctorName = (String) getIntent().getSerializableExtra("doctorName");


     //   edPhoneNo.addTextChangedListener(new MaskWatcher("(+92)####-#######"));

         submitModel = Shifa4U.mySharePrefrence.getSubmitModel();
        if (submitModel != null)
        {
            edPatientName.setText(submitModel.getPatientName());
            edPhoneNo.setText(submitModel.getContactNo());
            edMedicalIssues.setText(submitModel.getMedialIsuues());

            if (doctorName != null)
            {
                tVSpecialityName.setText(submitModel.getSpeciality());
                tVDoctorNames.setText(submitModel.getDoctorName());
                Shifa4U.mySharePrefrence.clearSubmitModel();
            }


        }


        if (doctorName != null) {
             specialitiyName = (String) getIntent().getSerializableExtra("specialityName");
             if(submitModel == null) {
                 PhysicianId = (String) getIntent().getSerializableExtra("PhysicianId");
                 MedicalSpecialityId = (String) getIntent().getSerializableExtra("MedicalSpecialityId");
                 ProductLineId = (String) getIntent().getSerializableExtra("ProductLineId");
             }
             else
             {
                 PhysicianId = submitModel.getPhysicianId();
                 MedicalSpecialityId = submitModel.getMedicalSpecialityId();
                 ProductLineId = submitModel.getProductLineId();
             }

            spSpeciality.setVisibility(View.GONE);
            spDoctorNames.setVisibility(View.GONE);
            tVSpecialityName.setVisibility(View.VISIBLE);
            tVDoctorNames.setVisibility(View.VISIBLE);
            tVDoctorNamesLabel.setVisibility(View.VISIBLE);
            tVSpecialityNameLabel.setVisibility(View.VISIBLE);
            if (submitModel == null) {
                tVDoctorNames.setText(doctorName);
                tVSpecialityName.setText(specialitiyName);
            }
        }
        else {
            spSpeciality.setVisibility(View.VISIBLE);
            spDoctorNames.setVisibility(View.VISIBLE);
            tVSpecialityName.setVisibility(View.GONE);
            tVDoctorNames.setVisibility(View.GONE);
            tVSpecialityNameLabel.setVisibility(View.GONE);
            tVDoctorNamesLabel.setVisibility(View.GONE);
           medicalBranches = Shifa4U.mySharePrefrence.getMedicalBranches();
            doctors = Shifa4U.mySharePrefrence.getDoctors();
            if (medicalBranches == null) {
                if (checkNetwork.isInternetAvailable()) {
                    getSpecialities(this);
                } else {
                    // Toast.makeText(this, "Internet Connection Required", Toast.LENGTH_SHORT).show();
                    Utility.openInternetAlert(SubmitActivity.this);
                }
            }
            else
            {
                specialitiesNames.add("Please Select Speciality");
                for (int i = 0; i < medicalBranches.size(); i++) {
                    specialitiesNames.add(medicalBranches.get(i).getName());
                }

                Shifa4U.mySharePrefrence.clearMedicalBrances();

                ArrayAdapter<String> dataAdapter;
                ArrayAdapter<String> dataAdapter2;


                doctorNames = new ArrayList<>();
                doctorNames.add("Select Doctor");
                if (doctors != null) {
                    for (int i = 0; i < doctors.size(); i++) {
                        doctorNames.add(doctors.get(i).getPhysicianName());
                    }
                }

                Shifa4U.mySharePrefrence.clearDoctors();
                if (doctorNames != null) {
                    dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, doctorNames);
                }
                else
                {
                    doctorNames = new ArrayList<>();
                    dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, doctorNames);

                }

                if (specialitiesNames != null) {
                    dataAdapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, specialitiesNames);

                } else {
                    specialitiesNames = new ArrayList<>();
                    dataAdapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, specialitiesNames);
                }

                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                spSpeciality.setAdapter(dataAdapter2);
                spDoctorNames.setAdapter(dataAdapter);
                if (submitModel != null) {
                    if (submitModel.getSpecialitySelected() != -1) {
                        spSpeciality.setSelection(submitModel.getSpecialitySelected());
                    }
                    if (submitModel.getDoctorSelected() != -1) {
                        spDoctorNames.setSelection(submitModel.getDoctorSelected());
                    }

                    Shifa4U.mySharePrefrence.clearSubmitModel();
                }
            }
        }



        btnRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ContactNumber = edPhoneNo.getText().toString().trim();
                PatientName = edPatientName.getText().toString().trim();
                MedicalIssues = edMedicalIssues.getText().toString();

                 if (PatientName.equals("")) {
                    Toast.makeText(SubmitActivity.this, "Patient Name is required", Toast.LENGTH_SHORT).show();
                }
                else if (ContactNumber.equals("+92 (   )")) {
                    Toast.makeText(SubmitActivity.this, "Contact No is required", Toast.LENGTH_SHORT).show();
                }
              else if (spSpeciality.getSelectedItemPosition() == 0) {
                    Toast.makeText(SubmitActivity.this, "Please Select Speciality", Toast.LENGTH_SHORT).show();
                } else if (spDoctorNames.getSelectedItemPosition() == 0) {
                    Toast.makeText(SubmitActivity.this, "Please Select Doctor Name", Toast.LENGTH_SHORT).show();
                }
                 else if (MedicalIssues.equals("")) {
                     Toast.makeText(SubmitActivity.this, "Medical Issues is required", Toast.LENGTH_SHORT).show();
                 }
                 else {

                    if (checkNetwork.isInternetAvailable()) {
                        if (Shifa4U.mySharePrefrence.getLogin() != null) {


                            SubmitYourRequest(SubmitActivity.this, PhysicianId, MedicalSpecialityId, ProductLineId, ContactNumber, PatientName, MedicalIssues);
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(SubmitActivity.this);
                            builder.setMessage("Please login in-order to check out")
                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                            SubmitModel temSubmitModel = new SubmitModel();
                                            temSubmitModel.setPatientName(PatientName);
                                            temSubmitModel.setContactNo(ContactNumber);
                                            temSubmitModel.setMedialIsuues(MedicalIssues);
                                            temSubmitModel.setProductLineId(ProductLineId);
                                            temSubmitModel.setMedicalSpecialityId(MedicalSpecialityId);
                                            temSubmitModel.setPhysicianId(PhysicianId);
                                            if (doctorName != null)
                                            {
                                                temSubmitModel.setSpeciality(specialitiyName);
                                                temSubmitModel.setDoctorName(doctorName);
                                            }
                                            else {
                                                temSubmitModel.setSpecialitySelected(spSpeciality.getSelectedItemPosition());
                                                temSubmitModel.setDoctorSelected(spDoctorNames.getSelectedItemPosition());
                                            }
                                            Shifa4U.mySharePrefrence.setSubmitModel(temSubmitModel);

                                            Intent myIntent = new Intent(SubmitActivity.this, LoginActivity.class);
                                            myIntent.putExtra("From", "submit");
                                            if (doctorName != null) {
                                                myIntent.putExtra("doctorName", "doctor");
                                            }

                                            isGoingToLoginPage = true;
                                            startActivity(myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
                                            finish();
                                        }
                                    })
                                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    }).show();
                        }
                    } else {
                        // Toast.makeText(SubmitActivity.this, "Internet connection Required", Toast.LENGTH_SHORT).show();
                        Utility.openInternetAlert(SubmitActivity.this);
                    }
                }
            }
        });

        spDoctorNames.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                    if (doctors != null && doctors.size()>0 && i>0) {
                        ProductLineId = doctors.get(i-1).getProductLineId();
                        PhysicianId = doctors.get(i-1).getPhysicianId();
                    }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spSpeciality.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (checkNetwork.isInternetAvailable()) {
                    if (i > 0) {
                        MedicalSpecialityId = medicalBranches.get(i-1).getMedicalBranchId();
                        if (doctors == null) {
                            getDoctorNames(SubmitActivity.this, medicalBranches.get(i - 1).getMedicalBranchId());
                        }
                    }
                }
                else {
                   // Toast.makeText(SubmitActivity.this, "Internet Connection Required", Toast.LENGTH_SHORT).show();
                    Utility.openInternetAlert(SubmitActivity.this);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    private void getSpecialities(final Context context) {
        StringRequest stringRequest = new GZipRequest(Request.Method.POST, myUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                medicalBranches = new ArrayList<>();
                Type listType = new TypeToken<List<MedicalBranches>>() {
                }.getType();
                medicalBranches = (List<MedicalBranches>) Utility.parseJSON(response , listType);

                if (medicalBranches != null) {
                    if (Shifa4U.mySharePrefrence.getLogin() == null) {
                        Shifa4U.mySharePrefrence.setMedicalBranches(medicalBranches);
                    }
                    specialitiesNames.add("Please Select Speciality");
                    for (int i = 0; i < medicalBranches.size(); i++) {
                        specialitiesNames.add(medicalBranches.get(i).getName());
                    }

                    ArrayAdapter<String> dataAdapter;
                    ArrayAdapter<String> dataAdapter2;


                    doctorNames = new ArrayList<>();
                    doctorNames.add("Select Doctor");
                    dataAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, doctorNames);

                    if (specialitiesNames != null) {
                        dataAdapter2 = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, specialitiesNames);

                    } else {
                        specialitiesNames = new ArrayList<>();
                        dataAdapter2 = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, specialitiesNames);
                    }

                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        spSpeciality.setAdapter(dataAdapter2);
                        spDoctorNames.setAdapter(dataAdapter);

                }

                progressDialog.dismiss();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(SubmitActivity.this, "Something went wrong! Please Try again later", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                HashMap<String, String> params2 = new HashMap<String, String>();
                return new JSONObject(params2).toString().getBytes();
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };

        requestQueue.add(stringRequest);
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Loading...");
     //   progressDialog.setCancelable(false);
        progressDialog.show();
    }
    private void getDoctorNames(final Context context, final String id) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, DoctorsUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                doctors = new ArrayList<>();

                Type listType = new TypeToken<List<Physician>>() {
                }.getType();
                doctors = (List<Physician>) Utility.parseJSON(response , listType);

                if (doctors != null) {
                    doctorNames.clear();
                    doctorNames.add("Select Doctor");
                    if (doctors.size()>0) {
                        if (Shifa4U.mySharePrefrence.getLogin() == null) {
                            Shifa4U.mySharePrefrence.setDoctors(doctors);
                        }
                        for (int i = 0; i < doctors.size(); i++) {
                            doctorNames.add(doctors.get(i).getPhysicianName());
                        }
                    }
                    else
                    {
                        ProductLineId = "";
                        PhysicianId = "";
                    }

                    ArrayAdapter<String> dataAdapter;
                    dataAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, doctorNames);
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        spDoctorNames.setAdapter(dataAdapter);

                }
                progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(SubmitActivity.this, "Something Went Wrong! Please try again later", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                HashMap<String, String> params2 = new HashMap<String, String>();
                params2.put("MedicalBranchId", id);
                return new JSONObject(params2).toString().getBytes();
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };
        requestQueue.add(stringRequest);
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Loading...");
       // progressDialog.setCancelable(false);
        progressDialog.show();
    }

    private void SubmitYourRequest(final Context context , final String physicianId , final String medicalSpecialityId , final String productLineId , final String contactNumber , final String patientName , final String medicalIssues  ) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SubmitUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();

                clearAllFields ();
                Toast.makeText(SubmitActivity.this, "Request Submitted Sucessfully", Toast.LENGTH_SHORT).show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.networkResponse.toString();
                Toast.makeText(SubmitActivity.this, "Something went wrong , Please try again later", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                HashMap<String, String> params2 = new HashMap<String, String>();
                params2.put("PhysicianId", physicianId);
                params2.put("MedicalSpecialityId", medicalSpecialityId);
                params2.put("ProductLineId", productLineId);
                params2.put("ContactNumber", contactNumber);
                params2.put("PatientName", patientName);
                params2.put("MedicalIssues", medicalIssues);
                return new JSONObject(params2).toString().getBytes();
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                String accessToken = Shifa4U.mySharePrefrence.getLogin().getAccessToken();
                headers.put("Authorization", "bearer "+ accessToken);
                return headers;
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };
        requestQueue.add(stringRequest);
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Loading...");
     //   progressDialog.setCancelable(false);
        progressDialog.show();
    }

    private void clearAllFields ()
    {
        edPatientName.setText("");
        edPhoneNo.setText("");
        edMedicalIssues.setText("");
        if (doctorNames != null) {
            if (doctorNames.size() > 0)
                spDoctorNames.setSelection(0);
        }
        if (specialitiesNames != null) {
            if (specialitiesNames.size() > 0)
                spSpeciality.setSelection(0);
        }
    }

    void setHeader()
    {


        HeaderManager headerManager = new HeaderManager(SubmitActivity.this, findViewById(R.id.header));
        headerManager.setMenutype(HeaderManager.BACK_MENU);
        headerManager.setShowFirstImageButton(false);
        headerManager.setShowSecoundImageButton(false);
        headerManager.setTitle("Submit Your Request");
        headerManager.setShowBottomView(true);
        headerManager.setHeaderColor(getResources().getColor(R.color.header_background_color));
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (!isGoingToLoginPage)
        {
            Shifa4U.mySharePrefrence.clearDoctors();
            Shifa4U.mySharePrefrence.clearMedicalBrances();
            Shifa4U.mySharePrefrence.clearSubmitModel();
        }
    }
}
