package com.udhc.shifa4u.Activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.udhc.shifa4u.Models.AmericanTeleclinic.Physician;
import com.udhc.shifa4u.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class PhysicianDetail extends AppCompatActivity {

    public String imageUrl = "https://api.shifa4u.com/api/common/v1/displayimagebyimageid?id=";
    @BindView(R.id.personPhotoInProfile)
    CircleImageView personPhotoInProfile;
    @BindView(R.id.frame)
    FrameLayout frame;
    @BindView(R.id.tvPersonNameInProfile)
    TextView tvPersonNameInProfile;
    @BindView(R.id.tvSpecialityDetail)
    TextView tvSpecialityDetail;
    @BindView(R.id.personLocationInProfile)
    TextView personLocationInProfile;
    @BindView(R.id.tvEducation)
    TextView tvEducation;
    @BindView(R.id.tvSpecialityTitle)
    TextView tvSpecialityTitle;
    @BindView(R.id.tvSpeciality)
    TextView tvSpeciality;
    @BindView(R.id.liSpeciality)
    LinearLayout liSpeciality;
    @BindView(R.id.tvConditionsTreatedTitle)
    TextView tvConditionsTreatedTitle;
    @BindView(R.id.tvConditionsTreated)
    TextView tvConditionsTreated;
    @BindView(R.id.liConditionsTreated)
    LinearLayout liConditionsTreated;
    @BindView(R.id.btnAppointment)
    Button btnAppointment;
    @BindView(R.id.provider_card_view)
    CardView providerCardView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_physician_detail);
        ButterKnife.bind(this);
        // To retrieve object in second Activity
        final Physician physician = (Physician) getIntent().getSerializableExtra("MyClass");
        if (physician != null) {
            tvPersonNameInProfile.setText(physician.getPhysicianNameWithDesignation().trim());
            personLocationInProfile.setText(physician.getCurrentPracticeLocation().trim());
            tvSpecialityDetail.setText(physician.getSpecialityName().trim());

            long id = Long.parseLong(physician.getImageMobId());
            ImageView personPhotoInProfile = (ImageView) findViewById(R.id.personPhotoInProfile);
            Glide.with(this).load(imageUrl + id).into(personPhotoInProfile);

            if (physician.getProfileDetailShifa4u() != null) {

                if (!physician.getProfileDetailShifa4u().equals("")) {

                    tvEducation.setVisibility(View.VISIBLE);
                /*    liSpeciality.setVisibility(View.GONE);
                    liConditionsTreated.setVisibility(View.GONE);*/
                    tvSpeciality.setVisibility(View.GONE);
                    tvSpecialityTitle.setVisibility(View.GONE);
                    tvConditionsTreated.setVisibility(View.GONE);
                    tvConditionsTreatedTitle.setVisibility(View.GONE);

                    Spanned result = fromHtml(physician.getProfileDetailShifa4u());
                    tvEducation.setText(result);
                } else {
                    tvEducation.setVisibility(View.GONE);
               /*     liSpeciality.setVisibility(View.VISIBLE);
                    liConditionsTreated.setVisibility(View.VISIBLE);*/
                    if (physician.getSpecialties() != null) {
                        tvSpeciality.setVisibility(View.VISIBLE);
                        tvSpecialityTitle.setVisibility(View.VISIBLE);
                        tvSpeciality.setText(physician.getSpecialties().trim());
                    }

                    if (physician.getConditionsTreated() != null) {
                        tvConditionsTreated.setVisibility(View.VISIBLE);
                        tvConditionsTreatedTitle.setVisibility(View.VISIBLE);
                        Spanned result = fromHtml(physician.getConditionsTreated().trim());
                        tvConditionsTreated.setText(result);
                    }

                }
            } else {
                tvEducation.setVisibility(View.GONE);
              /*  liSpeciality.setVisibility(View.VISIBLE);
                liConditionsTreated.setVisibility(View.VISIBLE);
*/

                if (physician.getSpecialties() != null) {
                    tvSpeciality.setVisibility(View.VISIBLE);
                    tvSpecialityTitle.setVisibility(View.VISIBLE);
                    tvSpeciality.setText(physician.getSpecialties().trim());
                }


                if (physician.getConditionsTreated() != null) {
                    tvConditionsTreated.setVisibility(View.VISIBLE);
                    tvConditionsTreatedTitle.setVisibility(View.VISIBLE);
                    Spanned result = fromHtml(physician.getConditionsTreated().trim());
                    tvConditionsTreated.setText(result);
                }
            }
        }


        btnAppointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(PhysicianDetail.this, SubmitActivity.class);
                if (physician != null) {
                    myIntent.putExtra("PhysicianId", physician.getPhysicianId());
                    myIntent.putExtra("ProductLineId", physician.getProductLineId());
                    myIntent.putExtra("MedicalSpecialityId", physician.getMedicalBranchId());
                    myIntent.putExtra("doctorName", physician.getPhysicianName());
                    myIntent.putExtra("specialityName", physician.getSpecialityName());
                }
                startActivity(myIntent);
                overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );
            }
        });

    }

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(html);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }
}
