package com.udhc.shifa4u.Activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.udhc.shifa4u.Adapters.CityAdapter;
import com.udhc.shifa4u.Adapters.CountryAdapter;
import com.udhc.shifa4u.Interfaces.MenuItemsInterface;
import com.udhc.shifa4u.Models.CityModel;
import com.udhc.shifa4u.Models.CountryModel;
import com.udhc.shifa4u.Models.MenuModel;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.MyGeneric;
import com.udhc.shifa4u.Utilities.MyLog;
import com.udhc.shifa4u.Utilities.MySharePrefrence;
import com.udhc.shifa4u.api.RestAPIFactory;
import com.udhc.shifa4u.api.RestApi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Splash extends Activity {

    private Spinner spinner_city;
    private Spinner spinner_country;
    private Button btn_get_started;

    long countryId, CityId=-1;
    private MySharePrefrence mySharePrefrence;
    private LinearLayout ll_city_country;
    private ProgressDialog progressDialog;
    ArrayList<CityModel> cityModels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        spinner_country = (Spinner) findViewById(R.id.spinner_country);
        spinner_city = (Spinner) findViewById(R.id.spinner_city);
        btn_get_started = (Button) findViewById(R.id.btn_get_started);
        ll_city_country = (LinearLayout) findViewById(R.id.ll_city_country);

        spinner_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getCitiesForCountry(String.valueOf(id));
                countryId = id;
                CityId = -1;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinner_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CityId = id;

                Shifa4U.mySharePrefrence.setSelectedCityIndex(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Shifa4U.mySharePrefrence.clearSelectedCityIndex();
            }
        });

        btn_get_started.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CityId > -1) {
//                    startActivity(new Intent(Splash.this, LoginActivity.class));
//                    finish();
                    setGlobalCity(String.valueOf(CityId));
                } else {
                    Toast.makeText(Splash.this, R.string.please_select_valid_city, Toast.LENGTH_LONG).show();
                }
            }
        });


        if (Shifa4U.mySharePrefrence.getCityId() != null) {
            ll_city_country.setVisibility(View.GONE);
            if (Shifa4U.mySharePrefrence.getLogin() != null) {
                startActivity(new Intent(Splash.this, MainActivity.class));
                finish();
            } else {
                startActivity(new Intent(Splash.this, LoginActivity.class));
                finish();
            }
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//
////                    try {
////                        Thread.sleep(2000);
////                    } catch (InterruptedException e) {
////                        e.printStackTrace();
////                    }
//
//                    if (Shifa4U.mySharePrefrence.getLogin() != null) {
//                        startActivity(new Intent(Splash.this, MainActivity.class));
//                        finish();
//                    } else {
//                        startActivity(new Intent(Splash.this, LoginActivity.class));
//                        finish();
//                    }
//
//                }
//
//            }, 0);
        } else {
            ll_city_country.setVisibility(View.VISIBLE);
            if (Shifa4U.checkNetwork.isInternetAvailable()) {
                getAllCountries();
            } else {
                Toast.makeText(Splash.this, R.string.network_not_available, Toast.LENGTH_SHORT).show();
            }
        }


    }

    public void showProgressDialog(String message) {
        if (progressDialog == null)
            progressDialog = new ProgressDialog(Splash.this);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    public void dismissProgressDislog() {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }


    public void setGlobalCity(final String cityID) {
        showProgressDialog("Please Wait");
        Call<ResponseBody> setCity = RestAPIFactory.getApi().setGlobalCity(String.valueOf(cityID));
        setCity.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    Shifa4U.mySharePrefrence.setCityId(cityID);
                    Shifa4U.mySharePrefrence.setCities(cityModels);
                    Shifa4U.mySharePrefrence.setCountryId(String.valueOf(countryId));
                    startActivity(new Intent(Splash.this, LoginActivity.class));
                    finish();
                    dismissProgressDislog();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dismissProgressDislog();
            }
        });
    }

    public void getAllCountries() {
        showProgressDialog("Please Wait");
        try {
            Call<ResponseBody> posts = RestAPIFactory.getApi().getAllCountries();
            posts.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                    if (response.isSuccessful()) {
                        List<CountryModel> countryModels=MyGeneric.processResponse(response,new TypeToken<List<CountryModel>>(){});
                        if (countryModels != null && countryModels.size() > 0) {
                            spinner_country.setAdapter(new CountryAdapter(Splash.this, (ArrayList<CountryModel>) countryModels));
                        } else {
                            spinner_country.setAdapter(null);
                        }
                        dismissProgressDislog();
                        Log.e("response", response.body().toString());
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                    dismissProgressDislog();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    public void getCitiesForCountry(String countryID) {
        showProgressDialog("Please Wait");
        try {
            Call<ResponseBody> posts = RestAPIFactory.getApi().getCitiesForCountry(countryID);
            posts.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                    if (response.isSuccessful()) {

                        List<CityModel> cityModels = MyGeneric.processResponse(response,new TypeToken<List<CityModel>>(){});

                        if (cityModels != null && cityModels.size() > 0) {
                            spinner_city.setAdapter(new CityAdapter(Splash.this, (ArrayList<CityModel>) cityModels));
                            Splash.this.cityModels = (ArrayList<CityModel>) cityModels;
                        } else {
                            spinner_city.setAdapter(null);
                            Shifa4U.mySharePrefrence.clearCities();
                        }
                        dismissProgressDislog();
                        Log.e("response", response.body().toString());
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                    dismissProgressDislog();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private MenuItemsInterface menuItemsInterface=new MenuItemsInterface() {
        @Override
        public void onItemClick(Integer position, MenuModel menuModel) {

        }

        @Override
        public void onButtonClick(Integer position, Object model, View button) {

        }

        @Override
        public void onItemClick(Integer position, Object o) {

        }
    };


    private  void printDuplicates (String word)
    {
        Map<Character , Integer> map = new HashMap<>();

        char [] ch = word.toCharArray();
        for (int i = 0; i < word.length(); i++) {

            if (map.containsKey(ch[i]))
            {
                map.put(ch[i] , map.get(ch[i])+1);
            }
            else {
                map.put(ch[i] , 1);
            }

        }


    }

}
