package com.udhc.shifa4u.Activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.udhc.shifa4u.R;

public class PharmacyActivity extends Fragment {
    Button btn_know_your_medicine;
    Button upload;
    Button btn__dont_know_your_medicine;
    Button goTOWebsite;
    Button sendNames;
    LinearLayout li_know;
    LinearLayout li_dont_know;
    private ProgressDialog progressDialog = null;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.activity_pharmacy, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI(view);
    }


    private void initUI(View view) {
        btn_know_your_medicine = view.findViewById(R.id.btn_know_your_medicine);
        btn__dont_know_your_medicine = view.findViewById(R.id.btn__dont_know_your_medicine);
        li_know = view.findViewById(R.id.li_know);
        li_dont_know = view.findViewById(R.id.li_dont_know);
        upload = view.findViewById(R.id.btn_upload_prescription);
        goTOWebsite = view.findViewById(R.id.btn_go_to_website);
        sendNames = view.findViewById(R.id.btn_write_medicine_names);

        if (Shifa4U.mySharePrefrence.getAccountActivityNames() !=null) {
            if (Shifa4U.mySharePrefrence.getAccountActivityNames().equals("pharmacy")) {
                li_dont_know.setVisibility(View.VISIBLE);
                Shifa4U.mySharePrefrence.removeAccountActivityNames("pharmacy");
            } else {
                li_know.setVisibility(View.VISIBLE);
                Shifa4U.mySharePrefrence.removeAccountActivityNames("pharmacy_gotowebsite");
            }
        }

        btn_know_your_medicine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                li_know.setVisibility(View.VISIBLE);
                li_dont_know.setVisibility(View.GONE);
            }
        });

        btn__dont_know_your_medicine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                li_know.setVisibility(View.GONE);
                li_dont_know.setVisibility(View.VISIBLE);
            }
        });

        goTOWebsite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Shifa4U.mySharePrefrence.getLogin() != null) {
                    openPharmacyUrl();
                }
                else
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Please login in-order to check out")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    Intent myIntent = new Intent(getActivity(), LoginActivity.class);
                                    myIntent.putExtra("From", "pharmacy");
                                    Shifa4U.mySharePrefrence.addAccountActivityNames("pharmacy_gotowebsite");
                                    // ((MainActivity)getActivity()).addmyAccountActivityName("pharmacy");
                                    startActivity(myIntent);
                                    //  getActivity(). finish();
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).show();
                }
            }
        });


        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Shifa4U.mySharePrefrence.getLogin() != null) {
                  Intent intent = new Intent(getActivity() , UploadPrescriptionPharmacyActivity.class);
                   // ((MainActivity)getActivity()).addmyAccountActivityName("pharmacy");
                    Shifa4U.mySharePrefrence.addAccountActivityNames("pharmacy");
                  startActivity(intent);
                }
                else
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Please login in-order to check out")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    Intent myIntent = new Intent(getActivity(), LoginActivity.class);
                                    myIntent.putExtra("From", "pharmacy");
                                    Shifa4U.mySharePrefrence.addAccountActivityNames("pharmacy");

                                   // ((MainActivity)getActivity()).addmyAccountActivityName("pharmacy");
                                    startActivity(myIntent);
                                 //  getActivity(). finish();
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).show();
                }

            }
        });


        sendNames.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (Shifa4U.mySharePrefrence.getLogin() != null) {
                   Intent intent = new Intent(getActivity() , SendMedicineNames.class);
                   // ((MainActivity)getActivity()).addmyAccountActivityName("pharmacy_gotowebsite");
                    Shifa4U.mySharePrefrence.addAccountActivityNames("pharmacy");
                   startActivity(intent);
                }

                else
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Please login in-order to check out")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    Intent myIntent = new Intent(getActivity(), LoginActivity.class);
                                    myIntent.putExtra("From", "pharmacy");
                                    Shifa4U.mySharePrefrence.addAccountActivityNames("pharmacy");
                                    startActivity(myIntent);
                                    getActivity(). finish();
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).show();
                }
            }
        });
    }

    public void showProgressDialog(String message) {
        if (progressDialog == null)
            progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    public void dismissProgressDislog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        }
    }

    public void openPharmacyUrl()
    {
       // ((MainActivity)getActivity()).addmyAccountActivityName("pharmacy");
        Shifa4U.mySharePrefrence.addAccountActivityNames("pharmacy_gotowebsite");
        String url = "https://sehat.com.pk/?affRefr=Shifa4u";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }
}
