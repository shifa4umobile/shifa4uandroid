package com.udhc.shifa4u.Activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.HeaderManager;

public class SendMedicineNames extends AppCompatActivity {
    private ProgressDialog progressDialog = null;
    private TextView txt_message ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_medicine_names);

        Button btn_send_names = findViewById(R.id.btn_send_names);
        txt_message = findViewById(R.id.txt_message);

        btn_send_names.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Shifa4U.checkNetwork.isInternetAvailable()) {
                if (!txt_message.getText().toString().trim().isEmpty()) {
                    // Stub implementation for the time being for the lack of sendMessage Api

                        showProgressDialog("Please wait..");

                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                // Do something after 5s = 5000ms
                                txt_message.setText("");
                                Toast.makeText(SendMedicineNames.this, "Message Sent Successfully", Toast.LENGTH_SHORT).show();
                                dismissProgressDislog();
                            }
                        }, 1000);
                    } else {
                        Toast.makeText(SendMedicineNames.this, "Please enter a message", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(SendMedicineNames.this, "Internet Connection Required", Toast.LENGTH_SHORT).show();
                }
            }
        });
        setHeader();
    }

    private void setHeader() {
        HeaderManager headerManager = new HeaderManager(SendMedicineNames.this, findViewById(R.id.header));
        headerManager.setMenutype(HeaderManager.BACK_MENU);
        headerManager.setShowFirstImageButton(false);
        headerManager.setShowSecoundImageButton(false);
        headerManager.setShowSecoundImageButton(false);
        headerManager.setCartVisibility(false);
        headerManager.setTitle("Medicine Names");
        headerManager.setBackMenuButtonVisibility(false);
        headerManager.setShowBottomView(false);
        headerManager.setHeaderColor(getResources().getColor(R.color.header_background_color));
    }

    public void showProgressDialog(String message) {
        if (progressDialog == null)
            progressDialog = new ProgressDialog(SendMedicineNames.this);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    public void dismissProgressDislog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        }
    }
}
