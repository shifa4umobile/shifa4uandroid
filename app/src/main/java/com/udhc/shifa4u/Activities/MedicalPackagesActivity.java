package com.udhc.shifa4u.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.udhc.shifa4u.Adapters.MedicalPackagesAdapter;
import com.udhc.shifa4u.Interfaces.MenuItemsInterface;
import com.udhc.shifa4u.Models.CartModel;
import com.udhc.shifa4u.Models.MedicalPackages.MedicalPackagesModel;
import com.udhc.shifa4u.Models.MenuModel;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.HeaderManager;
import com.udhc.shifa4u.Utilities.MyGeneric;
import com.udhc.shifa4u.Utilities.MyUtils;
import com.udhc.shifa4u.api.RestAPIFactory;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MedicalPackagesActivity extends AppCompatActivity {

    private ListView lv_labs;
    private ProgressDialog progressDialog;
    int i = 0;
    private MedicalPackagesAdapter medicalPackagesAdapter;

    ArrayList<MedicalPackagesModel> medicalPackagesModels = new ArrayList<>();
    private boolean flag_loading = false;
    private Button img_search;
    private EditText txt_search;
    private String searchQuery;
    private TextView txt_no_detail;
    private LinearLayout ll_main;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_labs);
        MyUtils.hideKeyboard(this);

        setHeader();
        lv_labs = (ListView) findViewById(R.id.lv_labs);
        img_search = (Button) findViewById(R.id.img_search);
        txt_search = (EditText) findViewById(R.id.txt_search);
        img_search.setVisibility(View.GONE);


        txt_search.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT,1F));

        txt_no_detail = (TextView) findViewById(R.id.txt_no_detail);
        ll_main = (LinearLayout) findViewById(R.id.ll_main);

        if (Shifa4U.checkNetwork.isInternetAvailable()) {
            getPaginateMedicalPackage("0");
        } else {
            Toast.makeText(MedicalPackagesActivity.this, R.string.network_not_available, Toast.LENGTH_SHORT).show();
        }
//        img_search.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (Shifa4U.checkNetwork.isInternetAvailable()) {
//                i = 0;
//                medicalPackagesModels = new ArrayList<>();
//                lv_labs.setAdapter(null);
//
//
//                    if (!txt_search.getText().toString().isEmpty()) {
//                        searchQuery = txt_search.getText().toString();
//                        getSearchlabTestByWord(String.valueOf(i));
//                    } else {
//                        getPaginateMedicalPackage(String.valueOf(i));
//                    }
//                } else {
//                    Toast.makeText(MedicalPackagesActivity.this, R.string.network_not_available, Toast.LENGTH_SHORT).show();
//                }
//            }
//        });


        lv_labs.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
//                if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
//                    if (Shifa4U.checkNetwork.isInternetAvailable()) {
//                    if (flag_loading == false) {
//                        flag_loading = true;
//
//                            if (txt_search.getText().toString().isEmpty())
//                                getPaginateMedicalPackage(String.valueOf(i));
//                            else
//                                getSearchlabTestByWord(String.valueOf(i));
//                            i++;
//                        }
//
//                    }else {
//                        Toast.makeText(MedicalPackagesActivity.this, R.string.network_not_available, Toast.LENGTH_SHORT).show();
//                    }
//                }
            }
        });


        lv_labs.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        lv_labs.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });




        txt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(medicalPackagesModels!=null && medicalPackagesModels.size()>0)
                {
                    medicalPackagesAdapter.getFilter().filter(s);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        setHeader();
    }

    private void setHeader() {
        HeaderManager headerManager = new HeaderManager(MedicalPackagesActivity.this, findViewById(R.id.header));
        headerManager.setMenutype(HeaderManager.BACK_MENU);
        headerManager.setShowFirstImageButton(false);
        headerManager.setShowSecoundImageButton(false);
        headerManager.setTitle(getString(R.string.medical__package_title));
        headerManager.setShowBottomView(true);
        headerManager.setHeaderColor(getResources().getColor(R.color.header_background_color));
    }

    void getPaginateMedicalPackage(String page_to_skip) {
        showProgressDialog("Please wait...");
//        String str = String.format("{\"SkipPages\":\"%s\",\"Records\":\"10\",\"CityId\":\"%s\"}", String.valueOf(page_to_skip), Shifa4U.mySharePrefrence.getCityId());

//        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), str);

        try {
            Call<ResponseBody> posts = RestAPIFactory.getApi().getAllMedicalPackage(Shifa4U.mySharePrefrence.getCityId());
            posts.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    dismissProgressDislog();
                    try {

                        try {

//                            String Json = GzipUtils.decompress(labTestModel.body().bytes());
//
//                            Type listType = new TypeToken<LabsModel>() {
//                            }.getType();
//                            LabsModel labsModel = new Gson().fromJson(Json, listType);

                            ArrayList<MedicalPackagesModel> medicalPackagesModels = MyGeneric.processResponse(response, new TypeToken<ArrayList<MedicalPackagesModel>>(){});

                            if (medicalPackagesModels != null && medicalPackagesModels.size() > 0) {

                                if (MedicalPackagesActivity.this.medicalPackagesModels.size() > 0) {
                                    MedicalPackagesActivity.this.medicalPackagesModels.addAll(medicalPackagesModels);
                                    medicalPackagesAdapter.notifyDataSetChanged();
                                } else {
                                    MedicalPackagesActivity.this.medicalPackagesModels.addAll(medicalPackagesModels);
                                    medicalPackagesAdapter = new MedicalPackagesAdapter(MedicalPackagesActivity.this, MedicalPackagesActivity.this.medicalPackagesModels, new MenuItemsInterface() {
                                        @Override
                                        public void onItemClick(Integer position, MenuModel menuModel) {

                                        }

                                        @Override
                                        public void onButtonClick(Integer position, Object model, View button) {
                                            if (button.getId() == R.id.btn_detail) {
                                                Intent i = new Intent(MedicalPackagesActivity.this, MedicalPackageDetailActivity.class);
                                                i.putExtra("Item", (MedicalPackagesModel)model);
                                                startActivity(i);
                                            } else if (button.getId() == R.id.btn_add_to_cart) {
                                                if ((MedicalPackagesModel)model != null) {
                                                    CartModel cartModel = new CartModel();

                                                    cartModel.setOriginalObject((MedicalPackagesModel)model);
                                                    cartModel.setProductId(((MedicalPackagesModel)model).getLabPackageId());
                                                    cartModel.setProductName(((MedicalPackagesModel)model).getName());
                                                    cartModel.setProductShortDescription(((MedicalPackagesModel)model).getShortDescription());

                                                    cartModel.setProductType(CartModel.product.MEDICAL_PACKAGE);

                                                    try {
                                                        MyUtils.addToCart(cartModel);
                                                        startActivity(new Intent(MedicalPackagesActivity.this, CheckoutActivity.class));
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }

                                                }
                                        }

                                        }

                                        @Override
                                        public void onItemClick(Integer position, Object o) {

                                        }
                                    });
                                    lv_labs.setAdapter(medicalPackagesAdapter);
                                }

                                flag_loading = false;
                            } else {
                                txt_no_detail.setVisibility(View.VISIBLE);
                                ll_main.setVisibility(View.GONE);
                            }

                        } catch (Exception e) {
                            txt_no_detail.setVisibility(View.VISIBLE);
                            ll_main.setVisibility(View.GONE);
                            e.printStackTrace();
                        }


                    } catch (Exception e) {
                        txt_no_detail.setVisibility(View.VISIBLE);
                        ll_main.setVisibility(View.GONE);
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    txt_no_detail.setVisibility(View.VISIBLE);
                    ll_main.setVisibility(View.GONE);
                    dismissProgressDislog();
                    Log.e("", "");
                }
            });

        } catch (Exception e) {

        }
    }




    public void showProgressDialog(String message) {
        if (progressDialog == null)
            progressDialog = new ProgressDialog(MedicalPackagesActivity.this);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    public void dismissProgressDislog() {
        if (progressDialog.isShowing() && progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }


}
