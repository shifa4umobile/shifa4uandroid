package com.udhc.shifa4u.Activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.HeaderViewListAdapter;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.udhc.shifa4u.Adapters.StaticHealthCareAdapter;
import com.udhc.shifa4u.Models.PhysiotherapyServiceTypesModel;
import com.udhc.shifa4u.Models.StaticHealthCareModel;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.GzipUtils;
import com.udhc.shifa4u.Utilities.HeaderManager;
import com.udhc.shifa4u.Utilities.MyLog;
import com.udhc.shifa4u.api.RestAPIFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HealthCareActivity extends AppCompatActivity {

    private ListView lv_health_care_static;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_care);


        String data = getAssetJsonData(getApplicationContext());
        Type type = new TypeToken<ArrayList<StaticHealthCareModel>>() {
        }.getType();
        ArrayList<StaticHealthCareModel> staticHealthCareModels = new Gson().fromJson(data, type);

        lv_health_care_static = (ListView) findViewById(R.id.lv_health_care_static);


        Dexter.withActivity(this)
                .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {

                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {

                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {

                    }
                })
                .check();


        if (staticHealthCareModels != null && staticHealthCareModels.size() > 0) {
            StaticHealthCareAdapter staticHealthCareAdapter = new StaticHealthCareAdapter(HealthCareActivity.this, staticHealthCareModels);
            lv_health_care_static.setAdapter(staticHealthCareAdapter);
        } else {
            lv_health_care_static.setAdapter(null);
        }


        lv_health_care_static.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(position==0)
                {
                    startActivity(new Intent(HealthCareActivity.this,phyotherapyActivity.class));
                }
                else if (position == 1) {
                    Intent intent=new Intent(HealthCareActivity.this,HomeCareSubTherapyActivity.class);
                    intent.putExtra("ProductTypePrefix","SPTH");
                    intent.putExtra("Title","Speech Therapy");
                    startActivity(intent);

                }
                if (position == 2) {
                    Intent intent=new Intent(HealthCareActivity.this,HomeCareSubTherapyActivity.class);
                    intent.putExtra("ProductTypePrefix","DIET");
                    intent.putExtra("Title","Dietitian");
                    startActivity(intent);

                }
                if (position == 3) {
                    Intent intent=new Intent(HealthCareActivity.this,HomeCareSubTherapyActivity.class);
                    intent.putExtra("ProductTypePrefix","");
                    intent.putExtra("Title","Nursing");
                    startActivity(intent);

                }
                if (position == 4) {
                    Intent intent=new Intent(HealthCareActivity.this,HomeCareSubTherapyActivity.class);
                    intent.putExtra("ProductTypePrefix","");
                    intent.putExtra("Title","Home Doctor");
                    startActivity(intent);

                }
            }
        });

        lv_health_care_static.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        HeaderManager headerManager = new HeaderManager(HealthCareActivity.this, findViewById(R.id.header));
        headerManager.setMenutype(HeaderManager.BACK_MENU);
        headerManager.setShowFirstImageButton(false);
        headerManager.setShowSecoundImageButton(false);
        headerManager.setTitle(getString(R.string.home_care_title));
//        headerManager.setShowBottomView(true);
        headerManager.setHeaderColor(getResources().getColor(R.color.header_background_color));
    }


    //    PermissionListener dialogOnDeniedPermissionListener =
//            DialogOnDeniedPermissionListener.Builder.withContext(this)
//                    .withTitle("Permission")
//                    .withMessage("Please allow app to access external storage")
//                    .withButtonText(android.R.string.ok)
//                    .withIcon(R.drawable.logo)
//                    .build();

    public static String getAssetJsonData(Context context) {
        String json = null;
        try {
            InputStream is = context.getAssets().open("health_care.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }

        MyLog.e("data", json);
        return json;

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

}
