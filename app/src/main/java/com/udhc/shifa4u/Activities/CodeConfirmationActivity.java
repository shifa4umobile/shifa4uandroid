package com.udhc.shifa4u.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.udhc.shifa4u.Models.CustomerProfileModel;
import com.udhc.shifa4u.Models.SignUpModel;
import com.udhc.shifa4u.Models.VerificationParametersModel;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.HeaderManager;
import com.udhc.shifa4u.Utilities.MyGeneric;
import com.udhc.shifa4u.api.RestAPIFactory;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class CodeConfirmationActivity extends AppCompatActivity {

    String myUrl = "https://api.shifa4u.com/api/account/v1/verifyConfirmationCode";

    private String from=null;

    ProgressDialog progressDialog;
    @BindView(R.id.btn_send)
    Button btnSend;
    @BindView(R.id.txt_Code)
    AutoCompleteTextView txtCode;

    RequestQueue requestQueue;

     String  email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_code_confirmation);
        ButterKnife.bind(this);

        HeaderManager headerManager = new HeaderManager(CodeConfirmationActivity.this, findViewById(R.id.header));
        headerManager.setMenutype(HeaderManager.BACK_MENU);
        headerManager.setShowFirstImageButton(false);
        headerManager.setShowSecoundImageButton(false);
        headerManager.setTitle("Code Verification");

        requestQueue = Volley.newRequestQueue(this);


        email= getIntent().getStringExtra("email");

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!txtCode.getText().toString().trim().isEmpty())
                {
                    VerificationParametersModel verificationParametersModel = new VerificationParametersModel();
                    if (email != null) {
                        verificationParametersModel.setLoginName(email);
                    }
                    else
                    {
                        verificationParametersModel.setLoginName(Shifa4U.mySharePrefrence.getProfile().getLoginName());
                    }
                    verificationParametersModel.setVerificationCode(txtCode.getText().toString());
                    codeConform(verificationParametersModel);
                   // verifyAccount(CodeConfirmationActivity.this , txtCode.getText().toString().trim() , email);

                }
                else if (txtCode.getText().toString().trim().length() < 6)
                {
                    Toast.makeText(CodeConfirmationActivity.this, "Code should be 6 characters", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(CodeConfirmationActivity.this, "Code can not be empty", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }


    private void verifyAccount(final Context context, final String Code , final String Email) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, myUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                Toast.makeText(context, "Account verified successfully", Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                if (error.networkResponse.statusCode == 400)
                {
                    Toast.makeText(context, ""+ error.getMessage(), Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(context, "Something went wrong. ", Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                HashMap<String, String> params2 = new HashMap<String, String>();
                params2.put("VerificationCode",String.valueOf(Code));
                params2.put("LoginName ", String.valueOf(Email));

                return new JSONObject(params2).toString().getBytes();
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };

        requestQueue.add(stringRequest);
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
    }

    public void codeConform(VerificationParametersModel verificationParametersModel) {

        progressDialog = new ProgressDialog(CodeConfirmationActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        Gson gson = new Gson();
        Type type = new TypeToken<VerificationParametersModel>() {
        }.getType();
        String json = gson.toJson(verificationParametersModel, type);

        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

        //showProgressDialog("Please Wait");
        try {
            Call<ResponseBody> posts = RestAPIFactory.getApi().VerifyAccount(body);
            posts.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                    if (response.code() == 200) {
                        Toast.makeText(CodeConfirmationActivity.this, "Account Successfully Verified", Toast.LENGTH_SHORT).show();
                        //String email = txt_email.getText().toString().trim();


                        if (email != null) {

                            Intent intent = new Intent(CodeConfirmationActivity.this, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            //intent.putExtra("email", email);
                            startActivity(intent);
                            finish();
                        }
                        else
                        {
                            getProfile();
                        }
                    }
                    else if (response.code() == 409)
                    {
                        Toast.makeText(CodeConfirmationActivity.this, ""+response.message(), Toast.LENGTH_SHORT).show();
                      //  Toast.makeText(SignupActivity.this, "User Already Exists", Toast.LENGTH_LONG).show();

                    }
                    else if (response.code() == 417) {
                        Toast.makeText(CodeConfirmationActivity.this, "Please Enter Valid Code", Toast.LENGTH_SHORT).show();
                        // Toast.makeText(SignupActivity.this, "Something went wrong. Please try again later!!", Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        Toast.makeText(CodeConfirmationActivity.this, ""+response.message(), Toast.LENGTH_SHORT).show();
                    }

                    progressDialog.dismiss();
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(CodeConfirmationActivity.this, "Something went wrong. Please try again", Toast.LENGTH_LONG).show();

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            //Toast.makeText(SignupActivity.this, "Exception +" + e.getMessage(), Toast.LENGTH_LONG).show();

        }


    }


    private void getProfile() {

        showProgressDialog("Please wait...");

        try {
            Call<ResponseBody> posts = RestAPIFactory.getApi().getCustomerProfile(Shifa4U.mySharePrefrence.getLogin().getTokenType() + " " + Shifa4U.mySharePrefrence.getLogin().getAccessToken());
            posts.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                    if (response.code() == 200 && response.body() != null) {
                        CustomerProfileModel customerProfileModel = MyGeneric.processResponse(response,new TypeToken<CustomerProfileModel>(){});
                        Shifa4U.mySharePrefrence.setProfile(customerProfileModel);
                        finish();
                    }
                    dismissProgressDislog();
                }
                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    dismissProgressDislog();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            dismissProgressDislog();
        }
    }

    public void showProgressDialog(String message) {
        if (progressDialog == null)
            progressDialog = new ProgressDialog(CodeConfirmationActivity.this);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    public void dismissProgressDislog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        }
    }




}
