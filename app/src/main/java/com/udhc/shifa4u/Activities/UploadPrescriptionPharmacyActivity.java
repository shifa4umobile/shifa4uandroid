package com.udhc.shifa4u.Activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.HeaderManager;
import com.udhc.shifa4u.api.RestAPIFactory;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UploadPrescriptionPharmacyActivity extends AppCompatActivity {
    Button btn_upload_image;
    Button btn_submit;
    Button btn_upload_pdf;
    private ImageView img_delete;
    private LinearLayout ll_attachment_container;
    private ProgressDialog progressDialog;
    private ArrayList<String> docPaths;
    private TextView txt_upload;
    private ArrayList<String> photoPaths ;
    private File file = null;


    private HeaderManager headerManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_prescription_pharmacy);
        btn_upload_image = findViewById(R.id.btn_upload_image_from_dialog);
        btn_upload_pdf = findViewById(R.id.btn_upload_pdf_from_dialog);
        btn_submit = findViewById(R.id.btn_submit);
        txt_upload = (TextView) findViewById(R.id.txt_upload_from_dialog);
        img_delete=(ImageView)findViewById(R.id.img_delete_from_dialog);
        ll_attachment_container = (LinearLayout) findViewById(R.id.ll_attachment_container_from_dialog);
        file = null;
        setHeader();


        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!txt_upload.getText().toString().contains("No File")) {
                    uploadPriscription();
                }
                else
                {
                    Toast.makeText(UploadPrescriptionPharmacyActivity.this, "Please Upload prescription", Toast.LENGTH_SHORT).show();
                }
            }
        });


        btn_upload_image.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {


                if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    //Log.v(TAG,"Permission is granted");
                    //File write logic here
                    ActivityCompat.requestPermissions(UploadPrescriptionPharmacyActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

                }
                else  if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(UploadPrescriptionPharmacyActivity.this, new String[]{Manifest.permission.CAMERA}, 1);
                }
                else {

                    FilePickerBuilder.getInstance().setMaxCount(5)
                            .setSelectedFiles(photoPaths)
                            .setActivityTheme(R.style.LibAppTheme)
                            .setMaxCount(1)
                            .pickPhoto(UploadPrescriptionPharmacyActivity.this);
                }
         /*    refreshImagePicker();
                imagePicker.choosePicture(true);*/
            }
        });


        img_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // ll_attachment_container.setVisibility(View.GONE);
                txt_upload.setText("No File Selected...");
                img_delete.setVisibility(View.GONE);
                if (photoPaths != null)
                    photoPaths.clear();
                if (docPaths !=null)
                    docPaths.clear();
                file=null;
            }
        });


        btn_upload_pdf.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(UploadPrescriptionPharmacyActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                }
                else {

                    FilePickerBuilder.getInstance().setMaxCount(5)
                            .setSelectedFiles(docPaths)
                            .setActivityTheme(R.style.LibAppTheme)
                            .setMaxCount(1)
                            .pickFile(UploadPrescriptionPharmacyActivity.this );
                }
            }
        });


    }


    private void uploadPriscription() {
        showProgressDialog("Please Wait...");

        Bitmap bitmap;
        try {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));

            int size = (int) file.length();
            byte[] bytes = new byte[size];
            try {

                buf.read(bytes, 0, bytes.length);
                buf.close();
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            FileInputStream fileInputStream = new FileInputStream(file);

            byte[] blob=bytes;
            Bitmap bmp= BitmapFactory.decodeByteArray(blob,0,blob.length);

            Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());

            Call<ResponseBody> posts = RestAPIFactory.getApi().UploadPrescriptionPatientCommunication(file);
            posts.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    dismissProgressDislog();
                    if (response.code() == 200) {
                        Toast.makeText(UploadPrescriptionPharmacyActivity.this, "Data submitted successfully", Toast.LENGTH_LONG).show();
                        file = null;
                        txt_upload.setText("");
                        ll_attachment_container.setVisibility(View.GONE);
                    } else {
                        Toast.makeText(UploadPrescriptionPharmacyActivity.this, "Something went wrong.\n Please try again later", Toast.LENGTH_LONG).show();
                    }
                }
                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    dismissProgressDislog();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setHeader() {
        headerManager = new HeaderManager(UploadPrescriptionPharmacyActivity.this, findViewById(R.id.header));
        headerManager.setMenutype(HeaderManager.BACK_MENU);
        headerManager.setShowFirstImageButton(false);
        headerManager.setShowSecoundImageButton(false);
        headerManager.setShowSecoundImageButton(false);
        headerManager.setCartVisibility(false);
        headerManager.setTitle("Upload Prescription");
        headerManager.setBackMenuButtonVisibility(false);
        headerManager.setShowBottomView(false);
        headerManager.setHeaderColor(getResources().getColor(R.color.header_background_color));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case FilePickerConst.REQUEST_CODE_PHOTO:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    if (docPaths != null)
                        docPaths.clear();
                    photoPaths = new ArrayList<>();
                    photoPaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA));
                }
                break;

            case FilePickerConst.REQUEST_CODE_DOC:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    if (photoPaths!= null)
                        photoPaths.clear();
                    docPaths = new ArrayList<>();
                    docPaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS));
                }
                break;
        }

        if (data != null)
           addThemToView(photoPaths, docPaths);
    }



    private void addThemToView(ArrayList<String> imagePaths, ArrayList<String> docPaths) {
        ArrayList<String> filePaths = new ArrayList<>();
        if (imagePaths != null) filePaths.addAll(imagePaths);

        if (docPaths != null) filePaths.addAll(docPaths);
        String filename=filePaths.get(0).substring(filePaths.get(0).lastIndexOf("/")+1);
        if (filePaths != null) {
            txt_upload.setText(filename);
            img_delete.setVisibility(View.VISIBLE);
            file = new File(filePaths.get(0));
            ll_attachment_container.setVisibility(View.VISIBLE);
        }
    }

    public void showProgressDialog(String message) {
        if (progressDialog == null)
            progressDialog = new ProgressDialog(UploadPrescriptionPharmacyActivity.this);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    private Bitmap convertToBitmap(File file) {
        String filePath = file.getPath();
        Bitmap bitmap = BitmapFactory.decodeFile(filePath);
        return bitmap;
    }

    public void dismissProgressDislog() {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }
}
