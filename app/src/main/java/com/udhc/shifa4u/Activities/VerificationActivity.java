package com.udhc.shifa4u.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.HeaderManager;
import com.udhc.shifa4u.api.RestAPIFactory;

import org.json.JSONException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VerificationActivity extends AppCompatActivity {

    private Button btn_continue_to_login;
    private Button btnEnterCode;
    private Button btnResendConfirmationCode;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);

        final String  email= getIntent().getStringExtra("email");

        HeaderManager headerManager = new HeaderManager(VerificationActivity.this, findViewById(R.id.header));
        headerManager.setMenutype(HeaderManager.BACK_MENU);
        headerManager.setShowFirstImageButton(false);
        headerManager.setShowSecoundImageButton(false);
        headerManager.setTitle(getString(R.string.verification_title));


        btn_continue_to_login=(Button)findViewById(R.id.btn_continue_to_login);
        btnResendConfirmationCode=(Button)findViewById(R.id.btn_resend_verification_email);
        btnEnterCode=(Button)findViewById(R.id.btnEnterCode);
        btn_continue_to_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(VerificationActivity.this,LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
            }
        });

        btnResendConfirmationCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("LoginName", email);

                try {
                    resendConfirmationCode(jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        btnEnterCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              Intent intent =  new Intent(VerificationActivity.this,CodeConfirmationActivity.class);
                intent.putExtra("email", email);
                startActivity(intent);
            }
        });


    }
    public void resendConfirmationCode(JsonObject jsonObject) throws JSONException {
        showProgressDialog("Please wait...");
        //  RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), "shahid.mrd@hotmail.com");

        //showProgressDialog("Please Wait");
        //   String email = jsonObject.getString("LoginName");
        try {
            Call<ResponseBody> posts = RestAPIFactory.getApi().ResendVerificationCode(jsonObject);
            posts.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.code() == 200) {
                        //showALertBox(ForgotPasswordActivity.this);

                    } else {
                      //  Toast.makeText(ForgotPasswordActivity.this, "Something went wrong.\n Please try again later", Toast.LENGTH_LONG).show();
                    }
                    dismissProgressDislog();
                }
                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    dismissProgressDislog();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showProgressDialog(String message) {
        if (progressDialog == null)
            progressDialog = new ProgressDialog(VerificationActivity.this);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    public void dismissProgressDislog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }
}
