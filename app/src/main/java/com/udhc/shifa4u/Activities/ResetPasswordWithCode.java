package com.udhc.shifa4u.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.reflect.TypeToken;
import com.udhc.shifa4u.Models.AmericanTeleclinic.Physician;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.HeaderManager;
import com.udhc.shifa4u.Utilities.Utility;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ResetPasswordWithCode extends AppCompatActivity {

    @BindView(R.id.txt_Code)
    AutoCompleteTextView txtCode;
    @BindView(R.id.txt_Password)
    AutoCompleteTextView txtPassword;
    @BindView(R.id.txt_confirm_password)
    AutoCompleteTextView txtConfirmPassword;
    @BindView(R.id.btn_send)
    Button btnSend;
    RequestQueue requestQueue;
    private HeaderManager headerManager;
    String myUrl = "https://api.shifa4u.com/api/account/v1/changemobileuserpassword";
    ProgressDialog progressDialog;
    String email = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password_with_code);
        ButterKnife.bind(this);
        setHeader();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            email = extras.getString("email");
        }

        requestQueue = Volley.newRequestQueue(this);
    }

    @OnClick(R.id.btn_send)
    public void onViewClicked() {
        String code = "", password = "", confirm_password = "";
        code = txtCode.getText().toString().trim();
        password = txtPassword.getText().toString().trim();
        confirm_password = txtConfirmPassword.getText().toString().trim();
        if (!code.isEmpty()) {
            if (code.length() ==6) {
                if (!password.isEmpty())
                {
                if (password.length() > 6) {
                    if (password.equals(confirm_password)) {
                        confirm(code, password, email);
                    } else {
                        Toast.makeText(this, "Password don't match", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, "Password Length should be atlest six characters", Toast.LENGTH_SHORT).show();

                }
            }
            else
                {
                    Toast.makeText(this, "Password Can not be empty", Toast.LENGTH_SHORT).show();
                }
        }
        else 
            {
                Toast.makeText(this, "Code should be of length 6", Toast.LENGTH_SHORT).show();
            }

        }
        else
        {
            Toast.makeText(this, "Code is empty", Toast.LENGTH_SHORT).show();
        }
    }


    private void confirm(final String code, final String password, final String email) {

            StringRequest stringRequest = new StringRequest(Request.Method.POST, myUrl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Toast.makeText(ResetPasswordWithCode.this, "Password Reset done successfully", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(ResetPasswordWithCode.this , LoginActivity.class));
                    progressDialog.dismiss();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    String err = error.getMessage();
                    
                    if (error.networkResponse.statusCode == 400)
                    {
                        Toast.makeText(ResetPasswordWithCode.this, "Reset code is incorrect", Toast.LENGTH_SHORT).show();
                    }
                    progressDialog.dismiss();
                }
            }) {
                @Override
                public byte[] getBody() throws AuthFailureError {
                    HashMap<String, String> params2 = new HashMap<String, String>();
                    params2.put("VerificationCode", code);
                    params2.put("Username", email);
                    params2.put("Password", password);
                    return new JSONObject(params2).toString().getBytes();
                }

                @Override
                public String getBodyContentType() {
                    return "application/json";
                }
            };

            requestQueue.add(stringRequest);
            progressDialog = new ProgressDialog(ResetPasswordWithCode.this);
            progressDialog.setMessage("Loading...");
            progressDialog.show();
    }


    private void setHeader() {
        headerManager = new HeaderManager(ResetPasswordWithCode.this, findViewById(R.id.header));
        headerManager.setMenutype(HeaderManager.BACK_MENU);
        headerManager.setShowFirstImageButton(false);
        headerManager.setShowSecoundImageButton(false);
        headerManager.setTitle("Reset Password");
//        headerManager.setShowBottomView(true);
        headerManager.setHeaderColor(getResources().getColor(R.color.header_background_color));
    }
}
