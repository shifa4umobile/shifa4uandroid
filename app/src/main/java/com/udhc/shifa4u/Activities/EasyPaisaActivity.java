package com.udhc.shifa4u.Activities;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.CookieManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.reflect.TypeToken;
import com.udhc.shifa4u.Models.AmericanTeleclinic.Physician;
import com.udhc.shifa4u.Models.CartModel;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.HeaderManager;
import com.udhc.shifa4u.Utilities.Utility;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

public class EasyPaisaActivity extends AppCompatActivity {

    final Activity activity = this;
    private HeaderManager headerManager;
    private WebView webView;
    boolean isFirst = true;
    String postData = null;
    String encryptedValue;
    String data;

    ProgressDialog progressDialog;
    String hashKey = "94KS420MZVU4E7JY";

    String firstUrl = "https://easypay.easypaisa.com.pk/easypay/Index.jsf";

    RequestQueue requestQueue;
    @TargetApi(Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // this.getWindow().requestFeature(Window.FEATURE_PROGRESS);
        setContentView(R.layout.activity_easy_paisa);

        setHeader();

        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        String OrderId = bundle.getString("OrderId");
        if (OrderId == null) {
            OrderId = "";
        }

        progressDialog = new ProgressDialog(EasyPaisaActivity.this);
        progressDialog.setMessage("Wait, Redirecting to easy...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        ArrayList<CartModel> cartModels = Shifa4U.mySharePrefrence.getCart();

        Shifa4U.mySharePrefrence.clearCart();

        String amount = "0";
        if (cartModels != null && cartModels.size() > 0) {

            Double cartTotal = 0.0;
            for (CartModel cartModel : cartModels) {
                cartTotal += cartModel.getPriice();
            }
             amount = String.valueOf(cartTotal);
        }
            data = "https://easypay.easypaisa.com.pk/easypay/Index.jsf";
            try {
                postData = "";
                postData += URLEncoder.encode("amount", "UTF-8")
                        + "=" + URLEncoder.encode(amount, "UTF-8");

                postData += "&" + URLEncoder.encode("autoRedirect", "UTF-8")
                        + "=" + URLEncoder.encode("1", "UTF-8");


                postData += "&" + URLEncoder.encode("orderRefNum", "UTF-8")
                        + "=" + URLEncoder.encode(OrderId, "UTF-8");

                postData += "&" + URLEncoder.encode("postBackURL", "UTF-8")
                        + "=" + "https://www.shifa4u.com/cart/easypayconfirm";


                postData += "&" + URLEncoder.encode("storeId", "UTF-8") + "="
                        + URLEncoder.encode("7954", "UTF-8");
                String mapString = "amount="+amount+"&autoRedirect=1&orderRefNum="+OrderId+"&postBackURL=https://www.shifa4u.com/cart/easypayconfirm&storeId=7954";

                Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
                final SecretKeySpec secretKey = new SecretKeySpec(hashKey.getBytes(), "AES");
                cipher.init(Cipher.ENCRYPT_MODE, secretKey);
                encryptedValue = android.util.Base64.encodeToString(cipher.doFinal(postData.getBytes()), android.util.Base64.NO_WRAP);
                String encrypted = URLEncoder.encode(encryptedValue, "UTF-8");
                postData = mapString + "&" + "merchantHashedReq=" + encrypted;

            } catch (Exception e) {
                e.printStackTrace();
            }


            webView = (WebView) findViewById(R.id.webview);

        if (android.os.Build.VERSION.SDK_INT >= 21) {
            CookieManager.getInstance().setAcceptThirdPartyCookies(webView, true);
        }else {
            CookieManager.getInstance().setAcceptCookie(true);
        }
            // WebView settings
            webView.setWebViewClient(new MyWebViewClient(this));
            webView.getSettings().setJavaScriptEnabled(true);
            //webView.getSettings().setUseWideViewPort(true);
            // webView.getSettings().setPluginState(WebSettings.PluginState.ON);
            //webView.setBackgroundColor(0x00000000);
            // Load url
            //Map<String, String> params = new HashMap<String, String>();
            //params.put("X-Frame-Options", "GOFORIT");

            Log.i("log_tag onCreate", "12");
            // webView.loadUrl("https://easypay.easypaisa.com.pk/easypay/Index.jsf?storeId=7954&amount=101&postBackURL=https://www.shifa4u.com/cart/easypayconfirm&orderRefNum=test2");
            webView.postUrl(data, postData.getBytes());
       /* String urlMade = data + "?" + postData;
        webView.loadUrl(urlMade);*/
            //webView.loadUrl("https://easypaystg.easypaisa.com.pk/easypay-admin/faces/pg/crm/Login.jsf");

        }

    private class MyWebViewClient extends WebViewClient {
        Context c ;
        MyWebViewClient (Context context)
        {
            this.c = context;
        }
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            if (url.contains("https://www.shifa4u.com/?paymentToken"))
            {
                startActivity(new Intent(EasyPaisaActivity.this,MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP));
                finish();
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            }
            else {
                view.loadUrl(url);
            }
            //  view.loadDataWithBaseURL(null,data,"text/html","utf-8",null);
            // webView.loadData(url, "text/html; charset=utf-8", "utf-8");

            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

            Log.e("purl", url);
            if (url.contains("Checkout")) {
                progressDialog.dismiss();
                webView.setVisibility(View.VISIBLE);
            }

            if (url.contains("https://www.shifa4u.com/?paymentToken"))
            {
                startActivity(new Intent(EasyPaisaActivity.this,MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP));
                finish();
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            }
            if(isFirst) {

                String[] ist = url.split("=");
                if (ist.length>1) {
                    isFirst = false;
                    String[] snd = ist[1].split("&");
                    String Token = snd[0];

                    Log.e("token", Token);
                    Log.e("posturl", ist[2]);
                    secondredirect(Token, view);
                }
            }

            //  mPB.setVisibility(View.GONE);

        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

        }

        @Override
        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {


            final AlertDialog.Builder builder = new AlertDialog.Builder(c);
            builder.setMessage("There Seems to be an SSL error. Do you want to proceed?");
            builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.proceed();
                }
            });
            builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.cancel();
                }
            });
            final AlertDialog dialog = builder.create();
            dialog.show();
        }

    }

    //
    private void secondredirect(String token, WebView view){
        String sData = null;
        String sURL = "https://easypay.easypaisa.com.pk/easypay/Confirm.jsf";


        try {
          /*  sData = URLEncoder.encode("auth_token", "UTF-8")
                    + "=" + URLEncoder.encode(token, "UTF-8");
*/
            sData =  URLEncoder.encode("postBackURL", "UTF-8") + "="
                    + URLEncoder.encode("https://www.shifa4u.com", "UTF-8");

            // String  temData = "auth_token=" + token + "&" +  "postBackURL=" +  "https://www.shifa4u.com/cart/easypayconfirm";
            String  temData = "auth_token=" + token + "&" +  sData;

            synchronized (this) {
                view.postUrl(sURL, temData.getBytes());
            }
           // progressDialog.dismiss();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
       // super.onBackPressed();

        startActivity(new Intent(EasyPaisaActivity.this,MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP));
        finish();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    private void SetOrientation(){
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    private void setHeader() {
        headerManager = new HeaderManager(this, findViewById(R.id.header));
        headerManager.setMenutype(HeaderManager.BACK_MENU);
        headerManager.setShowFirstImageButton(false);
        headerManager.setShowSecoundImageButton(false);
        headerManager.setTitle("EasyPaisa Portal");

        headerManager.setShowBottomView(false);
        headerManager.setHeaderColor(getResources().getColor(R.color.header_background_color));
    }
}
