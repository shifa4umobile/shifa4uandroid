package com.udhc.shifa4u.Activities;

public class BlogCategories
{
    private String BlogCategoryName;

    private String BlogCategoryId;

    public String getBlogCategoryName ()
    {
        return BlogCategoryName;
    }

    public void setBlogCategoryName (String BlogCategoryName)
    {
        this.BlogCategoryName = BlogCategoryName;
    }

    public String getBlogCategoryId ()
    {
        return BlogCategoryId;
    }

    public void setBlogCategoryId (String BlogCategoryId)
    {
        this.BlogCategoryId = BlogCategoryId;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [BlogCategoryName = "+BlogCategoryName+", BlogCategoryId = "+BlogCategoryId+"]";
    }
}
