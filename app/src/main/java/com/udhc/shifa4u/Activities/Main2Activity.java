package com.udhc.shifa4u.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.HeaderManager;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_what_is_telemedicine);
        setHeader();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setHeader();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition( R.anim.slide_in_right, R.anim.slide_out_right );
    }

    void setHeader()
    {
        HeaderManager headerManager = new HeaderManager(Main2Activity.this, findViewById(R.id.header));
        headerManager.setMenutype(HeaderManager.BACK_MENU);
        headerManager.setShowFirstImageButton(false);
        headerManager.setShowSecoundImageButton(false);
        headerManager.setTitle("What is TeleMedicine");
        headerManager.setShowBottomView(true);
        headerManager.setHeaderColor(getResources().getColor(R.color.header_background_color));
    }
}
