package com.udhc.shifa4u.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.udhc.shifa4u.Adapters.LabAdapter;
import com.udhc.shifa4u.Adapters.MedicalPackageLabAdapter;
import com.udhc.shifa4u.Adapters.RadioLogyLabAdapter;
import com.udhc.shifa4u.Interfaces.MenuItemsInterface;
import com.udhc.shifa4u.Models.CartModel;
import com.udhc.shifa4u.Models.LabDetailModel;
import com.udhc.shifa4u.Models.LabTestList;
import com.udhc.shifa4u.Models.MedicalPackages.LabPackagesComparison;
import com.udhc.shifa4u.Models.MedicalPackages.LabTest;
import com.udhc.shifa4u.Models.MedicalPackages.MedicalPackagesModel;
import com.udhc.shifa4u.Models.MenuModel;
import com.udhc.shifa4u.Models.RadioLogyDetail.RadioLogyDetailModel;
import com.udhc.shifa4u.Models.RadioLogyDetail.RadiologyLabsComparison;
import com.udhc.shifa4u.Models.Radiology;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.HeaderManager;
import com.udhc.shifa4u.Utilities.MyGeneric;
import com.udhc.shifa4u.Utilities.MyUtils;
import com.udhc.shifa4u.api.RestAPIFactory;

import java.util.ArrayList;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MedicalPackageDetailActivity extends AppCompatActivity {

    private TextView txt_detail_price;
    private TextView txt_detail_title;
    private TextView txt_detail_description;
    private TextView txt_detail_test_name;
    private TextView txt_detail_aka;
    private TextView txt_detail_sample_type;
    private AutoCompleteTextView txt_detail_labs;
    private ProgressDialog progressDialog;
    private LabTestList labTestList;
    private MedicalPackagesModel medicalPackageModel;
    private TextView txt_detail_radiology_description;
    private LinearLayout ll_medical_package_lab_tests;
    private TextView txt_medical_package_lab_test_lable;
    private Button btn_order;
    private LabPackagesComparison selectedLab = null;
    private HeaderManager headerManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medical_package_detail);

        setHeader();

        txt_detail_title = (TextView) findViewById(R.id.txt_detail_title);
        txt_detail_description = (TextView) findViewById(R.id.txt_detail_description);
        txt_detail_labs = (AutoCompleteTextView) findViewById(R.id.txt_detail_labs);
        txt_detail_price = (TextView) findViewById(R.id.txt_detail_price);
        txt_detail_radiology_description = (TextView) findViewById(R.id.txt_detail_radiology_description);
        txt_medical_package_lab_test_lable = (TextView) findViewById(R.id.txt_medical_package_lab_test_lable);
        ll_medical_package_lab_tests = (LinearLayout) findViewById(R.id.ll_medical_package_lab_tests);
        btn_order = (Button) findViewById(R.id.btn_order);


        txt_detail_radiology_description.setVisibility(View.GONE);

        txt_detail_labs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_detail_labs.showDropDown();
            }
        });

        txt_detail_labs.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {


            }
        });


        btn_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedLab != null) {
                    CartModel cartModel = new CartModel();
                    cartModel.setOriginalObject(medicalPackageModel);
                    cartModel.setProductId(medicalPackageModel.getLabPackageId());
                    cartModel.setProductName(medicalPackageModel.getName());
                    cartModel.setProductShortDescription(medicalPackageModel.getShortDescription());
                    cartModel.setSelectedLabId(((LabPackagesComparison) selectedLab).getLabId());
                    cartModel.setSelectedLabName(((LabPackagesComparison) selectedLab).getLabName());
                    cartModel.setPriice(((LabPackagesComparison) selectedLab).getYourPrice().doubleValue());
                    cartModel.setOriginalPriice(((LabPackagesComparison) selectedLab).getVendorPrice().doubleValue());
                    cartModel.setProductLineId(String.valueOf(((LabPackagesComparison) selectedLab).getPackageId()));

                    cartModel.setProductType(CartModel.product.MEDICAL_PACKAGE);


                    try {
                        MyUtils.addToCart(cartModel);
                        Toast.makeText(MedicalPackageDetailActivity.this, "Product added to cart", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(MedicalPackageDetailActivity.this, CheckoutActivity.class));
                        finish();
                    } catch (Exception e) {
                        if (e.getMessage().equalsIgnoreCase("Item Already Exist")) {

                            Toast.makeText(MedicalPackageDetailActivity.this, "Product added to cart", Toast.LENGTH_SHORT).show();
                        }
                        e.printStackTrace();
                    }

                    if(MyUtils.isLabExistInCart(cartModel))
                    {
                        showLabExistDialog(MedicalPackageDetailActivity.this);
                    }
                    setHeader();
                }
                else
                {
                    Toast.makeText(MedicalPackageDetailActivity.this, "Please Select Lab", Toast.LENGTH_SHORT).show();
                }

            }
        });


        if (getIntent().getSerializableExtra("Item") != null) {

            medicalPackageModel = (MedicalPackagesModel) getIntent().getSerializableExtra("Item");
            headerManager.setTitle(medicalPackageModel.getName()
            );

            try {

//                        ArrayList<LabDetailModel> labDetailModels = (ArrayList<LabDetailModel>) new MyGeneric<ArrayList<LabDetailModel>>().processResponse(response);

//                        String Json = GzipUtils.decompress(response.body().bytes());
//
//                        Type listType = new TypeToken<ArrayList<LabDetailModel>>() {
//                        }.getType();
//                        ArrayList<LabDetailModel> labDetailModels = new Gson().fromJson(Json, listType);


                if (medicalPackageModel != null) {
                    txt_detail_title.setText(medicalPackageModel.getName());
                    txt_detail_description.setText(medicalPackageModel.getShortDescription());

                    if (ll_medical_package_lab_tests.getChildCount() > 0) {
                        ll_medical_package_lab_tests.removeAllViews();
                    }
                    if (medicalPackageModel.getLabTests() != null && medicalPackageModel.getLabTests().size() > 0) {

                        txt_medical_package_lab_test_lable.setVisibility(View.VISIBLE);

                        for (LabTest labTest : medicalPackageModel.getLabTests()) {
                            View labTestItemView = LayoutInflater.from(MedicalPackageDetailActivity.this).inflate(R.layout.medical__package_labs_test_item, null);

                            TextView txt_medical_package_labs_test_name = (TextView) labTestItemView.findViewById(R.id.txt_medical_package_labs_test_name);
                            txt_medical_package_labs_test_name.setText(labTest.getLabTestName());
                            ll_medical_package_lab_tests.addView(labTestItemView);
                        }
                    }

                    if (medicalPackageModel.getLabPackagesComparison() != null && medicalPackageModel.getLabPackagesComparison().size() > 0) {
                        txt_detail_labs.setAdapter(new MedicalPackageLabAdapter(MedicalPackageDetailActivity.this, (ArrayList<LabPackagesComparison>) medicalPackageModel.getLabPackagesComparison(), new MenuItemsInterface() {
                            @Override
                            public void onItemClick(Integer position, MenuModel menuModel) {

                            }

                            @Override
                            public void onButtonClick(Integer position, Object model, View button) {

                            }

                            @Override
                            public void onItemClick(Integer position, Object o) {
                                txt_detail_labs.dismissDropDown();
                                txt_detail_labs.setText(((LabPackagesComparison) o).getLabName());
                                txt_detail_price.setText(Html.fromHtml("<font color='#002868'>Price :  </font>" + String.valueOf(((LabPackagesComparison) o).getYourPrice())));
                                selectedLab = ((LabPackagesComparison) o);
                            }
                        }));
                    }


                }


            } catch (Exception e) {
                e.printStackTrace();
            }


        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        setHeader();
    }

    private void setHeader() {
        headerManager = new HeaderManager(MedicalPackageDetailActivity.this, findViewById(R.id.header));
        headerManager.setMenutype(HeaderManager.BACK_MENU);
        headerManager.setShowFirstImageButton(false);
        headerManager.setShowSecoundImageButton(false);

        headerManager.setShowBottomView(true);
        headerManager.setHeaderColor(getResources().getColor(R.color.header_background_color));
    }

    public void showLabExistDialog(Context context) {

        AlertDialog.Builder alertDialog =new AlertDialog.Builder(context);
        alertDialog.setTitle("Warning");
        alertDialog.setMessage("Please note that different tests from different labs will require a separate sample for each lab. The home sampling services are provided by individual labs, and ordering different tests from different labs will require each of them to visit separately and draw separate samples");
        alertDialog.setNeutralButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }
}
