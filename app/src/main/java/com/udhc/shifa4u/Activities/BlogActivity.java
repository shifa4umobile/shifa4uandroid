package com.udhc.shifa4u.Activities;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.reflect.TypeToken;
import com.udhc.shifa4u.Adapters.BlogAdaptor;
import com.udhc.shifa4u.Models.AmericanTeleclinic.Blog;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.CheckNetwork;
import com.udhc.shifa4u.Utilities.GZipRequest;
import com.udhc.shifa4u.Utilities.HeaderManager;
import com.udhc.shifa4u.Utilities.Utility;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BlogActivity extends AppCompatActivity {

    private static final String TAG = "Blog Activity";
    @BindView(R.id.spCategories)
    Spinner spCategories;
    @BindView(R.id.blog_recycler_view)
    RecyclerView blogRecyclerView;
    ProgressDialog progressDialog;

    private boolean isFirstTime = true;

    int SkipPages = 0;
    int Records = 9;

    RequestQueue requestQueue;
    List<BlogCategories> blogCategories;
    CheckNetwork checkNetwork = new CheckNetwork(this);

    String myUrl = "https://api.shifa4u.com/api/blog/v1/paginateblog";
    String categoriesUrl = "https://api.shifa4u.com/api/blog/v1/paginateblogcategories";
    String filteredDataUrl = "https://api.shifa4u.com/api/blog/v1/paginateandfilter";


    private RecyclerView.LayoutManager layoutManager;

    private List<Blog> blogs;

    List<String> categoriesLIst = new ArrayList<>();

    BlogAdaptor adapter;
    private BroadcastReceiver networkChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("app","Network connectivity change");
          /*  if (!isFirstTime) {
                getAllData(BlogActivity.this);
                if (blogCategories == null)
                {
                    getCategories(BlogActivity.this);
                }
            }
            isFirstTime = false;*/
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkChangeReceiver, intentFilter);
        setHeader();
    }

    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver(networkChangeReceiver);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blog);
        ButterKnife.bind(this);
        setHeader();

        requestQueue = Volley.newRequestQueue(this);

        blogRecyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        blogRecyclerView.setLayoutManager(layoutManager);
        blogRecyclerView.setItemAnimator(new DefaultItemAnimator());



        spCategories.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (checkNetwork.isInternetAvailable()) {
                    if (i > 1) {
                        if (blogCategories != null) {
                            SkipPages = 100;
                            getFilteredData(BlogActivity.this, blogCategories.get(i - 2).getBlogCategoryId());
                        }
                    } else if (i == 1) {

                        SkipPages = 100;
                        blogs.clear();
                        adapter.notifyDataSetChanged();
                        getAllData(BlogActivity.this);

                    }
                }
                else 
                {
                   // Toast.makeText(BlogActivity.this, "Internet Connection Required", Toast.LENGTH_SHORT).show();
                    Utility.openInternetAlert(BlogActivity.this);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        blogs = new ArrayList<Blog>();
        adapter = new BlogAdaptor(blogs, this);
        blogRecyclerView.setAdapter(adapter);


        //initializeData();
        if (checkNetwork.isInternetAvailable()) {
            getCategories(this);
            getPaginatedData(this);
        }
        else
        {
           // Toast.makeText(this, "Internet Connection Required", Toast.LENGTH_SHORT).show();
            Utility.openInternetAlert(BlogActivity.this);
        }


        blogRecyclerView.addOnScrollListener(new EndlessScrollListener() {
            @Override
            public void onLoadMore() {
                if (SkipPages != 100) {
                    if (checkNetwork.isInternetAvailable()) {
                        SkipPages++;
                        getPaginatedData(BlogActivity.this);
                    }
                    else
                    {
                      //  Toast.makeText(BlogActivity.this, "Internet Connection Required", Toast.LENGTH_SHORT).show();
                        Utility.openInternetAlert(BlogActivity.this);
                    }
                }
            }
        });
    }

    private void getPaginatedData (  Context context )
    {
        StringRequest stringRequest = new GZipRequest(Request.Method.POST, myUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.i(TAG, "onResponse: "+ response);
                Type listType = new TypeToken<List<Blog>>() {}.getType();
                List<Blog> blogs = (List<Blog>) Utility.parseJSON(response , listType);


               // Collections.sort(blogs , Blog.BlogDateComparator);
                adapter.addAll(blogs);
                progressDialog.dismiss();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                HashMap<String, String> params2 = new HashMap<String, String>();
                params2.put("SkipPages", String.valueOf(SkipPages));
                params2.put("Records", String.valueOf(Records));
                return new JSONObject(params2).toString().getBytes();
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };

        requestQueue.add(stringRequest);
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

    }



    private void getAllData (  Context context )
    {
        StringRequest stringRequest = new GZipRequest(Request.Method.POST, myUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.i(TAG, "onResponse: "+ response);
                Type listType = new TypeToken<List<Blog>>() {}.getType();
                List<Blog> blogs = (List<Blog>) Utility.parseJSON(response, listType);


                adapter.addAll(blogs);
                progressDialog.dismiss();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                HashMap<String, String> params2 = new HashMap<String, String>();
              /*  params2.put("SkipPages", String.valueOf(SkipPages));
                params2.put("Records", String.valueOf(Records));*/
                return new JSONObject(params2).toString().getBytes();
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };

        requestQueue.add(stringRequest);
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

    }




    private void getCategories (final Context context )
    {
        StringRequest stringRequest = new GZipRequest(Request.Method.POST, categoriesUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.i(TAG, "onResponse: "+ response);
                Type listType = new TypeToken<List<BlogCategories>>() {}.getType();
                 blogCategories = (List<BlogCategories>)Utility.parseJSON(response , listType);

                if (blogCategories != null) {
                    categoriesLIst.add("Categories");
                    categoriesLIst.add("All");

                    for (int i = 0; i < blogCategories.size(); i++) {
                        categoriesLIst.add(blogCategories.get(i).getBlogCategoryName());
                    }
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, categoriesLIst);
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spCategories.setAdapter(dataAdapter);

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                HashMap<String, String> params2 = new HashMap<String, String>();
               // params2.put("SkipPages", String.valueOf(SkipPages));
               // params2.put("Records", String.valueOf(Records));
                return new JSONObject(params2).toString().getBytes();
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };

        requestQueue.add(stringRequest);


    }



    private void getFilteredData (final Context context , final String Id )
    {
        StringRequest stringRequest = new GZipRequest(Request.Method.POST, filteredDataUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.i(TAG, "onResponse: "+ response);


                blogs.clear();
                adapter.notifyDataSetChanged();
                Type listType = new TypeToken<List<Blog>>() {}.getType();
                List<Blog> filteredBlogs = (List<Blog>)Utility.parseJSON(response , listType);
                adapter.addAll(filteredBlogs);
                progressDialog.dismiss();


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                HashMap<String, String> params2 = new HashMap<String, String>();
                params2.put("BlogCategoryId", Id);
               // params2.put("Records", String.valueOf(Records));
                return new JSONObject(params2).toString().getBytes();
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };

        requestQueue.add(stringRequest);
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    void setHeader()
    {
        HeaderManager headerManager = new HeaderManager(BlogActivity.this, findViewById(R.id.header));
        headerManager.setMenutype(HeaderManager.BACK_MENU);
        headerManager.setShowFirstImageButton(false);
        headerManager.setShowSecoundImageButton(false);
        headerManager.setTitle("Blogs");
        headerManager.setShowBottomView(true);
        headerManager.setHeaderColor(getResources().getColor(R.color.header_background_color));
    }

}
