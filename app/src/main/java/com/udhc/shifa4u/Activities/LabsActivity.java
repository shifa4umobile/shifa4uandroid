package com.udhc.shifa4u.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.udhc.shifa4u.Adapters.LabsAdapter;
import com.udhc.shifa4u.Interfaces.MenuItemsInterface;
import com.udhc.shifa4u.Models.CartModel;
import com.udhc.shifa4u.Models.LabDetailModel;
import com.udhc.shifa4u.Models.LabTestList;
import com.udhc.shifa4u.Models.LabsModel;
import com.udhc.shifa4u.Models.MenuModel;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.HeaderManager;
import com.udhc.shifa4u.Utilities.MyGeneric;
import com.udhc.shifa4u.Utilities.MyUtils;
import com.udhc.shifa4u.api.RestAPIFactory;

import java.util.ArrayList;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LabsActivity extends AppCompatActivity {

    private ListView lv_labs;
    private ProgressDialog progressDialog;
    int skipPages = 0;
    private LabsAdapter labsAdapter;

    ArrayList<LabTestList> labTestLists = new ArrayList<>();
    private boolean flag_loading = false;
    private EditText txt_search;
    private String searchQuery;
    private TextView txt_no_detail;
    private LinearLayout ll_main;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_labs);
        MyUtils.hideKeyboard(this);

        setHeader();
        lv_labs = (ListView) findViewById(R.id.lv_labs);
        Button img_search = (Button) findViewById(R.id.img_search);
        txt_search = (EditText) findViewById(R.id.txt_search);

        txt_no_detail = (TextView) findViewById(R.id.txt_no_detail);
        ll_main = (LinearLayout) findViewById(R.id.ll_main);

        if (Shifa4U.checkNetwork.isInternetAvailable()) {
            getPaginateLabTests("0");
        } else {
            Toast.makeText(LabsActivity.this, R.string.network_not_available, Toast.LENGTH_SHORT).show();
        }
        img_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Shifa4U.checkNetwork.isInternetAvailable()) {
                    skipPages = 0;
                    labTestLists = new ArrayList<>();
                    lv_labs.setAdapter(null);


                    if (!txt_search.getText().toString().isEmpty()) {
                        searchQuery = txt_search.getText().toString();
                        getSearchlabTestByWord(String.valueOf(skipPages));
                    } else {
                        getPaginateLabTests(String.valueOf(skipPages));
                    }
                } else {
                    Toast.makeText(LabsActivity.this, R.string.network_not_available, Toast.LENGTH_SHORT).show();
                }
            }
        });


        lv_labs.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
                    if (Shifa4U.checkNetwork.isInternetAvailable()) {
                        if (flag_loading == false) {
                            flag_loading = true;

                            if (txt_search.getText().toString().isEmpty()) {
                                skipPages++;
                                getPaginateLabTests(String.valueOf(skipPages));

                            }

                         //   else
                              //  getSearchlabTestByWord(String.valueOf(i));

                        }

                    } else {
                        Toast.makeText(LabsActivity.this, R.string.network_not_available, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });


        lv_labs.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        lv_labs.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });


    }

    void getPaginateLabTests(String page_to_skip) {
        showProgressDialog("Please wait...");
        String str = String.format("{\"SkipPages\":\"%s\",\"Records\":\"10\",\"CityId\":\"%s\"}", String.valueOf(page_to_skip), Shifa4U.mySharePrefrence.getCityId());

        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), str);

        try {
            Call<ResponseBody> posts = RestAPIFactory.getApi().getPaginateLabTests(body);
            posts.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> labTestModel) {
                    dismissProgressDislog();
                    try {

                        try {

//                            String Json = GzipUtils.decompress(labTestModel.body().bytes());
//
//                            Type listType = new TypeToken<LabsModel>() {
//                            }.getType();
//                            LabsModel labsModel = new Gson().fromJson(Json, listType);

                            LabsModel labsModel = MyGeneric.processResponse(labTestModel, new TypeToken<LabsModel>() {
                            });

                            if (labsModel.getLabTestList() != null && labsModel.getLabTestList().size() > 0) {

                                if (labTestLists.size() > 0) {
                                    labTestLists.addAll(labsModel.getLabTestList());
                                    labsAdapter.notifyDataSetChanged();
                                } else {
                                    labTestLists.addAll(labsModel.getLabTestList());
                                    labsAdapter = new LabsAdapter(LabsActivity.this, labTestLists, new MenuItemsInterface() {
                                        @Override
                                        public void onItemClick(Integer position, MenuModel menuModel) {

                                        }

                                        @Override
                                        public void onButtonClick(Integer position, Object model, View button) {
                                            if (button.getId() == R.id.btn_detail) {
                                                Intent i = new Intent(LabsActivity.this, LabDetailActivity.class);
                                                i.putExtra("Item", labTestLists.get(position));
                                                startActivity(i);
                                            } else if (button.getId() == R.id.btn_add_to_cart) {
                                                getLabDetailByID(String.valueOf(labTestLists.get(position).getLabTestId()));
                                            }

                                        }

                                        @Override
                                        public void onItemClick(Integer position, Object o) {

                                        }
                                    });
                                    lv_labs.setAdapter(labsAdapter);
                                }

                                flag_loading = false;
                            } else {
                               /* txt_no_detail.setVisibility(View.VISIBLE);
                                ll_main.setVisibility(View.GONE);*/
                                Toast.makeText(LabsActivity.this, "No Results Found", Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            txt_no_detail.setVisibility(View.VISIBLE);
                            ll_main.setVisibility(View.GONE);
                            e.printStackTrace();
                        }


                    } catch (Exception e) {
                        txt_no_detail.setVisibility(View.VISIBLE);
                        ll_main.setVisibility(View.GONE);
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    txt_no_detail.setVisibility(View.VISIBLE);
                    ll_main.setVisibility(View.GONE);
                    dismissProgressDislog();
                    Log.e("", "");
                }
            });

        } catch (Exception e) {

        }
    }

    void setHeader()
    {
        HeaderManager headerManager = new HeaderManager(LabsActivity.this, findViewById(R.id.header));
        headerManager.setMenutype(HeaderManager.BACK_MENU);
        headerManager.setShowFirstImageButton(false);
        headerManager.setShowSecoundImageButton(false);
        headerManager.setTitle(getString(R.string.labs_title));
        headerManager.setShowBottomView(true);
        headerManager.setHeaderColor(getResources().getColor(R.color.header_background_color));
    }

    @Override
    protected void onResume() {
        super.onResume();
        setHeader();
    }

    void getSearchlabTestByWord(String page_to_skip) {
        showProgressDialog("Please wait...");
        String str;
        if (searchQuery != null && !searchQuery.isEmpty()) {
            str = String.format("{\"SkipPages\":\"%s\",\"Records\":\"10\",\"CityId\":\"%s\",\"SearchKeyWord\":\"%s\"}", String.valueOf(page_to_skip), Shifa4U.mySharePrefrence.getCityId(), searchQuery);

            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), str);

            try {
                Call<ResponseBody> posts = RestAPIFactory.getApi().getSearchlabTestByWord(body);
                posts.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> labTestModel) {
                        dismissProgressDislog();
                        try {

                            try {
                                LabsModel labsModel = MyGeneric.processResponse(labTestModel, new TypeToken<LabsModel>() {
                                });

                                if (labsModel != null && labsModel.getLabTestList().size() > 0) {

                                    if (labTestLists.size() > 0) {
                                        labTestLists.addAll(labsModel.getLabTestList());
                                        labsAdapter.notifyDataSetChanged();
                                    } else {
                                        labTestLists.addAll(labsModel.getLabTestList());
                                        labsAdapter = new LabsAdapter(LabsActivity.this, labTestLists, new MenuItemsInterface() {
                                            @Override
                                            public void onItemClick(Integer position, MenuModel menuModel) {

                                            }

                                            @Override
                                            public void onButtonClick(Integer position, Object model, View button) {
                                                if (button.getId() == R.id.btn_detail) {
                                                    Intent i = new Intent(LabsActivity.this, LabDetailActivity.class);
                                                    i.putExtra("Item", labTestLists.get(position));
                                                    startActivity(i);
                                                } else if (button.getId() == R.id.btn_add_to_cart) {
                                                    getLabDetailByID(String.valueOf(labTestLists.get(position).getLabTestId()));
                                                }

                                            }

                                            @Override
                                            public void onItemClick(Integer position, Object o) {

                                            }
                                        });
                                        lv_labs.setAdapter(labsAdapter);
                                    }

                                    flag_loading = false;
                                } else {
                                   /* txt_no_detail.setVisibility(View.VISIBLE);
                                    ll_main.setVisibility(View.GONE);*/
                                    Toast.makeText(LabsActivity.this, "No Results Found", Toast.LENGTH_SHORT).show();
                                }

                            } catch (Exception e) {
                                txt_no_detail.setVisibility(View.VISIBLE);
                                ll_main.setVisibility(View.GONE);
                                e.printStackTrace();
                            }


                        } catch (Exception e) {
                            txt_no_detail.setVisibility(View.VISIBLE);
                            ll_main.setVisibility(View.GONE);
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        txt_no_detail.setVisibility(View.VISIBLE);
                        ll_main.setVisibility(View.GONE);
                        dismissProgressDislog();
                        Log.e("", "");
                    }
                });

            } catch (Exception e) {

            }
        }
    }


    public void showProgressDialog(String message) {
        if (progressDialog == null)
            progressDialog = new ProgressDialog(LabsActivity.this);
        progressDialog.setMessage(message);
        progressDialog.setCanceledOnTouchOutside(false);
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    public void dismissProgressDislog() {
        if (progressDialog.isShowing() && progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }


    void getLabDetailByID(String labID) {
        showProgressDialog("Please wait...");

        String str = String.format("{\"MedicalProductId\":\"%s\",\"CityId\":\"%s\"}", String.valueOf(labID), Shifa4U.mySharePrefrence.getCityId());


        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), str);
        try {
            Call<ResponseBody> posts = RestAPIFactory.getApi().getLabTestDetailById(body);
            posts.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    dismissProgressDislog();
                    try {

                        ArrayList<LabDetailModel> labDetailModels = MyGeneric.processResponse(response, new TypeToken<ArrayList<LabDetailModel>>() {
                        });

                        if (labDetailModels != null && labDetailModels.size() > 0) {
                            CartModel cartModel = new CartModel();

                            cartModel.setOriginalObject(labDetailModels);
                            cartModel.setProductId(labDetailModels.get(0).getTestId());
                            cartModel.setProductName(labDetailModels.get(0).getTestName());
                            cartModel.setProductShortDescription(labDetailModels.get(0).getShortDescription());
                            cartModel.setProductLineId(String.valueOf(labDetailModels.get(0).getLabCenterProductId()));
                            cartModel.setProductType(CartModel.product.LAB_TEST);

                            MyUtils.addToCart(cartModel);
                            startActivity(new Intent(LabsActivity.this, CheckoutActivity.class));
                            setHeader();

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    dismissProgressDislog();
                    Log.e("", "");
                }
            });

        } catch (Exception e) {

        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }
}
