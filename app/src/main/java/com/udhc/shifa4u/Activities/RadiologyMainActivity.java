package com.udhc.shifa4u.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.udhc.shifa4u.Adapters.RadilogyCategories.ImagingCategoryAdapter;
import com.udhc.shifa4u.Interfaces.MenuItemsInterface;
import com.udhc.shifa4u.Models.MenuModel;
import com.udhc.shifa4u.Models.RadiologyCategories.ImagingCategory;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.HeaderManager;
import com.udhc.shifa4u.Utilities.MyGeneric;
import com.udhc.shifa4u.api.RestAPIFactory;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RadiologyMainActivity extends AppCompatActivity {

    private ProgressDialog progressDialog;
    private GridView grid_radiology_category;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_radiology_main);
        setHeader();
        grid_radiology_category=(GridView)findViewById(R.id.grid_radiology_category);

        if (Shifa4U.checkNetwork.isInternetAvailable()) {
            getAllImagingCategories();
        } else {
            Toast.makeText(RadiologyMainActivity.this, R.string.network_not_available, Toast.LENGTH_SHORT).show();
        }

    }

    private void setHeader() {
        HeaderManager headerManager = new HeaderManager(RadiologyMainActivity.this, findViewById(R.id.header));
        headerManager.setMenutype(HeaderManager.BACK_MENU);
        headerManager.setShowFirstImageButton(false);
        headerManager.setShowSecoundImageButton(false);
        headerManager.setTitle(getString(R.string.radio_logy_title));
        headerManager.setShowBottomView(true);
        headerManager.setHeaderColor(getResources().getColor(R.color.header_background_color));
    }

    void getAllImagingCategories() {
        showProgressDialog("Please wait...");

        try {
            Call<ResponseBody> posts = RestAPIFactory.getApi().getAllImagingCategories();
            posts.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    dismissProgressDislog();
                    try {
                        final ArrayList<ImagingCategory> imagingCategories = MyGeneric.processResponse(response, new TypeToken<ArrayList<ImagingCategory>>() {
                        });
                        if (imagingCategories != null) {
                            grid_radiology_category.setAdapter(new ImagingCategoryAdapter(RadiologyMainActivity.this, imagingCategories, new MenuItemsInterface() {
                                @Override
                                public void onItemClick(Integer position, MenuModel menuModel) {

                                }

                                @Override
                                public void onButtonClick(Integer position, Object model, View button) {



                                }

                                @Override
                                public void onItemClick(Integer position, Object o) {
                                    ImagingCategory imagingCategory = ((ImagingCategory) o);
                                    startActivity(new Intent(RadiologyMainActivity.this,RadioLogyActivity.class).putExtra("categoryID",imagingCategory.getImagingCategoryId()).putExtra("categoryName",imagingCategory.getName()));

                                }
                            }));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    dismissProgressDislog();
                    Log.e("", "");
                }
            });

        } catch (Exception e) {

        }

    }
    public void showProgressDialog(String message) {
        if (progressDialog == null)
            progressDialog = new ProgressDialog(RadiologyMainActivity.this);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    public void dismissProgressDislog() {
        if (progressDialog.isShowing() && progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }


}
