package com.udhc.shifa4u.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.HeaderManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OnlineDoctor extends AppCompatActivity {

    @BindView(R.id.btnProceed_online_doctor)
    Button btnProceedOnlineDoctor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_doctor);
        ButterKnife.bind(this);

        setHeader();
    }

    void setHeader() {
        HeaderManager headerManager = new HeaderManager(OnlineDoctor.this, findViewById(R.id.header));
        headerManager.setMenutype(HeaderManager.BACK_MENU);
        headerManager.setShowFirstImageButton(false);
        headerManager.setShowSecoundImageButton(false);
        headerManager.setTitle("Online Doctor");
        headerManager.setShowBottomView(true);
        headerManager.setHeaderColor(getResources().getColor(R.color.header_background_color));
    }

    @OnClick(R.id.btnProceed_online_doctor)
    public void onViewClicked() {

        startActivity(new Intent(OnlineDoctor.this, WatingRoom.class));
        finish();
    }
}
