package com.udhc.shifa4u.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.udhc.shifa4u.Adapters.LabAdapter;
import com.udhc.shifa4u.Adapters.RadioLogyLabAdapter;
import com.udhc.shifa4u.Adapters.VendorsLocationAdapter;
import com.udhc.shifa4u.Interfaces.MenuItemsInterface;
import com.udhc.shifa4u.Models.CartModel;
import com.udhc.shifa4u.Models.LabDetailModel;
import com.udhc.shifa4u.Models.LabTestList;
import com.udhc.shifa4u.Models.MenuModel;
import com.udhc.shifa4u.Models.RadioLogyDetail.Contact;
import com.udhc.shifa4u.Models.RadioLogyDetail.Location;
import com.udhc.shifa4u.Models.RadioLogyDetail.RadioLogyDetailModel;
import com.udhc.shifa4u.Models.RadioLogyDetail.RadiologyLabsComparison;
import com.udhc.shifa4u.Models.RadioLogyDetail.Vendor;
import com.udhc.shifa4u.Models.Radiology;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.HeaderManager;
import com.udhc.shifa4u.Utilities.MyGeneric;
import com.udhc.shifa4u.Utilities.MyUtils;
import com.udhc.shifa4u.api.RestAPIFactory;

import java.util.ArrayList;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LabDetailActivity extends AppCompatActivity {

    private TextView txt_detail_price;
    private TextView txt_detail_title;
    private TextView txt_detail_description;
    private TextView txt_detail_test_name;
    private TextView txt_detail_aka;
    private TextView txt_detail_sample_type;
    private AutoCompleteTextView txt_detail_labs;
    private ProgressDialog progressDialog;
    private LabTestList labTestList;
    private Radiology radiology;
    private TextView txt_detail_radiology_description;
    private Button btn_order;
    Object selectedLab = null;
    private ArrayList<LabDetailModel> labDetailModels;
    private RadioLogyDetailModel radioLogyDetailModels;
    private HeaderManager headerManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lab_detail);

        setHeader();


        txt_detail_title = (TextView) findViewById(R.id.txt_detail_title);
        txt_detail_description = (TextView) findViewById(R.id.txt_detail_description);
        txt_detail_test_name = (TextView) findViewById(R.id.txt_detail_test_name);
        txt_detail_aka = (TextView) findViewById(R.id.txt_detail_aka);
        txt_detail_sample_type = (TextView) findViewById(R.id.txt_detail_sample_type);
        txt_detail_labs = (AutoCompleteTextView) findViewById(R.id.txt_detail_labs);
        txt_detail_price = (TextView) findViewById(R.id.txt_detail_price);
        txt_detail_radiology_description = (TextView) findViewById(R.id.txt_detail_radiology_description);

        btn_order = (Button) findViewById(R.id.btn_order);

        txt_detail_radiology_description.setVisibility(View.GONE);

        txt_detail_labs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_detail_labs.showDropDown();
            }
        });

        txt_detail_labs.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {


            }
        });

        if (getIntent().getSerializableExtra("Item") != null) {

            if (getIntent().getSerializableExtra("Item") instanceof LabTestList) {
                labTestList = (LabTestList) getIntent().getSerializableExtra("Item");
                if (Shifa4U.checkNetwork.isInternetAvailable()) {
                    getLabDetailByID(String.valueOf(labTestList.getLabTestId()));
                } else {
                    Toast.makeText(LabDetailActivity.this, R.string.network_not_available, Toast.LENGTH_SHORT).show();
                }

                headerManager.setTitle(labTestList.getName());
            } else if (getIntent().getSerializableExtra("Item") instanceof Radiology) {
                radiology = (Radiology) getIntent().getSerializableExtra("Item");
                if (Shifa4U.checkNetwork.isInternetAvailable()) {
                    getRadioLogyDetailByID(String.valueOf(radiology.getImagingId()));
                } else {
                    Toast.makeText(LabDetailActivity.this, R.string.network_not_available, Toast.LENGTH_SHORT).show();
                }

                headerManager.setTitle(radiology.getImagingName());
            }


        }

        btn_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedLab != null) {
                    CartModel cartModel = new CartModel();
                    if (getIntent().getSerializableExtra("Item") instanceof LabTestList) {


                        cartModel.setOriginalObject(labDetailModels);
                        cartModel.setProductId(labDetailModels.get(0).getTestId());
                        cartModel.setProductName(labDetailModels.get(0).getTestName());
                        cartModel.setProductShortDescription(labDetailModels.get(0).getShortDescription());
                        cartModel.setSelectedLabId(((LabDetailModel) selectedLab).getLabId());
                        cartModel.setSelectedLabName(((LabDetailModel) selectedLab).getLabName());
                        cartModel.setPriice(((LabDetailModel) selectedLab).getYourPrice());
                        cartModel.setOriginalPriice(((LabDetailModel) selectedLab).getVendorPrice());
                        cartModel.setProductType(CartModel.product.LAB_TEST);
                        cartModel.setProductLineId(String.valueOf(labDetailModels.get(0).getLabCenterProductId()));
                    }
                    else if (getIntent().getSerializableExtra("Item") instanceof Radiology) {
                        com.udhc.shifa4u.Models.RadioLogyDetail.Radiology _radiology = radioLogyDetailModels.getRadiologies().get(0);
                        cartModel.setOriginalObject(_radiology);
                        cartModel.setProductId(_radiology.getImagingId());
                        cartModel.setProductName(_radiology.getImagingName());
                        cartModel.setProductShortDescription(_radiology.getShortDescription());
                        cartModel.setSelectedLabId(((RadiologyLabsComparison) selectedLab).getLabId());
                        cartModel.setSelectedLabName(((RadiologyLabsComparison) selectedLab).getLabName());
                        cartModel.setPriice(((RadiologyLabsComparison) selectedLab).getYourPrice());
                        cartModel.setOriginalPriice(((RadiologyLabsComparison) selectedLab).getVendorPrice());
                        cartModel.setProductLineId(String.valueOf(((RadiologyLabsComparison) selectedLab).getProductLineId()));
                        setupVendorsLocation(_radiology, ((RadiologyLabsComparison) selectedLab).getLabId(), cartModel);

                        cartModel.setProductType(CartModel.product.RADIOLOGY);


                    }


                    try {
                        MyUtils.addToCart(cartModel);
                        Toast.makeText(LabDetailActivity.this, "Product added to cart", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(LabDetailActivity.this, CheckoutActivity.class));
                        finish();
                    } catch (Exception e) {
                        if (e.getMessage().equalsIgnoreCase("Item Already Exist")) {

                            Toast.makeText(LabDetailActivity.this, "Product added to cart", Toast.LENGTH_SHORT).show();
                        }
                        e.printStackTrace();
                    }
                    if (MyUtils.isLabExistInCart(cartModel)) {
                        showLabExistDialog(LabDetailActivity.this);
                    }
                    setHeader();
                } else {
                    Toast.makeText(LabDetailActivity.this, "Please Select Lab", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }


    public void setupVendorsLocation(com.udhc.shifa4u.Models.RadioLogyDetail.Radiology radiology, Integer labId, CartModel cartModel) {
        ArrayList<Vendor> vendors = (ArrayList<Vendor>) radiology.getVendors();
        Vendor matchedVendor = null;

        if (vendors != null && vendors.size() > 0) {
            for (Vendor vendor : vendors) {
                if (vendor.getLabId() == labId) {
                    matchedVendor = vendor;
                    break;
                }
            }

            if (matchedVendor != null) {

                ArrayList<Location> locations = (ArrayList<Location>) matchedVendor.getLocations();

                if (locations != null && locations.size() > 0) {
                    cartModel.setSelectedContact(locations.get(0).getContacts().get(0));

                }
            }


        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        setHeader();
        if (getIntent().getSerializableExtra("Item") != null) {

            if (getIntent().getSerializableExtra("Item") instanceof LabTestList) {
                labTestList = (LabTestList) getIntent().getSerializableExtra("Item");

                headerManager.setTitle(labTestList.getName());
            } else if (getIntent().getSerializableExtra("Item") instanceof Radiology) {
                radiology = (Radiology) getIntent().getSerializableExtra("Item");

                headerManager.setTitle(radiology.getImagingName());
            }


        }
    }

    private void setHeader() {
        headerManager = new HeaderManager(LabDetailActivity.this, findViewById(R.id.header));
        headerManager.setMenutype(HeaderManager.BACK_MENU);
        headerManager.setShowFirstImageButton(false);
        headerManager.setShowSecoundImageButton(false);

        headerManager.setShowBottomView(true);
        headerManager.setHeaderColor(getResources().getColor(R.color.header_background_color));
    }

    public void showLabExistDialog(Context context) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle("Warning");
        alertDialog.setMessage("Please note that different tests from different labs will require a separate sample for each lab. The home sampling services are provided by individual labs, and ordering different tests from different labs will require each of them to visit separately and draw separate samples");
        alertDialog.setNeutralButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    void getLabDetailByID(String labID) {
        showProgressDialog("Please wait...");

        String str = String.format("{\"MedicalProductId\":\"%s\",\"CityId\":\"%s\"}", String.valueOf(labID), Shifa4U.mySharePrefrence.getCityId());


        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), str);

        //showProgressDialog("Please Wait");
        try {
            Call<ResponseBody> posts = RestAPIFactory.getApi().getLabTestDetailById(body);
            posts.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    dismissProgressDislog();
                    selectedLab = null;
                    try {

//                        ArrayList<LabDetailModel> labDetailModels = (ArrayList<LabDetailModel>) new MyGeneric<ArrayList<LabDetailModel>>().processResponse(response);

//                        String Json = GzipUtils.decompress(response.body().bytes());
//
//                        Type listType = new TypeToken<ArrayList<LabDetailModel>>() {
//                        }.getType();
//                        ArrayList<LabDetailModel> labDetailModels = new Gson().fromJson(Json, listType);

                        labDetailModels = MyGeneric.processResponse(response, new TypeToken<ArrayList<LabDetailModel>>() {
                        });

                        if (labDetailModels != null && labDetailModels.size() > 0) {
                            txt_detail_title.setText(labDetailModels.get(0).getTestName());
                            txt_detail_description.setText(labDetailModels.get(0).getShortDescription());
                            txt_detail_test_name.setText(Html.fromHtml("<font color='#002868'>Test Name :  </font>" + labDetailModels.get(0).getTestName()));
                            txt_detail_aka.setText(Html.fromHtml("<font color='#002868'>Also Known As :  </font>" + labDetailModels.get(0).getOtherNames()));
                            txt_detail_sample_type.setText(Html.fromHtml("<font color='#002868'>Sample Required :  </font>" + labTestList.getSampleType()));
                            txt_detail_labs.setAdapter(new LabAdapter(LabDetailActivity.this, labDetailModels, new MenuItemsInterface() {
                                @Override
                                public void onItemClick(Integer position, MenuModel menuModel) {

                                }

                                @Override
                                public void onButtonClick(Integer position, Object model, View button) {

                                }

                                @Override
                                public void onItemClick(Integer position, Object o) {
                                    txt_detail_labs.dismissDropDown();
                                    txt_detail_labs.setText(((LabDetailModel) o).getLabName());
                                    txt_detail_price.setText(Html.fromHtml("<font color='#002868'>Price :  </font>" + MyUtils.formateCurrency(((LabDetailModel) o).getYourPrice())));
                                    selectedLab = o;
                                }
                            }));

                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }


//
//                ItemModel itemModel = LoganSquare.parse(new GZIPInputStream(response.body().byteStream()), ItemModel.class);

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    dismissProgressDislog();
                    Log.e("", "");
                }
            });

        } catch (Exception e) {

        }

    }


    void getRadioLogyDetailByID(String labID) {
        showProgressDialog("Please wait...");

        String str = String.format("{\"MedicalProductId\":\"%s\",\"CityId\":\"%s\"}", String.valueOf(labID), Shifa4U.mySharePrefrence.getCityId());


        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), str);

        //showProgressDialog("Please Wait");
        try {
            Call<ResponseBody> posts = RestAPIFactory.getApi().getRadiologyDetailById(body);
            posts.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    dismissProgressDislog();
                    selectedLab = null;
                    try {
//                        String Json = GzipUtils.decompress(response.body().bytes());
//
//                        Type listType = new TypeToken<RadioLogyDetailModel>() {}.getType();
//                        RadioLogyDetailModel radioLogyDetailModels = new Gson().fromJson(Json, listType);

                        radioLogyDetailModels = MyGeneric.processResponse(response, new TypeToken<RadioLogyDetailModel>() {
                        });


                        if (radioLogyDetailModels != null) {
                            com.udhc.shifa4u.Models.RadioLogyDetail.Radiology _radiology = radioLogyDetailModels.getRadiologies().get(0);
                            txt_detail_title.setText(radiology.getImagingName());
                            txt_detail_description.setText(Html.fromHtml("<font color='#002868'>Imaging Name :  </font>" + _radiology.getImagingName()));
                            txt_detail_test_name.setText(Html.fromHtml("<font color='#002868'>Category :  </font>" + _radiology.getImagingCategoryName()));
                            txt_detail_aka.setText(Html.fromHtml("<font color='#002868'>Location :  </font>" + _radiology.getBodyLocationName()));
                            txt_detail_sample_type.setText(Html.fromHtml("<font color='#002868'>Sub Location :  </font>" + _radiology.getBodyLocationPartName()));
                            txt_detail_radiology_description.setVisibility(View.VISIBLE);
                            txt_detail_radiology_description.setText(_radiology.getShortDescription());
                            if (_radiology.getRadiologyLabsComparison() != null && _radiology.getRadiologyLabsComparison().size() > 0) {
                                txt_detail_labs.setAdapter(new RadioLogyLabAdapter(LabDetailActivity.this, (ArrayList<RadiologyLabsComparison>) _radiology.getRadiologyLabsComparison(), new MenuItemsInterface() {
                                    @Override
                                    public void onItemClick(Integer position, MenuModel menuModel) {

                                    }

                                    @Override
                                    public void onButtonClick(Integer position, Object model, View button) {

                                    }

                                    @Override
                                    public void onItemClick(Integer position, Object o) {
                                        txt_detail_labs.dismissDropDown();
                                        txt_detail_labs.setText(((RadiologyLabsComparison) o).getLabName());
                                        txt_detail_price.setText(Html.fromHtml("<font color='#002868'>Price :  </font>" + String.valueOf(((RadiologyLabsComparison) o).getYourPrice())));
                                        selectedLab = o;
                                    }
                                }));
                            }


                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }


//
//                ItemModel itemModel = LoganSquare.parse(new GZIPInputStream(response.body().byteStream()), ItemModel.class);

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    dismissProgressDislog();
                    Log.e("", "");
                }
            });

        } catch (Exception e) {

        }

    }


    public void showProgressDialog(String message) {
        if (progressDialog == null)
            progressDialog = new ProgressDialog(LabDetailActivity.this);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    public void dismissProgressDislog() {
        if (progressDialog.isShowing() && progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

}
