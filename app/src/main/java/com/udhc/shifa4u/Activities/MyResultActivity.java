package com.udhc.shifa4u.Activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.DownloadManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import com.squareup.otto.Subscribe;
import com.udhc.shifa4u.Fragments.LabTestResultFragment;
import com.udhc.shifa4u.Fragments.RadiologyResultFragment;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.Events;
import com.udhc.shifa4u.Utilities.GlobalBus;
import com.udhc.shifa4u.Utilities.HeaderManager;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MyResultActivity extends AppCompatActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private DownloadManager downloadManager;
    private long refid;

    HashMap<Integer, String> hashMap = new HashMap<>();

    private Uri Download_Uri;
    public String fileUrl = "https://api.shifa4u.com/api/common/v1/displayfilebyfilename?filename=";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_result2);
       setHeader();

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        registerReceiver(onComplete,
                new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
        GlobalBus.getBus().register(this);

    }

    private void setHeader() {
        HeaderManager headerManager = new HeaderManager(MyResultActivity.this, findViewById(R.id.header));
        headerManager.setMenutype(HeaderManager.BACK_MENU);
        headerManager.setShowFirstImageButton(false);
        headerManager.setShowSecoundImageButton(false);
        headerManager.setTitle(getString(R.string.my_results_title));
        headerManager.setHeaderColor(getResources().getColor(R.color.header_background_color));
    }

    @Override
    protected void onResume() {
        super.onResume();
        setHeader();
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new LabTestResultFragment(), "Lab Test result");
        adapter.addFragment(new RadiologyResultFragment(), "Radiology Result");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }
        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }


    @Subscribe
    @TargetApi(Build.VERSION_CODES.M)
    public  void downLoadResultFile (Events.DownloadResultFile uriPaths)
    {
        if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            // Log.v("", "Permission is granted");
            ActivityCompat.requestPermissions(MyResultActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
            //File write logic here
        }
        else {

            Download_Uri = Uri.parse(fileUrl+uriPaths.getMessage());
            DownloadManager.Request requestDM = new DownloadManager.Request(Download_Uri);
            requestDM.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
            requestDM.setAllowedOverRoaming(false);
            requestDM.setTitle("Downloading " + uriPaths.getMessage());
            requestDM.setDescription("Downloading " +uriPaths.getMessage() );
            requestDM.setVisibleInDownloadsUi(true);
            requestDM.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "/Shifa4u/" + "/" + uriPaths.getMessage());
            refid = downloadManager.enqueue(requestDM);

            hashMap.put(( int)refid , uriPaths.getMessage());
        }
    }

      BroadcastReceiver onComplete = new BroadcastReceiver() {

        public void onReceive(Context ctxt, Intent intent) {
            long referenceId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
            Log.e("IN", "" + referenceId);
            Log.e("INSIDE", "" + referenceId);

            if (refid == referenceId) {
                String fileName = hashMap.get((int)referenceId);
                NotificationCompat.Builder mBuilder =
                        new NotificationCompat.Builder(MyResultActivity.this, "notify_001");
             /*   Intent ii= new Intent();
                intent.setAction(android.content.Intent.ACTION_VIEW);*/

                File pdfFile= new File( Environment.getExternalStorageDirectory()+File.separator+"Download" +File.separator+"Shifa4u" +File.separator+fileName );
                Intent objIntent = new Intent(Intent.ACTION_VIEW);

                if (pdfFile.exists()) //Checking if the file exists or not
                {
                    MimeTypeMap mime = MimeTypeMap.getSingleton();
                    String ext=pdfFile.getName().substring(pdfFile.getName().lastIndexOf(".")+1);
                    String type = mime.getMimeTypeFromExtension(ext);
                    Uri photoURI = FileProvider.getUriForFile(MyResultActivity.this, getApplicationContext().getPackageName() + ".my.package.name.provider", pdfFile);
                    assert type != null;
                    if (type.contains("pdf")) {
                        objIntent.setDataAndType(photoURI, "application/pdf");
                    }
                    else
                    {
                        objIntent.setDataAndType(photoURI, "image/*");
                    }
                    objIntent.setFlags(Intent. FLAG_ACTIVITY_CLEAR_TOP);
                    grantAllUriPermissions(MyResultActivity.this, objIntent, photoURI);
                  //  startActivity(objIntent);//Starting the pdf viewer
                } else {

                    Toast.makeText(MyResultActivity.this, "The file not exists! ", Toast.LENGTH_SHORT).show();

                }

              //  Environment.DIRECTORY_DOWNLOADS, "/Shifa4u/" + "/" + uriPaths.getMessage()


                PendingIntent pendingIntent = PendingIntent.getActivity(MyResultActivity.this, 0,objIntent , 0);

                NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
                bigText.bigText("");
                bigText.setBigContentTitle("Download Complete");
                bigText.setSummaryText("Download Complete");

                mBuilder.setContentIntent(pendingIntent);
                mBuilder.setSmallIcon(R.drawable.ic_stat_name);
               /* if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    mBuilder.setColor(getResources().getColor(R.color.darkblue));
                }*/
                mBuilder.setLargeIcon(BitmapFactory.decodeResource(getResources(),
                        R.drawable.download_icon));
                mBuilder.setContentTitle("Download Complete");
                mBuilder.setContentText(fileName + " Downloaded");
                mBuilder.setPriority(Notification.PRIORITY_MAX);
                mBuilder.setStyle(bigText);
                NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    String channelId = "YOUR_CHANNEL_ID";
                    NotificationChannel channel = new NotificationChannel(channelId,
                            "Channel human readable title",
                            NotificationManager.IMPORTANCE_DEFAULT);
                    mNotificationManager.createNotificationChannel(channel);
                    mBuilder.setChannelId(channelId);
                }
                mNotificationManager.notify(((int) referenceId), mBuilder.build());
            }
        }
    };


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(onComplete);
        GlobalBus.getBus().unregister(this);
    }

    private void grantAllUriPermissions(Context context, Intent intent, Uri uri) {
        List<ResolveInfo> resInfoList = context.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        for (ResolveInfo resolveInfo : resInfoList) {
            String packageName = resolveInfo.activityInfo.packageName;
            context.grantUriPermission(packageName, uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
    }
}
