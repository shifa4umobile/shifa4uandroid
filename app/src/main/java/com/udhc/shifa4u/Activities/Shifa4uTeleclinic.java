package com.udhc.shifa4u.Activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.github.aakira.expandablelayout.ExpandableLinearLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.udhc.shifa4u.Models.MeetingConnectionResponse;
import com.udhc.shifa4u.Models.OnlineDoctorMeetingModel;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.HeaderManager;
import com.udhc.shifa4u.Utilities.MyGeneric;
import com.udhc.shifa4u.Utilities.Utility;
import com.udhc.shifa4u.api.RestAPIFactory;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Shifa4uTeleclinic extends AppCompatActivity {


    @BindView(R.id.btnWhatIsShifa4u)
    RelativeLayout btnWhatIsShifa4u;
    @BindView(R.id.buttonHowItWorks)
    RelativeLayout buttonHowItWorks;
    @BindView(R.id.btnHowItWorks)
    RelativeLayout btnHowItWorks;
    @BindView(R.id.buttonPricing)
    RelativeLayout buttonPricing;
    @BindView(R.id.lvPricing)
    ExpandableLinearLayout lvPricing;
    @BindView(R.id.btnPricing)
    RelativeLayout btnPricing;
    @BindView(R.id.buttonWhatIsShifa4u)
    RelativeLayout buttonWhatIsShifa4u;
    @BindView(R.id.lvWhatIsShifa4u)
    ExpandableLinearLayout lvWhatIsShifa4u;
    @BindView(R.id.lvHowItWorks)
    ExpandableLinearLayout lvHowItWorks;

    AutoCompleteTextView textTemprature;
    AutoCompleteTextView textHeartRate;

    List<String> tempratureList = new ArrayList<>();
    List<String> heartBeatList = new ArrayList<>();
    @BindView(R.id.connectToDoctor)
    Button connectToDoctor;
    @BindView(R.id.textDiastolic)
    AutoCompleteTextView textDiastolic;
    @BindView(R.id.textSystolic)
    AutoCompleteTextView textSystolic;
    @BindView(R.id.edComplaint)
    EditText edComplaint;

    OnlineDoctorMeetingModel onlineDoctorMeetingModel = new OnlineDoctorMeetingModel();


    private ProgressDialog progressDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shifa4u_teleclinic);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        for (int i = 90; i <= 110; i++) {
            tempratureList.add(String.valueOf(i));
        }

        for (int i = 0; i <= 200; i++) {
            heartBeatList.add(String.valueOf(i));
        }

      /*  txtTemprature = (AutoCompleteTextView) findViewById(R.id.txt_gender);
        textHeartBeat = (AutoCompleteTextView) findViewById(R.id.textHeartRate);*/
        ButterKnife.bind(this);
        setHeader();
        init();


        ArrayAdapter<String> tempratureAdaptor = new ArrayAdapter<String>(Shifa4uTeleclinic.this, R.layout.spinner_item, tempratureList);

        textTemprature.setAdapter(tempratureAdaptor);

        ArrayAdapter<String> heartBeatAdaptor = new ArrayAdapter<String>(Shifa4uTeleclinic.this, R.layout.spinner_item, heartBeatList);

        textHeartRate.setAdapter(heartBeatAdaptor);


        textTemprature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textTemprature.showDropDown();
            }
        });

        textHeartRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textHeartRate.showDropDown();
            }
        });
    }


    private void init() {
        Utility.setClick(btnWhatIsShifa4u, lvWhatIsShifa4u);
        Utility.setRotateAnimator(lvWhatIsShifa4u, buttonWhatIsShifa4u);

        Utility.setClick(btnHowItWorks, lvHowItWorks);
        Utility.setRotateAnimator(lvHowItWorks, buttonHowItWorks);

        Utility.setClick(btnPricing, lvPricing);
        Utility.setRotateAnimator(lvPricing, buttonPricing);

        textTemprature = findViewById(R.id.text_temprature);
        textHeartRate = findViewById(R.id.text_heart_rate);
    }


    void setHeader() {
        HeaderManager headerManager = new HeaderManager(Shifa4uTeleclinic.this, findViewById(R.id.header));
        headerManager.setMenutype(HeaderManager.BACK_MENU);
        headerManager.setShowFirstImageButton(false);
        headerManager.setShowSecoundImageButton(false);
        headerManager.setTitle("Shifa4u Telecllinic");
        headerManager.setShowBottomView(true);
        headerManager.setHeaderColor(getResources().getColor(R.color.header_background_color));
    }

    @OnClick(R.id.connectToDoctor)
    public void onViewClicked() {

        if (Shifa4U.mySharePrefrence.getLogin() != null) {
            if (!edComplaint.getText().toString().trim().isEmpty()) {

                onlineDoctorMeetingModel.setReasonForCall(edComplaint.getText().toString());
                onlineDoctorMeetingModel.setTemperature(textTemprature.getText().toString());
                onlineDoctorMeetingModel.setHeartRate(textHeartRate.getText().toString());
                onlineDoctorMeetingModel.setSystolic(textSystolic.getText().toString());
                onlineDoctorMeetingModel.setDiastolic(textDiastolic.getText().toString());

                getMeetingData();
            }
            else
            {
                Toast.makeText(this, "Please enter your reason for call", Toast.LENGTH_SHORT).show();
            }
        }
        else
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(Shifa4uTeleclinic.this);
            builder.setMessage("Please login in-order to check out")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            Intent myIntent = new Intent(Shifa4uTeleclinic.this, LoginActivity.class);
                            myIntent.putExtra("From", "Shifa4uTeleclinic");
                          //  myIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                          //  Shifa4U.mySharePrefrence.addAccountActivityNames("Shifa4uTeleclinic");
                            // ((MainActivity)getActivity()).addmyAccountActivityName("pharmacy");
                            startActivity(myIntent);
                            //  getActivity(). finish();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
        }



    }


    void getMeetingData() {

        showProgressDialog("Please wait...");
        Gson gson = new Gson();
        Type type = new TypeToken<OnlineDoctorMeetingModel>() {
        }.getType();
        String json = gson.toJson(onlineDoctorMeetingModel, type);

        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

        //showProgressDialog("Please Wait");
        try {
            Call<ResponseBody> posts = RestAPIFactory.getApi().meetingConnection( Shifa4U.mySharePrefrence.getLogin().getTokenType() + " " + Shifa4U.mySharePrefrence.getLogin().getAccessToken() , body);
            posts.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.code() == 200) {

                        MeetingConnectionResponse meetingConnectionResponse = MyGeneric.processResponse(response,new TypeToken<MeetingConnectionResponse>(){});

                        Shifa4U.mySharePrefrence.setMeetingResponse(meetingConnectionResponse);

                        startActivity(new Intent(Shifa4uTeleclinic.this, OnlineDoctor.class));


                    } else {
                        Toast.makeText(Shifa4uTeleclinic.this, "Something went wrong.\n Please try again later", Toast.LENGTH_LONG).show();
                    }

                    dismissProgressDislog();
                    // dismiss();
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            dismissProgressDislog();
        }
    }


    public void showProgressDialog(String message) {
        if (progressDialog == null)
            progressDialog = new ProgressDialog(Shifa4uTeleclinic.this);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    public void dismissProgressDislog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }
}
