package com.udhc.shifa4u.Activities;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.udhc.shifa4u.Models.Addresses;
import com.udhc.shifa4u.Models.AmericanTeleclinic.Physician;
import com.udhc.shifa4u.Models.CartModel;
import com.udhc.shifa4u.Models.ContactNumbers;
import com.udhc.shifa4u.Models.CustomerProfileModel;
import com.udhc.shifa4u.Models.ProfileModel;
import com.udhc.shifa4u.Models.RadioLogyDetail.Contact;
import com.udhc.shifa4u.Models.UserFamilyMembers;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.HeaderManager;
import com.udhc.shifa4u.Utilities.MyGeneric;
import com.udhc.shifa4u.Utilities.MyUtils;
import com.udhc.shifa4u.Utilities.Utility;
import com.udhc.shifa4u.api.RestAPIFactory;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderPreviewActivity extends AppCompatActivity {

    @BindView(R.id.btnAddPatient)
    ImageView btnAddPatient;
    @BindView(R.id.btnAddAddress)
    ImageView btnAddAddress;
    @BindView(R.id.btnAddPhoneNumber)
    ImageView btnAddPhoneNumber;
    private LinearLayout ll_lab_test_main;
    private LinearLayout ll_lab_test_container;
    private LinearLayout ll_radiology_main;
    private LinearLayout ll_radiology_container;
    private LinearLayout ll_home_care_main;
    private LinearLayout ll_home_care_container;
    private LinearLayout ll_medical_package_main;
    private LinearLayout ll_medical_package_container;
    private TextView txt_total;
    private HeaderManager headerManager;
    private Button btn_previous;
    private Button btn_next;
    private AutoCompleteTextView edAddressInOrderPreview;
    private AutoCompleteTextView edPatientName;
    private AutoCompleteTextView edPhoneInOrderPreview;

    List <ContactNumbers> contactNumbers;

    boolean isSecondTime = false;
    List <Addresses> addresses;

    List<String> localaddresses = new ArrayList<>();
    List<String> names = new ArrayList<>();

    List<String> phoneNumbers = new ArrayList<>();

    ArrayAdapter<String> addressAdaptor;
    ArrayAdapter<String> nameAdaptor;
    ArrayAdapter<String> phoneNoAdaptor;

    String addressContactId = "";
    String phoneNoContactId = "";

    private ProgressDialog progressDialog = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_preview);
        ButterKnife.bind(this);


        setHeader();
        ll_lab_test_main = (LinearLayout) findViewById(R.id.ll_lab_test_main);
        ll_lab_test_container = (LinearLayout) findViewById(R.id.ll_lab_test_container);
        edAddressInOrderPreview = (AutoCompleteTextView) findViewById(R.id.edAddressInOrderPreview);
        edPatientName = (AutoCompleteTextView) findViewById(R.id.edPatientName);
        edPhoneInOrderPreview = (AutoCompleteTextView) findViewById(R.id.edPhoneInOrderPreview);


        edPatientName.setText(Shifa4U.mySharePrefrence.getProfile().getFullName());
        edAddressInOrderPreview.setText(Shifa4U.mySharePrefrence.getProfile().getAddress());
        edPhoneInOrderPreview.setText(Shifa4U.mySharePrefrence.getProfile().getMobileNo());
     /*   setPhoneAdaptor();
        setAddressAdaptor();
        setPatientNameAdaptor();*/



        ll_radiology_main = (LinearLayout) findViewById(R.id.ll_radiology_main);
        ll_radiology_container = (LinearLayout) findViewById(R.id.ll_radiology_container);

        ll_home_care_main = (LinearLayout) findViewById(R.id.ll_home_care_main);
        ll_home_care_container = (LinearLayout) findViewById(R.id.ll_home_care_container);

        ll_medical_package_main = (LinearLayout) findViewById(R.id.ll_medical_package_main);
        ll_medical_package_container = (LinearLayout) findViewById(R.id.ll_medical_package_container);

        btn_previous = (Button) findViewById(R.id.btn_previous);
        btn_next = (Button) findViewById(R.id.btn_next);




        btnAddPatient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomPatientInfoDialogue cdd = new CustomPatientInfoDialogue(OrderPreviewActivity.this);
                cdd.show();
                Window window = cdd.getWindow();
                window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            }
        });

        btnAddAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomPatientAddressDialogue cdd = new CustomPatientAddressDialogue(OrderPreviewActivity.this);
                cdd.show();
                Window window = cdd.getWindow();
                window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

            }
        });

        btnAddPhoneNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CustomPatientPhoneNoDialogue cdd = new CustomPatientPhoneNoDialogue(OrderPreviewActivity.this);
                cdd.show();
                Window window = cdd.getWindow();
                window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

            }
        });

        btn_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Shifa4U.mySharePrefrence.getCart() != null && Shifa4U.mySharePrefrence.getCart().size() > 0) {
                    Intent intent = new Intent(new Intent(OrderPreviewActivity.this, PaymentModeActivity.class));
                    intent.putExtra("AddressContactId", addressContactId);
                    intent.putExtra("PhoneNoContactId", phoneNoContactId);
                    startActivity(intent);
                }
            }
        });


        txt_total = (TextView) findViewById(R.id.txt_total);



        getUserFamilyMembers();
        getUserAddresses();
        getUserContactNumbers();



    }


    @Override
    protected void onResume() {
        super.onResume();
        populateCart();
    }

    private void setHeader() {
        headerManager = new HeaderManager(OrderPreviewActivity.this, findViewById(R.id.header));
        headerManager.setMenutype(HeaderManager.BACK_MENU);
        headerManager.setShowFirstImageButton(false);
        headerManager.setShowSecoundImageButton(false);
        headerManager.setTitle(getString(R.string.order_preview_title));

        headerManager.setShowBottomView(true);
        headerManager.setHeaderColor(getResources().getColor(R.color.header_background_color));
    }

    public void populateCart() {

        ArrayList<CartModel> cartModels = Shifa4U.mySharePrefrence.getCart();
        if (cartModels != null && cartModels.size() > 0) {

            ll_lab_test_main.setVisibility(View.GONE);
            ll_radiology_main.setVisibility(View.GONE);
            ll_home_care_main.setVisibility(View.GONE);
            ll_medical_package_main.setVisibility(View.GONE);


            ll_lab_test_container.removeAllViews();
            ll_radiology_container.removeAllViews();
            ll_home_care_container.removeAllViews();
            ll_medical_package_container.removeAllViews();
            Double total = 0.0;
            for (int i = 0; i < cartModels.size(); i++) {
                if (cartModels.get(i).getProductType() == CartModel.product.LAB_TEST) {
                    ll_lab_test_main.setVisibility(View.VISIBLE);
                    ll_lab_test_container.addView(populateLabTest(cartModels.get(i)));
                } else if (cartModels.get(i).getProductType() == CartModel.product.RADIOLOGY) {
                    ll_radiology_main.setVisibility(View.VISIBLE);
                    ll_radiology_container.addView(populateRadioLogy(cartModels.get(i)));
                } else if (cartModels.get(i).getProductType() == CartModel.product.HOME_CARE) {
                    ll_home_care_main.setVisibility(View.VISIBLE);
                    ll_home_care_container.addView(populateHomeCare(cartModels.get(i)));
                } else if (cartModels.get(i).getProductType() == CartModel.product.MEDICAL_PACKAGE) {
                    ll_medical_package_main.setVisibility(View.VISIBLE);
                    ll_medical_package_container.addView(populateMedicalPackage(cartModels.get(i)));
                }


                if (cartModels.get(i).getPriice() != null) {
                    try {
                        total += cartModels.get(i).getPriice();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
            txt_total.setText(Html.fromHtml("<font color='#808080'>Grand Total:  </font><font color='#000000'>Rs. " + MyUtils.formateCurrency(total) + "</font>"));
        } else {
            ll_lab_test_main.setVisibility(View.GONE);
            ll_radiology_main.setVisibility(View.GONE);
            ll_home_care_main.setVisibility(View.GONE);
            ll_medical_package_main.setVisibility(View.GONE);
            txt_total.setText("00.00");
        }
    }


    public View populateLabTest(final CartModel cartModel) {

        final View v = LayoutInflater.from(OrderPreviewActivity.this).inflate(R.layout.cart_preview_item, null);
        v.setTag(cartModel);
        TextView txt_cart_preview_title = (TextView) v.findViewById(R.id.txt_cart_preview_title);
        TextView txt_cart_preview_lab = (TextView) v.findViewById(R.id.txt_cart_preview_lab);
        TextView txt_collection_method = (TextView) v.findViewById(R.id.txt_collection_method);
        TextView txt_price = (TextView) v.findViewById(R.id.txt_price);

        txt_cart_preview_title.setText(cartModel.getProductName());
        txt_cart_preview_lab.setText(cartModel.getSelectedLabName());
        if (cartModel.getPriice() != null) {

            txt_price.setText(Html.fromHtml("<font color='#000000'>Rs. </font><font color='#000000'>" + MyUtils.formateCurrency(cartModel.getPriice()) + "</font>"));
        } else {
            txt_price.setText(Html.fromHtml("<font color='#000000'>Rs. </font><font color='#000000'>00.00</font>"));
        }

        txt_collection_method.setText(cartModel.getCollectionType());

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, 10, 0, 10);
        v.setLayoutParams(layoutParams);
        return v;
    }


    public View populateRadioLogy(final CartModel cartModel) {

        final View v = LayoutInflater.from(OrderPreviewActivity.this).inflate(R.layout.cart_preview_item, null);
        v.setTag(cartModel);
        TextView txt_cart_preview_title = (TextView) v.findViewById(R.id.txt_cart_preview_title);
        TextView txt_cart_preview_lab = (TextView) v.findViewById(R.id.txt_cart_preview_lab);
        TextView txt_collection_method = (TextView) v.findViewById(R.id.txt_collection_method);
        TextView txt_price = (TextView) v.findViewById(R.id.txt_price);

        txt_cart_preview_title.setText(cartModel.getProductName());
        txt_cart_preview_lab.setText(cartModel.getSelectedLabName());
        if (cartModel.getPriice() != null) {

            txt_price.setText(Html.fromHtml("<font color='#000000'>Rs. </font><font color='#000000'>" + MyUtils.formateCurrency(cartModel.getPriice()) + "</font>"));
        } else {
            txt_price.setText(Html.fromHtml("<font color='#000000'>Rs. </font><font color='#000000'>00.00</font>"));
        }

        txt_collection_method.setText(cartModel.getCollectionType());

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, 10, 0, 10);
        v.setLayoutParams(layoutParams);
        return v;
    }


    public View populateMedicalPackage(final CartModel cartModel) {

        final View v = LayoutInflater.from(OrderPreviewActivity.this).inflate(R.layout.cart_preview_item, null);
        v.setTag(cartModel);
        TextView txt_cart_preview_title = (TextView) v.findViewById(R.id.txt_cart_preview_title);
        TextView txt_cart_preview_lab = (TextView) v.findViewById(R.id.txt_cart_preview_lab);
        TextView txt_collection_method = (TextView) v.findViewById(R.id.txt_collection_method);
        TextView txt_price = (TextView) v.findViewById(R.id.txt_price);

        txt_cart_preview_title.setText(cartModel.getProductName());
        txt_cart_preview_lab.setText(cartModel.getSelectedLabName());
        if (cartModel.getPriice() != null) {

            txt_price.setText(Html.fromHtml("<font color='#000000'>Rs. </font><font color='#000000'>" + MyUtils.formateCurrency(cartModel.getPriice()) + "</font>"));
        } else {
            txt_price.setText(Html.fromHtml("<font color='#000000'>Rs. </font><font color='#000000'>00.00</font>"));
        }

        txt_collection_method.setText(cartModel.getCollectionType());

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, 10, 0, 10);
        v.setLayoutParams(layoutParams);
        return v;
    }


    public View populateHomeCare(final CartModel cartModel) {

        final View v = LayoutInflater.from(OrderPreviewActivity.this).inflate(R.layout.cart_preview_item, null);
        v.setTag(cartModel);
        TextView txt_cart_preview_title = (TextView) v.findViewById(R.id.txt_cart_preview_title);
        TextView txt_cart_preview_lab = (TextView) v.findViewById(R.id.txt_cart_preview_lab);
        TextView txt_collection_method = (TextView) v.findViewById(R.id.txt_collection_method);
        TextView txt_price = (TextView) v.findViewById(R.id.txt_price);

        txt_cart_preview_title.setText(cartModel.getProductName());
        txt_cart_preview_lab.setVisibility(View.GONE);
        if (cartModel.getPriice() != null) {

            txt_price.setText(Html.fromHtml("<font color='#000000'>Rs. </font><font color='#000000'>" + MyUtils.formateCurrency(cartModel.getPriice()) + "</font>"));
        } else {
            txt_price.setText(Html.fromHtml("<font color='#000000'>Rs. </font><font color='#000000'>00.00</font>"));
        }

        txt_collection_method.setVisibility(View.GONE);


        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, 10, 0, 10);
        v.setLayoutParams(layoutParams);


        return v;
    }


    public void addAddress (String address)
    {

        localaddresses.add(address);
        addressAdaptor.notifyDataSetChanged();

      //  String Address
    }

    public void addName (String name)
    {

        names.add(name);
        nameAdaptor.notifyDataSetChanged();

      //  String Address
    }

    public void addPhoneNo (String phoneNo)
    {

        phoneNumbers.add(phoneNo);
        phoneNoAdaptor.notifyDataSetChanged();

      //  String Address
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }



    public class CustomPatientAddressDialogue extends Dialog implements
            View.OnClickListener {

        public Activity c;
        public Dialog d;
        Button btnDone;

        private HeaderManager headerManager;

        private AutoCompleteTextView txt_address;


        public CustomPatientAddressDialogue(Activity a) {
            super(a);
            // TODO Auto-generated constructor stub
            this.c = a;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.patient_address);

            setHeader();

            txt_address = (AutoCompleteTextView) findViewById(R.id.edAddress);
            btnDone = (Button) findViewById(R.id.btn_done);


            btnDone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!txt_address.getText().toString().equals("")) {
                        submitAddress();

                        dismiss();
                    }
                    else
                    {
                        Toast.makeText(c, "Address can not be empty", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }


        @Override
        public void onClick(View v) {

            dismiss();
        }

        void submitAddress() {

            Addresses addresses = new Addresses();
            addresses.setDescription(txt_address.getText().toString());
            showProgressDialog("Please wait...");
            Gson gson = new Gson();
            Type type = new TypeToken<Addresses>() {
            }.getType();
            String json = gson.toJson(addresses, type);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

            //showProgressDialog("Please Wait");
            try {
                Call<ResponseBody> posts = RestAPIFactory.getApi().saveuseraddresses( Shifa4U.mySharePrefrence.getLogin().getTokenType() + " " + Shifa4U.mySharePrefrence.getLogin().getAccessToken() , body);
                posts.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.code() == 200) {

                            Toast.makeText(OrderPreviewActivity.this, "Profile updated successfully", Toast.LENGTH_LONG).show();
                            getUserAddresses();


                        } else {
                            Toast.makeText(OrderPreviewActivity.this, "Something went wrong.\n Please try again later", Toast.LENGTH_LONG).show();
                        }

                        dismissProgressDislog();
                       // dismiss();
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        dismissProgressDislog();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        private void setHeader() {
            headerManager = new HeaderManager(c, findViewById(R.id.header));
            headerManager.setMenutype(HeaderManager.BACK_MENU);
            headerManager.setShowFirstImageButton(false);
            headerManager.setShowSecoundImageButton(false);
            headerManager.setShowSecoundImageButton(false);
            headerManager.setTitle("Add Address");
            headerManager.setBackMenuButtonVisibility(false);
            headerManager.setShowBottomView(false);
            headerManager.setHeaderColor(c.getResources().getColor(R.color.header_background_color));
        }

        private void Save ()
        {
            addAddress(txt_address.getText().toString());
        }
    }



    public class CustomPatientInfoDialogue extends Dialog implements
            View.OnClickListener {

        public Activity c;
        public Dialog d;
        public Button yes, no;
        @BindView(R.id.edPatientName)
        AutoCompleteTextView edPatientName;
        @BindView(R.id.edDob)
        EditText edDob;
        @BindView(R.id.edGender)
        AutoCompleteTextView edGender;
        @BindView(R.id.edRelationship)
        AutoCompleteTextView edRelationship;
        Button btnDone;
        private HeaderManager headerManager;

        private AutoCompleteTextView txt_gender;
        private AutoCompleteTextView txt_Relationship;

        private EditText txt_dob;

        String[] gender = new String[]{"Male", "Female"};

        String[] relationships = new String[]{"Mother", "Father", "Wife", "Son", "Daughter", "Brother", "Sister"};

        public CustomPatientInfoDialogue(Activity a) {
            super(a);
            // TODO Auto-generated constructor stub
            this.c = a;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.pateint_info);

            setHeader();

            txt_gender = (AutoCompleteTextView) findViewById(R.id.edGender);
            txt_dob = (EditText) findViewById(R.id.edDob);
            btnDone = (Button) findViewById(R.id.btn_done);
            txt_Relationship = (AutoCompleteTextView) findViewById(R.id.edRelationship);
            edPatientName = (AutoCompleteTextView) findViewById(R.id.edPatientName);

            ArrayAdapter<String> relationshipAdaptor = new ArrayAdapter<String>(c, R.layout.spinner_item, Arrays.asList(relationships));
            txt_Relationship.setAdapter(relationshipAdaptor);
            txt_Relationship.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    txt_Relationship.showDropDown();
                }
            });

            txt_dob.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDatePicker(c);
                }
            });

            btnDone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!edPatientName.getText().toString().trim().equals("")) {
                        if (!txt_dob.getText().toString().trim().equals("")) {
                            if (!txt_gender.getText().toString().trim().equals("")) {
                                if (!txt_Relationship.getText().toString().trim().equals("")) {

                                    submitPatientInfo();
                                 /*   final Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            // Do something after 5s = 5000ms
                                            getUserFamilyMembers();
                                        }
                                    }, 5000);*/

                                    dismiss();
                                }
                                else
                                {
                                    Toast.makeText(c, "Please select Relationship", Toast.LENGTH_SHORT).show();
                                }
                            }
                            else
                            {
                                Toast.makeText(c, "Please select gender", Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Toast.makeText(c, "Please select Date of Birth", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        Toast.makeText(c, "Patient Name can not be empty", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            ArrayAdapter<String> genderAdapter = new ArrayAdapter<String>(c, R.layout.spinner_item, Arrays.asList(gender));
            txt_gender.setAdapter(genderAdapter);
            txt_gender.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    txt_gender.showDropDown();
                }
            });
        }

        @TargetApi(Build.VERSION_CODES.HONEYCOMB)
        public void showDatePicker(Context context) {
            // Get Current Date
            final Calendar c = Calendar.getInstance();
            int mYear = c.get(Calendar.YEAR);
            int mMonth = c.get(Calendar.MONTH);
            int mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            txt_dob.setText((monthOfYear + 1) + "/" + dayOfMonth + "/" + year);

                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        }

        @Override
        public void onClick(View v) {

            dismiss();
        }


        void submitPatientInfo() {
            String relationshipPrefix = "";

            UserFamilyMembers userFamilyMembers = new UserFamilyMembers();
            userFamilyMembers.setName(edPatientName.getText().toString());
            userFamilyMembers.setDateofBirth(txt_dob.getText().toString());
            userFamilyMembers.setGender(txt_gender.getText().toString());
            if (txt_Relationship.getText().toString().equals("Mother"))
            {
                relationshipPrefix = "MTH";
            }
            else if (txt_Relationship.getText().toString().equals("Father"))
            {
                relationshipPrefix = "FTH";
            }

            else if (txt_Relationship.getText().toString().equals("Wife"))
            {
                relationshipPrefix = "WWF";
            }

            else if (txt_Relationship.getText().toString().equals("Son"))
            {
                relationshipPrefix = "SON";
            }

            else if (txt_Relationship.getText().toString().equals("Daughter"))
            {
                relationshipPrefix = "DTR";
            }

            else if (txt_Relationship.getText().toString().equals("Brother"))
            {
                relationshipPrefix = "BTR";
            }


            else if (txt_Relationship.getText().toString().equals("Sister"))
            {
                relationshipPrefix = "SSR";
            }
            userFamilyMembers.setRelationshipPrefix(relationshipPrefix);
            showProgressDialog("Please wait...");
            Gson gson = new Gson();
            Type type = new TypeToken<UserFamilyMembers>() {
            }.getType();
            String json = gson.toJson(userFamilyMembers, type);

            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

            //showProgressDialog("Please Wait");
            try {
                Call<ResponseBody> posts = RestAPIFactory.getApi().saveuserfamilymembers( Shifa4U.mySharePrefrence.getLogin().getTokenType() + " " + Shifa4U.mySharePrefrence.getLogin().getAccessToken() , body);
                posts.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.code() == 200) {

                            Toast.makeText(OrderPreviewActivity.this, "Profile updated successfully", Toast.LENGTH_LONG).show();
                            getUserFamilyMembers();

                        } else {
                            Toast.makeText(OrderPreviewActivity.this, "Something went wrong.\n Please try again later", Toast.LENGTH_LONG).show();
                        }

                        dismissProgressDislog();
                        // dismiss();
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        dismissProgressDislog();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void setHeader() {
            headerManager = new HeaderManager(c, findViewById(R.id.header));
            headerManager.setMenutype(HeaderManager.BACK_MENU);
            headerManager.setShowFirstImageButton(false);
            headerManager.setShowSecoundImageButton(false);
            headerManager.setShowSecoundImageButton(false);
            headerManager.setTitle("Add Famility Details");
            headerManager.setBackMenuButtonVisibility(false);

            headerManager.setShowBottomView(false);
            headerManager.setHeaderColor(c.getResources().getColor(R.color.header_background_color));
        }


        private void Save ()
        {
           addName(edPatientName.getText().toString() + " (" + txt_Relationship.getText().toString()+ ")");
        }


    }

    public class CustomPatientPhoneNoDialogue extends Dialog implements
            View.OnClickListener {

        public Activity c;
        public Dialog d;
        Button btnDone;

         AutoCompleteTextView txt_phoneNo;



        public CustomPatientPhoneNoDialogue(Activity a) {
            super(a);
            // TODO Auto-generated constructor stub
            this.c = a;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.patient_phone_no);

            ButterKnife.bind(c);
            setHeader();
             txt_phoneNo = (AutoCompleteTextView) findViewById(R.id.edPhoneNo);
            btnDone = (Button) findViewById(R.id.btn_done);


            btnDone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!txt_phoneNo.getText().toString().equals("")) {
                      //  addPhoneNo(txt_phoneNo.getText().toString());
                        submitPhoneNo();

                        dismiss();
                    }
                    else
                    {
                        Toast.makeText(c, "Phone Number Can not be empty.", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }


        @Override
        public void onClick(View v) {

            dismiss();
        }

        void submitPhoneNo() {

            ContactNumbers contactNumbers = new ContactNumbers();
            contactNumbers.setDescription(txt_phoneNo.getText().toString());
            showProgressDialog("Please wait...");
            Gson gson = new Gson();
            Type type = new TypeToken<ContactNumbers>() {
            }.getType();
            String json = gson.toJson(contactNumbers, type);

            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

            //showProgressDialog("Please Wait");
            try {
                Call<ResponseBody> posts = RestAPIFactory.getApi().saveusercontactnumbers( Shifa4U.mySharePrefrence.getLogin().getTokenType() + " " + Shifa4U.mySharePrefrence.getLogin().getAccessToken() , body);
                posts.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.code() == 200) {
                            Toast.makeText(OrderPreviewActivity.this, "Profile updated successfully", Toast.LENGTH_LONG).show();
                            getUserContactNumbers();

                        } else {
                            Toast.makeText(OrderPreviewActivity.this, "Something went wrong.\n Please try again later", Toast.LENGTH_LONG).show();
                        }

                        dismissProgressDislog();
                        // dismiss();
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        dismissProgressDislog();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void setHeader() {
            HeaderManager headerManager = new HeaderManager(c, findViewById(R.id.header));
            headerManager.setMenutype(HeaderManager.BACK_MENU);
            headerManager.setShowFirstImageButton(false);
            headerManager.setShowSecoundImageButton(false);
            headerManager.setShowSecoundImageButton(false);
            headerManager.setTitle("Add Phone No");
            headerManager.setBackMenuButtonVisibility(false);
            headerManager.setShowBottomView(false);
            headerManager.setHeaderColor(c.getResources().getColor(R.color.header_background_color));
        }
    }



    private void getUserFamilyMembers() {

        showProgressDialog("Please wait...");

        try {
            Call<ResponseBody> posts = RestAPIFactory.getApi().getuserfamilymembers(Shifa4U.mySharePrefrence.getLogin().getTokenType() + " " + Shifa4U.mySharePrefrence.getLogin().getAccessToken());
            posts.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
               //     dismissProgressDislog();
                    if (response.code() == 200 && response.body() != null) {

                        if (names != null)
                            names.clear();


                        List<UserFamilyMembers> userFamilyMembers = MyGeneric.processResponse(response, new TypeToken<List<UserFamilyMembers>>() {
                        });


                        if (userFamilyMembers != null) {
                            for (int i = 0; i < userFamilyMembers.size(); i++) {
                                names.add(userFamilyMembers.get(i).getName());
                            }
                        }


                        if (isSecondTime)
                        {
                            if (names.size()>0)
                            edPatientName.setText(names.get(names.size()-1));
                        }
                        setPatientNameAdaptor();

                     //   Toast.makeText(OrderPreviewActivity.this, "ada", Toast.LENGTH_SHORT).show();
/*
                        Shifa4U.mySharePrefrence.setProfile(customerProfileModel);
                        FillProfile(customerProfileModel);
                        if (Shifa4U.checkNetwork.isInternetAvailable()) {
                            getAllCountries();
                        } else {
                            Toast.makeText(MyProfileActivity.this, "Please Check your network connection", Toast.LENGTH_SHORT).show();
                        }*/
                    } else {
                       // Toast.makeText(MyProfileActivity.this, "Something went wrong.\n Please try again later", Toast.LENGTH_LONG).show();
                    }


                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    dismissProgressDislog();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            dismissProgressDislog();
        }


    }


  private void getUserContactNumbers() {


        try {
            Call<ResponseBody> posts = RestAPIFactory.getApi().getusercontactnumbers(Shifa4U.mySharePrefrence.getLogin().getTokenType() + " " + Shifa4U.mySharePrefrence.getLogin().getAccessToken());
            posts.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                   dismissProgressDislog();
                    if (response.code() == 200 && response.body() != null) {
                        if (phoneNumbers != null)
                        phoneNumbers.clear();

                         contactNumbers = MyGeneric.processResponse(response, new TypeToken<List<ContactNumbers>>() {
                        });

                         if (contactNumbers != null) {
                             if (contactNumbers.size()>0) {
                                 phoneNoContactId = contactNumbers.get(0).getContactId();

                                 for (int i = 0; i < contactNumbers.size(); i++) {
                                     phoneNumbers.add(contactNumbers.get(i).getDescription());
                                 }

                             }

                             if (isSecondTime)
                             {
                                 if (phoneNumbers.size()>0)
                                 edPhoneInOrderPreview.setText(phoneNumbers.get(phoneNumbers.size()-1));
                             }
                             else
                             {
                                 isSecondTime = true;
                             }
                             setPhoneAdaptor();
                         }

                    } else {
                  }


                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    dismissProgressDislog();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            dismissProgressDislog();
        }


    }


    private void getUserAddresses() {
        try {
            Call<ResponseBody> posts = RestAPIFactory.getApi().getuseraddresses(Shifa4U.mySharePrefrence.getLogin().getTokenType() + " " + Shifa4U.mySharePrefrence.getLogin().getAccessToken());
            posts.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                   // dismissProgressDislog();
                    if (response.code() == 200 && response.body() != null) {
                        if (localaddresses != null)
                        localaddresses.clear();


                        addresses = MyGeneric.processResponse(response, new TypeToken<List<Addresses>>() {
                        });

                        if (addresses != null) {
                            if (addresses.size()>0) {
                                addressContactId = addresses.get(0).getContactId();

                                for (int i = 0; i < addresses.size(); i++) {
                                    localaddresses.add(addresses.get(i).getDescription());
                                }
                            }

                           // addressAdaptor.notifyDataSetChanged();

                            if (isSecondTime)
                            {
                                if (addresses.size()>0)
                                edAddressInOrderPreview.setText(addresses.get(addresses.size()-1).getDescription());
                            }
                            setAddressAdaptor();
                        }



                    } else {
                 }


                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    dismissProgressDislog();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            dismissProgressDislog();
        }


    }

    public void showProgressDialog(String message) {
        if (progressDialog == null)
            progressDialog = new ProgressDialog(OrderPreviewActivity.this);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    public void dismissProgressDislog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        }
    }


    private  void setPhoneAdaptor ()
    {
        phoneNoAdaptor = new ArrayAdapter<String>(OrderPreviewActivity.this, R.layout.spinner_item, (phoneNumbers));

        edPhoneInOrderPreview.setAdapter(phoneNoAdaptor);
        edPhoneInOrderPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edPhoneInOrderPreview.showDropDown();
            }
        });

        edPhoneInOrderPreview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (contactNumbers != null) {
                    if (contactNumbers.size() >0)
                        phoneNoContactId = contactNumbers.get(i).getContactId();
                }
            }
        });
    }


    private void setAddressAdaptor ()
    {
        addressAdaptor = new ArrayAdapter<String>(OrderPreviewActivity.this, R.layout.spinner_item, (localaddresses));
        edAddressInOrderPreview.setAdapter(addressAdaptor);

        edAddressInOrderPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edAddressInOrderPreview.showDropDown();
            }
        });

        edAddressInOrderPreview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if ( addresses!= null) {
                    if (addresses.size() >0)
                        addressContactId = addresses.get(i).getContactId();
                }
            }
        });
    }

    private void setPatientNameAdaptor ()
    {
        nameAdaptor = new ArrayAdapter<String>(OrderPreviewActivity.this, R.layout.spinner_item, (names));
        edPatientName.setAdapter(nameAdaptor);
        edPatientName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edPatientName.showDropDown();
            }
        });
    }
}
