package com.udhc.shifa4u.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.aakira.expandablelayout.ExpandableLinearLayout;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.HeaderManager;
import com.udhc.shifa4u.Utilities.Utility;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HowItWorks extends AppCompatActivity {


    @BindView(R.id.buttonLayoutStepByStep)
    RelativeLayout buttonLayoutStepByStep;
    @BindView(R.id.btnStepByStepGuide)
    RelativeLayout btnStepByStepGuide;
    @BindView(R.id.lvStepByStepGuide)
    ExpandableLinearLayout lvStepByStepGuide;
    @BindView(R.id.buttonSecondOpinion)
    RelativeLayout btnSecondOpinion;
    @BindView(R.id.textViewSecondOpinion)
    TextView textViewSecondOpinion;
    @BindView(R.id.btnSecondOpinion)
    RelativeLayout buttonLayoutSecondOpininon;
    @BindView(R.id.lvSecondOpinion)
    ExpandableLinearLayout lvSecondOpinion;
    @BindView(R.id.buttonAfterConsultationSupport)
    RelativeLayout buttonLayoutAfterConsultationSupport;
    @BindView(R.id.btnAfterConsultationSupport)
    RelativeLayout btnAfterConsultationSupport;
    @BindView(R.id.lvAfterConsultationSupport)
    ExpandableLinearLayout lvAfterConsultationSupport;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_how_it_works);
        ButterKnife.bind(this);
        setHeader();
        init();

    }

    @Override
    protected void onResume() {
        super.onResume();
        setHeader();
    }

    private void init() {
        Utility.setRotateAnimator(lvStepByStepGuide , buttonLayoutStepByStep);
        Utility.setRotateAnimator(lvSecondOpinion , btnSecondOpinion);
        Utility.setRotateAnimator(lvAfterConsultationSupport , buttonLayoutAfterConsultationSupport);
        Utility.setClick(btnStepByStepGuide , lvStepByStepGuide);
        Utility.setClick(buttonLayoutSecondOpininon , lvSecondOpinion);
        Utility.setClick(btnAfterConsultationSupport , lvAfterConsultationSupport);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    void setHeader()
    {
        HeaderManager headerManager = new HeaderManager(HowItWorks.this, findViewById(R.id.header));
        headerManager.setMenutype(HeaderManager.BACK_MENU);
        headerManager.setShowFirstImageButton(false);
        headerManager.setShowSecoundImageButton(false);
        headerManager.setTitle("How It Works");
        headerManager.setShowBottomView(true);
        headerManager.setHeaderColor(getResources().getColor(R.color.header_background_color));
    }
}
