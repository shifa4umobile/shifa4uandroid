package com.udhc.shifa4u.Activities;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.Spanned;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;
import com.udhc.shifa4u.Models.AmericanTeleclinic.Blog;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.HeaderManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BlogDetail extends AppCompatActivity {

    @BindView(R.id.personPhotoDetail)
    RoundedImageView personPhotoDetail;
    @BindView(R.id.frame)
    FrameLayout frame;
    @BindView(R.id.tvTitleDetail)
    TextView tvTitleDetail;
    @BindView(R.id.tvDateDetail)
    TextView tvDateDetail;
    @BindView(R.id.tvAuthorNameDetail)
    TextView tvAuthorNameDetail;
    @BindView(R.id.tvDescritionDetail)
    TextView tvDescritionDetail;


    String imgaeUrl = "https://api.shifa4u.com/api/common/v1/downloadimage?fileName=";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blog_detail);
        ButterKnife.bind(this);

        Blog blog = (Blog) getIntent().getSerializableExtra("Blog");
        setHeader();

        if (blog != null)
        {


            tvTitleDetail.setText(blog.getTitle());


            String date = getDateInStringForm(blog.getPublishDateTime());
            tvDateDetail.setText(date);

            tvAuthorNameDetail.setText(blog.getAuthor());

            Spanned spannedString = fromHtml(blog.getDescrption());
            tvDescritionDetail.setText(spannedString);
            Glide.with(this).load(imgaeUrl + blog.getImageUrl()).into(personPhotoDetail);

        }
    }

    private String getDateInStringForm(String mDate)
    {
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {

            date = format.parse(mDate);
            System.out.println(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String dateTime = dateFormat.format(date);
        return  dateTime;
    }

    public static Spanned fromHtml(String html){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(html);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }
    private void setHeader() {
        HeaderManager headerManager = new HeaderManager(BlogDetail.this, findViewById(R.id.header));
        headerManager.setMenutype(HeaderManager.BACK_MENU);
        headerManager.setShowFirstImageButton(false);
        headerManager.setShowSecoundImageButton(false);
        headerManager.setShowSecoundImageButton(false);
        headerManager.setTitle("Detail");
        headerManager.setBackMenuButtonVisibility(true);

        headerManager.setShowBottomView(false);
        headerManager.setHeaderColor(BlogDetail.this.getResources().getColor(R.color.header_background_color));
    }
}
