package com.udhc.shifa4u.Activities;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;
import com.github.aakira.expandablelayout.ExpandableLinearLayout;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.HeaderManager;
import com.udhc.shifa4u.Utilities.InputStreamVolleyRequest;
import com.udhc.shifa4u.Utilities.Utility;

import java.io.FileOutputStream;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SampleRecommendation extends AppCompatActivity {
    @BindView(R.id.btnSRQ1)
    RelativeLayout btnSRQ1;
    @BindView(R.id.lvAnswerSRQ1)
    ExpandableLinearLayout lvAnswerSRQ1;
    @BindView(R.id.btnSRQ2)
    RelativeLayout btnSRQ2;
    @BindView(R.id.lvAnswerSRQ2)
    ExpandableLinearLayout lvAnswerSRQ2;
    @BindView(R.id.btnSRQ3)
    RelativeLayout btnSRQ3;
    @BindView(R.id.lvAnswerSRQ3)
    ExpandableLinearLayout lvAnswerSRQ3;
    @BindView(R.id.buttonSRQ1)
    RelativeLayout buttonSRQ1;
    @BindView(R.id.buttonSRQ2)
    RelativeLayout buttonSRQ2;
    @BindView(R.id.buttonSRQ3)
    RelativeLayout buttonSRQ3;
    @BindView(R.id.btnSecondOpinionPdfDownload)
    Button btnSecondOpinionPdfDownload;

    private ProgressDialog pDialog;


    @Override
    protected void onResume() {
        super.onResume();
        setHeader();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample_recommendation);
        ButterKnife.bind(this);

        btnSecondOpinionPdfDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getPdfFile();
            }
        });
        setHeader();
        init();
    }

    private void init() {
        Utility.setClick(btnSRQ1, lvAnswerSRQ1);
        Utility.setRotateAnimator(lvAnswerSRQ1, buttonSRQ1);
        Utility.setClick(btnSRQ2, lvAnswerSRQ2);
        Utility.setRotateAnimator(lvAnswerSRQ2, buttonSRQ2);
        Utility.setClick(btnSRQ3, lvAnswerSRQ3);
        Utility.setRotateAnimator(lvAnswerSRQ3, buttonSRQ3);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    void setHeader() {
        HeaderManager headerManager = new HeaderManager(SampleRecommendation.this, findViewById(R.id.header));
        headerManager.setMenutype(HeaderManager.BACK_MENU);
        headerManager.setShowFirstImageButton(false);
        headerManager.setShowSecoundImageButton(false);
        headerManager.setTitle("Sample Recommendation");
        headerManager.setShowBottomView(true);
        headerManager.setHeaderColor(getResources().getColor(R.color.header_background_color));
    }


    private void getPdfFile ()
    {
        String mUrl = "https://www.shifa4u.com/Content/images/doctors/sample-rec.pdf";;
        InputStreamVolleyRequest request = new InputStreamVolleyRequest(Request.Method.GET, mUrl,
                new Response.Listener<byte[]>() {
                    @Override
                    public void onResponse(byte[] response) {
                        // TODO handle the response
                        try {
                            if (response!=null) {

                                FileOutputStream outputStream;
                                String name="Second Opinion.pdf";
                                outputStream = openFileOutput(name, Context.MODE_PRIVATE);
                                outputStream.write(response);
                                outputStream.close();
                                pDialog.dismiss();
                                Toast.makeText(SampleRecommendation.this, "Download complete.", Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            Log.d("KEY_ERROR", "UNABLE TO DOWNLOAD FILE");
                            e.printStackTrace();
                        }
                    }
                } ,new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO handle the error
                error.printStackTrace();
                pDialog.dismiss();
            }
        }, null);
        RequestQueue mRequestQueue = Volley.newRequestQueue(getApplicationContext(), new HurlStack());
        mRequestQueue.add(request);
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        pDialog.show();
    }
}
