package com.udhc.shifa4u.Activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.HeaderManager;
import com.udhc.shifa4u.api.RestAPIFactory;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordActivity extends AppCompatActivity {

    private TextView txt_email;
    private Button btn_send;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        HeaderManager headerManager = new HeaderManager(ForgotPasswordActivity.this, findViewById(R.id.header));
        headerManager.setMenutype(HeaderManager.BACK_MENU);
        headerManager.setShowFirstImageButton(false);
        headerManager.setShowSecoundImageButton(false);
        headerManager.setTitle(getString(R.string.forgot_password_title));

        txt_email = (TextView) findViewById(R.id.txt_email);
        btn_send = (Button) findViewById(R.id.btn_send);

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                boolean isValidEmail =     android.util.Patterns.EMAIL_ADDRESS.matcher(txt_email.getText().toString().trim()).matches();

                if(Shifa4U.checkNetwork.isInternetAvailable())
                {
                    if (!txt_email.getText().toString().isEmpty()) {
                        
                        if (isValidEmail) {
                            JsonObject jsonObject = new JsonObject();

                            try {
                                jsonObject.addProperty("LoginName", txt_email.getText().toString());
                                requestForgotPassword(jsonObject);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        else 
                        {
                            Toast.makeText(ForgotPasswordActivity.this, "Please enter a valid email address", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        Toast.makeText(ForgotPasswordActivity.this, "Email can not be empty", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(ForgotPasswordActivity.this, R.string.internet_not_available, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    public void requestForgotPassword(JsonObject jsonObject) throws JSONException {
        showProgressDialog("Please wait...");
      //  RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), "shahid.mrd@hotmail.com");

        //showProgressDialog("Please Wait");
     //   String email = jsonObject.getString("LoginName");
        try {
            Call<ResponseBody> posts = RestAPIFactory.getApi().ForgotPasswordMobile(jsonObject);
            posts.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.code() == 200) {
                        showALertBox(ForgotPasswordActivity.this);

                    } else {
                        Toast.makeText(ForgotPasswordActivity.this, "Something went wrong.\n Please try again later", Toast.LENGTH_LONG).show();
                    }
                    dismissProgressDislog();
                }
                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    dismissProgressDislog();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public void showProgressDialog(String message) {
        if (progressDialog == null)
            progressDialog = new ProgressDialog(ForgotPasswordActivity.this);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    public void dismissProgressDislog() {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    private void showALertBox (final Context context)
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("A Reset code has been sent to you. Check your cellphone for text message or email!");
                alertDialogBuilder.setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {

                                Intent i = new Intent(ForgotPasswordActivity.this , ResetPasswordWithCode.class);
                                i.putExtra("email", txt_email.getText().toString().trim());
                                startActivity(i);
                            }
                        });



        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

}
