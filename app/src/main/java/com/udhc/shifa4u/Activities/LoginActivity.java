package com.udhc.shifa4u.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.udhc.shifa4u.Models.CustomerProfileModel;
import com.udhc.shifa4u.Models.LoginModel;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.MyGeneric;
import com.udhc.shifa4u.api.RestAPIFactory;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {
    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private TextView txt_sign_up;
    private ImageView img_forgotpassword;
    private ProgressDialog progressDialog;
    private ImageView img_show_pass;
    private ImageView img_guest;
    private String from=null;
//    private View mProgressView;
//    private View mLoginFormView;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);

        try {
            from = getIntent().getStringExtra("From");
        }catch(Exception e)
        {

        }
        mPasswordView = (EditText) findViewById(R.id.password);
        txt_sign_up = (TextView) findViewById(R.id.txt_sign_up);
        img_forgotpassword = (ImageView) findViewById(R.id.img_forgotpassword);
        img_show_pass = (ImageView) findViewById(R.id.img_show_pass);
        img_guest = (ImageView) findViewById(R.id.img_guest);
        img_show_pass.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPasswordView.getTransformationMethod() == null) {
                    mPasswordView.setTransformationMethod(new PasswordTransformationMethod());
                    img_show_pass.setImageDrawable(getResources().getDrawable(R.drawable.visibility_button));
                } else {
                    mPasswordView.setTransformationMethod(null);
                    img_show_pass.setImageDrawable(getResources().getDrawable(R.drawable.invisibility_button));
                }
                mPasswordView.setSelection(mPasswordView.getText().toString().length());


            }
        });


        img_guest.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
                finish();
            }
        });


        img_forgotpassword.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class));
            }
        });

        txt_sign_up.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, SignupActivity.class));
            }
        });


        Button mEmailSignInButton = (Button) findViewById(R.id.btn_sign_in);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Shifa4U.checkNetwork.isInternetAvailable()) {
                    if (!mEmailView.getText().toString().isEmpty()) {
                        boolean isValidEmail =     android.util.Patterns.EMAIL_ADDRESS.matcher(mEmailView.getText().toString().trim()).matches();
                        if (isValidEmail) {
                            if (!mPasswordView.getText().toString().isEmpty()) {
                                attemptLogin();
                            } else {
                                Toast.makeText(LoginActivity.this, "Please enter password", Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            Toast.makeText(LoginActivity.this, "Please Enter Valid Email", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(LoginActivity.this, "Please enter EmailID or User Name", Toast.LENGTH_SHORT).show();
                    }
                } else
                    Toast.makeText(LoginActivity.this, R.string.internet_not_available, Toast.LENGTH_SHORT).show();
            }
        });

//        mLoginFormView = findViewById(R.id.login_form);
//        mProgressView = findViewById(R.id.login_progress);
    }

    private void attemptLogin() {

        showProgressDialog("Please wait...");

        try {
            Call<ResponseBody> posts = RestAPIFactory.getApi().Login("password", mEmailView.getText().toString(),mPasswordView.getText().toString(), "aad4dc0739b64c529ab86c2126ed341c");
            posts.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    dismissProgressDislog();
                    if (response.code() == 200 && response.body() != null) {

                        LoginModel loginModel = MyGeneric.processResponse(response,new TypeToken<LoginModel>(){});

                        Shifa4U.mySharePrefrence.setLogin(loginModel);
                        getProfile();
                    } else {
                        Toast.makeText(LoginActivity.this, "Email or password is incorrect\n Please try again later", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    dismissProgressDislog();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            dismissProgressDislog();
        }


    }


    private void getProfile() {

        showProgressDialog("Please wait...");

        try {
            Call<ResponseBody> posts = RestAPIFactory.getApi().getCustomerProfile(Shifa4U.mySharePrefrence.getLogin().getTokenType() + " " + Shifa4U.mySharePrefrence.getLogin().getAccessToken());
            posts.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.code() == 200 && response.body() != null) {
                        CustomerProfileModel customerProfileModel = MyGeneric.processResponse(response,new TypeToken<CustomerProfileModel>(){});
                        Shifa4U.mySharePrefrence.setProfile(customerProfileModel);
                       if(from !=null && !from.isEmpty() && from.equalsIgnoreCase("checkout"))
                       {
                           startActivity(new Intent(LoginActivity.this, CheckoutActivity.class));
                       }
                       else if (from !=null && !from.isEmpty() && from.equalsIgnoreCase("submit"))
                       {
                            String doctorName = (String) getIntent().getSerializableExtra("doctorName");
                           Intent myIntent = new Intent(LoginActivity.this, SubmitActivity.class);
                           if (doctorName != null)
                           {
                               myIntent.putExtra("doctorName", "doctor");
                           }
                           startActivity(myIntent);
                       }

                       else if (from !=null && !from.isEmpty() && from.equals("Shifa4uTeleclinic"))
                       {
                          //  String doctorName = (String) getIntent().getSerializableExtra("doctorName");
                           Intent myIntent = new Intent(LoginActivity.this, Shifa4uTeleclinic.class);
                           startActivity(myIntent);
                       }

                       else if (Shifa4U.mySharePrefrence.getAccountActivityNames()!=null)
                       {
                           Intent myIntent = new Intent(LoginActivity.this, MainActivity.class);
                           startActivity(myIntent);
                           finish();
                       }

                       else
                       {
                           startActivity(new Intent(LoginActivity.this, MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
                       }
                        finish();
                    } else {
                        Toast.makeText(LoginActivity.this, "Something went wrong.\n Please try again later", Toast.LENGTH_LONG).show();
                    }
                    dismissProgressDislog();
                }
                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    dismissProgressDislog();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            dismissProgressDislog();
        }
    }


    public void showProgressDialog(String message) {
        if (progressDialog == null)
            progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    public void dismissProgressDislog() {
        if (progressDialog.isShowing() && progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }


}

