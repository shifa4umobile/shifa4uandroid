package com.udhc.shifa4u.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.udhc.shifa4u.Adapters.RecyclerViewRecyclerAdapter;
import com.udhc.shifa4u.Models.AmericanTeleclinic.FAQItemModel;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.HeaderManager;
import com.udhc.shifa4u.Utilities.Utility;

import java.util.ArrayList;
import java.util.List;

public class FrequentlyAskedQuestions extends AppCompatActivity {

    @Override
    protected void onResume() {
        super.onResume();
        setHeader();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frequently_asked_questions);
        setHeader();


        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.addItemDecoration(new DividerItemDecoration(this , 0));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        final List<FAQItemModel> data = new ArrayList<>();
        Utility.InitializeFAQ(data , this);

        recyclerView.setAdapter(new RecyclerViewRecyclerAdapter(data));
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition( R.anim.slide_in_right, R.anim.slide_out_right );
    }

    void setHeader()
    {
        HeaderManager headerManager = new HeaderManager(FrequentlyAskedQuestions.this, findViewById(R.id.header));
        headerManager.setMenutype(HeaderManager.BACK_MENU);
        headerManager.setShowFirstImageButton(false);
        headerManager.setShowSecoundImageButton(false);
        headerManager.setTitle("Frequently Asked Questions");
        headerManager.setShowBottomView(true);
        headerManager.setHeaderColor(getResources().getColor(R.color.header_background_color));
    }
}
