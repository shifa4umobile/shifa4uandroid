package com.udhc.shifa4u.Activities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.udhc.shifa4u.Adapters.AreaAdaptor;
import com.udhc.shifa4u.Adapters.CityAdapter;
import com.udhc.shifa4u.Adapters.CountryAdapter;
import com.udhc.shifa4u.Interfaces.MenuItemsInterface;
import com.udhc.shifa4u.Models.AreasModel;
import com.udhc.shifa4u.Models.CityModel;
import com.udhc.shifa4u.Models.CountryModel;
import com.udhc.shifa4u.Models.CustomerProfileModel;
import com.udhc.shifa4u.Models.MenuModel;
import com.udhc.shifa4u.Models.ProfileModel;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.GZipRequest;
import com.udhc.shifa4u.Utilities.HeaderManager;
import com.udhc.shifa4u.Utilities.MyGeneric;
import com.udhc.shifa4u.Utilities.MyLog;
import com.udhc.shifa4u.Utilities.Utility;
import com.udhc.shifa4u.api.RestAPIFactory;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyProfileActivity extends AppCompatActivity {

    private AutoCompleteTextView txt_gender;
    String[] gender = new String[]{"Male", "Female"};
    private ProgressDialog progressDialog = null;
    private AutoCompleteTextView txt_country;
    private AutoCompleteTextView txt_city;
    private AutoCompleteTextView txt_area;
    private ArrayList<CityModel> cityModels;
    List<String> areaList;
    private long countryId;
    RequestQueue requestQueue;


    static public   int countryIndex = 0;
    static  public  int cityIndex = 0;
    private long CityId;
    private long AreaId;
    private ArrayList<CountryModel> countryModels;
    private AutoCompleteTextView txt_first_name;
    private AutoCompleteTextView txt_last_name;
    private AutoCompleteTextView txt_dob;
    private AutoCompleteTextView txt_email;
    private AutoCompleteTextView txt_contact_number;
    private AutoCompleteTextView txt_address;
    private Button btn_submit;
    private CustomerProfileModel customerProfileModel;
    String AreaUrl = "https://api.shifa4u.com/api/common/v1/getareasforcity?cityId=";
    ArrayList<AreasModel> areasModelList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        setHeader();

        txt_gender = (AutoCompleteTextView) findViewById(R.id.txt_gender);
        txt_country = (AutoCompleteTextView) findViewById(R.id.txt_country);
        txt_city = (AutoCompleteTextView) findViewById(R.id.txt_city);
        
        txt_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(MyProfileActivity.this, "ah", Toast.LENGTH_SHORT).show();
                
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        txt_area = (AutoCompleteTextView) findViewById(R.id.txt_area);

        txt_first_name = (AutoCompleteTextView) findViewById(R.id.txt_first_name);
        txt_last_name = (AutoCompleteTextView) findViewById(R.id.txt_last_name);
        txt_dob = (AutoCompleteTextView) findViewById(R.id.txt_dob);
        txt_email = (AutoCompleteTextView) findViewById(R.id.txt_email);
        txt_contact_number = (AutoCompleteTextView) findViewById(R.id.txt_contact_number);
        txt_address = (AutoCompleteTextView) findViewById(R.id.txt_address);
        btn_submit = (Button) findViewById(R.id.btn_submit);

        ArrayAdapter<String> genderAdapter = new ArrayAdapter<String>(MyProfileActivity.this, R.layout.spinner_item, gender);
        txt_gender.setAdapter(genderAdapter);

        if (Shifa4U.checkNetwork.isInternetAvailable()) {
            getProfile();
        } else {
            Toast.makeText(MyProfileActivity.this, "Please Check your network connection", Toast.LENGTH_SHORT).show();
        }
        setHeader();

        areaList = new ArrayList<>();
        requestQueue = Volley.newRequestQueue(this);

        txt_gender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_gender.showDropDown();
            }
        });

        txt_country.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (countryModels != null && countryModels.size() > 0)
                    txt_country.showDropDown();

            }
        });

        txt_city.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (cityModels != null && cityModels.size() > 0)
                    txt_city.showDropDown();

            }
        });

        txt_area.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (cityModels != null && cityModels.size() > 0)
                    txt_area.showDropDown();

            }
        });


//        txt_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                getCitiesForCountry(String.valueOf(id));
//                countryId = id;
//                CityId = -1;
//                txt_city.setText("");
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//
//        txt_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                CityId = id;
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });


//        txt_country.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                getCitiesForCountry(String.valueOf(id));
//                countryId = id;
//                CityId = -1;
//                txt_city.setText("");
//                txt_country.dismissDropDown();
//                txt_country.setText();
//            }
//        });

//        txt_city.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//                CityId = id;
//                txt_city.dismissDropDown();
//            }
//        });


        txt_dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker();
            }
        });

        txt_gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);
//                ((TextView) parent.getChildAt(0)).setTextSize(getResources().getDimension(R.dimen._5sdp));
//                ((TextView) parent.getChildAt(0)).setPadding(0, 0, 0, 0);
//                ((TextView) parent.getChildAt(0)).setTypeface(null, Typeface.NORMAL);

             /*   txt_gender.setText(Arrays.asList(gender).get(position));

                txt_gender.dismissDropDown();*/
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        //        txt_dob.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (hasFocus) {
//
//                }
//
//            }
//        });

//        txt_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//
//
//        txt_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                CityId = id;
//
//                Shifa4U.mySharePrefrence.setSelectedCityIndex(position);
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//                Shifa4U.mySharePrefrence.clearSelectedCityIndex();
//            }
//        });


        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!txt_first_name.getText().toString().isEmpty()) {
                    if (!txt_last_name.getText().toString().isEmpty()) {
                        if (!txt_dob.getText().toString().isEmpty()) {
                            if (!txt_gender.getText().toString().isEmpty()) {
                                if (!txt_email.getText().toString().isEmpty()) {
                                    if (!txt_contact_number.getText().toString().isEmpty()) {
                                        if (!txt_country.getText().toString().isEmpty()) {
                                            if (!txt_city.getText().toString().isEmpty() && CityId > -1) {
                                                if (!txt_address.getText().toString().isEmpty()) {
                                                    if (!txt_area.getText().toString().isEmpty()){
                                                        if (Shifa4U.checkNetwork.isInternetAvailable()) {
                                                            submitProfile();
                                                        } else {
                                                            Toast.makeText(MyProfileActivity.this, R.string.internet_not_available, Toast.LENGTH_LONG).show();
                                                        }
                                                }
                                                else
                                                    {
                                                        Toast.makeText(MyProfileActivity.this, "Please Select Area", Toast.LENGTH_LONG).show();


                                                    }
                                                } else {
                                                    Toast.makeText(MyProfileActivity.this, " please fill address", Toast.LENGTH_LONG).show();
                                                }
                                            } else {
                                                Toast.makeText(MyProfileActivity.this, "please select city", Toast.LENGTH_LONG).show();
                                            }
                                        } else {
                                            Toast.makeText(MyProfileActivity.this, "please select country", Toast.LENGTH_LONG).show();
                                        }
                                    } else {
                                        Toast.makeText(MyProfileActivity.this, " please fill contact number", Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    Toast.makeText(MyProfileActivity.this, " please fill email", Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Toast.makeText(MyProfileActivity.this, " please select gender", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(MyProfileActivity.this, "please fill dob", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(MyProfileActivity.this, " please fill last name", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(MyProfileActivity.this, " please fill first name", Toast.LENGTH_LONG).show();
                }
            }
        });


    }

    private void setHeader() {
        HeaderManager headerManager = new HeaderManager(MyProfileActivity.this, findViewById(R.id.header));
        headerManager.setMenutype(HeaderManager.BACK_MENU);
        headerManager.setShowFirstImageButton(false);
        headerManager.setShowSecoundImageButton(false);
        headerManager.setTitle(getString(R.string.profile_title));
        headerManager.setShowBottomView(true);
        headerManager.setHeaderColor(getResources().getColor(R.color.header_background_color));
    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    private void getProfile() {

        showProgressDialog("Please wait...");

        try {
            Call<ResponseBody> posts = RestAPIFactory.getApi().getCustomerProfile(Shifa4U.mySharePrefrence.getLogin().getTokenType() + " " + Shifa4U.mySharePrefrence.getLogin().getAccessToken());
            posts.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    dismissProgressDislog();
                    if (response.code() == 200 && response.body() != null) {
                        customerProfileModel = MyGeneric.processResponse(response, new TypeToken<CustomerProfileModel>() {
                        });

                        Shifa4U.mySharePrefrence.setProfile(customerProfileModel);
                        FillProfile(customerProfileModel);
                        if (Shifa4U.checkNetwork.isInternetAvailable()) {
                            getAllCountries();
                        } else {
                            Toast.makeText(MyProfileActivity.this, "Please Check your network connection", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(MyProfileActivity.this, "Something went wrong.\n Please try again later", Toast.LENGTH_LONG).show();
                    }


                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    dismissProgressDislog();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            dismissProgressDislog();
        }


    }

    private void FillProfile(CustomerProfileModel body) {
        txt_first_name.setText(body.getFirstName());
        txt_last_name.setText(body.getLastName());
        txt_dob.setText(body.getDateofBirth());
      //  txt_gender.setText(body.getGender());
        txt_gender.setSelection(0);
        txt_gender.setListSelection(0);
        txt_gender.performCompletion();
        txt_gender.setText(body.getGender());

        ArrayAdapter<String> genderAdapter = new ArrayAdapter<String>(MyProfileActivity.this, R.layout.spinner_item, gender);
        txt_gender.setAdapter(genderAdapter);
     //   txt_gender.setText(body.getGender());
        txt_email.setText(body.getEmail());
        txt_contact_number.setText(body.getMobileNo());
        txt_address.setText(body.getAddress());

    }


    public void getAllCountries() {
        showProgressDialog("Please Wait");
        try {
            Call<ResponseBody> posts = RestAPIFactory.getApi().getAllCountries();
            posts.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    dismissProgressDislog();

                    if (response.isSuccessful()) {
                        List<CountryModel> countryModels = MyGeneric.processResponse(response, new TypeToken<List<CountryModel>>() {
                        });
                         if (countryModels != null && countryModels.size() > 0) {
                            txt_country.setAdapter(new CountryAdapter(MyProfileActivity.this, (ArrayList<CountryModel>) countryModels, new MenuItemsInterface() {
                                @Override
                                public void onItemClick(Integer position, MenuModel menuModel) {

                                }

                                @Override
                                public void onButtonClick(Integer position, Object model, View button) {

                                }

                                @Override
                                public void onItemClick(Integer position, Object o) {
                                    getCitiesForCountry(String.valueOf(((CountryModel)o).getCountryId()));
                                    countryId = ((CountryModel)o).getCountryId();
                                    CityId = -1;
                                    txt_city.setText("");
                                    txt_country.dismissDropDown();
                                    txt_country.setText(((CountryModel)o).getCountryName());
                                  //  countryIndex = position;
                                }
                            }));
                            MyProfileActivity.this.countryModels = (ArrayList<CountryModel>) countryModels;

                            int selectionIndex = -1;
                            if (customerProfileModel.getCountry() != null && customerProfileModel.getCountry() != "") {
                                if (countryModels != null && countryModels.size() > 0) {
                                    int i = 0;
                                    for (CountryModel countryModel : countryModels) {
                                        if (countryModel.getCountryId().equals(customerProfileModel.getCountryId())) {
                                            selectionIndex = i;
                                            getCitiesForCountry(String.valueOf(customerProfileModel.getCountryId()));
                                            break;
                                        }
                                        i++;
                                    }
                                }
                            }
                            if (selectionIndex != -1) {
                                txt_country.setSelection(selectionIndex);
                                txt_country.setListSelection(selectionIndex);
                                txt_country.performCompletion();
                                txt_country.setText(countryModels.get(selectionIndex).getCountryName());
                            }
                        } else {
                            txt_country.setAdapter(null);
                        }
                        Log.e("response", response.body().toString());
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                    dismissProgressDislog();
                }
            });
        } catch (Exception e) {
            dismissProgressDislog();
            e.printStackTrace();
        }
    }


    public void getCitiesForCountry(String countryID) {
        showProgressDialog("Please Wait");
        try {
            Call<ResponseBody> posts = RestAPIFactory.getApi().getCitiesForCountry(countryID);
            posts.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                    dismissProgressDislog();
                    if (response.isSuccessful()) {

                        List<CityModel> cityModels = MyGeneric.processResponse(response, new TypeToken<List<CityModel>>() {
                        });

                        if (cityModels != null) {
                            if (cityModels.size() > 0) {
                                txt_city.setAdapter(new CityAdapter(MyProfileActivity.this, (ArrayList<CityModel>) cityModels, menuItemsInterface));
                                MyProfileActivity.this.cityModels = (ArrayList<CityModel>) cityModels;

                                int selectionIndex = -1;
                                if (customerProfileModel.getCityId() != null && customerProfileModel.getCity() != "") {
                                    if (cityModels.size() > 0) {
                                        int i = 0;
                                        for (CityModel cityModel : cityModels) {
                                            if (cityModel.getCityId().equals(customerProfileModel.getCityId())) {
                                                selectionIndex = i;
                                                getArea(MyProfileActivity.this , customerProfileModel.getCityId());
                                                break;
                                            }
                                            i++;
                                        }
                                    }
                                }
                                if (selectionIndex != -1) {
                                    if (selectionIndex<cityModels.size()) {
                                      /*  txt_city.setSelection(selectionIndex);
                                        txt_city.setListSelection(selectionIndex);
                                        txt_city.performCompletion();*/
                                        txt_city.setText(cityModels.get(selectionIndex).getCityName());
                                    }
                                }
                            }
                            else
                            {
                                txt_city.setAdapter(null);
                                Shifa4U.mySharePrefrence.clearCities();
                            }

                        } else {
                            txt_city.setAdapter(null);
                            Shifa4U.mySharePrefrence.clearCities();
                        }
                        MyLog.e("response", response.body().toString());
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                    dismissProgressDislog();
                }
            });
        } catch (Exception e) {
            dismissProgressDislog();
            e.printStackTrace();
        }


    }

    public void showProgressDialog(String message) {
        if (progressDialog == null)
            progressDialog = new ProgressDialog(MyProfileActivity.this);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    public void dismissProgressDislog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        }
    }


    public void showDatePicker() {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        txt_dob.setText((monthOfYear + 1) + "/" + dayOfMonth + "/" + year);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
    }


    void submitProfile() {

        ProfileModel profileModel = new ProfileModel();
        profileModel.setFirstName(txt_first_name.getText().toString());
        profileModel.setLastName(txt_last_name.getText().toString());
        profileModel.setFullName(txt_first_name.getText().toString() + " " + txt_last_name.getText().toString());
        profileModel.setDateofBirth(txt_dob.getText().toString());
        profileModel.setGender(txt_gender.getText().toString());
        profileModel.setEmail(txt_email.getText().toString());
        profileModel.setMobileNo(txt_contact_number.getText().toString());
        profileModel.setCountryId(String.valueOf(countryId));
        profileModel.setCityId(String.valueOf(CityId));
        profileModel.setAreaId(String.valueOf(AreaId));
        profileModel.setAddress(txt_address.getText().toString());

        showProgressDialog("Please wait...");
        Gson gson = new Gson();
        Type type = new TypeToken<ProfileModel>() {
        }.getType();
        String json = gson.toJson(profileModel, type);

        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

        //showProgressDialog("Please Wait");
        try {
            Call<ResponseBody> posts = RestAPIFactory.getApi().updateCustomerProfile(body, Shifa4U.mySharePrefrence.getLogin().getTokenType() + " " + Shifa4U.mySharePrefrence.getLogin().getAccessToken());
            posts.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.code() == 200) {
//                        if (response.message().toString().equals("Conflict")) {
//                            Toast.makeText(MyProfileActivity.this, "The Information you have provided is already on server", Toast.LENGTH_LONG).show();
//                        } else {
//                            startActivity(new Intent(MyProfileActivity.this, VerificationActivity.class));
                        Toast.makeText(MyProfileActivity.this, "Profile updated successfully", Toast.LENGTH_LONG).show();
                        finish();
//                        }
                    } else {
                        Toast.makeText(MyProfileActivity.this, "Something went wrong.\n Please try again later", Toast.LENGTH_LONG).show();
                    }

                    dismissProgressDislog();
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    dismissProgressDislog();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private MenuItemsInterface menuItemsInterface = new MenuItemsInterface() {
        @Override
        public void onItemClick(Integer position, MenuModel menuModel) {

        }

        @Override
        public void onButtonClick(Integer position, Object model, View button) {

        }

        @Override
        public void onItemClick(Integer position, Object o) {
            CityId = ((CityModel)o).getCityId();
            txt_area.setText("");

            getArea(MyProfileActivity.this , (int) CityId);
            txt_city.dismissDropDown();
          //  cityIndex = position;
            txt_city.setText(((CityModel)o).getCityName()
            );
        }
    };



    private MenuItemsInterface menuItemsInterfaceForArea = new MenuItemsInterface() {
        @Override
        public void onItemClick(Integer position, MenuModel menuModel) {

        }

        @Override
        public void onButtonClick(Integer position, Object model, View button) {

        }

        @Override
        public void onItemClick(Integer position, Object o) {
            String temp = ((AreasModel)o).getAreaId();;
            AreaId = Long.parseLong(temp);

          //  getArea(MyProfileActivity.this , (int) CityId);
            txt_area.dismissDropDown();
          //  cityIndex = position;
            txt_area.setText(((AreasModel)o).getAreaName()
            );
        }
    };



    private void getArea(final Context context , int id) {
        StringRequest stringRequest = new GZipRequest(Request.Method.GET, AreaUrl+id, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                dismissProgressDislog();
                areasModelList = new ArrayList<>();
                Type type = new TypeToken<List<AreasModel>>() {
                }.getType();
                areasModelList = (ArrayList<AreasModel>) Utility.parseJSON(response, type);



                if (areasModelList != null) {
                    if (areasModelList.size() > 0) {
                        txt_area.setAdapter(new AreaAdaptor(MyProfileActivity.this, areasModelList, menuItemsInterfaceForArea));

                        int selectionIndex = -1;
                        if (customerProfileModel.getAreaId() != null && customerProfileModel.getArea() != "") {
                            Integer areaId = customerProfileModel.getAreaId();
                            if (areasModelList.size() > 0) {
                                int i = 0;
                                for (AreasModel areasModel : areasModelList) {
                                    if (areasModel.getAreaId().equals(String.valueOf(customerProfileModel.getAreaId()))) {
                                        selectionIndex = i;
                                        break;
                                    }
                                    i++;
                                }
                            }
                        }
                        if (selectionIndex != -1) {
                            if (selectionIndex<areasModelList.size()) {
                                      /*  txt_city.setSelection(selectionIndex);
                                        txt_city.setListSelection(selectionIndex);
                                        txt_city.performCompletion();*/
                                txt_area.setText(areasModelList.get(selectionIndex).getAreaName());
                            }
                        }
                    }
                    else
                    {
                        txt_area.setAdapter(null);
                      //  Shifa4U.mySharePrefrence.clearCities();
                    }

                } else {
                    txt_area.setAdapter(null);
                  //  Shifa4U.mySharePrefrence.clearCities();
                }
               // MyLog.e("response", response.body().toString());

             /*   String res = response;

                areasModelList = new ArrayList<AreasModel>();
                Type type = new TypeToken<List<AreasModel>>() {
                }.getType();
                areasModelList = (ArrayList<AreasModel>) Utility.parseJSON(response, type);
                if (areasModelList != null) {


                    areaList.clear();
                    areaList.add("Select Area");
                    for (int i = 0; i < areasModelList.size(); i++) {
                        areaList.add(areasModelList.get(i).getAreaName());
                    }



                    ArrayAdapter<String> dataAdapter;
                    if (txt_city != null) {
                        dataAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, areaList);

                    } else {
                        areaList = new ArrayList<>();
                        dataAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, areaList);

                    }

                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    // dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    txt_area.setAdapter(dataAdapter);
                }*/
            }


        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                HashMap<String, String> params2 = new HashMap<String, String>();
                return new JSONObject(params2).toString().getBytes();
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };

        requestQueue.add(stringRequest);
        showProgressDialog("Please Wait...");
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }
}
