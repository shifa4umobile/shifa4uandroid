package com.udhc.shifa4u.Activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.udhc.shifa4u.Adapters.LabAdapter;
import com.udhc.shifa4u.Adapters.MedicalPackagesAdapter;
import com.udhc.shifa4u.Interfaces.MenuItemsInterface;
import com.udhc.shifa4u.Models.CartModel;
import com.udhc.shifa4u.Models.CustomerPersonalContactsModel;
import com.udhc.shifa4u.Models.LabDetailModel;
import com.udhc.shifa4u.Models.MedicalPackages.MedicalPackagesModel;
import com.udhc.shifa4u.Models.MenuModel;
import com.udhc.shifa4u.Models.PaymentMode.FinalOrderDataModel;
import com.udhc.shifa4u.Models.PaymentMode.OrderDetail;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.GzipUtils;
import com.udhc.shifa4u.Utilities.HeaderManager;
import com.udhc.shifa4u.Utilities.InputStreamVolleyRequest;
import com.udhc.shifa4u.Utilities.MyGeneric;
import com.udhc.shifa4u.Utilities.MyLog;
import com.udhc.shifa4u.Utilities.MyUtils;
import com.udhc.shifa4u.api.RestAPIFactory;

import org.json.JSONObject;
import org.w3c.dom.ls.LSException;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class PaymentModeActivity extends AppCompatActivity {

    private HeaderManager headerManager;
    private ImageView img_bank_transfer;
    private ImageView img_easy_pay;
    private ImageView img_cod;
    int selectedPaymentMethod = -1;
    private Button btn_previous;
    private ProgressDialog progressDialog;
    private CustomerPersonalContactsModel customerPersonalContactsModel;
    private Button btn_next;
    private Button btn_download_voucher;
    private LinearLayout ll_order_complete;
    private TextView txt_order_number;
    InputStreamVolleyRequest request;
    private ScrollView scrollView;
    public String fileUrl = "https://api.shifa4u.com/api/common/v1/displayfilebyfilename?filename=";
    private Button btn_order_complete_next;
    public static boolean isOrderSuccess = false;
    private TextView txt_order_payment_info;

    int count;

    String addressContactId = "";
    String phoneNoContactId = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_mode);

        progressDialog= new ProgressDialog(this);

        Bundle bundle = getIntent().getExtras();
         addressContactId = bundle.getString("AddressContactId");
         phoneNoContactId = bundle.getString("PhoneNoContactId");


        img_cod = (ImageView) findViewById(R.id.img_cod);
        img_easy_pay = (ImageView) findViewById(R.id.img_easy_pay);
        img_bank_transfer = (ImageView) findViewById(R.id.img_bank_transfer);

        img_cod.setLayoutParams(new LinearLayout.LayoutParams(MATCH_PARENT, (int) getResources().getDimension(R.dimen._100sdp)));
        img_easy_pay.setLayoutParams(new LinearLayout.LayoutParams(MATCH_PARENT, (int) getResources().getDimension(R.dimen._100sdp)));
        img_bank_transfer.setLayoutParams(new LinearLayout.LayoutParams(MATCH_PARENT, (int) getResources().getDimension(R.dimen._100sdp)));
        btn_next = (Button) findViewById(R.id.btn_next);
        btn_download_voucher = (Button) findViewById(R.id.btn_download_voucher);
        btn_download_voucher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             //   downLoadVoucher ();
            }


        });
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if (selectedPaymentMethod == 2)
                {
                    startActivity(new Intent(PaymentModeActivity.this , EasyPaisaActivity.class));
                }*/
              //  else {
                    saveOrder();
               // }
            }
        });

        btn_previous = (Button) findViewById(R.id.btn_previous);
        scrollView = (ScrollView) findViewById(R.id.scrollView);
        ll_order_complete = (LinearLayout) findViewById(R.id.ll_order_complete);
        txt_order_number = (TextView) findViewById(R.id.txt_order_number);
        txt_order_payment_info= (TextView) findViewById(R.id.txt_order_payment_info);
        txt_order_payment_info.setVisibility(View.GONE);
        btn_order_complete_next = (Button) findViewById(R.id.btn_order_complete_next);

        scrollView.setVisibility(View.VISIBLE);
        ll_order_complete.setVisibility(View.GONE);

        btn_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        img_cod.setTag("0");
        img_easy_pay.setTag("0");
        img_bank_transfer.setTag("0");


        btn_order_complete_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PaymentModeActivity.this, MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
                finish();
            }
        });

        img_cod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_order_payment_info.setVisibility(View.VISIBLE);
                if (v.getTag().toString().equalsIgnoreCase("0")) {
                    v.setTag("1");
                    ((ImageView) v).setBackground((getResources().getDrawable(R.drawable.cod_checked)));
                    selectedPaymentMethod = 1;
                    img_easy_pay.setBackground(getResources().getDrawable(R.drawable.easy_pay_unchecked));
                    img_bank_transfer.setBackground(getResources().getDrawable(R.drawable.bank_transfer_unchecked));
                    img_easy_pay.setTag("0");
                    img_bank_transfer.setTag("0");
                } else {
                    v.setTag("0");
                    ((ImageView) v).setBackground(getResources().getDrawable(R.drawable.cod_unchecked));
                    selectedPaymentMethod = -1;
                }

                img_cod.setLayoutParams(new LinearLayout.LayoutParams(MATCH_PARENT, (int) getResources().getDimension(R.dimen._100sdp)));
                img_easy_pay.setLayoutParams(new LinearLayout.LayoutParams(MATCH_PARENT, (int) getResources().getDimension(R.dimen._100sdp)));
                img_bank_transfer.setLayoutParams(new LinearLayout.LayoutParams(MATCH_PARENT, (int) getResources().getDimension(R.dimen._100sdp)));

            }
        });

        img_easy_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_order_payment_info.setVisibility(View.GONE);
                if (v.getTag().toString().equalsIgnoreCase("0")) {
                    v.setTag("1");
                    ((ImageView) v).setBackground(getResources().getDrawable(R.drawable.easy_pay_checked));
                    selectedPaymentMethod = 2;
                   img_cod.setBackground(getResources().getDrawable(R.drawable.cod_unchecked));
                    img_bank_transfer.setBackground(getResources().getDrawable(R.drawable.bank_transfer_unchecked));
                    img_cod.setTag("0");
                    img_bank_transfer.setTag("0");
                } else {
                    v.setTag("0");
                    ((ImageView) v).setBackground(getResources().getDrawable(R.drawable.easy_pay_unchecked));
                    selectedPaymentMethod = -1;
                }
                img_cod.setLayoutParams(new LinearLayout.LayoutParams(MATCH_PARENT, (int) getResources().getDimension(R.dimen._100sdp)));
                img_easy_pay.setLayoutParams(new LinearLayout.LayoutParams(MATCH_PARENT, (int) getResources().getDimension(R.dimen._100sdp)));
                img_bank_transfer.setLayoutParams(new LinearLayout.LayoutParams(MATCH_PARENT, (int) getResources().getDimension(R.dimen._100sdp)));
            }
        });

        img_bank_transfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                txt_order_payment_info.setVisibility(View.GONE);
                if (v.getTag().toString().equalsIgnoreCase("0")) {
                    v.setTag("1");
                    ((ImageView) v).setBackground(getResources().getDrawable(R.drawable.bank_transfer_checked));
                    ((ImageView) v).setLayoutParams(new LinearLayout.LayoutParams(MATCH_PARENT, (int) getResources().getDimension(R.dimen._410sdp)));
                    selectedPaymentMethod = 3;
                    img_cod.setBackground(getResources().getDrawable(R.drawable.cod_unchecked));
                    img_easy_pay.setBackground(getResources().getDrawable(R.drawable.easy_pay_unchecked));
                    img_cod.setTag("0");
                    img_easy_pay.setTag("0");
                } else {
                    v.setTag("0");
                    ((ImageView) v).setBackground(getResources().getDrawable(R.drawable.bank_transfer_unchecked));
                    ((ImageView) v).setLayoutParams(new LinearLayout.LayoutParams(MATCH_PARENT, (int) getResources().getDimension(R.dimen._100sdp)));
                    selectedPaymentMethod = -1;
                }
                img_cod.setLayoutParams(new LinearLayout.LayoutParams(MATCH_PARENT, (int) getResources().getDimension(R.dimen._100sdp)));
                img_easy_pay.setLayoutParams(new LinearLayout.LayoutParams(MATCH_PARENT, (int) getResources().getDimension(R.dimen._100sdp)));
            }
        });


        ArrayList<CartModel> cartModels=Shifa4U.mySharePrefrence.getCart();

        if(cartModels!=null && cartModels.size()>0)
        {

            int cartTotal=0;
            for(CartModel cartModel:cartModels)
            {
                cartTotal+=cartModel.getPriice();
            }

            txt_order_payment_info.setText(String.format("Payment will be collected at the time of Home sampling. Kindly keep Rs. %d ready ",cartTotal));

        }



    }

    @Override
    protected void onResume() {
        super.onResume();
        setHeader();
        getCustomerPersonalContacts();
    }

    private void setHeader() {
        headerManager = new HeaderManager(this, findViewById(R.id.header));
        headerManager.setMenutype(HeaderManager.BACK_MENU);
        headerManager.setShowFirstImageButton(false);
        headerManager.setShowSecoundImageButton(false);
        headerManager.setTitle(getString(R.string.payment_mode_title));

        headerManager.setShowBottomView(true);
        headerManager.setHeaderColor(getResources().getColor(R.color.header_background_color));
    }

    public void showProgressDialog(String message) {
        if (progressDialog == null)
            progressDialog = new ProgressDialog(PaymentModeActivity.this);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    public void dismissProgressDislog() {
        if (progressDialog.isShowing() && progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }


    void getCustomerPersonalContacts() {
        showProgressDialog("Please wait...");
//        String str = String.format("{\"SkipPages\":\"%s\",\"Records\":\"10\",\"CityId\":\"%s\"}", String.valueOf(page_to_skip), Shifa4U.mySharePrefrence.getCityId());

//        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), str);

        try {
            Call<ResponseBody> posts = RestAPIFactory.getApi().getCustomerPersonalContacts(Shifa4U.mySharePrefrence.getLogin().getTokenType() + " " + Shifa4U.mySharePrefrence.getLogin().getAccessToken());
            posts.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    dismissProgressDislog();
                    try {

                        try {

//                            String Json = GzipUtils.decompress(response.body().bytes());
//                            MyLog.e("",Json);
//
//                            Type listType = new TypeToken<LabsModel>() {
//                            }.getType();
//                            LabsModel labsModel = new Gson().fromJson(Json, listType);

                            customerPersonalContactsModel = MyGeneric.processResponse(response, new TypeToken<CustomerPersonalContactsModel>() {
                            });


                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    dismissProgressDislog();
                    Log.e("", "");
                }
            });

        } catch (Exception e) {

        }
    }


    void saveOrder() {


        FinalOrderDataModel finalOrderDataModel = new FinalOrderDataModel();

        if (customerPersonalContactsModel != null) {
       /*     finalOrderDataModel.setCustomerAddress((customerPersonalContactsModel.getAddressContactId() != null && !customerPersonalContactsModel.getAddressContactId().isEmpty() && !customerPersonalContactsModel.getAddressContactId().equalsIgnoreCase("NULL")) ? customerPersonalContactsModel.getAddressContactId() : "");
            finalOrderDataModel.setCustomerContact((customerPersonalContactsModel.getPhoneNumberContactId() != null && !customerPersonalContactsModel.getPhoneNumberContactId().isEmpty() && !customerPersonalContactsModel.getPhoneNumberContactId().equalsIgnoreCase("NULL")) ? customerPersonalContactsModel.getPhoneNumberContactId() : "");
            */
            finalOrderDataModel.setCustomerAddress(addressContactId);
            finalOrderDataModel.setCustomerContact(phoneNoContactId);

        } else {
            finalOrderDataModel.setCustomerAddress("");
            finalOrderDataModel.setCustomerContact("");
        }

        if (selectedPaymentMethod == 1) {
            finalOrderDataModel.setPaymentMethodPrefix("BCOD");
        }
        else if (selectedPaymentMethod == 2)
        {
            finalOrderDataModel.setPaymentMethodPrefix("BEP");
        }
        else if (selectedPaymentMethod == 3) {
            finalOrderDataModel.setPaymentMethodPrefix("BNKTFR");
        } else {
            Toast.makeText(this, "Please select Payment mode", Toast.LENGTH_SHORT).show();
            return;
        }
        showProgressDialog("Please wait...");

        ArrayList<CartModel> cartModels = Shifa4U.mySharePrefrence.getCart();
        if (cartModels != null && cartModels.size() > 0) {
            ArrayList<OrderDetail> orderDetails = new ArrayList<>();
            for (int i = 0; i < cartModels.size(); i++) {

                OrderDetail orderDetail = new OrderDetail();

                if (cartModels.get(i).getProductType() == CartModel.product.LAB_TEST) {
                    orderDetail.setIsHomeSampleCollection("true");
                    orderDetail.setVendorLocationId("0");
                    orderDetail.setLabId(String.valueOf(cartModels.get(i).getSelectedLabId()));
                    orderDetail.setProductLineId(String.valueOf(cartModels.get(i).getProductLineId()));

                } else if (cartModels.get(i).getProductType() == CartModel.product.RADIOLOGY) {

                    orderDetail.setIsHomeSampleCollection("false");
                    orderDetail.setVendorLocationId(String.valueOf(cartModels.get(i).getSelectedContact().getVendorLocationId()));
                    orderDetail.setLabId(String.valueOf(cartModels.get(i).getSelectedLabId()));
                    orderDetail.setProductLineId(String.valueOf(cartModels.get(i).getProductLineId()));

                } else if (cartModels.get(i).getProductType() == CartModel.product.HOME_CARE) {

                    orderDetail.setIsHomeSampleCollection("true");
                    orderDetail.setVendorLocationId("0");
                    orderDetail.setLabId(String.valueOf(cartModels.get(i).getSelectedLabId()));
                    orderDetail.setProductLineId(String.valueOf(cartModels.get(i).getProductLineId()));

                } else if (cartModels.get(i).getProductType() == CartModel.product.MEDICAL_PACKAGE) {

                    orderDetail.setIsHomeSampleCollection("true");
                    orderDetail.setVendorLocationId("0");
                    orderDetail.setLabId(String.valueOf(cartModels.get(i).getSelectedLabId()));
                    orderDetail.setProductLineId(String.valueOf(cartModels.get(i).getProductLineId()));

                }

                orderDetails.add(orderDetail);

            }

            finalOrderDataModel.setOrderDetails(orderDetails);
        }


        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), new Gson().toJson(finalOrderDataModel));

        //showProgressDialog("Please Wait");
        try {
            Call<ResponseBody> posts = RestAPIFactory.getApi().saveOrder(Shifa4U.mySharePrefrence.getLogin().getTokenType() + " " + Shifa4U.mySharePrefrence.getLogin().getAccessToken(), body);
            posts.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    dismissProgressDislog();
                    isOrderSuccess = false;
                    try {

                        if (response.code() == 200) {
                            //{"Status":"Success","NetTotal":"16039.4","OrderId":"18Z10Z150000535"}
                            JSONObject jsonObject = new JSONObject(response.body().string());
                            if (jsonObject.optString("Status").equalsIgnoreCase("Success")) {

                                txt_order_number.setText("Your Order # " + jsonObject.optString("OrderId"));
                                isOrderSuccess = true;

                                Toast.makeText(PaymentModeActivity.this, "Order Placed Successfully", Toast.LENGTH_SHORT).show();

                                if (selectedPaymentMethod == 2)
                                {
                                    Intent intent = new Intent(PaymentModeActivity.this, EasyPaisaActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                    intent.putExtra("OrderId", jsonObject.optString("OrderId"));
                                    startActivity(intent);
                                    finish();
                                    //startActivity(new Intent(PaymentModeActivity.this , EasyPaisaActivity.class));

                                }
                                else
                                {
                                    Shifa4U.mySharePrefrence.clearCart();
                                    ll_order_complete.setVisibility(View.VISIBLE);
                                    scrollView.setVisibility(View.GONE);
                                }

                            } else {
                                Toast.makeText(PaymentModeActivity.this, "Something went wrong.\n Please try again later", Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            Toast.makeText(PaymentModeActivity.this, "Something went wrong.\n Please try again later", Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


//
//                ItemModel itemModel = LoganSquare.parse(new GZIPInputStream(response.body().byteStream()), ItemModel.class);

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    dismissProgressDislog();
                    Log.e("", "");
                }
            });

        } catch (Exception e) {

        }

    }

/*    @TargetApi(Build.VERSION_CODES.M)
    private void downLoadVoucher() {
        if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            // Log.v("", "Permission is granted");
            ActivityCompat.requestPermissions(PaymentModeActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
            //File write logic here

        } else {
            request = new InputStreamVolleyRequest(Request.Method.GET, fileUrl , new com.android.volley.Response.Listener<byte[]>() {
                @Override
                public void onResponse(byte[] response) {
                    try {
                        if (response != null) {

                            String filename = labtestResultModels.get(position).getPaths();

                            try {
                                long lenghtOfFile = response.length;

                                //covert reponse to input stream
                                InputStream input = new ByteArrayInputStream(response);
                                File path = Environment.getExternalStorageDirectory();
                                File file;
                                if (path.exists()) {
                                    file = new File(path, filename);
                                } else {
                                    path.mkdirs();
                                    file = new File(path, filename);
                                }
                                //  map.put("resume_path", file.toString());

                                if (!file.exists()) {
                                    file.createNewFile();
                                }
                                BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(file));
                                byte data[] = new byte[1024];

                                long total = 0;

                                while ((count = input.read(data)) != -1) {
                                    total += count;
                                    output.write(data, 0, count);
                                }

                                output.flush();

                                output.close();
                                input.close();
                                Toast.makeText(PaymentModeActivity.this, "Voucher downloaded Successfully", Toast.LENGTH_SHORT).show();
                                dismissProgressDislog();
                            } catch (IOException e) {
                                dismissProgressDislog();
                                e.printStackTrace();
                            }


                               *//* FileOutputStream outputStream;
                                String name=labtestResultModels.get(position).getPaths()   ;
                                outputStream = context.openFileOutput(name, Context.MODE_PRIVATE);
                                outputStream.write(response);
                                outputStream.close();
                                Toast.makeText(context, "Download complete.", Toast.LENGTH_LONG).show();
               *//*
                        }
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        Log.d("KEY_ERROR", "UNABLE TO DOWNLOAD FILE");
                        dismissProgressDislog();
                        e.printStackTrace();
                    }


                }
            }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    dismissProgressDislog();

                }
            }, null);
            RequestQueue mRequestQueue = Volley.newRequestQueue(PaymentModeActivity.this,
                    new HurlStack());
            mRequestQueue.add(request);
            showProgressDialog("Please wait while file is being downloaded");
        }

    }*/


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(isOrderSuccess)
        {
           startActivity(new Intent(PaymentModeActivity.this,MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
           finish();
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
        }else
        {
            finish();
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
        }
    }


}
