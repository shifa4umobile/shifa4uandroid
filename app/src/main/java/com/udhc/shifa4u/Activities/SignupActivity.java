package com.udhc.shifa4u.Activities;

import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.udhc.shifa4u.Interfaces.MenuItemsInterface;
import com.udhc.shifa4u.Models.AreasModel;
import com.udhc.shifa4u.Models.CityModel;
import com.udhc.shifa4u.Models.MenuModel;
import com.udhc.shifa4u.Models.SignUpModel;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.CustomWebViewClient;
import com.udhc.shifa4u.Utilities.GZipRequest;
import com.udhc.shifa4u.Utilities.HeaderManager;
import com.udhc.shifa4u.Utilities.Utility;
import com.udhc.shifa4u.api.RestAPIFactory;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignupActivity extends AppCompatActivity {

    private TextView txt_terms_of_condition;
    private View include_sign_up_first;
    private View include_sign_up_second;

    String[] gender = new String[]{"Male", "Female"};
    private EditText txt_first_name;
    private EditText txt_last_name;
    private EditText txt_email;
    private EditText txt_password;
    private EditText txt_confirm_password;
    private AutoCompleteTextView txt_gender;
    private TextView txt_dob;
    private Button btn_sign_up_first;
    private SignUpModel signUpModel = null;
    private Spinner spnr_area;
    private Spinner spnr_city;
    private EditText txt_address;
    private EditText txt_mobile_number;
    private Button btn_sign_up_final;
    private Button btn_skip;
    private ProgressDialog progressDialog;
    private TextView txt_terms_of_condition_second;
   // private Context _context;
    List<String> areaList;

    String AreaUrl = "https://api.shifa4u.com/api/common/v1/getareasforcity?cityId=";
    List <AreasModel> areasModelList;
    RequestQueue requestQueue;
    private String AreaId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        try {

            requestQueue = Volley.newRequestQueue(this);

            HeaderManager headerManager = new HeaderManager(SignupActivity.this, findViewById(R.id.header));
            headerManager.setMenutype(HeaderManager.BACK_MENU);
            headerManager.setShowFirstImageButton(false);
            headerManager.setBackMenuButtonVisibility(false);
            headerManager.setShowSecoundImageButton(false);
            headerManager.setTitle(getString(R.string.sign_up_title));

            txt_terms_of_condition = (TextView) findViewById(R.id.txt_terms_of_condition);
            txt_terms_of_condition_second = (TextView) findViewById(R.id.txt_terms_of_condition_second);

            include_sign_up_first = (View) findViewById(R.id.include_sign_up_first);
            include_sign_up_second = (View) findViewById(R.id.include_sign_up_second);
            txt_first_name = (EditText) findViewById(R.id.txt_first_name);
            txt_last_name = (EditText) findViewById(R.id.txt_last_name);
            txt_email = (EditText) findViewById(R.id.txt_email);
            txt_password = (EditText) findViewById(R.id.txt_password);
            txt_confirm_password = (EditText) findViewById(R.id.txt_confirm_password);
            txt_gender = (AutoCompleteTextView) findViewById(R.id.txt_gender);
            txt_dob = (TextView) findViewById(R.id.txt_dob);
            btn_sign_up_first = (Button) findViewById(R.id.btn_sign_up_first);

            spnr_area = (Spinner) findViewById(R.id.spnr_area);
            spnr_city = (Spinner) findViewById(R.id.spnr_city);
            txt_address = (EditText) findViewById(R.id.txt_address);
            txt_mobile_number = (EditText) findViewById(R.id.txt_mobile_number);
            btn_sign_up_final = (Button) findViewById(R.id.btn_sign_up_final);
            btn_skip = (Button) findViewById(R.id.btn_skip);

            ArrayAdapter<String> genderAdapter = new ArrayAdapter<String>(SignupActivity.this, R.layout.spinner_item, Arrays.asList(gender));
            txt_gender.setAdapter(genderAdapter);
            txt_gender.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    txt_gender.showDropDown();
                }
            });
            txt_gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Toast.makeText(SignupActivity.this, "ashaskdjh", Toast.LENGTH_SHORT).show();
//                ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);
//                ((TextView) parent.getChildAt(0)).setTextSize(getResources().getDimension(R.dimen._5sdp));
//                ((TextView) parent.getChildAt(0)).setPadding(0, 0, 0, 0);
//                ((TextView) parent.getChildAt(0)).setTypeface(null, Typeface.NORMAL);

                 /*   txt_gender.setText(Arrays.asList(gender).get(position - 1));
                    txt_gender.dismissDropDown();*/
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            String linkText = "By clicking sign up you confirm that you have read and agreed with the Terms and Conditions and the Privacy Policy.";

            SpannableString ss = new SpannableString(linkText);
            ClickableSpan termsAndCondition = new ClickableSpan() {
                @Override
                public void onClick(View widget) {
                    final Dialog dialog = new Dialog(SignupActivity.this);
                    dialog.setContentView(R.layout.fragment_static_pages);
                    dialog.setTitle("Terms And Conditions");
                    String htmlFilename = "";
                    htmlFilename = "terms_and_condition.html";
                    WebView wv_static_pages = (WebView) dialog.findViewById(R.id.wv_static_pages);
                    wv_static_pages.setWebViewClient(new CustomWebViewClient(getBaseContext()));
                    AssetManager mgr = getBaseContext().getAssets();
                    try {
                        InputStream in = mgr.open(htmlFilename, AssetManager.ACCESS_BUFFER);
                        String htmlContentInStringFormat = StreamToString(in);
                        in.close();
                        wv_static_pages.loadDataWithBaseURL(null, htmlContentInStringFormat, "text/html", "utf-8", null);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    dialog.show();
                    //    Toast.makeText(SignupActivity.this, "Terms and condition", Toast.LENGTH_LONG).show();
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setColor(Color.WHITE);
                }
            };

            ClickableSpan privacyPolicy = new ClickableSpan() {
                @Override
                public void onClick(View widget) {
                    final Dialog dialog = new Dialog(SignupActivity.this);
                    dialog.setContentView(R.layout.fragment_static_pages);
                    dialog.setTitle("Privacy Policy");
                    String htmlFilename = "";
                    htmlFilename = "privacy_policy.html";
                    WebView wv_static_pages = (WebView) dialog.findViewById(R.id.wv_static_pages);
                    wv_static_pages.setWebViewClient(new CustomWebViewClient(getBaseContext()));
                    AssetManager mgr = getBaseContext().getAssets();
                    try {
                        InputStream in = mgr.open(htmlFilename, AssetManager.ACCESS_BUFFER);
                        String htmlContentInStringFormat = StreamToString(in);
                        in.close();
                        wv_static_pages.loadDataWithBaseURL(null, htmlContentInStringFormat, "text/html", "utf-8", null);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    dialog.show();
                    //     Toast.makeText(SignupActivity.this, "Privacy Policy", Toast.LENGTH_LONG).show();
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setColor(Color.WHITE);
                }
            };
            ss.setSpan(termsAndCondition, 71, 91, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            ss.setSpan(privacyPolicy, 100, 114, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            txt_terms_of_condition.setText(ss);
            txt_terms_of_condition.setMovementMethod(LinkMovementMethod.getInstance());
            txt_terms_of_condition_second.setText(ss);
            txt_terms_of_condition_second.setMovementMethod(LinkMovementMethod.getInstance());
            txt_dob.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDatePicker();
                }
            });


            btn_sign_up_first.setOnClickListener(new View.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.GINGERBREAD)
                @Override
                public void onClick(View v) {

                    try {
                        boolean isValidEmail = android.util.Patterns.EMAIL_ADDRESS.matcher(txt_email.getText().toString().trim()).matches();
                        String myString = txt_gender.getText().toString();
                        if (!txt_first_name.getText().toString().isEmpty()) {
                            if (!txt_last_name.getText().toString().isEmpty()) {
                                if (!txt_email.getText().toString().isEmpty()) {
                                    if (isValidEmail) {

                                        if (!txt_mobile_number.getText().toString().isEmpty())
                                        {
                                        if (!txt_password.getText().toString().isEmpty() && txt_password.getText().toString().length() >= 6) {
                                            if (!txt_password.getText().toString().isEmpty()) {
                                                if (txt_confirm_password.getText().toString().equals(txt_password.getText().toString())) {
                                                    if (!txt_dob.getText().toString().isEmpty()) {
                                                        if (!txt_gender.getText().toString().equals("")) {
                                                            fillSignUpModelFirst();
                                                        } else {
                                                            Toast.makeText(SignupActivity.this, "Please select Gender", Toast.LENGTH_LONG).show();
                                                        }

                                                    } else {
                                                        Toast.makeText(SignupActivity.this, "Please select Date of birth", Toast.LENGTH_LONG).show();
                                                    }
                                                } else {
                                                    Toast.makeText(SignupActivity.this, "Password mismatch", Toast.LENGTH_LONG).show();
                                                }
                                            } else {
                                                Toast.makeText(SignupActivity.this, "Please enter confirm password", Toast.LENGTH_LONG).show();
                                            }
                                        } else {
                                            Toast.makeText(SignupActivity.this, "Please enter password. Password  length should be atleast 6", Toast.LENGTH_LONG).show();
                                        }
                                    }
                                        else {
                                            Toast.makeText(SignupActivity.this, "Please enter Phone Number", Toast.LENGTH_LONG).show();
                                        }
                                }

                                    else {
                                        Toast.makeText(SignupActivity.this, "Please enter valid email", Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    Toast.makeText(SignupActivity.this, "Please enter email", Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Toast.makeText(SignupActivity.this, "Please enter last name", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(SignupActivity.this, "Please enter first name", Toast.LENGTH_LONG).show();
                        }

                    } catch (Exception e) {
                        Log.i("", "onClick: " + e.getMessage());
                    }
                }
            });


            btn_sign_up_final.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (Shifa4U.checkNetwork.isInternetAvailable()) {
                        if (spnr_city.getSelectedItemPosition() !=0 && spnr_area.getSelectedItemPosition() == 0)
                        {
                            Toast.makeText(SignupActivity.this, "Please select area too", Toast.LENGTH_SHORT).show();
                        }
                        else if (spnr_area.getSelectedItemPosition() != 0 && spnr_city.getSelectedItemPosition() == 0)
                        {
                            Toast.makeText(SignupActivity.this, "Please select City too", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            fillSignUpModelSecond();
                        }
                    } else {
                        Toast.makeText(SignupActivity.this, R.string.internet_not_available, Toast.LENGTH_SHORT).show();
                    }
                }
            });

            btn_skip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Shifa4U.checkNetwork.isInternetAvailable()) {
                        fillSignUpModelSecond();
                    } else {
                        Toast.makeText(SignupActivity.this, R.string.internet_not_available, Toast.LENGTH_SHORT).show();
                    }
                }
            });

            final List<CityModel> cities = Shifa4U.mySharePrefrence.getCities();
            if (cities != null) {
                ArrayAdapter<String> dataAdapter;
                ArrayAdapter<String> dataAdapter2;
                List<String> cityList = new ArrayList<>();
                areaList = new ArrayList<>();
                cityList.add("Select City");

                for (int i = 0; i < cities.size(); i++) {
                    cityList.add(cities.get(i).getCityName());
                }
             //   areaList.add("Select Area");
                dataAdapter = new ArrayAdapter<String>(SignupActivity.this, android.R.layout.simple_spinner_item, cityList);
             //   dataAdapter2 = new ArrayAdapter<String>(SignupActivity.this, android.R.layout.simple_spinner_item, areaList);
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
              //  dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spnr_city.setAdapter(dataAdapter);
              //  spnr_area.setAdapter(dataAdapter2);
                //  spnr_city.setAdapter(new CityAdapter(SignupActivity.this, Shifa4U.mySharePrefrence.getCities(),menuItemsInterface));
                spnr_city.setSelection(Shifa4U.mySharePrefrence.getSelectedCityIndex() + 1);
            }


            spnr_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);
                    ((TextView) parent.getChildAt(0)).setTextSize(getResources().getDimension(R.dimen._5sdp));
                    ((TextView) parent.getChildAt(0)).setPadding(0, 0, 0, 0);
                    ((TextView) parent.getChildAt(0)).setTypeface(null, Typeface.NORMAL);

                if (cities != null&& position !=0)
               getArea(SignupActivity.this, cities.get(position-1).getCityId());

                    // List <CityModel> cities  = Shifa4U.mySharePrefrence.getCities();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            spnr_area.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);
                    ((TextView) parent.getChildAt(0)).setTextSize(getResources().getDimension(R.dimen._5sdp));
                    ((TextView) parent.getChildAt(0)).setPadding(0, 0, 0, 0);
                    ((TextView) parent.getChildAt(0)).setTypeface(null, Typeface.NORMAL);
                    // List <CityModel> cities  = Shifa4U.mySharePrefrence.getCities();

                    if (position!=0)
                    AreaId = areasModelList.get(position-1).getAreaId();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
        catch (Exception e)
        {
            Log.i("", "onCreate: " + e.getMessage());
        }
    }

    private void fillSignUpModelFirst() {
        signUpModel = new SignUpModel();
        signUpModel.setFirstName(txt_first_name.getText().toString());
        signUpModel.setLastName(txt_last_name.getText().toString());
        signUpModel.setEmail(txt_email.getText().toString());
        signUpModel.setLoginName(txt_email.getText().toString());
        signUpModel.setPassword(txt_password.getText().toString());
        signUpModel.setConfirmPassword(txt_confirm_password.getText().toString());
        signUpModel.setGender(txt_gender.getText().toString());

        signUpModel.setDateOfBirth(txt_dob.getText().toString());
        signUpModel.setFatherName("");
        include_sign_up_first.setVisibility(View.GONE);
        include_sign_up_second.setVisibility(View.VISIBLE);
    }

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    private void fillSignUpModelSecond() {
     //   signUpModel.setAreaId(spnr_area.getSelectedItem() == null ? "" : spnr_area.getSelectedItem().toString());
        signUpModel.setAddress(txt_address.getText().toString().isEmpty() ? "" : txt_address.getText().toString());
        signUpModel.setPhoneNumber(txt_mobile_number.getText().toString().isEmpty() ? "" : txt_mobile_number.getText().toString());
        signUpModel.setAreaId(AreaId);

            if (Shifa4U.checkNetwork.isInternetAvailable()) {
                register();
            } else {
                Toast.makeText(SignupActivity.this, R.string.internet_not_available, Toast.LENGTH_SHORT).show();
            }

    }


    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void showDatePicker() {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        txt_dob.setText((monthOfYear + 1) + "/" + dayOfMonth + "/" + year);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
    }



//    public void getAreasForCity() {
//
//        showProgressDialog("Please wait...");
////        Gson gson = new Gson();
////        Type type = new TypeToken<SignUpModel>() {
////        }.getType();
////        String json = gson.toJson(signUpModel, type);
////
////        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);
//
//        //showProgressDialog("Please Wait");
//        try {
//            Call<ResponseBody> posts = RestAPIFactory.getApi().getAreasForCity(body);
//            posts.enqueue(new Callback<ResponseBody>() {
//                @Override
//                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                    if (response.code() == 200) {
//                        if (response.message().toString().equals("Conflict")) {
//                            Toast.makeText(SignupActivity.this, "The Information you have provided is already on server", Toast.LENGTH_LONG).show();
//                        } else {
//                            startActivity(new Intent(SignupActivity.this, VerificationActivity.class));
//                            finish();
//                        }
//                    } else {
//                        Toast.makeText(SignupActivity.this, "Something went wrong.\n Please try again later", Toast.LENGTH_LONG).show();
//                    }
//
//                    dismissProgressDislog();
//                }
//
//                @Override
//                public void onFailure(Call<ResponseBody> call, Throwable t) {
//                    dismissProgressDislog();
//                }
//            });
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//
//    }


    public void register() {

        showProgressDialog("Please wait...");
        Gson gson = new Gson();
        Type type = new TypeToken<SignUpModel>() {
        }.getType();
        String json = gson.toJson(signUpModel, type);

        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), json);

        //showProgressDialog("Please Wait");
        try {
            Call<ResponseBody> posts = RestAPIFactory.getApi().mobileRegistration(body);
            posts.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.code() == 200) {
                        String email = txt_email.getText().toString().trim();
                        Intent intent = new Intent(SignupActivity.this, VerificationActivity.class);
                        intent.putExtra("email", email);
                            startActivity(intent);
                            finish();
                        }
                        else if (response.code() == 409)
                    {
                        Toast.makeText(SignupActivity.this, "User Already Exists", Toast.LENGTH_LONG).show();

                    }
                     else {
                        Toast.makeText(SignupActivity.this, "Something went wrong. Please try again later!!", Toast.LENGTH_LONG).show();
                    }

                    dismissProgressDislog();
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    dismissProgressDislog();
                    Toast.makeText(SignupActivity.this, "Something went wrong. Please try again", Toast.LENGTH_LONG).show();

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(SignupActivity.this, "Exception +" + e.getMessage(), Toast.LENGTH_LONG).show();

        }


    }


    public void showProgressDialog(String message) {
        if (progressDialog == null)
            progressDialog = new ProgressDialog(SignupActivity.this);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    public void dismissProgressDislog() {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }


    private MenuItemsInterface menuItemsInterface=new MenuItemsInterface() {
        @Override
        public void onItemClick(Integer position, MenuModel menuModel) {

        }

        @Override
        public void onButtonClick(Integer position, Object model, View button) {

        }

        @Override
        public void onItemClick(Integer position, Object o) {

        }
    };

    private void getArea(final Context context , int id) {
        StringRequest stringRequest = new GZipRequest(Request.Method.GET, AreaUrl+id, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                dismissProgressDislog();
                String res = response;

                areasModelList = new ArrayList<>();
                Type type = new TypeToken<List<AreasModel>>() {
                }.getType();
                areasModelList = (ArrayList) Utility.parseJSON(response, type);
                if (areasModelList != null) {


                    areaList.clear();
                    areaList.add("Select Area");
                    for (int i = 0; i < areasModelList.size(); i++) {
                        areaList.add(areasModelList.get(i).getAreaName());
                    }



                    ArrayAdapter<String> dataAdapter;
                    if (spnr_city != null) {
                        dataAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, areaList);

                    } else {
                        areaList = new ArrayList<>();
                        dataAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, areaList);

                    }

                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    // dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spnr_area.setAdapter(dataAdapter);
                }


                }


        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissProgressDislog();
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                HashMap<String, String> params2 = new HashMap<String, String>();
                return new JSONObject(params2).toString().getBytes();
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };

        requestQueue.add(stringRequest);
        showProgressDialog("");
    }



    @Override
    public void onBackPressed() {
        if (include_sign_up_first.getVisibility() == View.GONE)
        {
            include_sign_up_first.setVisibility(View.VISIBLE);
            include_sign_up_second.setVisibility(View.GONE);
        }
        else {
            super.onBackPressed();

            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
        }
    }

    public static String StreamToString(InputStream in) throws IOException {
        if(in == null) {
            return "";
        }
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        } finally {
        }
        return writer.toString();
    }
}
