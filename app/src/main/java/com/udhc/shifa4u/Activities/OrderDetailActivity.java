package com.udhc.shifa4u.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.udhc.shifa4u.Adapters.MyOrderDetailAdapter;
import com.udhc.shifa4u.Interfaces.MenuItemsInterface;
import com.udhc.shifa4u.Models.MenuModel;
import com.udhc.shifa4u.Models.MyOrdersMode.AllOrdersModel;
import com.udhc.shifa4u.Models.MyOrdersMode.OpenOrder;
import com.udhc.shifa4u.Models.MyOrdersMode.OrderDetailsVM;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.HeaderManager;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class OrderDetailActivity extends AppCompatActivity {

    private HeaderManager headerManager;
    private ListView lv_order_detail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);
        setHeader();

        String orderJson = getIntent().getStringExtra("orderModel");

        TypeToken type = new TypeToken<OpenOrder>() {
        };
        OpenOrder orderModel = new Gson().fromJson(orderJson, type.getType());
        Log.e("", orderModel.toString());

        lv_order_detail = (ListView) findViewById(R.id.lv_order_detail);

        if (orderModel != null) {


            ArrayList<OrderDetailsVM> orderDetailsVMS = (ArrayList<OrderDetailsVM>) orderModel.getOrderDetailsVM();

            View footerView = LayoutInflater.from(OrderDetailActivity.this).inflate(R.layout.my_order_detail_footer, null);

            TextView txt_order_footer_date=(TextView)footerView.findViewById(R.id.txt_order_footer_date);
            TextView txt_order_footer_order_number=(TextView)footerView.findViewById(R.id.txt_order_footer_order_number);
            TextView txt_footer_payment_mode=(TextView)footerView.findViewById(R.id.txt_footer_payment_mode);
            TextView txt_footer_payment_status=(TextView)footerView.findViewById(R.id.txt_footer_payment_status);
            TextView txt_order_footer_price=(TextView)footerView.findViewById(R.id.txt_order_footer_price);

            txt_order_footer_date.setText(orderModel.getOrderDate());
            txt_order_footer_order_number.setText(orderModel.getOrderId());
            txt_footer_payment_mode.setText(orderModel.getPaymentMethod());
            txt_footer_payment_status.setText(orderModel.getPaymentStatus());
            txt_order_footer_price.setText("Rs. "+orderModel.getTotalAmount());


            if (orderDetailsVMS != null && orderDetailsVMS.size() > 0) {
                lv_order_detail.setAdapter(new MyOrderDetailAdapter(OrderDetailActivity.this, orderDetailsVMS, new MenuItemsInterface() {
                    @Override
                    public void onItemClick(Integer position, MenuModel menuModel) {

                    }

                    @Override
                    public void onButtonClick(Integer position, Object model, View button) {

                    }

                    @Override
                    public void onItemClick(Integer position, Object o) {

                    }
                }));

                lv_order_detail.addFooterView(footerView);
            }
        }
    }


    private void setHeader() {
        headerManager = new HeaderManager(OrderDetailActivity.this, findViewById(R.id.header));
        headerManager.setMenutype(HeaderManager.BACK_MENU);
        headerManager.setShowFirstImageButton(false);
        headerManager.setShowSecoundImageButton(false);
        headerManager.setTitle(getString(R.string.order_detail_title));
//        headerManager.setShowBottomView(true);
        headerManager.setHeaderColor(getResources().getColor(R.color.header_background_color));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }
}
