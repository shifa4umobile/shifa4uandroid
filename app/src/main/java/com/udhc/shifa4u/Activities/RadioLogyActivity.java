package com.udhc.shifa4u.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.udhc.shifa4u.Adapters.RadilogyCategories.BodyLocationAdapter;
import com.udhc.shifa4u.Adapters.RadilogyCategories.BodyLocationPartAdapter;
import com.udhc.shifa4u.Adapters.RadilogyCategories.BodyLocationPartTypeAdapter;
import com.udhc.shifa4u.Adapters.RadioLogyAdapter;
import com.udhc.shifa4u.Interfaces.MenuItemsInterface;
import com.udhc.shifa4u.Models.CartModel;
import com.udhc.shifa4u.Models.MenuModel;
import com.udhc.shifa4u.Models.RadioLogyDetail.RadioLogyDetailModel;
import com.udhc.shifa4u.Models.Radiology;
import com.udhc.shifa4u.Models.RadiologyCategories.BodyLocation;
import com.udhc.shifa4u.Models.RadiologyCategories.BodyLocationPart;
import com.udhc.shifa4u.Models.RadiologyCategories.BodyLocationPartType;
import com.udhc.shifa4u.Models.RadiologyModel;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.GZipRequest;
import com.udhc.shifa4u.Utilities.GzipUtils;
import com.udhc.shifa4u.Utilities.HeaderManager;
import com.udhc.shifa4u.Utilities.MyGeneric;
import com.udhc.shifa4u.Utilities.MyUtils;
import com.udhc.shifa4u.Utilities.Utility;
import com.udhc.shifa4u.api.RestAPIFactory;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RadioLogyActivity extends AppCompatActivity {

    private ListView lv_radio_logy;
    private Button img_search;
    //    private EditText txt_search;
    private ProgressDialog progressDialog;
    private String searchQuery;
    private ArrayList<Radiology> radiologyModels = new ArrayList<>();
    private boolean flag_loading = false;
    private int skipPages = 0;
    private RadioLogyAdapter radioLogyAdapter;
    private TextView txt_no_detail;
    private LinearLayout ll_main;
    private AutoCompleteTextView txt_category;
    private AutoCompleteTextView txt_body_location;
    private AutoCompleteTextView txt_sub_location;
    private AutoCompleteTextView txt_type;
    private Button btn_search_categories;
    private ImageView img_open_categories;
    private LinearLayout ll_container;
    private Integer imagingCategoryId;
    private Integer bodyLocationId;
    private Integer bodyLocationPartId;
    private Integer bodyLocationPartTypeId;
    private boolean isPagination = true;

    String myUrl = "https://api.shifa4u.com/api/Imaging/v1/paginateradilogy";
    //String myUrl = "https://api.shifa4u.com/api/Imaging/v1/paginateandfilterradiology";

    RequestQueue requestQueue;

    int totalRadioligyRecords = 0;


    List<Integer> rediologyIds = new ArrayList<>();
    private TextView txt_search_via;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_radio_logy);
        MyUtils.hideKeyboard(this);


        imagingCategoryId = getIntent().getIntExtra("categoryID", -1);

        lv_radio_logy = (ListView) findViewById(R.id.lv_radio_logy);
//        img_search = (Button) findViewById(R.id.img_search);
//        txt_search = (EditText) findViewById(R.id.txt_search);

        requestQueue = Volley.newRequestQueue(this); // 'this' is the Context

        txt_search_via = (TextView) findViewById(R.id.txt_search_via);
        txt_category = (AutoCompleteTextView) findViewById(R.id.txt_category);
        txt_body_location = (AutoCompleteTextView) findViewById(R.id.txt_body_location);
        txt_sub_location = (AutoCompleteTextView) findViewById(R.id.txt_sub_location);
        txt_type = (AutoCompleteTextView) findViewById(R.id.txt_type);
        btn_search_categories = (Button) findViewById(R.id.btn_search_categories);
        img_open_categories = (ImageView) findViewById(R.id.img_open_categories);
        ll_container = (LinearLayout) findViewById(R.id.ll_container);

        setHeader();
        img_open_categories.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ll_container.getTag().toString().equalsIgnoreCase("0")) {
                    ll_container.setVisibility(View.VISIBLE);
                    ll_container.setTag("1");
                    img_open_categories.setRotation(180);
//                    if(txt_category.getAdapter()==null)
//                    getAllImagingCategories();
                } else {
                    ll_container.setVisibility(View.GONE);
                    ll_container.setTag("0");
                    img_open_categories.setRotation(0);
                }
            }
        });

        txt_category.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                txt_category.showDropDown();
            }
        });
        txt_body_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_body_location.showDropDown();
            }
        });
        txt_sub_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_sub_location.showDropDown();
            }
        });
        txt_type.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                txt_type.showDropDown();
            }
        });


        txt_no_detail = (TextView) findViewById(R.id.txt_no_detail);
        ll_main = (LinearLayout) findViewById(R.id.ll_main);
        if (Shifa4U.checkNetwork.isInternetAvailable()) {
            //  getPaginatedRadioligyViaVolley(String.valueOf(skipPages));
//            getPaginatedRadioligyViaVolley(skipPages , RadioLogyActivity.this);
            radiologyModels = new ArrayList<>();
            Paginateandfilterradiology(skipPages);
        } else {
            Toast.makeText(RadioLogyActivity.this, R.string.network_not_available, Toast.LENGTH_SHORT).show();
        }
//        img_search.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (Shifa4U.checkNetwork.isInternetAvailable()) {
//                    searchQuery = "";
//                    radiologyModels = new ArrayList<>();
//                    lv_radio_logy.setAdapter(null);
//                    isPagination=true;
//                    if (!txt_search.getText().toString().isEmpty()) {
//                        searchQuery = txt_search.getText().toString();
//                        GetLabImagingsBySearchKeyWord(searchQuery);
//                    } else {
//                        getPaginatedRadioligyViaVolley(skipPages , RadioLogyActivity.this);
//                      //  getPaginateRadilogy(String.valueOf(0));
//                    }
//                } else {
//                    Toast.makeText(RadioLogyActivity.this, R.string.network_not_available, Toast.LENGTH_SHORT).show();
//                }
//
//
//            }
//        });

//        btn_search_categories.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (Shifa4U.checkNetwork.isInternetAvailable()) {
//                    isPagination=true;
//
//                    if (ll_container.getVisibility()==View.VISIBLE) {
//                        ll_container.setVisibility(View.GONE);
//                        ll_container.setTag("0");
//                    }
//                    radiologyModels=new ArrayList<>();
//                    skipPages=0;
//                    Paginateandfilterradiology(skipPages);
//
//                } else {
//                    Toast.makeText(RadioLogyActivity.this, R.string.network_not_available, Toast.LENGTH_SHORT).show();
//                }
//            }
//        });


        lv_radio_logy.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
                    if (Shifa4U.checkNetwork.isInternetAvailable()) {
                        if (flag_loading == false && isPagination) {
                            flag_loading = true;

                            skipPages++;
                            //  getPaginatedRadioligyViaVolley(skipPages , RadioLogyActivity.this);
                            Paginateandfilterradiology(skipPages);
                        }
                    } else {
                        Toast.makeText(RadioLogyActivity.this, R.string.network_not_available, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        HeaderManager headerManager = new HeaderManager(RadioLogyActivity.this, findViewById(R.id.header));
        headerManager.setMenutype(HeaderManager.BACK_MENU);
        headerManager.setShowFirstImageButton(false);
        headerManager.setShowSecoundImageButton(false);
        headerManager.setTitle(getString(R.string.radio_logy_title) + " - " + getIntent().getStringExtra("categoryName"));
        headerManager.setShowBottomView(true);
        txt_search_via.setText(getIntent().getStringExtra("categoryName"));
        headerManager.setHeaderColor(getResources().getColor(R.color.header_background_color));
    }

    private void setHeader() {
        HeaderManager headerManager = new HeaderManager(RadioLogyActivity.this, findViewById(R.id.header));
        headerManager.setMenutype(HeaderManager.BACK_MENU);
        headerManager.setShowFirstImageButton(false);
        headerManager.setShowSecoundImageButton(false);
        headerManager.setTitle(getString(R.string.radio_logy_title) + " - " + getIntent().getStringExtra("categoryName"));
        headerManager.setShowBottomView(true);
        txt_search_via.setText(getIntent().getStringExtra("categoryName"));
        headerManager.setHeaderColor(getResources().getColor(R.color.header_background_color));
    }

    private void getPaginateRadilogy(String page_to_skip) {
        showProgressDialog("Please wait...");
        String str = String.format("{\"SkipPages\":\"%s\",\"Records\":\"10\",\"CityId\":\"%s\"}", String.valueOf(page_to_skip), Shifa4U.mySharePrefrence.getCityId());
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), str);

        try {
            Call<ResponseBody> posts = RestAPIFactory.getApi().getPaginateRadilogy(body);
            posts.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> radiologyModelResponse) {
                    dismissProgressDislog();
                    try {

                        try {
                            String Json = GzipUtils.decompress(radiologyModelResponse.body().bytes());

                            Type listType = new TypeToken<RadiologyModel>() {
                            }.getType();
                            RadiologyModel radiologyModel = new Gson().fromJson(Json, listType);

                            List<Integer> tempIds = new ArrayList<>();

                            for (int i = 0; i < radiologyModel.getRadiologies().size(); i++) {
                                tempIds.add(radiologyModel.getRadiologies().get(i).getImagingId());
                            }

                            if (radiologyModel.getRadiologies() != null && radiologyModel.getRadiologies().size() > 0) {

                                if (radiologyModels.size() > 0) {
                                    radiologyModels.addAll(radiologyModel.getRadiologies());
                                    radioLogyAdapter.notifyDataSetChanged();


                                    rediologyIds.addAll(tempIds);
                                    Set<Integer> setids = new HashSet<Integer>(rediologyIds);

                                    Set<Radiology> set = new HashSet<Radiology>(radiologyModels);


                                    if (setids.size() < rediologyIds.size()) {
                                        Toast.makeText(RadioLogyActivity.this, "Duplicate", Toast.LENGTH_SHORT).show();
                                    }

                                    if (set.size() < radiologyModels.size()) {
                                        Toast.makeText(RadioLogyActivity.this, "Duplicate", Toast.LENGTH_SHORT).show();
                                    }

                                    List<Integer> templist = new ArrayList<>();
                                    for (int i = 0; i < radiologyModels.size(); i++) {
                                        templist.add(radiologyModels.get(i).getImagingId());
                                    }


                                    System.out.println(462 + ": " + Collections.frequency(templist, 462));

                                } else {
                                    radiologyModels.addAll(radiologyModel.getRadiologies());
                                    radioLogyAdapter = new RadioLogyAdapter(RadioLogyActivity.this, radiologyModels, new MenuItemsInterface() {
                                        @Override
                                        public void onItemClick(Integer position, MenuModel menuModel) {

                                        }

                                        @Override
                                        public void onButtonClick(Integer position, Object model, View button) {
                                            if (button.getId() == R.id.btn_detail) {
                                                Intent i = new Intent(RadioLogyActivity.this, LabDetailActivity.class);
                                                i.putExtra("Item", radiologyModels.get(position));
                                                startActivity(i);
                                            } else if (button.getId() == R.id.btn_add_to_cart) {
                                                getRadioLogyDetailByID(String.valueOf(radiologyModels.get(position).getImagingId()));
                                            }


                                        }

                                        @Override
                                        public void onItemClick(Integer position, Object o) {

                                        }
                                    });
                                    lv_radio_logy.setAdapter(radioLogyAdapter);
                                }

                                flag_loading = false;
                            } else {
                                Toast.makeText(RadioLogyActivity.this, "No records Found", Toast.LENGTH_SHORT).show();
                            }


                        } catch (Exception e) {
                            txt_no_detail.setVisibility(View.VISIBLE);
                            ll_main.setVisibility(View.GONE);
                            e.printStackTrace();
                        }


                    } catch (Exception e) {
                        txt_no_detail.setVisibility(View.VISIBLE);
                        ll_main.setVisibility(View.GONE);
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    dismissProgressDislog();
                    txt_no_detail.setVisibility(View.VISIBLE);
                    ll_main.setVisibility(View.GONE);
                    Log.e("", "");
                }
            });

        } catch (Exception e) {

        }
    }


    private void GetLabImagingsBySearchKeyWord(String searchQuery) {


        showProgressDialog("Please wait...");
        String str;

        if (searchQuery != null && !searchQuery.isEmpty()) {
            str = String.format("{\"SkipPages\":\"%s\",\"Records\":\"10\",\"SearchKeyWord\":\"%s\",\"CityId\":\"%s\"}", String.valueOf(0), searchQuery, Shifa4U.mySharePrefrence.getCityId());


            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), str);

            try {
                Call<ResponseBody> posts = RestAPIFactory.getApi().GetLabImagingsBySearchKeyWord(body);
                posts.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> radiologyModelResponse) {
                        dismissProgressDislog();
                        try {

                            try {

                                String Json = GzipUtils.decompress(radiologyModelResponse.body().bytes());

                                Type listType = new TypeToken<List<Radiology>>() {
                                }.getType();

                           /*     Type listType = new TypeToken<Radiology>() {
                                }.getType();*/
                                List<Radiology> radiologies = (List<Radiology>) Utility.parseJSON(Json, listType);


                                if (radiologies != null && radiologies.size() > 0) {

                                    if (radiologyModels.size() > 0) {
                                        radiologyModels.addAll(radiologies);
                                        radioLogyAdapter.notifyDataSetChanged();
                                    } else {
                                        radiologyModels.addAll(radiologies);
                                        radioLogyAdapter = new RadioLogyAdapter(RadioLogyActivity.this, radiologyModels, new MenuItemsInterface() {
                                            @Override
                                            public void onItemClick(Integer position, MenuModel menuModel) {

                                            }

                                            @Override
                                            public void onButtonClick(Integer position, Object model, View button) {
                                                if (button.getId() == R.id.btn_detail) {
                                                    Intent i = new Intent(RadioLogyActivity.this, LabDetailActivity.class);
                                                    i.putExtra("Item", radiologyModels.get(position));
                                                    startActivity(i);
                                                } else if (button.getId() == R.id.btn_add_to_cart) {
                                                    getRadioLogyDetailByID(String.valueOf(radiologyModels.get(position).getImagingId()));
                                                }

                                            }

                                            @Override
                                            public void onItemClick(Integer position, Object o) {

                                            }
                                        });
                                        lv_radio_logy.setAdapter(radioLogyAdapter);
                                    }

                                    flag_loading = false;
                                } else {
//                                    txt_no_detail.setVisibility(View.VISIBLE);
//                                    ll_main.setVisibility(View.GONE);
                                    lv_radio_logy.setAdapter(null);
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
//                                txt_no_detail.setVisibility(View.VISIBLE);
//                                ll_main.setVisibility(View.GONE);
                                lv_radio_logy.setAdapter(null);
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                            txt_no_detail.setVisibility(View.VISIBLE);
                            ll_main.setVisibility(View.GONE);
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        txt_no_detail.setVisibility(View.VISIBLE);
                        ll_main.setVisibility(View.GONE);
                        dismissProgressDislog();
                        Log.e("", "");
                    }
                });

            } catch (Exception e) {

            }
        }
    }


    public void showProgressDialog(String message) {
        if (progressDialog == null)
            progressDialog = new ProgressDialog(RadioLogyActivity.this);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    public void dismissProgressDislog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }


    void getRadioLogyDetailByID(String labID) {
        showProgressDialog("Please wait...");
        String str = String.format("{\"MedicalProductId\":\"%s\",\"CityId\":\"%s\"}", String.valueOf(labID), Shifa4U.mySharePrefrence.getCityId());
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), str);
        try {
            Call<ResponseBody> posts = RestAPIFactory.getApi().getRadiologyDetailById(body);
            posts.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    dismissProgressDislog();
                    try {
                        RadioLogyDetailModel radioLogyDetailModels = MyGeneric.processResponse(response, new TypeToken<RadioLogyDetailModel>() {
                        });
                        if (radioLogyDetailModels != null) {
                            com.udhc.shifa4u.Models.RadioLogyDetail.Radiology _radiology = radioLogyDetailModels.getRadiologies().get(0);
                            CartModel cartModel = new CartModel();
                            cartModel.setOriginalObject(_radiology);
                            cartModel.setProductId(_radiology.getImagingId());
                            cartModel.setProductName(_radiology.getImagingName());
                            cartModel.setProductShortDescription(_radiology.getShortDescription());
                            cartModel.setProductType(CartModel.product.RADIOLOGY);
                            MyUtils.addToCart(cartModel);
                            startActivity(new Intent(RadioLogyActivity.this, CheckoutActivity.class));
                            // finish();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    dismissProgressDislog();
                    Log.e("", "");
                }
            });

        } catch (Exception e) {

        }

    }


//    void getAllImagingCategories() {
//        showProgressDialog("Please wait...");
//
//        try {
//            Call<ResponseBody> posts = RestAPIFactory.getApi().getAllImagingCategories();
//            posts.enqueue(new Callback<ResponseBody>() {
//                @Override
//                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                    dismissProgressDislog();
//                    try {
//                        final ArrayList<ImagingCategory> imagingCategories = MyGeneric.processResponse(response, new TypeToken<ArrayList<ImagingCategory>>() {
//                        });
//                        if (imagingCategories != null) {
//                            txt_category.setAdapter(new ImagingCategoryAdapter(RadioLogyActivity.this, imagingCategories, new MenuItemsInterface() {
//                                @Override
//                                public void onItemClick(Integer position, MenuModel menuModel) {
//
//                                }
//
//                                @Override
//                                public void onButtonClick(Integer position, Object model, View button) {
//
//                                }
//
//                                @Override
//                                public void onItemClick(Integer position, Object o) {
//                                    ImagingCategory imagingCategory = ((ImagingCategory) o);
//                                    txt_category.setText(imagingCategory.getName());
//                                    imagingCategoryId = imagingCategory.getImagingCategoryId();
//
//                                    txt_body_location.setText("");
//                                    txt_body_location.setAdapter(null);
//                                    bodyLocationId = null;
//
//                                    txt_sub_location.setText("");
//                                    txt_sub_location.setAdapter(null);
//                                    bodyLocationPartId = null;
//
//                                    txt_type.setText("");
//                                    txt_type.setAdapter(null);
//                                    bodyLocationPartTypeId = null;
//                                    BodyLocationByImagingCategoryId(imagingCategoryId);
//                                }
//                            }));
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<ResponseBody> call, Throwable t) {
//                    dismissProgressDislog();
//                    Log.e("", "");
//                }
//            });
//
//        } catch (Exception e) {
//
//        }
//
//    }

    void BodyLocationByImagingCategoryId(int categoryID) {
        showProgressDialog("Please wait...");
        String str = String.format("{\"ImagingCategoryId\":\"%s\"}", String.valueOf(categoryID));
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), str);
        try {
            Call<ResponseBody> posts = RestAPIFactory.getApi().BodyLocationByImagingCategoryId(body);
            posts.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    dismissProgressDislog();
                    try {
                        ArrayList<BodyLocation> bodyLocations = MyGeneric.processResponse(response, new TypeToken<ArrayList<BodyLocation>>() {
                        });
                        if (bodyLocations != null) {
                            txt_body_location.setAdapter(new BodyLocationAdapter(RadioLogyActivity.this, bodyLocations, new MenuItemsInterface() {
                                @Override
                                public void onItemClick(Integer position, MenuModel menuModel) {

                                }

                                @Override
                                public void onButtonClick(Integer position, Object model, View button) {

                                }

                                @Override
                                public void onItemClick(Integer position, Object o) {
                                    BodyLocation bodyLocation = ((BodyLocation) o);
                                    txt_body_location.setText(bodyLocation.getName());
                                    bodyLocationId = bodyLocation.getBodyLocationId();

                                    txt_sub_location.setText("");
                                    txt_sub_location.setAdapter(null);
                                    bodyLocationPartId = null;

                                    txt_type.setText("");
                                    txt_type.setAdapter(null);
                                    bodyLocationPartTypeId = null;


                                    BodyLocationPartByBodyLocationAndCategoryId(bodyLocationId);
                                }
                            }));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    dismissProgressDislog();
                    Log.e("", "");
                }
            });

        } catch (Exception e) {

        }

    }


    void BodyLocationPartByBodyLocationAndCategoryId(int BodyLocationId) {
        showProgressDialog("Please wait...");
        String str = String.format("{\"ImagingCategoryId\":\"%s\",\"BodyLocationId\":\"%s\"}", String.valueOf(imagingCategoryId), String.valueOf(bodyLocationId));
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), str);
        try {
            Call<ResponseBody> posts = RestAPIFactory.getApi().BodyLocationPartByBodyLocationAndCategoryId(body);
            posts.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    dismissProgressDislog();
                    if (Shifa4U.checkNetwork.isInternetAvailable()) {
                        isPagination = true;
                        radiologyModels = new ArrayList<>();
                        skipPages = 0;
                        Paginateandfilterradiology(skipPages);
                    } else {
                        Toast.makeText(RadioLogyActivity.this, R.string.network_not_available, Toast.LENGTH_SHORT).show();
                    }
                    try {
                        ArrayList<BodyLocationPart> bodyLocationParts = MyGeneric.processResponse(response, new TypeToken<ArrayList<BodyLocationPart>>() {
                        });
                        if (bodyLocationParts != null) {
                            txt_sub_location.setAdapter(new BodyLocationPartAdapter(RadioLogyActivity.this, bodyLocationParts, new MenuItemsInterface() {
                                @Override
                                public void onItemClick(Integer position, MenuModel menuModel) {

                                }

                                @Override
                                public void onButtonClick(Integer position, Object model, View button) {

                                }

                                @Override
                                public void onItemClick(Integer position, Object o) {
                                    BodyLocationPart bodyLocationPart = ((BodyLocationPart) o);
                                    txt_sub_location.setText(bodyLocationPart.getName());
                                    bodyLocationPartId = bodyLocationPart.getBodyLocationPartId();

                                    txt_type.setText("");
                                    txt_type.setAdapter(null);
                                    bodyLocationPartTypeId = null;

                                    BodyLocationPartTypeByCategoryAndOthers(bodyLocationPartId);
                                }
                            }));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    dismissProgressDislog();
                    Log.e("", "");
                }
            });

        } catch (Exception e) {

        }

    }

    void BodyLocationPartTypeByCategoryAndOthers(int BodyLocationPartId) {
        showProgressDialog("Please wait...");
        String str = String.format("{\"ImagingCategoryId\":\"%s\",\"BodyLocationId\":\"%s\",\"BodyLocationPartId\":\"%s\"}", String.valueOf(imagingCategoryId), String.valueOf(bodyLocationId), String.valueOf(BodyLocationPartId));
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), str);
        try {
            Call<ResponseBody> posts = RestAPIFactory.getApi().BodyLocationPartTypeByCategoryAndOthers(body);
            posts.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    dismissProgressDislog();
                    if (Shifa4U.checkNetwork.isInternetAvailable()) {
                        isPagination = true;
                        radiologyModels = new ArrayList<>();
                        skipPages = 0;
                        Paginateandfilterradiology(skipPages);
                    } else {
                        Toast.makeText(RadioLogyActivity.this, R.string.network_not_available, Toast.LENGTH_SHORT).show();
                    }
                    try {
                        ArrayList<BodyLocationPartType> bodyLocationPartTypes = MyGeneric.processResponse(response, new TypeToken<ArrayList<BodyLocationPartType>>() {
                        });
                        if (bodyLocationPartTypes != null) {
                            txt_type.setAdapter(new BodyLocationPartTypeAdapter(RadioLogyActivity.this, bodyLocationPartTypes, new MenuItemsInterface() {
                                @Override
                                public void onItemClick(Integer position, MenuModel menuModel) {

                                }

                                @Override
                                public void onButtonClick(Integer position, Object model, View button) {

                                }

                                @Override
                                public void onItemClick(Integer position, Object o) {
                                    BodyLocationPartType bodyLocationPartType = ((BodyLocationPartType) o);
                                    txt_type.setText(bodyLocationPartType.getName());
                                    bodyLocationPartTypeId = bodyLocationPartType.getBodyLocationPartTypeId();

                                    if (Shifa4U.checkNetwork.isInternetAvailable()) {
                                        isPagination = true;
                                        radiologyModels = new ArrayList<>();
                                        skipPages = 0;
                                        Paginateandfilterradiology(skipPages);
                                    } else {
                                        Toast.makeText(RadioLogyActivity.this, R.string.network_not_available, Toast.LENGTH_SHORT).show();
                                    }

                                }
                            }));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    dismissProgressDislog();
                    Log.e("", "");
                }
            });

        } catch (Exception e) {

        }

    }


    private void Paginateandfilterradiology(int pageNo) {


        showProgressDialog("Please wait...");

        String str = imagingCategoryId == null ? String.format("{\"ImagingCategoryId\":\"%s\",", "") : String.format("{\"ImagingCategoryId\":\"%s\",", String.valueOf(imagingCategoryId));
        str += bodyLocationId == null ? String.format("\"BodyLocationId\":\"%s\",", "") : String.format("\"BodyLocationId\":\"%s\",", String.valueOf(bodyLocationId));
        str += bodyLocationPartId == null ? String.format("\"BodyLocationPartId\":\"%s\",", "") : String.format("\"BodyLocationPartId\":\"%s\",", String.valueOf(bodyLocationPartId));
        str += String.format("\"SkipPages\":\"%s\",\"CityId\":\"%s\",\"Records\":\"%s\",", pageNo, Shifa4U.mySharePrefrence.getCityId(), 10);
        str += bodyLocationPartTypeId == null ? String.format("\"BodyLocationPartTypeId\":\"%s\"}", "") : String.format("\"BodyLocationPartTypeId\":\"%s\"}", String.valueOf(bodyLocationPartTypeId));


        //String str = String.format("{\"ImagingCategoryId\":\"%s\",\"BodyLocationId\":\"%s\",\"BodyLocationPartId\":\"%s\",\"BodyLocationPartTypeId\":\"%s\"}", String.valueOf(imagingCategoryId), String.valueOf(bodyLocationId), String.valueOf(bodyLocationPartId), String.valueOf(bodyLocationPartTypeId));


        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), str);

        try {
            Call<ResponseBody> posts = RestAPIFactory.getApi().Paginateandfilterradiology(body);
            posts.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> radiologyModelResponse) {
                    dismissProgressDislog();
                    try {

                        try {

                            String Json = GzipUtils.decompress(radiologyModelResponse.body().bytes());
                            JSONObject jsonObject = null;
                            if (!Json.contains("TotalRecords") && !Json.contains("Radiologies")) {
                                jsonObject = new JSONObject();
                                jsonObject.put("TotalRecords", 0);

                                jsonObject.put("Radiologies", new JSONArray(Json));

                                Json = jsonObject.toString();

                            }


                            Type listType = new TypeToken<RadiologyModel>() {
                            }.getType();
                            RadiologyModel radiologyModel = new Gson().fromJson(Json, listType);


                            if (radiologyModel.getRadiologies() != null && radiologyModel.getRadiologies().size() > 0) {

                                if (radiologyModels.size() > 0) {
                                    radiologyModels.addAll(radiologyModel.getRadiologies());
                                    radioLogyAdapter.notifyDataSetChanged();
                                } else {
                                    radiologyModels.addAll(radiologyModel.getRadiologies());
                                    radioLogyAdapter = new RadioLogyAdapter(RadioLogyActivity.this, radiologyModels, new MenuItemsInterface() {
                                        @Override
                                        public void onItemClick(Integer position, MenuModel menuModel) {

                                        }

                                        @Override
                                        public void onButtonClick(Integer position, Object model, View button) {
                                            if (button.getId() == R.id.btn_detail) {
                                                Intent i = new Intent(RadioLogyActivity.this, LabDetailActivity.class);
                                                i.putExtra("Item", radiologyModels.get(position));
                                                startActivity(i);
                                            } else if (button.getId() == R.id.btn_add_to_cart) {
                                                getRadioLogyDetailByID(String.valueOf(radiologyModels.get(position).getImagingId()));
                                            }

                                        }

                                        @Override
                                        public void onItemClick(Integer position, Object o) {

                                        }
                                    });
                                    lv_radio_logy.setAdapter(radioLogyAdapter);
                                }

                                flag_loading = false;
                            } else {
//                                txt_no_detail.setVisibility(View.VISIBLE);
//                                ll_main.setVisibility(View.GONE);
                                if (radiologyModels.size() == 0)
                                    lv_radio_logy.setAdapter(null);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            isPagination = false;
                            lv_radio_logy.setAdapter(null);
//                            txt_no_detail.setVisibility(View.VISIBLE);
//                            ll_main.setVisibility(View.GONE);
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                        txt_no_detail.setVisibility(View.VISIBLE);
                        ll_main.setVisibility(View.GONE);
                    }

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    txt_no_detail.setVisibility(View.VISIBLE);
                    ll_main.setVisibility(View.GONE);
                    dismissProgressDislog();
                    Log.e("", "");
                }
            });

        } catch (Exception e) {

        }

        if (pageNo == 0) {
            BodyLocationByImagingCategoryId(imagingCategoryId);
        }

    }


    private void getPaginatedRadioligyViaVolley(final long skipPages, final Context context) {

        StringRequest gZipRequest = new GZipRequest(Request.Method.POST, myUrl, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Type listType = new TypeToken<RadiologyModel>() {
                }.getType();
                RadiologyModel radiologyModel = new Gson().fromJson(response, listType);

                totalRadioligyRecords = radiologyModel.getTotalRecords();

                List<Integer> tempIds = new ArrayList<>();

                for (int i = 0; i < radiologyModel.getRadiologies().size(); i++) {
                    tempIds.add(radiologyModel.getRadiologies().get(i).getImagingId());
                }


                if (radiologyModel.getRadiologies() != null && radiologyModel.getRadiologies().size() > 0) {

                    if (radiologyModels.size() > 0) {
                        radiologyModels.addAll(radiologyModel.getRadiologies());
                        radioLogyAdapter.notifyDataSetChanged();

                        Toast.makeText(RadioLogyActivity.this, "" + radiologyModels.size(), Toast.LENGTH_SHORT).show();


                        rediologyIds.addAll(tempIds);
               /*         Set<Integer> setids = new HashSet<Integer>(rediologyIds);
                        Set<Radiology> set = new HashSet<Radiology>(radiologyModels);
                        int sizeRadioligyIds = rediologyIds.size();
                        if (setids.size() < rediologyIds.size())
                        {
                            Toast.makeText(RadioLogyActivity.this, "Duplicate", Toast.LENGTH_SHORT).show();
                        }

                        if(set.size() < radiologyModels.size()){
                            Toast.makeText(RadioLogyActivity.this, "Duplicate", Toast.LENGTH_SHORT).show();
                        }*/

                    } else {
                        radiologyModels.addAll(radiologyModel.getRadiologies());
                        radioLogyAdapter = new RadioLogyAdapter(RadioLogyActivity.this, radiologyModels, new MenuItemsInterface() {
                            @Override
                            public void onItemClick(Integer position, MenuModel menuModel) {

                            }

                            @Override
                            public void onButtonClick(Integer position, Object model, View button) {
                                if (button.getId() == R.id.btn_detail) {
                                    Intent i = new Intent(RadioLogyActivity.this, LabDetailActivity.class);
                                    i.putExtra("Item", radiologyModels.get(position));
                                    startActivity(i);
                                } else if (button.getId() == R.id.btn_add_to_cart) {
                                    getRadioLogyDetailByID(String.valueOf(radiologyModels.get(position).getImagingId()));
                                }


                            }

                            @Override
                            public void onItemClick(Integer position, Object o) {

                            }
                        });
                        lv_radio_logy.setAdapter(radioLogyAdapter);
                    }
                    flag_loading = false;
                } else {
                    Toast.makeText(RadioLogyActivity.this, "No records Found", Toast.LENGTH_SHORT).show();
                }

                //      adapter.addAll(p);

                progressDialog.dismiss();

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                HashMap<String, String> params2 = new HashMap<String, String>();
                params2.put("SkipPages", String.valueOf(skipPages));
                params2.put("Records", String.valueOf(194));
                // params2.put("CityId", Shifa4U.mySharePrefrence.getCityId());
                return new JSONObject(params2).toString().getBytes();
            }


            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };

        requestQueue.add(gZipRequest);
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }


}
