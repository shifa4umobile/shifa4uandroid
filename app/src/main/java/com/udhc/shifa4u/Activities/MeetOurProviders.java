package com.udhc.shifa4u.Activities;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.aakira.expandablelayout.ExpandableLinearLayout;
import com.google.gson.reflect.TypeToken;
import com.udhc.shifa4u.Adapters.RVAdapter;
import com.udhc.shifa4u.Models.AmericanTeleclinic.MedicalBranches;
import com.udhc.shifa4u.Models.AmericanTeleclinic.Physician;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.CheckNetwork;
import com.udhc.shifa4u.Utilities.GZipRequest;
import com.udhc.shifa4u.Utilities.HeaderManager;
import com.udhc.shifa4u.Utilities.Utility;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MeetOurProviders extends AppCompatActivity {


    @BindView(R.id.button)
    RelativeLayout button;
    @BindView(R.id.btnConsultantPhysciansTeam)
    RelativeLayout btnConsultantPhysciansTeam;
    @BindView(R.id.lvConsultantPhyscianTeam)
    ExpandableLinearLayout lvConsultantPhyscianTeam;
    @BindView(R.id.provider_recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.spGender)
    Spinner spGender;
    @BindView(R.id.spCategories)
    Spinner spCategories;
    @BindView(R.id.btnSearchViaCategories)
    Button btnSearchViaCategories;
    @BindView(R.id.exSearchViaCategories)
    ExpandableLinearLayout exSearchViaCategories;
    private LinearLayoutManager layoutManager;
    private List<Physician> providers;
    ProgressDialog progressDialog;

    private  boolean isFirstTime = true;

    List<MedicalBranches> medicalBranches;

    private boolean isGenderselected = false , isCategoriesSelected = false;

    CheckNetwork checkNetwork = new CheckNetwork(this);

    String myUrl = "https://api.shifa4u.com/api/AmericanTeleClinic/v1/GetAllATPPhysicians";
    String categoriesUrl = "https://api.shifa4u.com/api/AmericanTeleClinic/v1/GetAllMedicalBranches";


    RVAdapter adapter;
    int SkipPages = 1;
    int Records = 9;
    RequestQueue requestQueue;

    List<String> genderList = new ArrayList<>();
    List<String> categoryList = new ArrayList<>();
    String GenderProfileId = "0", MedicalBranchId = "0";


    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkChangeReceiver, intentFilter);
        setHeader();
    }


    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver(networkChangeReceiver);
    }


    private BroadcastReceiver networkChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("app","Network connectivity change");
        /*    if (!isFirstTime) {
                getData(SkipPages, MeetOurProviders.this);
                if (medicalBranches == null)
                {

                        getSpecialities(MeetOurProviders.this);

                }
            }
            isFirstTime = false;*/
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meet_our_providers);
        ButterKnife.bind(this);
        setHeader();
        boolean a = isNetworkConnected();
        boolean b = Utility.isInternetAvailable();
            requestQueue = Volley.newRequestQueue(this); // 'this' is the Context
        init();
     /*   btnSearchViaCategories.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkNetwork.isInternetAvailable()) {
                    getViaCategories(MeetOurProviders.this, MedicalBranchId, GenderProfileId);
                   // isCategoriesSelected = spCategories.getSelectedItemPosition() !=0;
                 //   isGenderselected = spGender.getSelectedItemPosition() !=0;
                }
                else
                {
                  //  Toast.makeText(MeetOurProviders.this, "Internet Connection Required", Toast.LENGTH_SHORT).show();
                    Utility.openInternetAlert(MeetOurProviders.this);
                }
            }
        });*/

       /* spGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (i == 1) {
                    GenderProfileId = "10";
                } else if (i == 2) {
                    GenderProfileId = "11";
                } else {
                    GenderProfileId = "0";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });*/

        spCategories.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i>0) {
                    if (i > 1) {
                        MedicalBranchId = medicalBranches.get(i - 2).getMedicalBranchId();
                    } else if (i == 1) {
                        MedicalBranchId = "0";
                    }

                    getViaCategories(MeetOurProviders.this, MedicalBranchId, GenderProfileId);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.provider_recycler_view);
        providers = new ArrayList<Physician>();
        adapter = new RVAdapter(providers, this);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        genderList.add("Select Gender");
        genderList.add("Male");
        genderList.add("Female");
        ArrayAdapter<String> dataAdapter;
        dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, genderList);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spGender.setAdapter(dataAdapter);

        //Volley
        final CheckNetwork checkNetwork = new CheckNetwork(this);
        if (checkNetwork.isInternetAvailable()) {
            getSpecialities(this);
            getData(SkipPages, this);
        }
        else
        {
           // Toast.makeText(this, "Internet Connection Required", Toast.LENGTH_SHORT).show();
            Utility.openInternetAlert(MeetOurProviders.this);
        }

        recyclerView.addOnScrollListener(new EndlessScrollListener() {
            @Override
            public void onLoadMore() {
                if (checkNetwork.isInternetAvailable()) {
                    if (spCategories.getSelectedItemPosition() == 0) {
                        SkipPages++;
                        getData(SkipPages, MeetOurProviders.this);
                    }
                }
                else
                {
                  //  Toast.makeText(MeetOurProviders.this, "Internet connection required", Toast.LENGTH_SHORT).show();
                    Utility.openInternetAlert(MeetOurProviders.this);
                }
            }
        });
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        assert cm != null;
        return cm.getActiveNetworkInfo() != null;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    private void getData(final long skipPages, final Context context) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, myUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Type listType = new TypeToken<List<Physician>>() {
                }.getType();
                List<Physician> p = (List<Physician>) Utility.parseJSON(response, listType);
                synchronized (this) {
                    adapter.addAll(p);
                }
                progressDialog.dismiss();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                HashMap<String, String> params2 = new HashMap<String, String>();
                params2.put("SkipPages", String.valueOf(skipPages));
                params2.put("Records", String.valueOf(Records));
                return new JSONObject(params2).toString().getBytes();
            }
            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };

        requestQueue.add(stringRequest);
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
    }

    private void getSpecialities(final Context context) {
        StringRequest stringRequest = new GZipRequest(Request.Method.POST, categoriesUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                medicalBranches = new ArrayList<>();
                Type listType = new TypeToken<List<MedicalBranches>>() {
                }.getType();
                medicalBranches = (List<MedicalBranches>) Utility.parseJSON(response, listType);


                if (medicalBranches != null) {
                    categoryList.add("Choose Speciality");
                    categoryList.add("All");

                    for (int i = 0; i < medicalBranches.size(); i++) {
                        categoryList.add(medicalBranches.get(i).getName());
                    }

                    ArrayAdapter<String> dataAdapter;

                    if (categoryList != null) {
                        dataAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, categoryList);

                    } else {
                        categoryList = new ArrayList<>();
                        dataAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, categoryList);

                    }

                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    // dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spCategories.setAdapter(dataAdapter);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                HashMap<String, String> params2 = new HashMap<String, String>();
                return new JSONObject(params2).toString().getBytes();
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };

        requestQueue.add(stringRequest);
    }


    private void getViaCategories(final Context context, final String id, final String GenderProfileId) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, myUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                providers.clear();
                adapter.notifyDataSetChanged();
                Type listType = new TypeToken<List<Physician>>() {
                }.getType();
                List<Physician> p = (List<Physician>) Utility.parseJSON(response, listType);
                synchronized (this) {
                    adapter.addAll(p);
                }
                progressDialog.dismiss();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                HashMap<String, String> params2 = new HashMap<String, String>();
                params2.put("MedicalBranchId", id);
                params2.put("GenderProfileId", GenderProfileId);
                return new JSONObject(params2).toString().getBytes();
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };

        requestQueue.add(stringRequest);
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
    }

    private void init ()
    {
        Utility.setClick(btnConsultantPhysciansTeam , lvConsultantPhyscianTeam);
     /*   Utility.setClick(btnExpandSearchViaCategories , exSearchViaCategories);
        Utility.setRotateAnimator(exSearchViaCategories , arrowSearchViaCategories);*/
        Utility.setRotateAnimator(lvConsultantPhyscianTeam , button);
    }


    void setHeader()
    {
        HeaderManager headerManager = new HeaderManager(MeetOurProviders.this, findViewById(R.id.header));
        headerManager.setMenutype(HeaderManager.BACK_MENU);
        headerManager.setShowFirstImageButton(false);
        headerManager.setShowSecoundImageButton(false);
        headerManager.setTitle("Meet Our Providers");
        headerManager.setShowBottomView(true);
        headerManager.setHeaderColor(getResources().getColor(R.color.header_background_color));
    }
}
