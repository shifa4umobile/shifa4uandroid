package com.udhc.shifa4u.Activities;

import android.app.Application;
import android.content.Context;

import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.udhc.shifa4u.Utilities.Base64ImageDownloader;
import com.udhc.shifa4u.Utilities.CheckNetwork;
import com.udhc.shifa4u.Utilities.MySharePrefrence;
import com.udhc.shifa4u.Utilities.SSLCertificateHandler;

/**
 * Created by Ali Imran Bangash on 6/29/2018.
 */

public class Shifa4U extends Application {
    public static MySharePrefrence mySharePrefrence;
    public static CheckNetwork checkNetwork;
    private Object options;
    public static Shifa4U shifa4U;
    public static Context context;


    public static Shifa4U getIntence() {
        if (shifa4U == null) {
            shifa4U = new Shifa4U();
        }
        return shifa4U;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        mySharePrefrence = new MySharePrefrence(Shifa4U.this);
        context = this;
        checkNetwork = new CheckNetwork(Shifa4U.this);
      SSLCertificateHandler.nuke();
        initImageLoader(this);
    }

    public Context getContext()
    {
        return context;
    }

    public static void initImageLoader(Context context) {
        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
        config.threadPriority(Thread.NORM_PRIORITY - 2);
        config.denyCacheImageMultipleSizesInMemory();
        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        config.diskCacheSize(50 * 1024 * 1024); // 50 MiB
        config.tasksProcessingOrder(QueueProcessingType.LIFO);
        config.writeDebugLogs();
        config.imageDownloader(new Base64ImageDownloader(context));
        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config.build());
    }

}
