package com.udhc.shifa4u.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;
import com.udhc.shifa4u.Adapters.LabAdapter;
import com.udhc.shifa4u.Adapters.MedicalPackageLabAdapter;
import com.udhc.shifa4u.Adapters.RadioLogyLabAdapter;
import com.udhc.shifa4u.Adapters.VendorsLocationAdapter;
import com.udhc.shifa4u.Interfaces.MenuItemsInterface;
import com.udhc.shifa4u.Models.CartModel;
import com.udhc.shifa4u.Models.LabDetailModel;
import com.udhc.shifa4u.Models.MedicalPackages.LabPackagesComparison;
import com.udhc.shifa4u.Models.MedicalPackages.MedicalPackagesModel;
import com.udhc.shifa4u.Models.MenuModel;
import com.udhc.shifa4u.Models.RadioLogyDetail.Contact;
import com.udhc.shifa4u.Models.RadioLogyDetail.Location;
import com.udhc.shifa4u.Models.RadioLogyDetail.RadiologyLabsComparison;
import com.udhc.shifa4u.Models.RadioLogyDetail.Vendor;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.HeaderManager;
import com.udhc.shifa4u.Utilities.MyUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.TreeMap;

public class CheckoutActivity extends AppCompatActivity {

    private LinearLayout ll_lab_test_main;
    private LinearLayout ll_lab_test_container;
    private LinearLayout ll_radiology_main;
    private LinearLayout ll_radiology_container;
    private LinearLayout ll_home_care_main;
    private LinearLayout ll_home_care_container;
    private LinearLayout ll_medical_package_main;
    private LinearLayout ll_medical_package_container;
    private TextView txt_extra_off;
    private TextView txt_sub_total;
    private TextView txt_total;
    private HeaderManager headerManager;
    ArrayList<String> collection = new ArrayList<>();
    ArrayList<String> collection_radio = new ArrayList<>();
    private Button btn_proceed_checkout;
    private ArrayList<CartModel> cartModels;
    private Button btn_continue_order;
    private TextView txt_sub_total_label;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);


        setHeader();
        collection.add("Home Collection");
        collection_radio.add("Walking");
        ll_lab_test_main = (LinearLayout) findViewById(R.id.ll_lab_test_main);
        ll_lab_test_container = (LinearLayout) findViewById(R.id.ll_lab_test_container);

        ll_radiology_main = (LinearLayout) findViewById(R.id.ll_radiology_main);
        ll_radiology_container = (LinearLayout) findViewById(R.id.ll_radiology_container);

        ll_home_care_main = (LinearLayout) findViewById(R.id.ll_home_care_main);
        ll_home_care_container = (LinearLayout) findViewById(R.id.ll_home_care_container);

        ll_medical_package_main = (LinearLayout) findViewById(R.id.ll_medical_package_main);
        ll_medical_package_container = (LinearLayout) findViewById(R.id.ll_medical_package_container);


        btn_proceed_checkout = (Button) findViewById(R.id.btn_proceed_checkout);
        btn_proceed_checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Shifa4U.mySharePrefrence.getLogin() != null) {
                    proceedCheckout();
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(CheckoutActivity.this);
                    builder.setMessage("Please login in-order to check out")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    startActivity(new Intent(CheckoutActivity.this, LoginActivity.class).putExtra("From","checkout").setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
                                    finish();
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).show();
                }
            }
        });
        btn_continue_order = (Button) findViewById(R.id.btn_continue_order);
        btn_continue_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        txt_total = (TextView) findViewById(R.id.txt_total);
        txt_sub_total = (TextView) findViewById(R.id.txt_sub_total);
        txt_extra_off = (TextView) findViewById(R.id.txt_extra_off);
        txt_sub_total_label = (TextView) findViewById(R.id.txt_sub_total_label);

        populateCart();


    }

    private void proceedCheckout() {
        if (cartModels != null && cartModels.size() > 0) {
            boolean isEverythingSelected = true;
            for (CartModel cartModelFinal : cartModels) {
                if (cartModelFinal.getProductType() == CartModel.product.LAB_TEST) {
                    if (cartModelFinal.getSelectedLabId() < 1) {
                        isEverythingSelected = false;
                    }
                } else if (cartModelFinal.getProductType() == CartModel.product.RADIOLOGY) {
                    if (cartModelFinal.getSelectedLabId() < 1) {
                        isEverythingSelected = false;
                    }
                }
//                else if (cartModelFinal.getProductType() == CartModel.product.HOME_CARE) {
//
//                }
                else if (cartModelFinal.getProductType() == CartModel.product.MEDICAL_PACKAGE) {
                    if (cartModelFinal.getSelectedLabId() < 1) {
                        isEverythingSelected = false;
                    }
                }
            }

            if (!isEverythingSelected) {
                Toast.makeText(CheckoutActivity.this, "Please select all details", Toast.LENGTH_SHORT).show();
            } else {
                startActivity(new Intent(CheckoutActivity.this, OrderPreviewActivity.class));
            }

        }

    }

    private void setHeader() {
        headerManager = new HeaderManager(CheckoutActivity.this, findViewById(R.id.header));
        headerManager.setMenutype(HeaderManager.BACK_MENU);
        headerManager.setShowFirstImageButton(false);
        headerManager.setShowSecoundImageButton(false);
        headerManager.setMenuButtonVisibility(true);
        headerManager.setTitle(getString(R.string.checkout_title));

        headerManager.setShowBottomView(true);
        headerManager.setHeaderColor(getResources().getColor(R.color.header_background_color));
    }


    public View populateLabTest(final CartModel cartModel) {

        final View v = LayoutInflater.from(CheckoutActivity.this).inflate(R.layout.check_out_lab_test_item, null);
        v.setTag(cartModel);
        TextView txt_title = (TextView) v.findViewById(R.id.txt_title);
        ImageView img_delete = (ImageView) v.findViewById(R.id.img_delete);
        final AutoCompleteTextView txt_detail_labs = (AutoCompleteTextView) v.findViewById(R.id.txt_detail_labs);
        final AutoCompleteTextView txt_collection_method = (AutoCompleteTextView) v.findViewById(R.id.txt_collection_method);
        final TextView txt_price = (TextView) v.findViewById(R.id.txt_price);

        txt_title.setText(cartModel.getProductName());
        txt_detail_labs.setText(cartModel.getSelectedLabName());
        if (cartModel.getPriice() != null) {

            txt_price.setText(Html.fromHtml("<font color='#242424'>Price :  </font><font color='#2a9f40'>" + MyUtils.formateCurrency(cartModel.getPriice()) + "</font>"));
        } else {
            txt_price.setText(Html.fromHtml("<font color='#242424'>Price :  </font><font color='#2a9f40'>00.00</font>"));
        }
        txt_detail_labs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_detail_labs.showDropDown();
            }
        });

        ArrayList<TreeMap> treeMaps = (ArrayList) cartModel.getOriginalObject();


        ArrayList<LabDetailModel> labDetailModels = null;
        if (treeMaps != null && treeMaps.size() > 0) {
            Gson gson = new Gson();
            Type type = new TypeToken<ArrayList<LabDetailModel>>() {
            }.getType();
            String json = gson.toJson(treeMaps, type);


            if (json != null) {
                gson = new Gson();
                type = new TypeToken<ArrayList<LabDetailModel>>() {
                }.getType();
                labDetailModels = gson.fromJson(json, type);
            }

        }
        cartModel.setOriginalObject(labDetailModels);
        ArrayAdapter<String> genderAdapter = new ArrayAdapter<String>(CheckoutActivity.this, R.layout.spinner_item, collection);

        txt_collection_method.setAdapter(genderAdapter);
        txt_collection_method.setText(collection.get(0));
        cartModel.setCollectionType(collection.get(0));
        txt_collection_method.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_collection_method.showDropDown();
            }
        });

        txt_collection_method.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                txt_collection_method.dismissDropDown();
            }
        });

        txt_detail_labs.setAdapter(new LabAdapter(CheckoutActivity.this, (ArrayList) cartModel.getOriginalObject(), new MenuItemsInterface() {
            @Override
            public void onItemClick(Integer position, MenuModel menuModel) {

            }

            @Override
            public void onButtonClick(Integer position, Object model, View button) {

            }

            @Override
            public void onItemClick(Integer position, Object o) {
                txt_detail_labs.dismissDropDown();
                txt_detail_labs.setText(((LabDetailModel) o).getLabName());
                txt_price.setText(Html.fromHtml("<font color='#242424'>Price :  </font><font color='#2a9f40'>" + MyUtils.formateCurrency(((LabDetailModel) o).getYourPrice()) + "<//font>"));
                txt_detail_labs.setTag(o);


                CartModel cartModel1 = (CartModel) v.getTag();

                cartModel.setPriice((((LabDetailModel) o).getYourPrice()).doubleValue());
                cartModel.setOriginalPriice((((LabDetailModel) o).getVendorPrice()).doubleValue());
                cartModel.setSelectedLabId(((LabDetailModel) o).getLabId());
                cartModel.setSelectedLabName(((LabDetailModel) o).getLabName());
                cartModel.setProductLineId(String.valueOf(((LabDetailModel) o).getLabCenterProductId()));

                Shifa4U.mySharePrefrence.updateCartSingleItem(cartModel);
                populateCart();
                if (MyUtils.isLabExistInCart(cartModel)) {
                    showLabExistDialog(CheckoutActivity.this);
                }

            }
        }));


        img_delete.setTag(cartModel);


        img_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(CheckoutActivity.this);
                builder.setMessage("Are you sure, you want to delete this Item from cart?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ArrayList<CartModel> cartModels = Shifa4U.mySharePrefrence.getCart();
                                CartModel cartModel1_tag = (CartModel) v.getTag();
                                if (cartModels != null && cartModels.size() > 0) {
                                    for (CartModel cartModel1 : cartModels) {
                                        if (cartModel1_tag.getProductId() == cartModel1.getProductId() && cartModel1_tag.getProductType() == cartModel1.getProductType()) {
                                            cartModels.remove(cartModel1);
                                            Shifa4U.mySharePrefrence.updateCart(cartModels);
                                            if (cartModels.size()== 0)
                                            {
                                                startActivity(new Intent(CheckoutActivity.this ,MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
                                                finish();

                                            }
                                            populateCart();
                                            
                                            break;
                                        }
                                    }
                                }
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();
            }
        });
        return v;
    }


    public View populateRadioLogy(final CartModel cartModel) {

        final View v = LayoutInflater.from(CheckoutActivity.this).inflate(R.layout.check_out_radio_logy_item, null);
        v.setTag(cartModel);
        TextView txt_title = (TextView) v.findViewById(R.id.txt_title);
        ImageView img_delete = (ImageView) v.findViewById(R.id.img_delete);
        final AutoCompleteTextView txt_detail_labs = (AutoCompleteTextView) v.findViewById(R.id.txt_detail_labs);
        final AutoCompleteTextView txt_collection_method = (AutoCompleteTextView) v.findViewById(R.id.txt_collection_method);
        txt_collection_method.setHint("Category");
        final AutoCompleteTextView txt_location = (AutoCompleteTextView) v.findViewById(R.id.txt_location);
        final TextView txt_price = (TextView) v.findViewById(R.id.txt_price);

        txt_title.setText(cartModel.getProductName());
        txt_detail_labs.setText(cartModel.getSelectedLabName());

        if (cartModel.getPriice() != null) {

            txt_price.setText(Html.fromHtml("<font color='#242424'>Rs.  </font><font color='#2a9f40'>" +MyUtils.formateCurrency(cartModel.getPriice()) + "</font>"));
        } else {
            txt_price.setText(Html.fromHtml("<font color='#242424'>Rs.  </font><font color='#2a9f40'>00.00</font>"));
        }
        txt_detail_labs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_detail_labs.showDropDown();
            }
        });

//        ArrayList<TreeMap> treeMaps=(ArrayList) cartModel.getOriginalObject();


        com.udhc.shifa4u.Models.RadioLogyDetail.Radiology radiology = null;
        if (cartModel.getOriginalObject() != null) {
            Gson gson = new Gson();
            Type type = new TypeToken<LinkedTreeMap>() {
            }.getType();
            String json = gson.toJson(cartModel.getOriginalObject(), type);


            if (json != null) {
                gson = new Gson();
                type = new TypeToken<com.udhc.shifa4u.Models.RadioLogyDetail.Radiology>() {
                }.getType();
                radiology = gson.fromJson(json, type);
            }

        }
        cartModel.setOriginalObject(radiology);
        ArrayAdapter<String> genderAdapter = new ArrayAdapter<String>(CheckoutActivity.this, R.layout.spinner_item, collection_radio);

        txt_collection_method.setAdapter(genderAdapter);
        txt_collection_method.setText(collection_radio.get(0));
        cartModel.setCollectionType(collection_radio.get(0));
        txt_collection_method.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_collection_method.showDropDown();
            }
        });

        txt_collection_method.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                txt_collection_method.dismissDropDown();
            }
        });

//        txt_location.setText(radiology.getBodyLocationName());
        if (cartModel.getSelectedContact() != null) {
            txt_location.setText(cartModel.getSelectedContact().getDescription());

            setupVendorsLocation(cartModel, txt_location);
        }


        txt_detail_labs.setAdapter(new RadioLogyLabAdapter(CheckoutActivity.this, (ArrayList<RadiologyLabsComparison>) ((com.udhc.shifa4u.Models.RadioLogyDetail.Radiology) cartModel.getOriginalObject()).getRadiologyLabsComparison(), new MenuItemsInterface() {
            @Override
            public void onItemClick(Integer position, MenuModel menuModel) {

            }

            @Override
            public void onButtonClick(Integer position, Object model, View button) {

            }

            @Override
            public void onItemClick(Integer position, Object o) {
                txt_detail_labs.dismissDropDown();
                txt_detail_labs.setText(((RadiologyLabsComparison) o).getLabName());
                txt_price.setText(Html.fromHtml("<font color='#242424'>Rs.  </font><font color='#2a9f40'>" + MyUtils.formateCurrency(((RadiologyLabsComparison) o).getYourPrice()) + "<//font>"));
                txt_detail_labs.setTag(o);


                CartModel cartModel1 = (CartModel) v.getTag();
                cartModel.setPriice((((RadiologyLabsComparison) o).getYourPrice()).doubleValue());
                cartModel.setOriginalPriice((((RadiologyLabsComparison) o).getVendorPrice()).doubleValue());
                cartModel.setSelectedLabId(((RadiologyLabsComparison) o).getLabId());
                cartModel.setSelectedLabName(((RadiologyLabsComparison) o).getLabName());
                cartModel.setProductLineId(String.valueOf(((RadiologyLabsComparison) o).getProductLineId()));


                setupVendorsLocation(cartModel, txt_location);


                Shifa4U.mySharePrefrence.updateCartSingleItem(cartModel);
                populateCart();
                if (MyUtils.isLabExistInCart(cartModel)) {
                    showLabExistDialog(CheckoutActivity.this);
                }

            }
        }));


        img_delete.setTag(cartModel);


        img_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(CheckoutActivity.this);
                builder.setMessage("Are you sure, you want to delete this Item from cart?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ArrayList<CartModel> cartModels = Shifa4U.mySharePrefrence.getCart();
                                CartModel cartModel1_tag = (CartModel) v.getTag();
                                if (cartModels != null && cartModels.size() > 0) {
                                    for (CartModel cartModel1 : cartModels) {
                                        if (cartModel1_tag.getProductId() == cartModel1.getProductId() && cartModel1_tag.getProductType() == cartModel1.getProductType()) {
                                            cartModels.remove(cartModel1);
                                            Shifa4U.mySharePrefrence.updateCart(cartModels);
                                            if (cartModels.size() == 0)
                                            {
                                               startActivity(new Intent(CheckoutActivity.this ,MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
                                                finish();
                                            }
                                            populateCart();
                                            break;
                                        }
                                    }
                                }
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();
            }
        });

        return v;
    }


    public void setupVendorsLocation(final CartModel cartModel, final AutoCompleteTextView txt_location) {
        ArrayList<Vendor> vendors = (ArrayList<Vendor>) ((com.udhc.shifa4u.Models.RadioLogyDetail.Radiology) cartModel.getOriginalObject()).getVendors();
        Vendor matchedVendor = null;

        if (vendors != null && vendors.size() > 0) {
            for (Vendor vendor : vendors) {
                if (vendor.getLabId() == cartModel.getSelectedLabId()) {
                    matchedVendor = vendor;
                    break;
                }
            }

            if (matchedVendor != null) {

                ArrayList<Location> locations = (ArrayList<Location>) matchedVendor.getLocations();

                if (locations != null && locations.size() > 0) {
                    cartModel.setSelectedContact(locations.get(0).getContacts().get(0));

                    txt_location.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            txt_location.showDropDown();

                        }
                    });
                    txt_location.setAdapter(new VendorsLocationAdapter(CheckoutActivity.this, (ArrayList<Location>) matchedVendor.getLocations(), new MenuItemsInterface() {
                        @Override
                        public void onItemClick(Integer position, MenuModel menuModel) {

                        }

                        @Override
                        public void onButtonClick(Integer position, Object model, View button) {

                        }

                        @Override
                        public void onItemClick(Integer position, Object o) {
                            txt_location.setText(((Contact) o).getDescription());
                            cartModel.setSelectedContact(((Contact) o));
                            txt_location.dismissDropDown();
                            Shifa4U.mySharePrefrence.updateCartSingleItem(cartModel);
                            populateCart();
                        }
                    }));
                }
            }


        }
    }


    public View populateMedicalPackage(final CartModel cartModel) {

        final View v = LayoutInflater.from(CheckoutActivity.this).inflate(R.layout.check_out_lab_test_item, null);
        v.setTag(cartModel);
        TextView txt_title = (TextView) v.findViewById(R.id.txt_title);
        final ImageView img_delete = (ImageView) v.findViewById(R.id.img_delete);
        final AutoCompleteTextView txt_detail_labs = (AutoCompleteTextView) v.findViewById(R.id.txt_detail_labs);
        final AutoCompleteTextView txt_collection_method = (AutoCompleteTextView) v.findViewById(R.id.txt_collection_method);
        final TextView txt_price = (TextView) v.findViewById(R.id.txt_price);

        txt_title.setText(cartModel.getProductName());
        txt_detail_labs.setText(cartModel.getSelectedLabName());
        if (cartModel.getPriice() != null) {

            txt_price.setText(Html.fromHtml("<font color='#242424'>Rs.  </font><font color='#2a9f40'>" + MyUtils.formateCurrency(cartModel.getPriice()) + "</font>"));
        } else {
            txt_price.setText(Html.fromHtml("<font color='#242424'>Rs.  </font><font color='#2a9f40'>00.00</font>"));
        }
        txt_detail_labs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_detail_labs.showDropDown();
            }
        });

//        ArrayList<TreeMap> treeMaps = (ArrayList) cartModel.getOriginalObject();


        MedicalPackagesModel medicalPackagesModel = null;
        if (cartModel.getOriginalObject() != null) {
            Gson gson = new Gson();
            Type type = new TypeToken<LinkedTreeMap>() {
            }.getType();
            String json = gson.toJson(cartModel.getOriginalObject(), type);


            if (json != null) {
                gson = new Gson();
                type = new TypeToken<MedicalPackagesModel>() {
                }.getType();
                medicalPackagesModel = gson.fromJson(json, type);
            }

        }
        cartModel.setOriginalObject(medicalPackagesModel);

        ArrayAdapter<String> genderAdapter = new ArrayAdapter<String>(CheckoutActivity.this, R.layout.spinner_item, collection);

        txt_collection_method.setAdapter(genderAdapter);
        txt_collection_method.setText(collection.get(0));
        cartModel.setCollectionType(collection.get(0));
        txt_collection_method.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_collection_method.showDropDown();
            }
        });

        txt_collection_method.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                txt_collection_method.dismissDropDown();
            }
        });

        txt_detail_labs.setAdapter(new MedicalPackageLabAdapter(CheckoutActivity.this, (ArrayList) medicalPackagesModel.getLabPackagesComparison(), new MenuItemsInterface() {
            @Override
            public void onItemClick(Integer position, MenuModel menuModel) {

            }

            @Override
            public void onButtonClick(Integer position, Object model, View button) {

            }

            @Override
            public void onItemClick(Integer position, Object o) {
                txt_detail_labs.dismissDropDown();
                txt_detail_labs.setText(((LabPackagesComparison) o).getLabName());
                txt_price.setText(Html.fromHtml("<font color='#242424'>Rs.  </font><font color='#2a9f40'>RS. " + MyUtils.formateCurrency(((LabPackagesComparison) o).getYourPrice()) + "<//font>"));
                txt_detail_labs.setTag(o);

                CartModel cartModel1 = (CartModel) v.getTag();

                cartModel.setPriice((((LabPackagesComparison) o).getYourPrice()).doubleValue());
                cartModel.setOriginalPriice((((LabPackagesComparison) o).getVendorPrice()).doubleValue());
                cartModel.setSelectedLabId(((LabPackagesComparison) o).getLabId());
                cartModel.setSelectedLabName(((LabPackagesComparison) o).getLabName());
                cartModel.setProductLineId(String.valueOf(((LabPackagesComparison) o).getPackageId()));

                Shifa4U.mySharePrefrence.updateCartSingleItem(cartModel);
                populateCart();
                if (MyUtils.isLabExistInCart(cartModel)) {
                    showLabExistDialog(CheckoutActivity.this);
                }
            }
        }));

        img_delete.setTag(cartModel);


        img_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(CheckoutActivity.this);
                builder.setMessage("Are you sure, you want to delete this Item from cart?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ArrayList<CartModel> cartModels = Shifa4U.mySharePrefrence.getCart();
                                CartModel cartModel1_tag = (CartModel) v.getTag();
                                if (cartModels != null && cartModels.size() > 0) {
                                    for (CartModel cartModel1 : cartModels) {
                                        if (cartModel1_tag.getProductId() == cartModel1.getProductId() && cartModel1_tag.getProductType() == cartModel1.getProductType()) {
                                            cartModels.remove(cartModel1);
                                            Shifa4U.mySharePrefrence.updateCart(cartModels);
                                            if (cartModels.size() == 0)
                                            {
                                                startActivity(new Intent(CheckoutActivity.this ,MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
                                                finish();
                                            }
                                            populateCart();
                                            break;
                                        }
                                    }
                                }
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();
            }
        });


        return v;
    }


    public View populateHomeCare(final CartModel cartModel) {

        View v = LayoutInflater.from(CheckoutActivity.this).inflate(R.layout.check_out_lab_test_item, null);

        TextView txt_title = (TextView) v.findViewById(R.id.txt_title);
        ImageView img_delete = (ImageView) v.findViewById(R.id.img_delete);
        final AutoCompleteTextView txt_detail_labs = (AutoCompleteTextView) v.findViewById(R.id.txt_detail_labs);
        AutoCompleteTextView txt_collection_method = (AutoCompleteTextView) v.findViewById(R.id.txt_collection_method);
        LinearLayout ll_dropdown = (LinearLayout) v.findViewById(R.id.ll_dropdown);
        ll_dropdown.setVisibility(View.GONE);
        final TextView txt_price = (TextView) v.findViewById(R.id.txt_price);

        txt_title.setText(cartModel.getProductName());
        txt_detail_labs.setText(cartModel.getSelectedLabName());
        if (cartModel.getPriice() != null) {

            txt_price.setText(Html.fromHtml("<font color='#242424'>Rs.  </font><font color='#2a9f40'>" + MyUtils.formateCurrency(cartModel.getPriice()) + "</font>"));
        } else {
            txt_price.setText(Html.fromHtml("<font color='#242424'>Rs.  </font><font color='#2a9f40'>00.00</font>"));
        }
        txt_detail_labs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_detail_labs.showDropDown();
            }
        });

        img_delete.setTag(cartModel);


        img_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(CheckoutActivity.this);
                builder.setMessage("Are you sure, you want to delete this Item from cart?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ArrayList<CartModel> cartModels = Shifa4U.mySharePrefrence.getCart();
                                CartModel cartModel1_tag = (CartModel) v.getTag();
                                if (cartModels != null && cartModels.size() > 0) {
                                    for (CartModel cartModel1 : cartModels) {
                                        if (cartModel1_tag.getProductId() == cartModel1.getProductId() && cartModel1_tag.getProductType() == cartModel1.getProductType()) {
                                            cartModels.remove(cartModel1);
                                            Shifa4U.mySharePrefrence.updateCart(cartModels);
                                            if (cartModels.size() == 0)
                                            {
                                                startActivity(new Intent(CheckoutActivity.this ,MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
                                                finish();
                                           }
                                            populateCart();
                                            break;
                                        }
                                    }
                                }
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();
            }
        });

        return v;
    }


    public void populateCart() {

        cartModels = Shifa4U.mySharePrefrence.getCart();
        if (cartModels != null && cartModels.size() > 0) {

            ll_lab_test_main.setVisibility(View.GONE);
            ll_radiology_main.setVisibility(View.GONE);
            ll_home_care_main.setVisibility(View.GONE);
            ll_medical_package_main.setVisibility(View.GONE);


            ll_lab_test_container.removeAllViews();
            ll_radiology_container.removeAllViews();
            ll_home_care_container.removeAllViews();
            ll_medical_package_container.removeAllViews();
            Double sub_total = 0.0;
            Double total = 0.0;
            for (int i = 0; i < cartModels.size(); i++) {
                if (cartModels.get(i).getProductType() == CartModel.product.LAB_TEST) {
                    ll_lab_test_main.setVisibility(View.VISIBLE);
                    ll_lab_test_container.addView(populateLabTest(cartModels.get(i)));
                } else if (cartModels.get(i).getProductType() == CartModel.product.RADIOLOGY) {
                    ll_radiology_main.setVisibility(View.VISIBLE);
                    ll_radiology_container.addView(populateRadioLogy(cartModels.get(i)));
                } else if (cartModels.get(i).getProductType() == CartModel.product.HOME_CARE) {
                    ll_home_care_main.setVisibility(View.VISIBLE);
                    ll_home_care_container.addView(populateHomeCare(cartModels.get(i)));
                } else if (cartModels.get(i).getProductType() == CartModel.product.MEDICAL_PACKAGE) {
                    ll_medical_package_main.setVisibility(View.VISIBLE);
                    ll_medical_package_container.addView(populateMedicalPackage(cartModels.get(i)));
                }


                if (cartModels.get(i).getPriice() != null) {
                    sub_total += cartModels.get(i).getPriice();
                }

                if (cartModels.get(i).getOriginalPriice() != null) {
                    total += cartModels.get(i).getOriginalPriice();
                }

                txt_sub_total.setText("Rs. " + MyUtils.formateCurrency(total));

//                if (cartModels != null && cartModels.size() > 0)
//                {
//                    if(cartModels.size()==1)
//                    {
//                        txt_sub_total_label.setText(String.format("Sub Total %s item", String.valueOf(cartModels.size())));
//                    }else
//                    {
//                        txt_sub_total_label.setText(String.format("Sub Total %s items", String.valueOf(cartModels.size())));
//                    }
//                }

                Double difference=sub_total - total;
                if(difference<0)
                {
                    difference=difference*-1;
                }

                txt_extra_off.setText("Rs. " + MyUtils.formateCurrency(difference));
                txt_total.setText("Rs. " + MyUtils.formateCurrency(sub_total));
            }
        } else {
            ll_lab_test_main.setVisibility(View.GONE);
            ll_radiology_main.setVisibility(View.GONE);
            ll_home_care_main.setVisibility(View.GONE);
            ll_medical_package_main.setVisibility(View.GONE);
            txt_sub_total.setText("00.00");
            txt_extra_off.setText("00.00");
            txt_total.setText("00.00");
        }
    }


    public void showLabExistDialog(Context context) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle("Warning");
        alertDialog.setMessage("Please note that different tests from different labs will require a separate sample for each lab. The home sampling services are provided by individual labs, and ordering different tests from different labs will require each of them to visit separately and draw separate samples");
        alertDialog.setNeutralButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }


}
