package com.udhc.shifa4u.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.udhc.shifa4u.Adapters.LabTestResultAdapter;
import com.udhc.shifa4u.Adapters.PhysiotherapyServiceTypesAdapter;
import com.udhc.shifa4u.Adapters.SubTerapyAdapter;
import com.udhc.shifa4u.Interfaces.MenuItemsInterface;
import com.udhc.shifa4u.Models.CartModel;
import com.udhc.shifa4u.Models.HomeCareSubServicesModel;
import com.udhc.shifa4u.Models.LabtestResultModel;
import com.udhc.shifa4u.Models.MedicalPackages.LabPackagesComparison;
import com.udhc.shifa4u.Models.MedicalPackages.MedicalPackagesModel;
import com.udhc.shifa4u.Models.MenuModel;
import com.udhc.shifa4u.Models.PhysiotherapyServiceTypesModel;
import com.udhc.shifa4u.Models.Procedure;
import com.udhc.shifa4u.Models.ProfileModel;
import com.udhc.shifa4u.Models.RadioLogyDetail.RadiologyLabsComparison;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.GzipUtils;
import com.udhc.shifa4u.Utilities.HeaderManager;
import com.udhc.shifa4u.Utilities.MyUtils;
import com.udhc.shifa4u.api.RestAPIFactory;

import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeCareSubTherapyActivity extends AppCompatActivity {

    String ProductTypePrefix = "";
    private ProgressDialog progressDialog;
    private ListView lv_sub_therapy;
    private LinearLayout lv_list_container;
    long specialMedicalId = 0;
    private HeaderManager headerManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_care_sub_therapy);

        ProductTypePrefix = getIntent().getStringExtra("ProductTypePrefix");

        setHeader();
        specialMedicalId = getIntent().getLongExtra("spcialMedicalId", 0);

        lv_list_container = (LinearLayout) findViewById(R.id.lv_list_container);
        if (!ProductTypePrefix.equals("")) {
            getPhysiotherapyServiceTypes(ProductTypePrefix);
        } else {
            lv_list_container.setVisibility(View.GONE);
        }

        lv_sub_therapy = (ListView) findViewById(R.id.lv_sub_therapy);


    }

    @Override
    protected void onResume() {
        super.onResume();
        setHeader();
    }

    private void setHeader() {
        headerManager = new HeaderManager(HomeCareSubTherapyActivity.this, findViewById(R.id.header));
        headerManager.setMenutype(HeaderManager.BACK_MENU);
        headerManager.setShowFirstImageButton(false);
        headerManager.setShowSecoundImageButton(false);
//        headerManager.setShowBottomView(true);
        headerManager.setHeaderColor(getResources().getColor(R.color.header_background_color));

        String title =getIntent().getStringExtra("Title");
        if (title.contains("Physio"))
        {
           title =  title + " Sub Categories";
        }
        headerManager.setTitle(title);
    }


    void getPhysiotherapyServiceTypes(String prefix) {
        showProgressDialog("Please wait...");

        String str = String.format("{\"SelectedLetter\": \"All\",\"SkipPages\": \"0\",\"Records\": \"20\",\"MedicalSpecialityId\": \"%s\",\"ProductTypePrefix\" : \"%s\"}", String.valueOf(specialMedicalId), prefix);


        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), str);

        //showProgressDialog("Please Wait");
        try {
            Call<ResponseBody> posts = RestAPIFactory.getApi().getPaginaTableHomeCareServices(body);
            posts.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    dismissProgressDislog();
                    try {
                        String Json = GzipUtils.decompress(response.body().bytes());

                        Type listType = new TypeToken<HomeCareSubServicesModel>() {
                        }.getType();
                        HomeCareSubServicesModel homeCareSubServicesModel = new Gson().fromJson(Json, listType);
                        SubTerapyAdapter subTerapyAdapter = new SubTerapyAdapter(HomeCareSubTherapyActivity.this, (ArrayList<Procedure>) homeCareSubServicesModel.getProcedures(), new MenuItemsInterface() {
                            @Override
                            public void onItemClick(Integer position, MenuModel menuModel) {

                            }

                            @Override
                            public void onButtonClick(Integer position, Object model, View button) {
                                if (button.getId() == R.id.btn_order) {
                                    if ((Procedure) model != null) {
                                        CartModel cartModel = new CartModel();

                                        cartModel.setOriginalObject((Procedure) model);
                                        cartModel.setProductId(((Procedure) model).getMedicalProductId());
                                        cartModel.setProductName(((Procedure) model).getMedicalProductName());
                                        cartModel.setProductShortDescription(((Procedure) model).getMedicalProductDescription());
                                        cartModel.setPriice(((Procedure) model).getProductLineDiscountedPrice());
                                        cartModel.setOriginalPriice(((Procedure) model).getMedicalProductPrice());
                                        cartModel.setProductType(CartModel.product.HOME_CARE);
                                        cartModel.setProductLineId(String.valueOf(((Procedure) model).getProductLineId()));

                                        try {
                                            MyUtils.addToCart(cartModel);
                                            Toast.makeText(HomeCareSubTherapyActivity.this, "Product added to cart", Toast.LENGTH_SHORT).show();
                                            startActivity(new Intent(HomeCareSubTherapyActivity.this, CheckoutActivity.class));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                        setHeader();

                                    }
                                }

                            }

                            @Override
                            public void onItemClick(Integer position, Object o) {

                            }
                        });
                        lv_sub_therapy.setAdapter(subTerapyAdapter);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


//
//                ItemModel itemModel = LoganSquare.parse(new GZIPInputStream(response.body().byteStream()), ItemModel.class);

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    dismissProgressDislog();
                    Log.e("", "");
                }
            });

        } catch (Exception e) {

        }

    }

    public void showProgressDialog(String message) {
        if (progressDialog == null)
            progressDialog = new ProgressDialog(HomeCareSubTherapyActivity.this);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    public void dismissProgressDislog() {
        if (progressDialog.isShowing() && progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }
}
