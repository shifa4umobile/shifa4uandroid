package com.udhc.shifa4u.Fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.MyUtils;
import com.udhc.shifa4u.api.RestAPIFactory;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ContactUsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ContactUsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private Button btn_submit;
    private EditText txt_message;
    private AutoCompleteTextView txt_contact_number;
    private AutoCompleteTextView txt_email;
    private AutoCompleteTextView txt_name;
    private ProgressDialog progressDialog;

    public ContactUsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LiveChatFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ContactUsFragment newInstance(String param1, String param2) {
        ContactUsFragment fragment = new ContactUsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.activity_contact_us, container, false);
    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        txt_name = (AutoCompleteTextView) view.findViewById(R.id.txt_name);
        txt_email = (AutoCompleteTextView) view.findViewById(R.id.txt_email);
        txt_contact_number = (AutoCompleteTextView) view.findViewById(R.id.txt_contact_number);
        txt_message = (EditText) view.findViewById(R.id.txt_message);
        btn_submit = (Button) view.findViewById(R.id.btn_submit);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!txt_name.getText().toString().isEmpty() && !txt_name.getText().toString().equals("")) {
                    if (!txt_email.getText().toString().isEmpty() && !txt_email.getText().toString().equals("")) {
                        if (MyUtils.isValidEmail(txt_email.getText().toString())) {
                            if (!txt_contact_number.getText().toString().isEmpty() && !txt_contact_number.getText().toString().equals("")) {
                                if (!txt_message.getText().toString().isEmpty() && !txt_message.getText().toString().equals("")) {
                                    contactUs();
                                } else {
                                    //fill Message
                                    Toast.makeText(getActivity(), "Please add some message", Toast.LENGTH_LONG).show();
                                }
                            } else {
                                //fill phone
                                Toast.makeText(getActivity(), "Please add phone Number", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            //fill valid email
                            Toast.makeText(getActivity(), "Please enter valid email address", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        //fill email
                        Toast.makeText(getActivity(), "Please add email address", Toast.LENGTH_LONG).show();
                    }
                } else {
                    //fill name
                    Toast.makeText(getActivity(), "Please add Name", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    private void contactUs() {
        showProgressDialog("Please Wait...");
        try {
            Call<ResponseBody> posts = RestAPIFactory.getApi().contactUs(null,txt_name.getText().toString(), txt_email.getText().toString(), txt_contact_number.getText().toString(), txt_message.getText().toString(), "CNTCUS");
            posts.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    dismissProgressDislog();
                    if (response.code() == 200) {
                        Toast.makeText(getActivity(), "Data submitted successfully", Toast.LENGTH_LONG).show();
                        txt_name.setText("");
                        txt_email.setText("");
                        txt_contact_number.setText("");
                        txt_message.setText("");
//                        }
                    } else {
                        Toast.makeText(getActivity(), "Something went wrong.\n Please try again later", Toast.LENGTH_LONG).show();
                    }


                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    dismissProgressDislog();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

//
//    @Override
//    protected void onResume() {
//        super.onResume();
//
//    }

    public void showProgressDialog(String message) {
        if (progressDialog == null)
            progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    public void dismissProgressDislog() {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }
//
//    private void setHeader() {
//        HeaderManager headerManager = new HeaderManager(getActivity(), findViewById(R.id.header));
//        headerManager.setMenutype(HeaderManager.BACK_MENU);
//        headerManager.setShowFirstImageButton(false);
//        headerManager.setShowSecoundImageButton(false);
//        headerManager.setTitle(getString(R.string.title_contact_us));
//        headerManager.setShowBottomView(true);
//        headerManager.setHeaderColor(getResources().getColor(R.color.header_background_color));
//    }


}
