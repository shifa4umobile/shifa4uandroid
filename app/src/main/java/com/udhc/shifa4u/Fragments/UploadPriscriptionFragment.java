package com.udhc.shifa4u.Fragments;


import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myhexaville.smartimagepicker.ImagePicker;
import com.myhexaville.smartimagepicker.OnImagePickedListener;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.HeaderManager;
import com.udhc.shifa4u.Utilities.MyUtils;
import com.udhc.shifa4u.api.RestAPIFactory;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link UploadPriscriptionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UploadPriscriptionFragment extends AppCompatActivity {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private Button btn_submit;
    private EditText txt_message;
    private AutoCompleteTextView txt_contact_number;
    private AutoCompleteTextView txt_email;
    private AutoCompleteTextView txt_name;
    private ProgressDialog progressDialog;
    private ImagePicker imagePicker;
    private Button btn_upload;
    private Button btn_upload_pdf;

    private final String[] imageExtensions =  new String[] {"jpg", "png", "gif","jpeg"};
    private final String[] pdfExtensions =  new String[] {"pdf"};

    public static final int RC_PHOTO_PICKER_PERM = 123;
    public static final int RC_FILE_PICKER_PERM = 321;
    private static final int CUSTOM_REQUEST_CODE = 532;
    private File file = null;
    private ArrayList<String> photoPaths ;
    private ArrayList<String> docPaths;
    private TextView txt_upload;
    private LinearLayout ll_attachment_container;
    private ImageView img_delete;
    private HeaderManager headerManager;


    public UploadPriscriptionFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LiveChatFragment.
     */
    // TODO: Rename and change types and number of parameters
  /*  public static UploadPriscriptionFragment newInstance(String param1, String param2) {
        UploadPriscriptionFragment fragment = new UploadPriscriptionFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }*/

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_upload_priscription);

        setHeader();
        if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            //Log.v(TAG,"Permission is granted");
            //File write logic here
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

        }

       else if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            //Log.v(TAG,"Permission is granted");
            //File write logic here
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 1);

        }

        txt_name = (AutoCompleteTextView) findViewById(R.id.txt_name);
        txt_email = (AutoCompleteTextView) findViewById(R.id.txt_email);
        txt_contact_number = (AutoCompleteTextView) findViewById(R.id.txt_contact_number);
        txt_message = (EditText) findViewById(R.id.txt_message);
        btn_submit = (Button) findViewById(R.id.btn_submit);
        btn_upload = (Button) findViewById(R.id.btn_upload_image);
        btn_upload_pdf = (Button) findViewById(R.id.btn_upload_pdf);
        txt_upload = (TextView) findViewById(R.id.txt_upload);
        ll_attachment_container = (LinearLayout) findViewById(R.id.ll_attachment_container);
        img_delete=(ImageView)findViewById(R.id.img_delete);
        file = null;

        btn_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    //Log.v(TAG,"Permission is granted");
                    //File write logic here
                    ActivityCompat.requestPermissions(UploadPriscriptionFragment.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

                }
                else  if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(UploadPriscriptionFragment.this, new String[]{Manifest.permission.CAMERA}, 1);
                }
                else {

                    FilePickerBuilder.getInstance().setMaxCount(5)
                            .setSelectedFiles(photoPaths)
                            .setActivityTheme(R.style.LibAppTheme)
                            .setMaxCount(1)
                            .pickPhoto(UploadPriscriptionFragment.this);
                }
               /* refreshImagePicker();
                imagePicker.choosePicture(true);*/
            }
        });


        btn_upload_pdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    //Log.v(TAG,"Permission is granted");
                    //File write logic here
                    ActivityCompat.requestPermissions(UploadPriscriptionFragment.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

                }
                else {

                    FilePickerBuilder.getInstance().setMaxCount(5)
                            .setSelectedFiles(docPaths)
                            .setActivityTheme(R.style.LibAppTheme)
                            .setMaxCount(1)
                            .pickFile(UploadPriscriptionFragment.this );
                }
               /* refreshImagePicker();
                imagePicker.choosePicture(true);*/
            }
        });

        img_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_attachment_container.setVisibility(View.GONE);
                if (photoPaths != null)
                photoPaths.clear();
                if (docPaths !=null)
                docPaths.clear();
                file=null;
            }
        });


        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean isValidEmail = android.util.Patterns.EMAIL_ADDRESS.matcher(txt_email.getText().toString().trim()).matches();

                if (!txt_name.getText().toString().isEmpty() && !txt_name.getText().toString().equals("")) {
                    if (!txt_email.getText().toString().isEmpty() && !txt_email.getText().toString().equals("")) {
                        if (MyUtils.isValidEmail(txt_email.getText().toString())) {
                            if (!txt_contact_number.getText().toString().isEmpty() && !txt_contact_number.getText().toString().equals("")) {
                                if (!txt_message.getText().toString().isEmpty() && !txt_message.getText().toString().equals("")) {
                                    if (!txt_upload.getText().toString().equals("")) {
                                        uploadPriscription();
                                    }
                                    else
                                    {
                                        Toast.makeText(UploadPriscriptionFragment.this, "Please Upload prescription", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    //fill Message
                                    Toast.makeText(UploadPriscriptionFragment.this, "Please add some message", Toast.LENGTH_LONG).show();
                                }
                            } else {
                                //fill phone
                                Toast.makeText(UploadPriscriptionFragment.this, "Please add phone Number", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            //fill valid email
                            Toast.makeText(UploadPriscriptionFragment.this, "Please enter valid email address", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        //fill email
                        Toast.makeText(UploadPriscriptionFragment.this, "Please add email address", Toast.LENGTH_LONG).show();
                    }
                } else {
                    //fill name
                    Toast.makeText(UploadPriscriptionFragment.this, "Please add Name", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    void setHeader()
    {
        HeaderManager headerManager = new HeaderManager(UploadPriscriptionFragment.this, findViewById(R.id.header));
        headerManager.setMenutype(HeaderManager.BACK_MENU);
        headerManager.setShowFirstImageButton(false);
        headerManager.setShowSecoundImageButton(false);
        headerManager.setTitle("Upload Prescription");
        headerManager.setShowBottomView(true);
        headerManager.setHeaderColor(getResources().getColor(R.color.header_background_color));
    }

 /*   @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_upload_priscription, container, false);
    }*/

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case FilePickerConst.REQUEST_CODE_PHOTO:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    if (docPaths != null)
                        docPaths.clear();
                    photoPaths = new ArrayList<>();
                    photoPaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA));
                }
                break;

            case FilePickerConst.REQUEST_CODE_DOC:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    if (photoPaths!= null)
                        photoPaths.clear();
                    docPaths = new ArrayList<>();
                    docPaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS));
                }
                break;
        }

        if (data != null)
        addThemToView(photoPaths, docPaths);

      //  imagePicker.handleActivityResult(resultCode, requestCode, data);
    }

    private void addThemToView(ArrayList<String> imagePaths, ArrayList<String> docPaths) {
        ArrayList<String> filePaths = new ArrayList<>();
        if (imagePaths != null) filePaths.addAll(imagePaths);

        if (docPaths != null) filePaths.addAll(docPaths);
        String filename=filePaths.get(0).substring(filePaths.get(0).lastIndexOf("/")+1);
        if (filePaths != null) {
            txt_upload.setText(filename);
            file = new File(filePaths.get(0));
            ll_attachment_container.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults[0]== PackageManager.PERMISSION_GRANTED){
            Log.v("","Permission: "+permissions[0]+ "was "+grantResults[0]);
            //resume tasks needing this permission
        }
     //   imagePicker.handlePermission(requestCode, grantResults);
    }

    private void uploadPriscription() {
        showProgressDialog("Please Wait...");

        Bitmap bitmap;
        try {
            
          /*  if (isImage(file))
            {*/
               bitmap =  convertToBitmap (file);

              //  Toast.makeText(this, "Image", Toast.LENGTH_SHORT).show();
            //}
          /*  else
            {
                convertToPdf(file);
                Toast.makeText(this, "Pdf", Toast.LENGTH_SHORT).show();
            }*/

            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));

            int size = (int) file.length();
            byte[] bytes = new byte[size];
            try {

                buf.read(bytes, 0, bytes.length);
                buf.close();
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            FileInputStream fileInputStream = new FileInputStream(file);

            byte[] blob=bytes;
            Bitmap bmp=BitmapFactory.decodeByteArray(blob,0,blob.length);

            Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());

            Call<ResponseBody> posts = RestAPIFactory.getApi().contactUs(file, txt_name.getText().toString(),txt_contact_number.getText().toString() ,    txt_email.getText().toString(), txt_message.getText().toString(), "");
            posts.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    dismissProgressDislog();
                    if (response.code() == 200) {
                        Toast.makeText(UploadPriscriptionFragment.this, "Data submitted successfully", Toast.LENGTH_LONG).show();
                        txt_name.setText("");
                        txt_email.setText("");
                        txt_contact_number.setText("");
                        txt_message.setText("");
                        file = null;
                        txt_upload.setText("");
                        ll_attachment_container.setVisibility(View.GONE);
//                        }
                    } else {
                        Toast.makeText(UploadPriscriptionFragment.this, "Something went wrong.\n Please try again later", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    dismissProgressDislog();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private Bitmap convertToBitmap(File file) {


        String filePath = file.getPath();
        Bitmap bitmap = BitmapFactory.decodeFile(filePath);
        return bitmap;
    }

    public void showProgressDialog(String message) {
        if (progressDialog == null)
            progressDialog = new ProgressDialog(UploadPriscriptionFragment.this);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    public void dismissProgressDislog() {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    private void refreshImagePicker() {
        imagePicker = new ImagePicker(this,
                null,
                new OnImagePickedListener() {
                    @Override
                    public void onImagePicked(Uri imageUri) {
                        file = imagePicker.getImageFile();
                        //Picasso.with(UploadPriscriptionFragment.this).load(imageUri).into(txt_upload);
                        ll_attachment_container.setVisibility(View.VISIBLE);
                    }
                });

//            imagePicker.setWithImageCrop(
//                    1,
//                    1
//            );

    }

    public boolean isImage(File file)
    {
        for (String extension : imageExtensions)
        {
            if (file.getName().toLowerCase().endsWith(extension))
            {
                return true;
            }
        }
        return false;
    }


}
