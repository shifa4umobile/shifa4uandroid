package com.udhc.shifa4u.Fragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.udhc.shifa4u.Activities.MyOrderActivity;
import com.udhc.shifa4u.Activities.OrderDetailActivity;
import com.udhc.shifa4u.Activities.Shifa4U;
import com.udhc.shifa4u.Adapters.LabTestResultAdapter;
import com.udhc.shifa4u.Adapters.MyOrderAdapter;
import com.udhc.shifa4u.Interfaces.MenuItemsInterface;
import com.udhc.shifa4u.Models.LabtestResultModel;
import com.udhc.shifa4u.Models.MenuModel;
import com.udhc.shifa4u.Models.MyOrdersMode.AllOrdersModel;
import com.udhc.shifa4u.Models.MyOrdersMode.CanceledOrder;
import com.udhc.shifa4u.Models.MyOrdersMode.ClosedOrders;
import com.udhc.shifa4u.Models.MyOrdersMode.OpenOrder;
import com.udhc.shifa4u.Models.MyOrdersMode.OrderDetailsVM;
import com.udhc.shifa4u.Models.PaymentMode.OrderDetail;
import com.udhc.shifa4u.Models.RadioLogyDetail.Contact;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.MyGeneric;
import com.udhc.shifa4u.api.RestAPIFactory;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MyOrderListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyOrderListFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private ProgressDialog progressDialog;
    private ListView lv_my_order;


    public MyOrderListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LabTestResultFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MyOrderListFragment newInstance(String param1, String param2) {
        MyOrderListFragment fragment = new MyOrderListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public static MyOrderListFragment newInstance(String param1) {
        MyOrderListFragment fragment = new MyOrderListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_lab_test_result, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        lv_my_order = (ListView) view.findViewById(R.id.lv_lab_test_result);

        int tabType = 0;

        if (mParam1.equalsIgnoreCase("Open")) {
            tabType = 1;
        } else if (mParam1.equalsIgnoreCase("Closed")) {
            tabType = 2;
        } else if (mParam1.equalsIgnoreCase("Canceled")) {
            tabType = 3;
        }
        lv_my_order.setAdapter(new MyOrderAdapter(getActivity(), MyOrderActivity.allOrdersModel, tabType, new MenuItemsInterface() {
            @Override
            public void onItemClick(Integer position, MenuModel menuModel) {
            }

            @Override
            public void onButtonClick(Integer position, Object model, View button) {
            }

            @Override
            public void onItemClick(Integer position, Object o) {

                Intent intent = new Intent(getActivity(), OrderDetailActivity.class);
                Bundle bundle = new Bundle();
                ArrayList<OrderDetailsVM> orderDetailsVMS = null;
                String orderJson = null;
                Gson gson = new Gson();
                if (o instanceof OpenOrder) {
                    orderJson= gson.toJson(((OpenOrder) o));
                } else if (o instanceof ClosedOrders) {
                    orderJson= gson.toJson(((ClosedOrders) o));
                } else if (o instanceof CanceledOrder) {
                    orderJson= gson.toJson(((CanceledOrder) o));
                }

                intent.putExtra("orderModel", orderJson);
                getActivity().startActivity(intent);

            }
        }));

        if (Shifa4U.checkNetwork.isInternetAvailable()) {

        } else {
            Toast.makeText(getActivity(), R.string.internet_not_available, Toast.LENGTH_SHORT).show();
        }
    }


}
