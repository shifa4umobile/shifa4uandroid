package com.udhc.shifa4u.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.udhc.shifa4u.Activities.Shifa4U;
import com.udhc.shifa4u.Adapters.CityAdapter;
import com.udhc.shifa4u.Adapters.CountryAdapter;
import com.udhc.shifa4u.Interfaces.FragmentInterface;
import com.udhc.shifa4u.Interfaces.MenuItemsInterface;
import com.udhc.shifa4u.Models.CityModel;
import com.udhc.shifa4u.Models.CountryModel;
import com.udhc.shifa4u.Models.MenuModel;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.MyGeneric;
import com.udhc.shifa4u.api.RestAPIFactory;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link ChangeCityFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChangeCityFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private AutoCompleteTextView txt_change_city;
    private AutoCompleteTextView txt_change_country;
    private ProgressDialog progressDialog;
    private ArrayList<CityModel> cityModels;
    private Button btn_save;

FragmentInterface fragmentInterface;
    public ChangeCityFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public ChangeCityFragment(FragmentInterface fragmentInterface) {
        // Required empty public constructor
        this.fragmentInterface=fragmentInterface;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ChangeCityFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ChangeCityFragment newInstance(String param1, String param2) {
        ChangeCityFragment fragment = new ChangeCityFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_change_city, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        txt_change_city = (AutoCompleteTextView) view.findViewById(R.id.txt_change_city);
        txt_change_country = (AutoCompleteTextView) view.findViewById(R.id.txt_change_country);
        btn_save = (Button) view.findViewById(R.id.btn_save);


        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CityId > -1) {
                    if (Shifa4U.checkNetwork.isInternetAvailable()) {
                        setGlobalCity(String.valueOf(CityId));
                    } else {
                        Toast.makeText(getActivity(), R.string.network_not_available, Toast.LENGTH_SHORT).show();
                    }
                }
            }

        });


        txt_change_city.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                txt_change_city.showDropDown();
            }
        });

        txt_change_country.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                txt_change_country.showDropDown();
            }
        });


        if (Shifa4U.checkNetwork.isInternetAvailable())
        {
            getAllCountries();
            getCitiesForCountry(Shifa4U.mySharePrefrence.getCountryId());
        } else
        {
            Toast.makeText(getActivity(), R.string.network_not_available, Toast.LENGTH_SHORT).show();
        }

    }


    public void getCitiesForCountry(String countryID) {
        showProgressDialog("Please Wait");
        try {
            Call<ResponseBody> posts = RestAPIFactory.getApi().getCitiesForCountry(countryID);
            posts.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                    if (response.isSuccessful()) {

                        List<CityModel> cityModels = MyGeneric.processResponse(response,new TypeToken<List<CityModel>>(){});

                        if (cityModels != null && cityModels.size() > 0) {
                            txt_change_city.setAdapter(new CityAdapter(getActivity(), (ArrayList<CityModel>) cityModels, menuItemsInterface));
                            cityModels = (ArrayList<CityModel>) cityModels;
                            if(Shifa4U.mySharePrefrence.getCityId()!=null && !Shifa4U.mySharePrefrence.getCityId().isEmpty())
                            {
                               if(cityModels!=null && cityModels.size()>0)
                               {
                                   for(CityModel cityModel:cityModels)
                                   {
                                       if(cityModel.getCityId()==Integer.valueOf(Shifa4U.mySharePrefrence.getCityId()))
                                       {
                                           txt_change_city.setText(cityModel.getCityName());
                                           CityId = cityModel.getCityId();
                                       }
                                   }
                               }
                            }
                        } else {
                            txt_change_city.setAdapter(null);
                            Shifa4U.mySharePrefrence.clearCities();
                        }
                        dismissProgressDislog();
                        Log.e("response", response.body().toString());
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                    dismissProgressDislog();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    public void showProgressDialog(String message) {
        if (progressDialog == null)
            progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    public void dismissProgressDislog() {
        if(progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        }
    }


    private int CityId;
    private MenuItemsInterface menuItemsInterface = new MenuItemsInterface() {
        @Override
        public void onItemClick(Integer position, MenuModel menuModel) {

        }

        @Override
        public void onButtonClick(Integer position, Object model, View button) {

        }

        @Override
        public void onItemClick(Integer position, Object o) {
            CityId = -1;
            txt_change_city.dismissDropDown();
            txt_change_city.setText(((CityModel) o).getCityName());
            CityId = ((CityModel) o).getCityId();
        }
    };

    public void setGlobalCity(final String cityID) {
        showProgressDialog("Please Wait");
        Call<ResponseBody> setCity = RestAPIFactory.getApi().setGlobalCity(String.valueOf(cityID));
        setCity.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dismissProgressDislog();
                if (response.code() == 200) {
                    Shifa4U.mySharePrefrence.setCityId(cityID);
                    Shifa4U.mySharePrefrence.setCities(cityModels);
//                    startActivity(new Intent(getActivity(), LoginActivity.class));
//                    finish();

                    Toast.makeText(getActivity(), R.string.change_city_success_message, Toast.LENGTH_LONG).show();
                    fragmentInterface.onCloseFragent(null);

                } else {
                    Toast.makeText(getActivity(), R.string.api_error_message, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dismissProgressDislog();
            }
        });
    }



    public void getAllCountries() {
        showProgressDialog("Please Wait");
        try {
            Call<ResponseBody> posts = RestAPIFactory.getApi().getAllCountries();
            posts.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                    if (response.isSuccessful()) {
                        List<CountryModel> countryModels=MyGeneric.processResponse(response,new TypeToken<List<CountryModel>>(){});
                        if (countryModels != null && countryModels.size() > 0) {
                            txt_change_country.setAdapter(new CountryAdapter(getActivity(), (ArrayList<CountryModel>) countryModels));


                            if(Shifa4U.mySharePrefrence.getCountryId()!=null && !Shifa4U.mySharePrefrence.getCountryId().isEmpty())
                            {
                                if(countryModels!=null && countryModels.size()>0)
                                {
                                    for(CountryModel countryModel:countryModels)
                                    {
                                        if(countryModel.getCountryId()==Integer.valueOf(Shifa4U.mySharePrefrence.getCountryId()))
                                        {
                                            txt_change_country.setText(countryModel.getCountryName());
                                         //   CityId = cityModel.getCityId();
                                        }
                                    }
                                }
                            }
                        } else {
                            txt_change_country.setAdapter(null);
                        }
                        dismissProgressDislog();
                        Log.e("response", response.body().toString());
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                    dismissProgressDislog();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            dismissProgressDislog();
        }
    }
}
