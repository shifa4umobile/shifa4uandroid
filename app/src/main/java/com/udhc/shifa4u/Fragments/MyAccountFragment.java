package com.udhc.shifa4u.Fragments;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.gson.JsonObject;
import com.udhc.shifa4u.Activities.CodeConfirmationActivity;
import com.udhc.shifa4u.Activities.MainActivity;
import com.udhc.shifa4u.Activities.MyOrderActivity;
import com.udhc.shifa4u.Activities.MyProfileActivity;
import com.udhc.shifa4u.Activities.MyResultActivity;
import com.udhc.shifa4u.Activities.Shifa4U;
import com.udhc.shifa4u.Activities.VerificationActivity;
import com.udhc.shifa4u.Models.CustomerProfileModel;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.api.RestAPIFactory;

import org.json.JSONException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MyAccountFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyAccountFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private ImageView img_my_profile;
    private ImageView img_lab_results;
    private ImageView img_my_appointments;
    private ImageView img_your_orders;

    private ProgressDialog progressDialog;


    public MyAccountFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MyAccountFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MyAccountFragment newInstance(String param1, String param2) {
        MyAccountFragment fragment = new MyAccountFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_account, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        img_my_profile = (ImageView) view.findViewById(R.id.img_my_profile);
        img_lab_results = (ImageView) view.findViewById(R.id.img_lab_results);
        img_my_appointments = (ImageView) view.findViewById(R.id.img_my_appointments);
        img_your_orders = (ImageView) view.findViewById(R.id.img_your_orders);

        img_my_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).addmyAccountActivityName("myprofile");
                startActivity(new Intent(getActivity(), MyProfileActivity.class));
            }
        });

        img_lab_results.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CustomerProfileModel customerProfileModel = Shifa4U.mySharePrefrence.getProfile();
                if (customerProfileModel != null)
                {
                    if (customerProfileModel.isVerified())
                    {
                        ((MainActivity)getActivity()).addmyAccountActivityName("myresult");
                        startActivity(new Intent(getActivity(), MyResultActivity.class));
                    }
                    else
                    {
                        AlertDialog.Builder builder;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Dialog_Alert);
                        } else {
                            builder = new AlertDialog.Builder(getActivity());
                        }
                        builder.setTitle("Account Not Verified")
                                .setMessage("Please verify your account first to access lab results.")
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // continue with delete
                                        JsonObject jsonObject = new JsonObject();
                                        String email = Shifa4U.mySharePrefrence.getProfile().getLoginName();
                                        jsonObject.addProperty("LoginName", email);

                                        try {
                                            resendConfirmationCode(jsonObject);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing

                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }
                }

            }
        });
        img_your_orders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).addmyAccountActivityName("myorder");
                startActivity(new Intent(getActivity(), MyOrderActivity.class));
            }
        });


    }

    public void resendConfirmationCode(JsonObject jsonObject) throws JSONException {
        showProgressDialog("Please wait...");
        //  RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), "shahid.mrd@hotmail.com");

        //showProgressDialog("Please Wait");
        //   String email = jsonObject.getString("LoginName");
        try {
            Call<ResponseBody> posts = RestAPIFactory.getApi().ResendVerificationCode(jsonObject);
            posts.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.code() == 200) {
                        //showALertBox(ForgotPasswordActivity.this);
                        ((MainActivity)getActivity()).addmyAccountActivityName("codecon_firmation");
                        startActivity(new Intent(getActivity() , CodeConfirmationActivity.class));

                    } else {
                        //  Toast.makeText(ForgotPasswordActivity.this, "Something went wrong.\n Please try again later", Toast.LENGTH_LONG).show();
                    }
                    dismissProgressDislog();
                }
                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    dismissProgressDislog();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void showProgressDialog(String message) {
        if (progressDialog == null)
            progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    public void dismissProgressDislog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        }
    }

}
