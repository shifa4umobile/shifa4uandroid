package com.udhc.shifa4u.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.udhc.shifa4u.Activities.AmericanTeleclinicActivity;
import com.udhc.shifa4u.Activities.HealthCareActivity;
import com.udhc.shifa4u.Activities.LabsActivity;
import com.udhc.shifa4u.Activities.MedicalPackagesActivity;
import com.udhc.shifa4u.Activities.RadiologyMainActivity;
import com.udhc.shifa4u.Activities.Shifa4uTeleclinic;
import com.udhc.shifa4u.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link HomeFragment} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private LinearLayout img_lab;
    private LinearLayout img_radiology;
    private LinearLayout img_medical_packages;
    private LinearLayout img_pharmacy;
    private LinearLayout img_american_teleclinic;
    private LinearLayout img_homecare;


    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.activity_home_new, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI(view);
    }

    private void initUI(View view) {
        img_lab = (LinearLayout) view.findViewById(R.id.img_lab);
        img_radiology = (LinearLayout) view.findViewById(R.id.img_radiology);
        img_medical_packages = (LinearLayout) view.findViewById(R.id.img_medical_packages);
        img_pharmacy = (LinearLayout) view.findViewById(R.id.img_pharmacy);
        img_american_teleclinic = (LinearLayout) view.findViewById(R.id.img_american_teleclinic);
        img_homecare = (LinearLayout) view.findViewById(R.id.img_homecare);


        img_lab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), LabsActivity.class));
                getActivity().overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );
            }
        });
        img_radiology.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), RadiologyMainActivity.class));
                getActivity().overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );
            }
        });
        img_medical_packages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), MedicalPackagesActivity.class));
                getActivity().overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );
            }
        });
        img_pharmacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), Shifa4uTeleclinic.class));
                getActivity().overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );
            }
        });
        img_american_teleclinic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), AmericanTeleclinicActivity.class));
                getActivity().overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );
            }
        });
        img_homecare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), HealthCareActivity.class));
                getActivity().overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );
            }
        });
    }
}
