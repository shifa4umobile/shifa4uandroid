package com.udhc.shifa4u.Fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.udhc.shifa4u.Activities.Shifa4U;
import com.udhc.shifa4u.Adapters.LabTestResultAdapter;
import com.udhc.shifa4u.Adapters.RadiologyResultAdapter;
import com.udhc.shifa4u.Models.LabtestResultModel;
import com.udhc.shifa4u.Models.RadiologyResultModel;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.MyGeneric;
import com.udhc.shifa4u.api.RestAPIFactory;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RadiologyResultFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RadiologyResultFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private ListView lv_radiology_result;
    private ProgressDialog progressDialog;


    public RadiologyResultFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment RadiologyResultFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static RadiologyResultFragment newInstance(String param1, String param2) {
        RadiologyResultFragment fragment = new RadiologyResultFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        lv_radiology_result=(ListView)view.findViewById(R.id.lv_radiology_result);

        if(Shifa4U.checkNetwork.isInternetAvailable())
        {
            getRadioLogyResult();
        }
        else
        {
            Toast.makeText(getActivity(), R.string.internet_not_available, Toast.LENGTH_SHORT).show();
        }
    }

    private void getRadioLogyResult() {
        showProgressDialog("Please wait...");

        try {
            Call<ResponseBody> posts = RestAPIFactory.getApi().getRadilogyResults(Shifa4U.mySharePrefrence.getLogin().getTokenType() + " " + Shifa4U.mySharePrefrence.getLogin().getAccessToken());
            posts.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.code() == 200 && response.body() != null) {
                        List<RadiologyResultModel> radiologyResultModels = MyGeneric.processResponse(response,new TypeToken<List<RadiologyResultModel>>(){});

                        if(radiologyResultModels!=null && radiologyResultModels.size()>0)
                        {
                            RadiologyResultAdapter radiologyResultAdapter=new RadiologyResultAdapter(getActivity(), (ArrayList<RadiologyResultModel>) radiologyResultModels);
                            lv_radiology_result.setAdapter(radiologyResultAdapter);
                        }
                        else
                        {
                            lv_radiology_result.setAdapter(null);
                        }
                    } else {
                        Toast.makeText(getActivity(), "Something went wrong.\n Please try again later", Toast.LENGTH_LONG).show();
                    }

                    dismissProgressDislog();
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    dismissProgressDislog();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            dismissProgressDislog();
        }


    }


    public void showProgressDialog(String message) {
        if (progressDialog == null)
            progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    public void dismissProgressDislog() {
        if (progressDialog.isShowing() && progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_radiology_result, container, false);
    }

}
