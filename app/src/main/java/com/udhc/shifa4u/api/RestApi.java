package com.udhc.shifa4u.api;

import com.google.gson.JsonObject;

import java.io.File;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Ali Imran Bangash on 5/5/2018.
 */

public interface RestApi {

    @GET("api/common/v1/getallcountries")
    Call<ResponseBody> getAllCountries();

    @GET("api/common/v1/getcitiesforcountry")
    Call<ResponseBody> getCitiesForCountry(@Query("countryId") String countryId);


    @POST("api/common/v1/setglobalCity")
    @Headers({
            "conten: application/json",
            "Content-Type: application/x-www-form-urlencoded"
    })
    @FormUrlEncoded
    Call<ResponseBody> setGlobalCity(@Field("cityId") String cityId);


    @POST("api/account/v1/MobileRegistration")
    @Headers({"Content-Type: application/json"})
    Call<ResponseBody> mobileRegistration(@Body RequestBody requestBody);

    @POST("api/account/v1/verifyConfirmationCode")
    @Headers({"Content-Type: application/json"})
    Call<ResponseBody> VerifyAccount(@Body RequestBody requestBody);


    @POST("api/account/v1/ForgotPasswordMobile")
    @Headers({"Content-Type: application/json"})
    Call<ResponseBody> ForgotPasswordMobile(@Body JsonObject email);


    @POST("api/account/v1/ResendConfirmationEmailOrSms")
    @Headers({"Content-Type: application/json"})
    Call<ResponseBody> ResendVerificationCode(@Body JsonObject email);

    @POST("token")
    @Headers({
            "Content-Type: application/x-www-form-urlencoded"
    })
    @FormUrlEncoded
    Call<ResponseBody> Login(@Field("grant_type") String grant_type,
                             @Field("username") String username,
                             @Field("password") String password,
                             @Field("client_id") String client_id

    );

        @GET("api/customeraccount/v1/customerprofile")
    Call<ResponseBody> getCustomerProfile(@Header("Authorization") String Authorization);


   @GET("api/customeraccount/v1/getuserfamilymembers")
    Call<ResponseBody> getuserfamilymembers(@Header("Authorization") String Authorization);


   @POST("api/CustomerAccount/v1/meetingconnection")
    Call<ResponseBody> meetingConnection(@Header("Authorization") String Authorization , @Body RequestBody requestBody);


   @POST("api/customeraccount/v1/saveuserfamilymembers")
    Call<ResponseBody> saveuserfamilymembers(@Header("Authorization") String Authorization , @Body RequestBody requestBody);

  @GET("api/customeraccount/v1/getuseraddresses")
    Call<ResponseBody> getuseraddresses(@Header("Authorization") String Authorization);


@POST("api/customeraccount/v1/saveuseraddresses")
    Call<ResponseBody> saveuseraddresses(@Header("Authorization") String Authorization , @Body RequestBody requestBody);

  @GET("api/customeraccount/v1/getusercontactnumbers")
    Call<ResponseBody> getusercontactnumbers(@Header("Authorization") String Authorization);


  @POST("api/customeraccount/v1/saveusercontactnumbers")
    Call<ResponseBody> saveusercontactnumbers(@Header("Authorization") String Authorization , @Body RequestBody requestBody);


//    @GET("api/LabTest/v1/GetAllLabTests")
//    @Headers({"Content-Type: application/x-www-form-urlencoded"})//"Accept-Encoding:gzip,deflate",
//    Call<ResponseBody> GetAllLabTests();


    @POST("api/customeraccount/v1/updatecustomerprofile")
    @Headers({"Content-Type: application/json"})
    Call<ResponseBody> updateCustomerProfile(@Body RequestBody requestBody, @Header("Authorization") String Authorization);

    @GET("api/customeraccount/v1/labtestresults")
    Call<ResponseBody> getLabTestResults(@Header("Authorization") String Authorization);

    @GET("api/customeraccount/v1/radilogyresults")
    Call<ResponseBody> getRadilogyResults(@Header("Authorization") String Authorization);

    @GET("api/customeraccount/v1/displayresultfromfilename")
    Call<ResponseBody> displayResultFromFilename(@Header("Authorization") String Authorization, @Query("filename") String FileName);

    @GET("api/homecare/getphysiotherapyservicetypes")
    Call<ResponseBody> getPhysiotherapyServiceTypes();

    @POST("api/homecare/getpaginatablehomecareservices")
    @Headers({"Content-Type: application/json"})
    Call<ResponseBody> getPaginaTableHomeCareServices(@Body RequestBody requestBody);


    @POST("api/LabTest/v1/paginatelabtests")
    @Headers({"Content-Type: application/json"})
    Call<ResponseBody> getPaginateLabTests(@Body RequestBody requestBody);


    @POST("api/LabTest/v1/searchlabtestbyword")
    @Headers({"Content-Type: application/json"})
    Call<ResponseBody> getSearchlabTestByWord(@Body RequestBody requestBody);


    @POST("api/Imaging/v1/paginateradilogy")
    @Headers({"Content-Type: application/json"})
    Call<ResponseBody> getPaginateRadilogy(@Body RequestBody requestBody);

    @POST("api/Imaging/v1/GetLabImagingsBySearchKeyWord")
    @Headers({"Content-Type: application/json"})
    Call<ResponseBody> GetLabImagingsBySearchKeyWord(@Body RequestBody requestBody);

    @POST("api/LabTest/v1/getlabtestdetailbyid")
    @Headers({"Content-Type: application/json"})
    Call<ResponseBody> getLabTestDetailById(@Body RequestBody requestBody);


    @POST("api/Imaging/v1/radiologydetailbyid")
    @Headers({"Content-Type: application/json"})
    Call<ResponseBody> getRadiologyDetailById(@Body RequestBody requestBody);

    @GET("api/LabPackage/v1/GetAllLabPackages")
    Call<ResponseBody> getAllMedicalPackage(@Query("cityId") String cityId);

    @GET("api/v1/common/getareasforcity")
    Call<ResponseBody> getAreasForCity(@Query("cityId") String cityId);


    @GET("api/account/v1/GetCustomerPersonalContacts")
    Call<ResponseBody> getCustomerPersonalContacts(@Header("Authorization") String Authorization);


    @POST("api/cart/v1/saveorder")
    @Headers({"Content-Type: application/json"})
    Call<ResponseBody> saveOrder(@Header("Authorization") String Authorization, @Body RequestBody requestBody);


    @POST("api/common/v1/uploadprescription")
    @Headers({
            "Content-Type: application/x-www-form-urlencoded"
    })
    @FormUrlEncoded
    Call<ResponseBody> contactUs(@Field("PrescriptionUrl") File file,
                                 @Field("Name") String Name,
                                 @Field("ContactNo") String ContactNo,
                                 @Field("Email") String Email,
                                 @Field("Message") String Message,
                                 @Field("ContactUsType") String ContactUsType
    );


    @POST("api/common/v1/uploadprescriptionpatientcommunication")
    @Headers({
            "Content-Type: application/x-www-form-urlencoded"
    })
    @FormUrlEncoded
    Call<ResponseBody> UploadPrescriptionPatientCommunication(@Field("PrescriptionUrl") File file);

    @GET("api/customeraccount/v1/allCustomerOrders")
    Call<ResponseBody> getAllCustomerOrders(@Header("Authorization") String Authorization);

    @GET("api/Imaging/v1/GetAllImagingCategories")
    Call<ResponseBody> getAllImagingCategories();

    @POST("api/Imaging/v1/BodyLocationByImagingCategoryId")
    @Headers({"Content-Type: application/json"})
    Call<ResponseBody> BodyLocationByImagingCategoryId(@Body RequestBody requestBody);


    @POST("api/Imaging/v1/BodyLocationPartByBodyLocationAndCategoryId")
    @Headers({"Content-Type: application/json"})
    Call<ResponseBody> BodyLocationPartByBodyLocationAndCategoryId(@Body RequestBody requestBody);

    @POST("api/Imaging/v1/BodyLocationPartTypeByCategoryAndOthers")
    @Headers({"Content-Type: application/json"})
    Call<ResponseBody> BodyLocationPartTypeByCategoryAndOthers(@Body RequestBody requestBody);


    @POST("api/Imaging/v1/paginateandfilterradiology")
    @Headers({"Content-Type: application/json"})
    Call<ResponseBody> Paginateandfilterradiology(@Body RequestBody requestBody);
}
