package com.udhc.shifa4u.Utilities;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;

import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by Ali Imran Bangash on 7/21/2018.
 */

public class MyGeneric {

    private static String json;

//    public MyGeneric(T data) {
//        this.data = data;
//    }


    public static <T> T processResponse(Response<ResponseBody> response,TypeToken<T> typeToken) {
        // use class information somehow...
        String encoding=null;
        try {
            encoding=response.raw().networkResponse().headers().get("Content-Encoding");

        }catch(Exception e)
        {
            encoding=null;
        }

        if (encoding!=null && encoding.equalsIgnoreCase("gzip")) {
            try {
                json = GzipUtils.decompress(response.body().bytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                json = response.body().string().toString();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (json!=null && !json.isEmpty())
        {
            return  new Gson().fromJson(json, typeToken.getType());
        }


        return null;
    }


}
