package com.udhc.shifa4u.Utilities;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.net.InetAddress;

/**
 * Created by Ali Imran Bangash on 6/29/2018.
 */

public class CheckNetwork {

    public  Context context;
    public CheckNetwork(Context context)
    {
        this.context=context;
    }

    public boolean isInternetAvailable()
    {
//        if(isNetworkConnected())
//        {
//            try {
//                String command = "ping -c 1 google.com";
//                return (Runtime.getRuntime().exec (command).waitFor() == 0);
//
//            } catch (Exception e) {
//                return false;
//            }
//        }
        return isNetworkConnected();
    }

    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        /// if no network is available networkInfo will be null
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }

}
