package com.udhc.shifa4u.Utilities;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.udhc.shifa4u.BuildConfig;

/**
 * Created by Ali Imran Bangash on 6/28/2018.
 */

public class MyLog{
//    public static Context context;
//    MyLog(Context context) {
//        this.context=context;
//    }
//
    public static void e(String tag,String message)
    {
        if(BuildConfig.LOG)
        Log.e(tag,message);
    }
    public static void d(String tag,String message)
    {
        if(BuildConfig.LOG)
        Log.d(tag,message);
    }
    public static void v(String tag,String message)
    {
        if(BuildConfig.LOG)
        Log.v(tag,message);
    }
    public static void i(String tag,String message)
    {
        if(BuildConfig.LOG)
        Log.i(tag,message);
    }
    public static void w(String tag,String message)
    {
        if(BuildConfig.LOG)
        Log.w(tag,message);
    }


}
