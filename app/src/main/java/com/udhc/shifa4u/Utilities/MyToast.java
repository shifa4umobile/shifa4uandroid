package com.udhc.shifa4u.Utilities;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.widget.Toast;

/**
 * Created by Ali Imran Bangash on 6/28/2018.
 */

public class MyToast extends Toast {
    /**
     * Construct an empty Toast object.  You must call {@link #setView} before you
     * can call {@link #show}.
     *
     * @param context The context to use.  Usually your {@link Application}
     *                or {@link Activity} object.
     */

   public static Context context;
    public MyToast(Context context) {
        super(context);
        this.context=context;
    }

}
