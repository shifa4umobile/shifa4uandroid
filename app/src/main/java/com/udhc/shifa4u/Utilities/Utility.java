package com.udhc.shifa4u.Utilities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.github.aakira.expandablelayout.ExpandableLayoutListenerAdapter;
import com.github.aakira.expandablelayout.ExpandableLinearLayout;
import com.github.aakira.expandablelayout.Utils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.udhc.shifa4u.Activities.MainActivity;
import com.udhc.shifa4u.Models.AmericanTeleclinic.Blog;
import com.udhc.shifa4u.Models.AmericanTeleclinic.FAQItemModel;
import com.udhc.shifa4u.R;

import java.lang.reflect.Type;
import java.net.InetAddress;
import java.util.List;

public class Utility {


    public static ObjectAnimator createRotateAnimator(final View target, final float from, final float to) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(target, "rotation", from, to);
        animator.setDuration(300);
        animator.setInterpolator(Utils.createInterpolator(Utils.LINEAR_INTERPOLATOR));
        return animator;
    }

    public static void setRotateAnimator(ExpandableLinearLayout expandableLinearLayout, final View rotateVIew) {
        expandableLinearLayout.setListener(new ExpandableLayoutListenerAdapter() {
            @Override
            public void onPreOpen() {
                createRotateAnimator(rotateVIew, 0f, 180f).start();
            }

            @Override
            public void onPreClose() {
                createRotateAnimator(rotateVIew, 180f, 0f).start();
            }
        });
    }


    public static void setClick(View button , final ExpandableLinearLayout expandableLinearLayout)
    {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                expandableLinearLayout.toggle();
            }
        });
    }



    public static boolean isInternetAvailable() {

            try {
                InetAddress ipAddr = InetAddress.getByName("google.com");
                //You can replace it with your name
                return !ipAddr.equals("");

            } catch (Exception e) {
                return false;
            }

    }

    public static  void InitializeFAQ (List <FAQItemModel> data , Context context)
    {
        data.add(new FAQItemModel(
                context.getResources().getString(R.string.FAQ1),context.getResources().getString(R.string.FAQA1),
                Utils.createInterpolator(Utils.ACCELERATE_INTERPOLATOR)));
        data.add(new FAQItemModel(
                context.getResources().getString(R.string.FAQ2),context.getResources().getString(R.string.FAQA2),
                Utils.createInterpolator(Utils.ACCELERATE_INTERPOLATOR)));
        data.add(new FAQItemModel(
                context.getResources().getString(R.string.FAQ3),context.getResources().getString(R.string.FAQA3),
                Utils.createInterpolator(Utils.ACCELERATE_INTERPOLATOR)));
        data.add(new FAQItemModel(
                context.getResources().getString(R.string.FAQ4),context.getResources().getString(R.string.FAQA4),
                Utils.createInterpolator(Utils.ACCELERATE_INTERPOLATOR)));
        data.add(new FAQItemModel(
                context.getResources().getString(R.string.FAQ5),context.getResources().getString(R.string.FAQA5),
                Utils.createInterpolator(Utils.ACCELERATE_INTERPOLATOR)));
        data.add(new FAQItemModel(
                context.getResources().getString(R.string.FAQ6),context.getResources().getString(R.string.FAQA6),
                Utils.createInterpolator(Utils.ACCELERATE_INTERPOLATOR)));
        data.add(new FAQItemModel(
                context.getResources().getString(R.string.FAQ7),context.getResources().getString(R.string.FAQA7),
                Utils.createInterpolator(Utils.ACCELERATE_INTERPOLATOR)));
        data.add(new FAQItemModel(
                context.getResources().getString(R.string.FAQ8),context.getResources().getString(R.string.FAQA8),
                Utils.createInterpolator(Utils.ACCELERATE_INTERPOLATOR)));
        data.add(new FAQItemModel(
                context.getResources().getString(R.string.FAQ9),context.getResources().getString(R.string.FAQA9),
                Utils.createInterpolator(Utils.ACCELERATE_INTERPOLATOR)));
        data.add(new FAQItemModel(
                context.getResources().getString(R.string.FAQ10),context.getResources().getString(R.string.FAQA10),
                Utils.createInterpolator(Utils.ACCELERATE_INTERPOLATOR)));
       data.add(new FAQItemModel(
                context.getResources().getString(R.string.FAQ11),context.getResources().getString(R.string.FAQA11),
                Utils.createInterpolator(Utils.ACCELERATE_INTERPOLATOR)));

    }

    public static void AnimateThis (final Button btn, final View linLay , boolean isVisible)
    {
        if (isVisible)
        {
            linLay.animate()
                    .alpha(0.0f)
                    .setDuration(500)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            linLay.setVisibility(View.GONE);
                            btn.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow_android,0);
                        }
                    });

        }
        else
        {
            linLay.setVisibility(View.VISIBLE);
            linLay.animate()
                    .alpha(0.0f);

            linLay.animate()
                    .alpha(1.0f)
                    .setDuration(500)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            linLay.setVisibility(View.VISIBLE);
                            btn.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.up_arrow_android,0);   }
                    });

        }
    }



    public static  Object parseJSON(String response , Type listType) {
        Gson gson = new GsonBuilder().create();
        List<Blog> blogs = gson.fromJson(response, listType);
        return blogs;
    }


    public static void openInternetAlert(final Activity context){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setMessage("Internet Connection Required");
                alertDialogBuilder.setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                              //  Toast.makeText(context,"You clicked yes button",Toast.LENGTH_LONG).show();
                            }
                        });


        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void showAlert (Context context ,  String title , String message)
    {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(context);
        }
        builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
}
