package com.udhc.shifa4u.Utilities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v4.view.GravityCompat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.udhc.shifa4u.Activities.CheckoutActivity;
import com.udhc.shifa4u.Activities.EasyPaisaActivity;
import com.udhc.shifa4u.Activities.MainActivity;
import com.udhc.shifa4u.Activities.OrderPreviewActivity;
import com.udhc.shifa4u.Activities.PaymentModeActivity;
import com.udhc.shifa4u.Activities.Shifa4U;
import com.udhc.shifa4u.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ali Imran Bangash on 6/29/2018.
 */

public class HeaderManager {

    public static final int MAIN_MENU = 0;
    public static final int BACK_MENU = 1;
    private  AutoCompleteTextView imageMenu;
    private FrameLayout fl_right_button_first;
    private TextView txt_item_count;
    private View header_bottom_view;
    RelativeLayout rl_header_main;

    List<String> menuItems = new ArrayList<>();
    ArrayAdapter<String> addressAdaptor;


    int menutype = 0;
    boolean showFirstImageButton = false;
    boolean showSecoundImageButton = false;


    boolean showBottomView = false;
    String title;

    View headerView;
    Context context;

    ImageView img_left_button, img_right_button_first, img_right_button_second;
    TextView txt_title;

    int headerColor;

    public int getHeaderColor() {
        return headerColor;
    }

    public void setHeaderColor(int headerColor) {
        this.headerColor = headerColor;
        headerView.setBackgroundColor(headerColor);
    }

    public HeaderManager(final Context context, View headerView) {
        this.headerView = headerView;
        this.context = context;

        if (headerView != null) {
            img_left_button = (ImageView) headerView.findViewById(R.id.img_left_button);
            imageMenu = (AutoCompleteTextView) headerView.findViewById(R.id.imageMenu);

            menuItems.add("Empty Cart");


            addressAdaptor = new ArrayAdapter<String>(context, R.layout.spinner_item, (menuItems));
            imageMenu.setAdapter(addressAdaptor);
            imageMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageMenu.showDropDown();
                }
            });
            imageMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    showAlertMessage("Warning" , "Are you sure you want to delete all the items from cart?");

                }
            });
            img_right_button_first = (ImageView) headerView.findViewById(R.id.img_right_button_first);
            img_right_button_second = (ImageView) headerView.findViewById(R.id.img_right_button_second);
            txt_title = (TextView) headerView.findViewById(R.id.txt_title);
            header_bottom_view = (View) headerView.findViewById(R.id.header_bottom_view);
            txt_item_count = (TextView) headerView.findViewById(R.id.txt_item_count);
            fl_right_button_first = (FrameLayout) headerView.findViewById(R.id.fl_right_button_first);

            img_left_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (menutype == BACK_MENU) {


                        if(((Activity) context) instanceof EasyPaisaActivity)
                        {
                            context.startActivity(new Intent(((Activity) context),MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
                            ((Activity) context).finish();
                            ((Activity)context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
                        }
                        if(((Activity) context) instanceof PaymentModeActivity)
                        {
                            if(PaymentModeActivity.isOrderSuccess)
                            {
                                context.startActivity(new Intent(((Activity) context),MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
                                ((Activity) context).finish();
                                ((Activity)context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
                            }else
                            {
                                ((Activity) context).finish();
                                ((Activity)context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
                            }
                        }else
                        {
                           ((Activity) context).finish();
                           ((Activity)context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);


                        }
                    } else {
                        if (!MainActivity.drawer.isDrawerOpen(GravityCompat.START)) {
                            MainActivity.drawer.openDrawer(GravityCompat.START);
                        }
                    }
                }
            });
            img_right_button_first.setVisibility(View.GONE);
            fl_right_button_first.setVisibility(View.GONE);
            if (Shifa4U.mySharePrefrence.getCart() != null && Shifa4U.mySharePrefrence.getCart().size() > 0) {
                if ((Activity) context instanceof CheckoutActivity) {
                    img_right_button_first.setVisibility(View.GONE);
                    fl_right_button_first.setVisibility(View.GONE);
                } else if ((Activity) context instanceof OrderPreviewActivity) {
                    img_right_button_first.setVisibility(View.GONE);
                    fl_right_button_first.setVisibility(View.GONE);
                }
                else if ((Activity) context instanceof EasyPaisaActivity)
                {
                    img_right_button_first.setVisibility(View.GONE);
                    fl_right_button_first.setVisibility(View.GONE);
                }
                else if ((Activity) context instanceof PaymentModeActivity)
                {
                    img_right_button_first.setVisibility(View.GONE);
                    fl_right_button_first.setVisibility(View.GONE);
                }
                else {
                    img_right_button_first.setVisibility(View.VISIBLE);
                    fl_right_button_first.setVisibility(View.VISIBLE);
                }
                txt_item_count.setText(String.valueOf(Shifa4U.mySharePrefrence.getCart().size()));
                img_right_button_first.setImageDrawable(context.getResources().getDrawable(R.drawable.cart));
//                fl_right_button_first.setVisibility(View.VISIBLE);
                fl_right_button_first.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        context.startActivity(new Intent(context, CheckoutActivity.class));
                    }
                });
            }


        }

    }

    public void setBackMenuButtonVisibility (boolean visibility)
    {
        if(visibility)
        img_left_button.setVisibility(View.VISIBLE);
        else
            img_left_button.setVisibility(View.GONE);
    }


    public void setCartVisibility (boolean visibility)
    {
        if(visibility)
            fl_right_button_first.setVisibility(View.VISIBLE);
        else
            fl_right_button_first.setVisibility(View.GONE);
    }


  public void setMenuButtonVisibility (boolean visibility)
    {
        if(visibility)
        imageMenu.setVisibility(View.VISIBLE);
        else
            imageMenu.setVisibility(View.GONE);
    }

    public int getMenutype() {
        return menutype;
    }

    public void setMenutype(int menutype) {
        this.menutype = menutype;
        if (menutype == MAIN_MENU) {
            img_left_button.setImageDrawable(context.getResources().getDrawable(R.drawable.menu));
        } else {
            img_left_button.setImageDrawable(context.getResources().getDrawable(R.drawable.left_arrow));
        }

    }

    public boolean isShowFirstImageButton() {
        return showFirstImageButton;
    }

    public void setShowFirstImageButton(boolean showFirstImageButton) {
//        this.showFirstImageButton = showFirstImageButton;
//
//        if(showFirstImageButton)
//        {
//            img_right_button_first.setVisibility(View.VISIBLE);
//        }else
//        {
//            if(!isShowSecoundImageButton())
//                img_right_button_first.setVisibility(View.INVISIBLE);
//            else
//                img_right_button_first.setVisibility(View.GONE);

      // }

    }

    public boolean isShowSecoundImageButton() {
        return showSecoundImageButton;
    }

    public void setShowSecoundImageButton(boolean showSecoundImageButton) {
//        this.showSecoundImageButton = showSecoundImageButton;
//        if(showSecoundImageButton)
//        {
//            img_right_button_second.setVisibility(View.VISIBLE);
//        }else
//        {
//            img_right_button_second.setVisibility(View.GONE);
//        }
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
        txt_title.setText(title);
    }

    public boolean isShowBottomView() {
        return showBottomView;
    }

    public void setShowBottomView(boolean showBottomView) {
        this.showBottomView = showBottomView;
        if (showBottomView) {
            header_bottom_view.setVisibility(View.VISIBLE);
        }
    }

   private void showAlertMessage (String title , String message)
   {
       AlertDialog.Builder builder;
       if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
           builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
       } else {
           builder = new AlertDialog.Builder(context);
       }
       builder.setTitle(title)
               .setMessage(message)
               .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int which) {

                       Shifa4U.mySharePrefrence.clearCart();
                       context.startActivity(new Intent(((Activity) context),MainActivity.class));
                       ((Activity) context).finish();
                       ((Activity)context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
                       // continue with delete
                   }
               })
               .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int which) {
                       // do nothing
                   }
               })
               .setIcon(android.R.drawable.ic_dialog_alert)
               .show();
   }

}
