package com.udhc.shifa4u.Utilities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.udhc.shifa4u.Activities.Shifa4U;
import com.udhc.shifa4u.Models.CartModel;
import com.udhc.shifa4u.R;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Ali Imran Bangash on 7/13/2018.
 */

public class MyUtils {

    public static void
    addToCart(CartModel cartModel) throws Exception {
        boolean isItemAlreadyAdded = false;
        int position = 0;
        ArrayList<CartModel> cartModels = Shifa4U.mySharePrefrence.getCart();
        if (cartModels != null && cartModels.size() > 0) {
            for (int i = 0; i < cartModels.size(); i++) {
                CartModel cartModel1 = cartModels.get(i);
                if (cartModel1.getProductId() == cartModel.getProductId() && cartModel1.getProductType() == cartModel.getProductType()) {
                    position = i;
                    isItemAlreadyAdded = true;
                    break;
                }
            }
        }

//        if(isLabExistInCart(cartModel))
//        {
//            new MyUtils().showLabExistDialog(Shifa4U.context);
//        }

        if (!isItemAlreadyAdded) {
            Shifa4U.mySharePrefrence.addToCart(cartModel);
        } else {
            cartModels.set(position, cartModel);
            Shifa4U.mySharePrefrence.updateCart(cartModels);
            //throw new Exception("Item Already Exist");
        }

    }

    public void showLabExistDialog(Context context) {

        AlertDialog.Builder alertDialog =new AlertDialog.Builder(Shifa4U.getIntence().getContext());
        alertDialog.setTitle("Warning");
        alertDialog.setMessage("Please note that different tests from different labs will require a separate sample for each lab. The home sampling services are provided by individual labs, and ordering different tests from different labs will require each of them to visit separately and draw separate samples");
        alertDialog.setNeutralButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }


    public static boolean isLabExistInCart(CartModel cartModel) {

        ArrayList<Integer> foundLabIds = new ArrayList<>();

        if (cartModel.getSelectedLabId() != -1) {
            ArrayList<CartModel> cartModels = Shifa4U.mySharePrefrence.getCart();
            if (cartModels != null && cartModels.size() > 0) {
                for (CartModel cartModel1 : cartModels) {
                    if (cartModel1.getSelectedLabId() != -1) {
                        if (!foundLabIds.contains(cartModel1.getSelectedLabId())) {
                            foundLabIds.add(cartModel1.getSelectedLabId());
                        }
                    }
                }

                if (foundLabIds.size() == 1) {
                    if (foundLabIds.contains(cartModel.getSelectedLabId())) {
                        return false;
                    } else {
                        return true;
                    }
                } else if (foundLabIds.size() > 1) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }

    }
//    public static void hideKeyboard(Activity activity) {
//        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
//        //Find the currently focused view, so we can grab the correct window token from it.
//        View view = activity.getCurrentFocus();
//        //If no view currently has focus, create a new one, just so we can grab a window token from it
//        if (view == null) {
//            view = new View(activity);
//        }
//        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//    }
    public static void hideKeyboard(Activity activity) {
        View view = activity.findViewById(R.id.txt_search);
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }


    public static String formateCurrency(Double amount)
    {
        NumberFormat numberFormat = NumberFormat.getInstance();

        // setting number of decimal places
        numberFormat.setMinimumFractionDigits(0);
        numberFormat.setMaximumFractionDigits(0);

        // you can also define the length of integer
        // that is the count of digits before the decimal point
        numberFormat.setMinimumIntegerDigits(1);
        numberFormat.setMaximumIntegerDigits(10);

        // if you want the number format to have commas
        // to separate the decimals the set as true
        numberFormat.setGroupingUsed(true);
//        NumberFormat format = NumberFormat.getNumberInstance();
        return numberFormat.format(amount);

    }
    public static String formateCurrency(Integer amount)
    {
        NumberFormat numberFormat = NumberFormat.getInstance();

        // setting number of decimal places
        numberFormat.setMinimumFractionDigits(2);
        numberFormat.setMaximumFractionDigits(2);

        // you can also define the length of integer
        // that is the count of digits before the decimal point
        numberFormat.setMinimumIntegerDigits(1);
        numberFormat.setMaximumIntegerDigits(10);

        // if you want the number format to have commas
        // to separate the decimals the set as true
        numberFormat.setGroupingUsed(true);
//        NumberFormat format = NumberFormat.getNumberInstance();
        return numberFormat.format(amount);

    }



}
