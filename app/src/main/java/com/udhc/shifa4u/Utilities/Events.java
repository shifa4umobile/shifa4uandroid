package com.udhc.shifa4u.Utilities;

public class Events {

    public static class DownloadResultFile {

        private String path;

        public DownloadResultFile(String path) {
            this.path = path;
        }

        public String getMessage() {
            return path;
        }
    }


    public static class ConnectWithDoctor {

        private String path;

        public ConnectWithDoctor(String path) {
            this.path = path;
        }

        public String getMessage() {
            return path;
        }
    }


}