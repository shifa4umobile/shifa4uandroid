package com.udhc.shifa4u.Utilities;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.udhc.shifa4u.Models.AmericanTeleclinic.MedicalBranches;
import com.udhc.shifa4u.Models.AmericanTeleclinic.Physician;
import com.udhc.shifa4u.Models.AmericanTeleclinic.SubmitModel;
import com.udhc.shifa4u.Models.CartModel;
import com.udhc.shifa4u.Models.CityModel;
import com.udhc.shifa4u.Models.CustomerProfileModel;
import com.udhc.shifa4u.Models.LoginModel;
import com.udhc.shifa4u.Models.MeetingConnectionResponse;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ali Imran Bangash on 6/29/2018.
 */

public class MySharePrefrence {

    public static final String mypreference = "mypref";
    public static final String cityId = "cityId";
    public static final String cityName = "cityName";
    public static final String cities = "cities";
    public static final String medicalBranchesPrefName = "medicalBranches";
    public static final String doctorsPrefName = "doctors";
    public static final String submit = "submit";
    public static final String login = "login";

    public static final String profile = "profile";
    public static final String meegtingConnectionResponse = "meegtingConnectionResponse";
    public static final String MeetingId = "MeetingId";
    public static final String UniqueId = "UniqueId";
    public static final String PhycianId = "PhycianId";
    public static final String selectedCityIndex = "selectedCityIndex";
    public static final String countryId = "countryId";
    public static final String nameId = "nameId";


    public static final String cart = "cart";


    Context context;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;



    public MySharePrefrence(Context context) {
        this.context = context;
        sharedpreferences = context.getSharedPreferences(mypreference, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
    }

    public String getCityId() {
        return sharedpreferences.getString(cityId, null);
    }

    public void setCityId(String cityID) {
        editor.putString(cityId, cityID);
        editor.commit();
    }

    public void addAccountActivityNames(String name) {
        editor.putString(nameId, name);
        editor.commit();
    }


    public void removeAccountActivityNames(String name) {
        editor.remove(nameId);
        editor.commit();
    }

    public String getAccountActivityNames() {
        return sharedpreferences.getString(nameId, null);
    }


    public String getCityName() {
        return sharedpreferences.getString(cityName, null);
    }


    public void setCityName(String city_name) {
        editor.putString(cityName, city_name);
        editor.commit();
    }

    public ArrayList<CityModel> getCities() {
        String json = sharedpreferences.getString(cities, null);

        ArrayList<CityModel> cityModels = null;
        if (json != null) {
            Gson gson = new Gson();
            Type type = new TypeToken<ArrayList<CityModel>>() {
            }.getType();
            cityModels = gson.fromJson(json, type);
        }
        return cityModels;
    }


    public SubmitModel getSubmitModel() {
        String json = sharedpreferences.getString(submit, null);

        SubmitModel cityModels = null;
        if (json != null) {
            Gson gson = new Gson();
           /* Type type = new TypeToken<SubmitModel>() {
            }.getType();*/
            cityModels = gson.fromJson(json, SubmitModel.class);
        }
        return cityModels;
    }

    public void setSubmitModel (SubmitModel submitModel)
    {
        Gson gson = new Gson();
       /* Type type = new TypeToken<SubmitModel>() {
        }.getType();*/
        String json = gson.toJson(submitModel);
        editor.putString(submit, json);
        editor.commit();


        SubmitModel submitModel1 = getSubmitModel();
    }


    public void clearSubmitModel() {
        editor.remove(submit);
        editor.commit();
    }

    public void setCities(ArrayList<CityModel> cityModels) {
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<CityModel>>() {
        }.getType();
        String json = gson.toJson(cityModels, type);
        editor.putString(this.cities, json);
        editor.commit();
    }


     public void setMedicalBranches(List<MedicalBranches> medicalBranches) {
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<MedicalBranches>>() {
        }.getType();
        String json = gson.toJson(medicalBranches, type);
        editor.putString(medicalBranchesPrefName, json);
        editor.commit();
    }

    public void setDoctors(List<Physician> doctors) {
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<Physician>>() {
        }.getType();
        String json = gson.toJson(doctors, type);
        editor.putString(doctorsPrefName, json);
        editor.commit();
    }

    public ArrayList<MedicalBranches> getMedicalBranches() {
        String json = sharedpreferences.getString(medicalBranchesPrefName, null);

        ArrayList<MedicalBranches> medicalBranches = null;
        if (json != null) {
            Gson gson = new Gson();
            Type type = new TypeToken<ArrayList<MedicalBranches>>() {
            }.getType();
            medicalBranches = gson.fromJson(json, type);
        }
        return medicalBranches;
    }


    public ArrayList<Physician> getDoctors() {
        String json = sharedpreferences.getString(doctorsPrefName, null);

        ArrayList<Physician> doctors = null;
        if (json != null) {
            Gson gson = new Gson();
            Type type = new TypeToken<ArrayList<Physician>>() {
            }.getType();
            doctors = gson.fromJson(json, type);
        }
        return doctors;
    }

    public void clearMedicalBrances() {
        editor.remove(medicalBranchesPrefName);
        editor.commit();
    }

    public void clearCities() {
        editor.remove(this.cities);
        editor.commit();
    }

  public void clearDoctors() {
        editor.remove(doctorsPrefName);
        editor.commit();
    }


    public int getSelectedCityIndex() {
        return sharedpreferences.getInt(selectedCityIndex, -1);
    }

    public void setSelectedCityIndex(int selectedCity) {
        editor.putInt(this.selectedCityIndex, selectedCity);
        editor.commit();
    }

    public void clearSelectedCityIndex() {
        editor.remove(this.selectedCityIndex);
        editor.commit();
    }

    public LoginModel getLogin() {
        String json = sharedpreferences.getString(login, null);

        LoginModel loginModel = null;
        if (json != null) {
            Gson gson = new Gson();
            Type type = new TypeToken<LoginModel>() {
            }.getType();
            loginModel = gson.fromJson(json, type);
        }
        return loginModel;
    }

    public void setLogin(LoginModel loginModel) {
        Gson gson = new Gson();
        Type type = new TypeToken<LoginModel>() {
        }.getType();
        String json = gson.toJson(loginModel, type);
        editor.putString(login, json);
        editor.commit();
    }

    public void clearLogin() {
        editor.remove(login);
        editor.commit();
    }


    public CustomerProfileModel getProfile() {
        String json = sharedpreferences.getString(profile, null);

        CustomerProfileModel customerProfileModel = null;
        if (json != null) {
            Gson gson = new Gson();
            Type type = new TypeToken<CustomerProfileModel>() {
            }.getType();
            customerProfileModel = gson.fromJson(json, type);
        }
        return customerProfileModel;
    }

    public void setMeetingResponse(MeetingConnectionResponse meetingResponse) {
        Gson gson = new Gson();
        Type type = new TypeToken<MeetingConnectionResponse>() {
        }.getType();
        String json = gson.toJson(meetingResponse, type);
        editor.putString(meegtingConnectionResponse, json);
        editor.commit();
    }

    public void removeMeetingConnectionResponse ()
    {
        editor.remove(meegtingConnectionResponse);
        editor.commit();
    }

    public MeetingConnectionResponse getConnectionResponse() {
        String json = sharedpreferences.getString(meegtingConnectionResponse, null);

        MeetingConnectionResponse meetingConnectionResponse = null;
        if (json != null) {
            Gson gson = new Gson();
            Type type = new TypeToken<MeetingConnectionResponse>() {
            }.getType();
            meetingConnectionResponse = gson.fromJson(json, type);
        }
        return meetingConnectionResponse;
    }

    public void setProfile(CustomerProfileModel profileModel) {
        Gson gson = new Gson();
        Type type = new TypeToken<CustomerProfileModel>() {
        }.getType();
        String json = gson.toJson(profileModel, type);
        editor.putString(profile, json);
        editor.commit();
    }



    public void setPhysicianId(long physicianId) {
        editor.putLong(PhycianId, physicianId);
        editor.commit();
    }


    public String getPhycianId() {
        return sharedpreferences.getString(PhycianId, null);
    }


    public void setUniqueId(String meetingId) {
        editor.putString(UniqueId, meetingId);
        editor.commit();
    }

    public void removeUniqueId() {
        editor.remove(UniqueId);
        editor.commit();
    }

      public String getUniqueId() {
        return sharedpreferences.getString(UniqueId, null);
    }


    public void setMeetingId(String meetingId) {
        editor.putString(MeetingId, meetingId);
        editor.commit();
    }

    public void removeMeetingId() {
        editor.remove(MeetingId);
        editor.commit();
    }


    public void removePhycianId() {
        editor.remove(PhycianId);
        editor.commit();
    }

      public String getMeetingId() {
        return sharedpreferences.getString(MeetingId, null);
    }



    public void clearProfile() {
        editor.remove(profile);
        editor.commit();
    }


    public String getCountryId() {
        return sharedpreferences.getString(countryId, null);
    }

    public void setCountryId(String countryID) {
        editor.putString(this.countryId, countryID);
        editor.commit();
    }

    public void clearCountryId() {
        editor.remove(countryId);
        editor.commit();
    }


    public ArrayList<CartModel> getCart() {

        String json = sharedpreferences.getString(cart, null);

        ArrayList<CartModel> cartModels = null;
        if (json != null) {
            Gson gson = new Gson();
            Type type = new TypeToken<ArrayList<CartModel>>() {
            }.getType();
            cartModels = gson.fromJson(json, type);
        }
        return cartModels;
    }

    public void addToCart(CartModel cartModel) {

        ArrayList<CartModel> cartModels = getCart();
        if (cartModels != null && cartModels.size() > 0) {
            cartModels.add(cartModel);
        } else {
            cartModels = new ArrayList<CartModel>();
            cartModels.add(cartModel);
        }
        if (cartModels != null && cartModels.size() > 0) {
            Gson gson = new Gson();
            Type type = new TypeToken<ArrayList<CartModel>>() {
            }.getType();
            String json = gson.toJson(cartModels, type);
            editor.putString(this.cart, json);
            editor.commit();
        }

    }

    public void updateCart(ArrayList<CartModel> cartModels) {
        if (cartModels != null && cartModels.size() > 0) {
            Gson gson = new Gson();
            Type type = new TypeToken<ArrayList<CartModel>>() {
            }.getType();
            String json = gson.toJson(cartModels, type);
            editor.putString(this.cart, json);
            editor.commit();
        } else {
            editor.remove(this.cart);
            editor.commit();
        }

    }

    public void updateCartSingleItem(CartModel cartModel) {
        if (cartModel != null) {

            ArrayList<CartModel> cartModels = getCart();


            if (cartModels != null && cartModels.size() > 0) {
                for (int i = 0; i < cartModels.size(); i++) {
                    if (cartModels.get(i).getProductId() == cartModel.getProductId() && cartModels.get(i).getProductType() == cartModel.getProductType()) {
                        cartModels.set(i, cartModel);
                        break;
                    }
                }
            }


            Gson gson = new Gson();
            Type type = new TypeToken<ArrayList<CartModel>>() {
            }.getType();
            String json = gson.toJson(cartModels, type);
            editor.putString(this.cart, json);
            editor.commit();
        } else {
            editor.remove(this.cart);
            editor.commit();
        }

    }

    public void clearCart() {
        editor.remove(this.cart);
        editor.commit();
    }



}
