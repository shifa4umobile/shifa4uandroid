package com.udhc.shifa4u.Adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.udhc.shifa4u.Interfaces.MenuItemsInterface;
import com.udhc.shifa4u.Models.CityModel;
import com.udhc.shifa4u.Models.LabTestList;
import com.udhc.shifa4u.R;

import java.util.ArrayList;

/**
 * Created by Ali Imran Bangash on 6/27/2018.
 */

public class LabsAdapter extends BaseAdapter implements Filterable {
    Context context;
    ArrayList<LabTestList> labTestLists;
    MenuItemsInterface menuItemsInterface;

    public LabsAdapter(Context context, ArrayList<LabTestList> labTestLists, MenuItemsInterface menuItemsInterface) {
        this.context = context;
        this.labTestLists = labTestLists;
        this.menuItemsInterface = menuItemsInterface;
    }

    @Override
    public int getCount() {
        return labTestLists.size();
    }

    @Override
    public Object getItem(int position) {
        return labTestLists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return labTestLists.get(position).getLabTestId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        convertView = LayoutInflater.from(context).inflate(R.layout.labs_item_layout, null);
        TextView txt_title = (TextView) convertView.findViewById(R.id.txt_title);
        TextView txt_detail = (TextView) convertView.findViewById(R.id.txt_detail);
        Button btn_detail = (Button) convertView.findViewById(R.id.btn_detail);
        Button btn_add_to_cart = (Button) convertView.findViewById(R.id.btn_add_to_cart);
        txt_title.setText(labTestLists.get(position).getName());
        txt_detail.setText(labTestLists.get(position).getShortDescription());

        btn_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuItemsInterface.onButtonClick(position, labTestLists.get(position), v);
            }
        });
        btn_add_to_cart .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuItemsInterface.onButtonClick(position, labTestLists.get(position), v);
            }
        });



        return convertView;
    }

    @Nullable
    @Override
    public CharSequence[] getAutofillOptions() {
        return new CharSequence[0];
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            return null;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            String str = ((CityModel) resultValue).getCityName();
            return str;
        }

//        @Override
//        protected FilterResults performFiltering(CharSequence constraint) {
//            if (constraint != null) {
//                suggestions.clear();
//                for (CountryModel countryModel : tempItems) {
//                    if (countryModel.getCountryName().toLowerCase().contains(constraint.toString().toLowerCase())) {
//                        suggestions.add(countryModel);
//                    }
//                }
//                FilterResults filterResults = new FilterResults();
//                filterResults.values = suggestions;
//                filterResults.count = suggestions.size();
//                return filterResults;
//            } else {
//                return new FilterResults();
//            }
//        }
//
//        @Override
//        protected void publishResults(CharSequence constraint, FilterResults results) {
//            ArrayList<CountryModel> filterList = (ArrayList<CountryModel>) results.values;
//            if (results != null && results.count > 0) {
//                filterList.clear();
//                for (CountryModel countryModel :  filterList) {
//                    add(countryModel);
//                    notifyDataSetChanged();
//                }
//            }
//        }
    };
}
