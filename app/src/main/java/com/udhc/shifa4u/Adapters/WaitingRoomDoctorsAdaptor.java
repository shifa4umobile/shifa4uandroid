package com.udhc.shifa4u.Adapters;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.RequestQueue;
import com.udhc.shifa4u.Activities.MeetOurProviders;
import com.udhc.shifa4u.Activities.WatingRoom;
import com.udhc.shifa4u.Models.AmericanTeleclinic.Physician;
import com.udhc.shifa4u.Models.AmericanTeleclinic.ProviderImages;
import com.udhc.shifa4u.R;
import java.util.ArrayList;
import java.util.List;

public class WaitingRoomDoctorsAdaptor extends RecyclerView.Adapter<WaitingRoomDoctorsAdaptor.MyViewHolder> {

    List<Physician> providers;
    WatingRoom context;
    Bitmap bitmap;
    ArrayList<ProviderImages> images;

    ArrayList<String> doctorsNames;
    ArrayList<String> specialitiesNames;

    public String imageUrl = "https://api.shifa4u.com/api/common/v1/displayimagebyimageid?id=";
    public String fileUrl = "https://api.shifa4u.com/api/common/v1/displayfilebyfilename?filename=";




    public WaitingRoomDoctorsAdaptor(List<Physician> providers, WatingRoom context) {

        this.providers = providers;
        this.context = context;
        images = new ArrayList<>();
        doctorsNames = new ArrayList<>();
        specialitiesNames = new ArrayList<>();
    }



    public static class MyViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView name;
        TextView speciality;
        TextView location;
        ImageView personPhoto;
        ImageView flagImage;


        MyViewHolder(View itemView) {
            super(itemView);
            cv = itemView.findViewById(R.id.provider_card_view);
            name = itemView.findViewById(R.id.tvPersonName);
            speciality = itemView.findViewById(R.id.tvSpeciality);
            location = itemView.findViewById(R.id.personLocation);
            personPhoto = itemView.findViewById(R.id.personPhoto);
            flagImage = itemView.findViewById(R.id.flagImage);
        }
    }



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.waiting_room_doctors_layout, parent, false);
        return new MyViewHolder(view);
    }

    @SuppressLint("StaticFieldLeak")
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        TextView textViewName = holder.name;
        TextView textViewSpeciality = holder.speciality;
        final TextView textViewLocation = holder.location;
        ImageView profileImage = holder.personPhoto;
        ImageView flagImage = holder.flagImage;

        textViewName.setText(providers.get(listPosition).getPhysicianNameWithDesignation().trim());
        textViewSpeciality.setText(providers.get(listPosition).getSpecialityName().trim());
        textViewLocation.setText(providers.get(listPosition).getCurrentPracticeLocation().trim());
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(html);
        }
    }

    @Override
    public int getItemCount() {
        return providers.size();
    }
}
