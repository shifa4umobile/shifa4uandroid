package com.udhc.shifa4u.Adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.udhc.shifa4u.Interfaces.MenuItemsInterface;
import com.udhc.shifa4u.Models.CityModel;
import com.udhc.shifa4u.Models.RadioLogyDetail.Location;
import com.udhc.shifa4u.Models.RadioLogyDetail.Vendor;
import com.udhc.shifa4u.R;

import java.util.ArrayList;

/**
 * Created by Ali Imran Bangash on 6/27/2018.
 */

public class VendorsLocationAdapter extends BaseAdapter implements Filterable {
    Context context;
    ArrayList<Location> locations;
    MenuItemsInterface menuItemsInterface;

    public VendorsLocationAdapter(Context context, ArrayList<Location> locations, MenuItemsInterface menuItemsInterface) {
        this.context = context;
        this.locations = locations;
        this.menuItemsInterface = menuItemsInterface;
    }

    @Override
    public int getCount() {
        return locations.size();
    }

    @Override
    public Object getItem(int position) {
        return locations.get(position);
    }

    @Override
    public long getItemId(int position) {
        return locations.get(position).getVendorLocationId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Location location = locations.get(position);
        convertView = LayoutInflater.from(context).inflate(R.layout.spinner_item, null);
        TextView textView = (TextView) convertView.findViewById(R.id.textView);
        if (location.getContacts()!=null && location.getContacts().size()>0)
        {
            for(int i=0;i<location.getContacts().size();i++)
            {
                if(location.getContacts().get(i).getTypeName().equalsIgnoreCase("Address"))
                {
                    textView.setText(location.getContacts().get(i).getDescription());
                    convertView.setTag(location.getContacts().get(i));
                }
            }
        }
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuItemsInterface.onItemClick(position,v.getTag());
            }
        });
            return convertView;
    }

    @Nullable
    @Override
    public CharSequence[] getAutofillOptions() {
        return new CharSequence[0];
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            return null;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            Location location=((Location) resultValue);
            String str="";
            for(int i=0;i<location.getContacts().size();i++)
            {
                if(location.getContacts().get(i).getTypeName().equalsIgnoreCase("Address"))
                {
                    str=location.getContacts().get(i).getDescription();
                    break;
                }
            }

            return str;
        }

//        @Override
//        protected FilterResults performFiltering(CharSequence constraint) {
//            if (constraint != null) {
//                suggestions.clear();
//                for (CountryModel countryModel : tempItems) {
//                    if (countryModel.getCountryName().toLowerCase().contains(constraint.toString().toLowerCase())) {
//                        suggestions.add(countryModel);
//                    }
//                }
//                FilterResults filterResults = new FilterResults();
//                filterResults.values = suggestions;
//                filterResults.count = suggestions.size();
//                return filterResults;
//            } else {
//                return new FilterResults();
//            }
//        }
//
//        @Override
//        protected void publishResults(CharSequence constraint, FilterResults results) {
//            ArrayList<CountryModel> filterList = (ArrayList<CountryModel>) results.values;
//            if (results != null && results.count > 0) {
//                filterList.clear();
//                for (CountryModel countryModel :  filterList) {
//                    add(countryModel);
//                    notifyDataSetChanged();
//                }
//            }
//        }
    };
}
