package com.udhc.shifa4u.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.aakira.expandablelayout.ExpandableLayout;
import com.github.aakira.expandablelayout.ExpandableLayoutListenerAdapter;
import com.github.aakira.expandablelayout.ExpandableLinearLayout;
import com.udhc.shifa4u.Models.AmericanTeleclinic.FAQItemModel;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.Utility;

import java.util.List;


public class RecyclerViewRecyclerAdapter extends RecyclerView.Adapter<RecyclerViewRecyclerAdapter.ViewHolder> {

    private static final int TYPE_ONE = 1;
    private static final int TYPE_TWO = 2;
    private static final int TYPE_THREE = 3;
    private final List<FAQItemModel> data;
    private Context context;
    private SparseBooleanArray expandState = new SparseBooleanArray();


    public RecyclerViewRecyclerAdapter(final List<FAQItemModel> data) {
        this.data = data;
        for (int i = 0; i < data.size(); i++) {
            expandState.append(i, false);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        this.context = parent.getContext();

        if (viewType == TYPE_THREE) {
            return new ViewHolder(LayoutInflater.from(context)
                    .inflate(R.layout.recycler_view_list_row_layout_3, parent, false));
        } else if (viewType == TYPE_TWO) {
            return new ViewHolder(LayoutInflater.from(context)
                    .inflate(R.layout.recycler_view_list_row_layout_2, parent, false));
        } else {
            return new ViewHolder(LayoutInflater.from(context)
                    .inflate(R.layout.recycler_view_list_row, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final FAQItemModel item = data.get(position);
        holder.setIsRecyclable(false);
        holder.textView.setText(item.question);
        if (holder.getItemViewType() == TYPE_ONE) {
            holder.tvAnswer.setText(item.answer);
        }
        holder.expandableLayout.setInRecyclerView(true);
        holder.expandableLayout.setInterpolator(item.interpolator);
        holder.expandableLayout.setExpanded(false);
        holder.expandableLayout.setListener(new ExpandableLayoutListenerAdapter() {
            @Override
            public void onPreOpen() {
                Utility.createRotateAnimator(holder.buttonLayout, 0f, 180f).start();
                expandState.put(position, true);
            }

            @Override
            public void onPreClose() {
                Utility.createRotateAnimator(holder.buttonLayout, 180f, 0f).start();
                expandState.put(position, false);
            }
        });

        holder.buttonLayout.setRotation(expandState.get(position) ? 180f : 0f);
        holder.buttonLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                onClickButton(holder.expandableLayout);
            }
        });

        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickButton(holder.expandableLayout);
            }
        });
    }

    private void onClickButton(final ExpandableLayout expandableLayout) {
        expandableLayout.toggle();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 1) {
            return TYPE_TWO;
        } else if (position == 9) {
            return TYPE_THREE;
        } else {
            return TYPE_ONE;
        }

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;
        public TextView tvAnswer;
        public RelativeLayout buttonLayout;
        /**
         * You must use the ExpandableLinearLayout in the recycler view.
         * The ExpandableRelativeLayout doesn't work.
         */
        public ExpandableLinearLayout expandableLayout;

        public ViewHolder(View v) {
            super(v);
            textView = (TextView) v.findViewById(R.id.textView);
            tvAnswer = (TextView) v.findViewById(R.id.tvAnswer);
            buttonLayout = (RelativeLayout) v.findViewById(R.id.button);
            expandableLayout = (ExpandableLinearLayout) v.findViewById(R.id.expandableLayout);
        }
    }
}