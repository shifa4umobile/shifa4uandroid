package com.udhc.shifa4u.Adapters;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.makeramen.roundedimageview.RoundedImageView;
import com.udhc.shifa4u.Activities.BlogActivity;
import com.udhc.shifa4u.Activities.BlogDetail;
import com.udhc.shifa4u.Models.AmericanTeleclinic.Blog;
import com.udhc.shifa4u.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class BlogAdaptor extends RecyclerView.Adapter<BlogAdaptor.MyViewHolder> {

    List<Blog> blogs;
    BlogActivity context;
    String imgaeUrl = "https://api.shifa4u.com/api/common/v1/downloadimage?fileName=";


    public BlogAdaptor(List<Blog> blogs, BlogActivity context) {

        this.blogs = blogs;
        this.context = context;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView tvTitle;
        TextView tvDate;
        TextView tvAuthorName;
        TextView tvDetail;
       // ImageView personPhoto;
        RoundedImageView personPhoto;
        TextView btnViewProfile;


        MyViewHolder(View itemView) {
            super(itemView);
            cv = itemView.findViewById(R.id.provider_card_view);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvAuthorName = itemView.findViewById(R.id.tvAuthorName);
            tvDetail = itemView.findViewById(R.id.tvDetail);
            personPhoto = itemView.findViewById(R.id.personPhoto);
            btnViewProfile = itemView.findViewById(R.id.btnReadMore);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.blog_card_view, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        TextView tvTitle = holder.tvTitle;
        TextView tvDate = holder.tvDate;
        TextView tvAuthorName = holder.tvAuthorName;
        TextView tvDetail = holder.tvDetail;
       // ImageView profileImage = holder.personPhoto;
        RoundedImageView personPhoto = new RoundedImageView(context);
        personPhoto = holder.personPhoto;
        TextView btnViewProfile = holder.btnViewProfile;
        tvTitle.setText(blogs.get(listPosition).getTitle());

        String dateTime = getDateInStringForm(blogs.get(listPosition).getPublishDateTime());
        tvDate.setText(dateTime);

        tvAuthorName.setText(blogs.get(listPosition).getAuthor());
        Spanned result = fromHtml(blogs.get(listPosition).getShortDescription());
        tvDetail.setText(result);

        RequestOptions requestOption = new RequestOptions().placeholder(R.drawable.place_holder).centerCrop();
        String imageURL = blogs.get(listPosition).getImageUrl();
        Glide.with(context).load(imgaeUrl + imageURL).apply(requestOption).into(personPhoto);

        btnViewProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent myIntent = new Intent(context, BlogDetail.class);
                myIntent.putExtra("Blog", blogs.get(listPosition));
                context.startActivity(myIntent);
                context.overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );
            }
        });

    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        if (blogs != null)
        return blogs.size();
        else
        {
            return 0;
        }
    }

    public void addAll (List<Blog> temBlogs)
    {
        //mLoadedItems++;
        this.blogs.addAll(temBlogs);
        notifyDataSetChanged();
    }

    public static Spanned fromHtml(String html){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(html);
        }
    }


    private String getDateInStringForm(String mDate)
    {
        Date date = new Date();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {

            date = format.parse(mDate);
            System.out.println(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return  dateFormat.format(date);
    }
}
