package com.udhc.shifa4u.Adapters.RadilogyCategories;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.udhc.shifa4u.Interfaces.MenuItemsInterface;
import com.udhc.shifa4u.Models.RadiologyCategories.BodyLocation;
import com.udhc.shifa4u.Models.RadiologyCategories.BodyLocationPart;
import com.udhc.shifa4u.R;

import java.util.ArrayList;

/**
 * Created by Ali Imran Bangash on 6/27/2018.
 */

public class BodyLocationPartAdapter extends BaseAdapter implements Filterable {
    Context context;
    ArrayList<BodyLocationPart> bodyLocationParts;
    MenuItemsInterface menuItemsInterface;

    public BodyLocationPartAdapter(Context context, ArrayList<BodyLocationPart> bodyLocationParts, MenuItemsInterface menuItemsInterface) {
        this.context = context;
        this.bodyLocationParts = bodyLocationParts;
        this.menuItemsInterface = menuItemsInterface;
    }

    public BodyLocationPartAdapter(Context context, ArrayList<BodyLocationPart> bodyLocationParts) {
        this.context = context;
        this.bodyLocationParts = bodyLocationParts;
    }

    @Override
    public int getCount() {
        return bodyLocationParts.size();
    }

    @Override
    public Object getItem(int position) {
        return bodyLocationParts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return bodyLocationParts.get(position).getBodyLocationPartId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        convertView = LayoutInflater.from(context).inflate(R.layout.spinner_item, null);
        TextView textViewCountryName = (TextView) convertView.findViewById(R.id.textView);
        textViewCountryName.setText(bodyLocationParts.get(position).getName());

        if (menuItemsInterface != null) {
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    menuItemsInterface.onItemClick(position, bodyLocationParts.get(position));
                }
            });
        }


        return convertView;
    }

    @Nullable
    @Override
    public CharSequence[] getAutofillOptions() {
        return new CharSequence[0];
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            return null;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            String str = ((BodyLocationPart) resultValue).getName();
            return str;
        }

//        @Override
//        protected FilterResults performFiltering(CharSequence constraint) {
//            if (constraint != null) {
//                suggestions.clear();
//                for (CountryModel countryModel : tempItems) {
//                    if (countryModel.getCountryName().toLowerCase().contains(constraint.toString().toLowerCase())) {
//                        suggestions.add(countryModel);
//                    }
//                }
//                FilterResults filterResults = new FilterResults();
//                filterResults.values = suggestions;
//                filterResults.count = suggestions.size();
//                return filterResults;
//            } else {
//                return new FilterResults();
//            }
//        }
//
//        @Override
//        protected void publishResults(CharSequence constraint, FilterResults results) {
//            ArrayList<CountryModel> filterList = (ArrayList<CountryModel>) results.values;
//            if (results != null && results.count > 0) {
//                filterList.clear();
//                for (CountryModel countryModel :  filterList) {
//                    add(countryModel);
//                    notifyDataSetChanged();
//                }
//            }
//        }
    };
}
