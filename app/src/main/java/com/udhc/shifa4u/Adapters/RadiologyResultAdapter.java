package com.udhc.shifa4u.Adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.udhc.shifa4u.Models.RadiologyResultModel;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.Events;
import com.udhc.shifa4u.Utilities.GlobalBus;

import java.util.ArrayList;

/**
 * Created by Ali Imran Bangash on 7/8/2018.
 */

public class RadiologyResultAdapter extends BaseAdapter {

    Activity context;
    ArrayList<RadiologyResultModel> radiologyResultModel;
    int count;
    private ProgressDialog progressDialog;
    public RadiologyResultAdapter(Activity context, ArrayList<RadiologyResultModel> radiologyResultModel)
    {
        this.context=context;
        this.radiologyResultModel =radiologyResultModel;
    }

    @Override
    public int getCount() {
        return radiologyResultModel.size();
    }

    @Override
    public Object getItem(int position) {
        return radiologyResultModel.get(position);
    }

    @Override
    public long getItemId(int position) {
        return radiologyResultModel.get(position).getPatientResultId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView= LayoutInflater.from(context).inflate(R.layout.result_layout,null);
        TextView txt_detail=(TextView)convertView.findViewById(R.id.txt_detail);
        TextView txt_order_no=(TextView)convertView.findViewById(R.id.txt_order_no);
        TextView txt_order_date=(TextView)convertView.findViewById(R.id.txt_order_date);
        Button btn_download_pdf=(Button)convertView.findViewById(R.id.btn_download_pdf);
        txt_detail.setText(radiologyResultModel.get(position).getReportDetail());
        txt_order_no.setText(Html.fromHtml("<b>Order Reference Number: ")+ radiologyResultModel.get(position).getOrderId());
        txt_order_date.setText(Html.fromHtml("<b>Order Date: ")+ radiologyResultModel.get(position).getOrderDate());

        btn_download_pdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Events.DownloadResultFile activityFragmentMessageEvent =
                        new Events.DownloadResultFile(String.valueOf(radiologyResultModel.get(position).getPaths()));
                GlobalBus.getBus().post(activityFragmentMessageEvent);
            }
        });
        return convertView;
    }

    public void showProgressDialog(String message) {
        if (progressDialog == null)
            progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    public void dismissProgressDislog() {
        if (progressDialog.isShowing() && progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }
}
