package com.udhc.shifa4u.Adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.udhc.shifa4u.Interfaces.MenuItemsInterface;
import com.udhc.shifa4u.Models.AreasModel;
import com.udhc.shifa4u.Models.CityModel;
import com.udhc.shifa4u.R;

import java.util.ArrayList;

public class AreaAdaptor extends BaseAdapter implements Filterable {

    Context context;
    ArrayList<AreasModel> areasModels;
    MenuItemsInterface menuItemsInterface;


    public AreaAdaptor(Context context, ArrayList<AreasModel> areaModels, MenuItemsInterface menuItemsInterface) {
        this.context = context;
        this.areasModels = areaModels;
        this.menuItemsInterface = menuItemsInterface;
    }

    public AreaAdaptor(Context context, ArrayList<AreasModel> areasModels) {
        this.context = context;
        this.areasModels = areasModels;
    }

    @Override
    public int getCount() {
        return areasModels.size();
    }

    @Override
    public Object getItem(int position) {
        return areasModels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return Long.parseLong(areasModels.get(position).getAreaId());
    }
    @Nullable
    @Override
    public CharSequence[] getAutofillOptions() {
        return new CharSequence[0];
    }


    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        view = LayoutInflater.from(context).inflate(R.layout.spinner_item, null);
        TextView textViewCountryName = (TextView) view.findViewById(R.id.textView);
        textViewCountryName.setText(areasModels.get(i).getAreaName());

        if (menuItemsInterface != null) {
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    menuItemsInterface.onItemClick(i, areasModels.get(i));
                }
            });
        }


        return view ;
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            return null;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            String str = ((AreasModel) resultValue).getAreaName();
            return str;
        }

//        @Override
//        protected FilterResults performFiltering(CharSequence constraint) {
//            if (constraint != null) {
//                suggestions.clear();
//                for (CountryModel countryModel : tempItems) {
//                    if (countryModel.getCountryName().toLowerCase().contains(constraint.toString().toLowerCase())) {
//                        suggestions.add(countryModel);
//                    }
//                }
//                FilterResults filterResults = new FilterResults();
//                filterResults.values = suggestions;
//                filterResults.count = suggestions.size();
//                return filterResults;
//            } else {
//                return new FilterResults();
//            }
//        }
//
//        @Override
//        protected void publishResults(CharSequence constraint, FilterResults results) {
//            ArrayList<CountryModel> filterList = (ArrayList<CountryModel>) results.values;
//            if (results != null && results.count > 0) {
//                filterList.clear();
//                for (CountryModel countryModel :  filterList) {
//                    add(countryModel);
//                    notifyDataSetChanged();
//                }
//            }
//        }
    };
}
