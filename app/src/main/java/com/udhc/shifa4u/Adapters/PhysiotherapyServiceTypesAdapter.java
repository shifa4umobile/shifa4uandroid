package com.udhc.shifa4u.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.chrynan.picassobase64.Picasso;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.udhc.shifa4u.Models.PhysiotherapyServiceTypesModel;
import com.udhc.shifa4u.Models.StaticHealthCareModel;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.Base64ImageDownloader;

import java.util.ArrayList;

/**
 * Created by Ali Imran Bangash on 7/11/2018.
 */

public class PhysiotherapyServiceTypesAdapter extends BaseAdapter {

    private final DisplayImageOptions options;
    Context context;
    ArrayList<PhysiotherapyServiceTypesModel> physiotherapyServiceTypesModels;

    public PhysiotherapyServiceTypesAdapter(Context context, ArrayList<PhysiotherapyServiceTypesModel> physiotherapyServiceTypesModels) {
        this.context = context;
        this.physiotherapyServiceTypesModels = physiotherapyServiceTypesModels;

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.logo)
                .showImageForEmptyUri(R.drawable.logo)
                .showImageOnFail(R.drawable.logo)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .extraForDownloader(new Base64ImageDownloader(context))
                .displayer(new RoundedBitmapDisplayer(20))
                .build();







    }

    @Override
    public int getCount() {
        return physiotherapyServiceTypesModels.size();
    }

    @Override
    public Object getItem(int position) {
        return physiotherapyServiceTypesModels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return physiotherapyServiceTypesModels.get(position).getMedicalSpecialityId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.health_care_static, null);

        ImageView img_health_care = (ImageView) convertView.findViewById(R.id.img_health_care);
        TextView txt_title_health_care = (TextView) convertView.findViewById(R.id.txt_title_health_care);
        TextView txt_des_health_care = (TextView) convertView.findViewById(R.id.txt_des_health_care);

        try {
//            ImageLoader.getInstance().displayImage("http://www.33junaid.xyz/api/common/v1/displayfilebyfileid?id="+physiotherapyServiceTypesModels.get(position).getImageUrlWeb(), img_health_care, options);
            Picasso.with(context).load("https://api.shifa4u.com/api/common/v1/displayfilebyfileid?id="+physiotherapyServiceTypesModels.get(position).getImageUrlWeb()).into(img_health_care);
        } catch (Exception e) {
            e.printStackTrace();
        }

        txt_title_health_care.setText(physiotherapyServiceTypesModels.get(position).getMedicalSpecialityName());
        txt_des_health_care.setText(physiotherapyServiceTypesModels.get(position).getDescription());


        return convertView;
    }
}
