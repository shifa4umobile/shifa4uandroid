package com.udhc.shifa4u.Adapters;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.udhc.shifa4u.Activities.MeetOurProviders;
import com.udhc.shifa4u.Models.AmericanTeleclinic.Physician;
import com.udhc.shifa4u.Activities.PhysicianDetail;
import com.udhc.shifa4u.Activities.SubmitActivity;
import com.udhc.shifa4u.Models.AmericanTeleclinic.ProviderImages;
import com.udhc.shifa4u.R;

import java.util.ArrayList;
import java.util.List;

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.MyViewHolder> {

    List<Physician> providers;
    MeetOurProviders context;
    Bitmap bitmap;
    ArrayList<ProviderImages> images;
    RequestQueue requestQueue;

    ArrayList<String> doctorsNames;
    ArrayList<String> specialitiesNames;

     public String imageUrl = "https://api.shifa4u.com/api/common/v1/displayimagebyimageid?id=";
     public String fileUrl = "https://api.shifa4u.com/api/common/v1/displayfilebyfilename?filename=";




    public RVAdapter(List<Physician> providers, MeetOurProviders context) {

        this.providers = providers;
        this.context = context;
        images = new ArrayList<>();
        doctorsNames = new ArrayList<>();
        specialitiesNames = new ArrayList<>();


        requestQueue = Volley.newRequestQueue(context);
    }



    public static class MyViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView name;
        TextView speciality;
        TextView location;
        ImageView personPhoto;
        ImageView flagImage;
        Button btnViewProfile;
        Button btnAppointment;


        MyViewHolder(View itemView) {
            super(itemView);
            cv = itemView.findViewById(R.id.provider_card_view);
            name = itemView.findViewById(R.id.tvPersonName);
            speciality = itemView.findViewById(R.id.tvSpeciality);
            location = itemView.findViewById(R.id.personLocation);
            personPhoto = itemView.findViewById(R.id.personPhoto);
            flagImage = itemView.findViewById(R.id.flagImage);
            btnViewProfile = itemView.findViewById(R.id.btnViewProfle);
            btnAppointment = itemView.findViewById(R.id.btnAppointment);
        }
    }



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.provider_card_view, parent, false);
        return new MyViewHolder(view);
    }

    @SuppressLint("StaticFieldLeak")
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        TextView textViewName = holder.name;
        TextView textViewSpeciality = holder.speciality;
        final TextView textViewLocation = holder.location;
        ImageView profileImage = holder.personPhoto;
        ImageView flagImage = holder.flagImage;
        Button btnViewProfile = holder.btnViewProfile;
        Button btnAppointment = holder.btnAppointment;

        textViewName.setText(providers.get(listPosition).getPhysicianNameWithDesignation().trim());
        textViewSpeciality.setText(providers.get(listPosition).getSpecialityName().trim());
        textViewLocation.setText(providers.get(listPosition).getCurrentPracticeLocation().trim());

        final long flagId = Long.parseLong(providers.get(listPosition).getFlagImageId());
        Glide.with(context).load(imageUrl + flagId).into(flagImage);

        final long id = Long.parseLong(providers.get(listPosition).getImageMobId());
        RequestOptions requestOption = new RequestOptions().placeholder(R.drawable.place_holder).centerCrop();
        Glide.with(context).load(imageUrl+id).apply(requestOption).into(profileImage);

        btnAppointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                for (int i = 0; i < providers.size(); i++) {
                    doctorsNames.add(providers.get(i).getPhysicianName());
                    specialitiesNames.add(providers.get(i).getSpecialityName());
                }
                Intent myIntent = new Intent(context, SubmitActivity.class);
                myIntent.putExtra("PhysicianId", providers.get(listPosition).getPhysicianId());
                myIntent.putExtra("ProductLineId", providers.get(listPosition).getProductLineId());
                myIntent.putExtra("MedicalSpecialityId", providers.get(listPosition).getMedicalBranchId());
                myIntent.putExtra("doctorName", providers.get(listPosition).getPhysicianName());
                myIntent.putExtra("specialityName", providers.get(listPosition).getSpecialityName());
                context.startActivity(myIntent);
                context.overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );
            }
        });

        btnViewProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent myIntent = new Intent(context, PhysicianDetail.class);
                myIntent.putExtra("MyClass", providers.get(listPosition));
                context.startActivity(myIntent);
                context.overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );

            }
        });

    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(html);
        }
    }

    public void addAll (List<Physician> temProviders , ProgressDialog progressDialog)
    {
        //mLoadedItems++;
        this.providers.addAll(temProviders);
        notifyDataSetChanged();
        progressDialog.dismiss();
    }

    public void addAll (List<Physician> temProviders )
    {
        //mLoadedItems++;
        this.providers.addAll(temProviders);
        notifyDataSetChanged();
      //  progressDialog.dismiss();
    }

    @Override
    public int getItemCount() {
        return providers.size();
    }



}