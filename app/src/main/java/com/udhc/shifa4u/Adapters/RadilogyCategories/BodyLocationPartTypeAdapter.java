package com.udhc.shifa4u.Adapters.RadilogyCategories;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.udhc.shifa4u.Interfaces.MenuItemsInterface;
import com.udhc.shifa4u.Models.RadiologyCategories.BodyLocationPart;
import com.udhc.shifa4u.Models.RadiologyCategories.BodyLocationPartType;
import com.udhc.shifa4u.R;

import java.util.ArrayList;

/**
 * Created by Ali Imran Bangash on 6/27/2018.
 */

public class BodyLocationPartTypeAdapter extends BaseAdapter implements Filterable {
    Context context;
    ArrayList<BodyLocationPartType> bodyLocationPartTypes;
    MenuItemsInterface menuItemsInterface;

    public BodyLocationPartTypeAdapter(Context context, ArrayList<BodyLocationPartType> bodyLocationPartTypes, MenuItemsInterface menuItemsInterface) {
        this.context = context;
        this.bodyLocationPartTypes = bodyLocationPartTypes;
        this.menuItemsInterface = menuItemsInterface;
    }

    public BodyLocationPartTypeAdapter(Context context, ArrayList<BodyLocationPartType> bodyLocationPartTypes) {
        this.context = context;
        this.bodyLocationPartTypes = bodyLocationPartTypes;
    }

    @Override
    public int getCount() {
        return bodyLocationPartTypes.size();
    }

    @Override
    public Object getItem(int position) {
        return bodyLocationPartTypes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return bodyLocationPartTypes.get(position).getBodyLocationPartTypeId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        convertView = LayoutInflater.from(context).inflate(R.layout.spinner_item, null);
        TextView textViewCountryName = (TextView) convertView.findViewById(R.id.textView);
        textViewCountryName.setText(bodyLocationPartTypes.get(position).getName());

        if (menuItemsInterface != null) {
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    menuItemsInterface.onItemClick(position, bodyLocationPartTypes.get(position));
                }
            });
        }


        return convertView;
    }

    @Nullable
    @Override
    public CharSequence[] getAutofillOptions() {
        return new CharSequence[0];
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            return null;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            String str = ((BodyLocationPartType) resultValue).getName();
            return str;
        }

//        @Override
//        protected FilterResults performFiltering(CharSequence constraint) {
//            if (constraint != null) {
//                suggestions.clear();
//                for (CountryModel countryModel : tempItems) {
//                    if (countryModel.getCountryName().toLowerCase().contains(constraint.toString().toLowerCase())) {
//                        suggestions.add(countryModel);
//                    }
//                }
//                FilterResults filterResults = new FilterResults();
//                filterResults.values = suggestions;
//                filterResults.count = suggestions.size();
//                return filterResults;
//            } else {
//                return new FilterResults();
//            }
//        }
//
//        @Override
//        protected void publishResults(CharSequence constraint, FilterResults results) {
//            ArrayList<CountryModel> filterList = (ArrayList<CountryModel>) results.values;
//            if (results != null && results.count > 0) {
//                filterList.clear();
//                for (CountryModel countryModel :  filterList) {
//                    add(countryModel);
//                    notifyDataSetChanged();
//                }
//            }
//        }
    };
}
