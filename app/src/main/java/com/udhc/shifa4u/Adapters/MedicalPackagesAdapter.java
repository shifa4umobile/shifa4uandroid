package com.udhc.shifa4u.Adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.nostra13.universalimageloader.cache.disc.DiskCache;
import com.udhc.shifa4u.Interfaces.MenuItemsInterface;
import com.udhc.shifa4u.Models.CityModel;
import com.udhc.shifa4u.Models.MedicalPackages.MedicalPackagesModel;
import com.udhc.shifa4u.R;

import java.util.ArrayList;

/**
 * Created by Ali Imran Bangash on 6/27/2018.
 */

public class MedicalPackagesAdapter extends BaseAdapter implements Filterable {
    Context context;
    ArrayList<MedicalPackagesModel> medicalPackagesModels;
    ArrayList<MedicalPackagesModel> mStringFilterList;
    MenuItemsInterface menuItemsInterface;
    private MedicalPackageNameFilter medicalPackageNameFilter;

    public MedicalPackagesAdapter(Context context, ArrayList<MedicalPackagesModel> medicalPackagesModels, MenuItemsInterface menuItemsInterface) {
        this.context = context;
        this.medicalPackagesModels = medicalPackagesModels;
        this.mStringFilterList=medicalPackagesModels;
        this.menuItemsInterface = menuItemsInterface;
    }

    @Override
    public int getCount() {
        return medicalPackagesModels.size();
    }

    @Override
    public Object getItem(int position) {
        return medicalPackagesModels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return medicalPackagesModels.get(position).getLabPackageId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        convertView = LayoutInflater.from(context).inflate(R.layout.labs_item_layout, null);
        TextView txt_title = (TextView) convertView.findViewById(R.id.txt_title);
        TextView txt_detail = (TextView) convertView.findViewById(R.id.txt_detail);
        Button btn_detail = (Button) convertView.findViewById(R.id.btn_detail);
        Button btn_add_to_cart = (Button) convertView.findViewById(R.id.btn_add_to_cart);
        txt_title.setText(medicalPackagesModels.get(position).getName());
        txt_detail.setText(medicalPackagesModels.get(position).getShortDescription());

        btn_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuItemsInterface.onButtonClick(position, medicalPackagesModels.get(position), v);
            }
        });

        btn_add_to_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuItemsInterface.onButtonClick(position, medicalPackagesModels.get(position), v);
            }
        });

        return convertView;
    }

    @Nullable
    @Override
    public CharSequence[] getAutofillOptions() {
        return new CharSequence[0];
    }

    @Override
    public Filter getFilter() {
        if(medicalPackageNameFilter==null)
        {
            medicalPackageNameFilter=new MedicalPackageNameFilter();
        }
        return medicalPackageNameFilter;
    }

    private class MedicalPackageNameFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {
                ArrayList<MedicalPackagesModel> medicalPackagesModels = new ArrayList<MedicalPackagesModel>();
                for (int i = 0; i < mStringFilterList.size(); i++) {
                    if ( (mStringFilterList.get(i).getName().toUpperCase() )
                            .contains(constraint.toString().toUpperCase())) {

                        MedicalPackagesModel country = mStringFilterList.get(i);

                        medicalPackagesModels.add(country);
                    }
                }
                results.count = medicalPackagesModels.size();
                results.values = medicalPackagesModels;
            } else {
                results.count = mStringFilterList.size();
                results.values = mStringFilterList;
            }
            return results;

        }

        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            medicalPackagesModels = (ArrayList<MedicalPackagesModel>) results.values;
            notifyDataSetChanged();
        }

    }
}
