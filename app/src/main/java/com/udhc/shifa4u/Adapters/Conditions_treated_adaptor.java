package com.udhc.shifa4u.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.udhc.shifa4u.R;

import java.util.List;

public class Conditions_treated_adaptor extends RecyclerView.Adapter<Conditions_treated_adaptor.ConditionsViewHolder>{

    List<String> items;
    Conditions_treated_adaptor (List<String> items)
    {
        this.items = items;
    }

    public static class ConditionsViewHolder extends RecyclerView.ViewHolder{


        TextView tvCondition;



        ConditionsViewHolder(View itemView) {
            super(itemView);
            tvCondition = (TextView)itemView.findViewById(R.id.tVConditions);

        }
    }
    @NonNull
    @Override
    public ConditionsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.conditions_treated_cardview, parent, false);



        ConditionsViewHolder conditionsViewHolder = new ConditionsViewHolder(view);
        return conditionsViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ConditionsViewHolder holder, int position) {

        TextView tvCondition = holder.tvCondition;
        tvCondition.setText(items.get(position));
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }



    @Override
    public int getItemCount() {
       return items.size();
    }
}
