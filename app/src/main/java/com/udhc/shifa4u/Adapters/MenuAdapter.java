package com.udhc.shifa4u.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.udhc.shifa4u.Interfaces.MenuItemsInterface;
import com.udhc.shifa4u.Models.MenuModel;
import com.udhc.shifa4u.R;

import java.util.ArrayList;

/**
 * Created by Ali Imran Bangash on 7/2/2018.
 */

public class MenuAdapter extends BaseAdapter {
    Context context;
    ArrayList<MenuModel> menuModels;
    MenuItemsInterface menuItemsInterface;
    public MenuAdapter(Context context, ArrayList<MenuModel> menuModels, MenuItemsInterface menuItemsInterface)
    {
        this.context=context;
        this.menuModels=menuModels;
        this.menuItemsInterface=menuItemsInterface;
    }

    @Override
    public int getCount() {
        return menuModels.size();
    }

    @Override
    public Object getItem(int position) {
        return menuModels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView= LayoutInflater.from(context).inflate(R.layout.menu_item,null);

        TextView txt_menu_item=(TextView)convertView.findViewById(R.id.txt_menu_item);
        ImageView img_menu_item=(ImageView)convertView.findViewById(R.id.img_menu_item);

        txt_menu_item.setText(menuModels.get(position).getName());
        img_menu_item.setImageDrawable(context.getResources().getDrawable(menuModels.get(position).getDrawable()));

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuItemsInterface.onItemClick(position,menuModels.get(position));
            }
        });


        return convertView;
    }
}
