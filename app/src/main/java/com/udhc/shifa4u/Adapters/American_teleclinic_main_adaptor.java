package com.udhc.shifa4u.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.udhc.shifa4u.R;

import java.util.List;

public class American_teleclinic_main_adaptor extends RecyclerView.Adapter<American_teleclinic_main_adaptor.AmericanViewHolder>  {

    List<String> items;
    Context context;
    public American_teleclinic_main_adaptor(List<String> items , Context context)
    {
        this.items = items;
        this.context = context;
    }
    @NonNull
    @Override
    public AmericanViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listview_item, parent, false);

       AmericanViewHolder conditionsViewHolder = new AmericanViewHolder(view);
        return conditionsViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull AmericanViewHolder holder, int position) {
        TextView textViewName = holder.textViewName;
        textViewName.setText(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class AmericanViewHolder extends RecyclerView.ViewHolder{


        TextView textViewName;

        AmericanViewHolder(View itemView) {
            super(itemView);
            textViewName = (TextView)itemView.findViewById(R.id.textViewName);

        }
    }

}
