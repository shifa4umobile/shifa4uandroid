package com.udhc.shifa4u.Adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.udhc.shifa4u.Interfaces.MenuItemsInterface;
import com.udhc.shifa4u.Models.MyOrdersMode.AllOrdersModel;
import com.udhc.shifa4u.Models.MyOrdersMode.CanceledOrder;
import com.udhc.shifa4u.Models.MyOrdersMode.ClosedOrders;
import com.udhc.shifa4u.Models.MyOrdersMode.OpenOrder;
import com.udhc.shifa4u.Models.MyOrdersMode.OrderDetailsVM;
import com.udhc.shifa4u.R;

import java.util.ArrayList;

/**
 * Created by Ali Imran Bangash on 7/8/2018.
 */

public class MyOrderDetailAdapter extends BaseAdapter {

    Context context;
    AllOrdersModel allOrdersModel;
    private ProgressDialog progressDialog;

    ArrayList<OrderDetailsVM> orderDetailsVMS;

    MenuItemsInterface menuItemsInterface;

    int tabType;

    public MyOrderDetailAdapter(Context context, ArrayList<OrderDetailsVM> orderDetailsVMS, MenuItemsInterface menuItemsInterface) {
        this.menuItemsInterface = menuItemsInterface;
        this.context = context;
        this.orderDetailsVMS = orderDetailsVMS;

    }

    @Override
    public int getCount() {
        return orderDetailsVMS.size();
    }

    @Override
    public Object getItem(int position) {

        return orderDetailsVMS.get(position);
    }

    @Override
    public long getItemId(int position) {

        return orderDetailsVMS.get(position).getItemId();

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.my_order_detail_item, null);
        TextView txt_product_name = (TextView) convertView.findViewById(R.id.txt_product_name);
        TextView txt_order_ref_code = (TextView) convertView.findViewById(R.id.txt_order_ref_code);
        TextView txt_order_status = (TextView) convertView.findViewById(R.id.txt_order_status);
        TextView txt_provider = (TextView) convertView.findViewById(R.id.txt_provider);
        TextView txt_category = (TextView) convertView.findViewById(R.id.txt_category);
        TextView txt_order_price = (TextView) convertView.findViewById(R.id.txt_order_price);


        txt_product_name.setText(orderDetailsVMS.get(position).getItemName());
        txt_order_ref_code.setText(Html.fromHtml(orderDetailsVMS.get(position).getOrderId()));
        txt_order_status.setText(Html.fromHtml(orderDetailsVMS.get(position).getOrderStatus()));
        txt_provider.setText(Html.fromHtml(orderDetailsVMS.get(position).getVendorName()));
        txt_category.setText(Html.fromHtml(orderDetailsVMS.get(position).getCategory()));
        txt_order_price.setText(Html.fromHtml("Rs. "+orderDetailsVMS.get(position).getPrice()));


//        convertView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (tabType == 1) {
//                    menuItemsInterface.onItemClick(position, orderDetailsVMS.get(position));
//                } else if (tabType == 2) {
//                    menuItemsInterface.onItemClick(position, closedOrders.get(position));
//
//                } else if (tabType == 3) {
//                    menuItemsInterface.onItemClick(position, canceledOrders.get(position));
//
//                }
//
//            }
//        });


//        AbsListView.LayoutParams layoutParams=new AbsListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        layoutParams.setMargins(0,0,0,10);
//        convertView.setLayoutParams(layoutParams);
        return convertView;
    }


}
