package com.udhc.shifa4u.Adapters.RadilogyCategories;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.udhc.shifa4u.Interfaces.MenuItemsInterface;
import com.udhc.shifa4u.Models.CityModel;
import com.udhc.shifa4u.Models.RadiologyCategories.ImagingCategory;
import com.udhc.shifa4u.R;

import java.util.ArrayList;

/**
 * Created by Ali Imran Bangash on 6/27/2018.
 */

public class ImagingCategoryAdapter extends BaseAdapter implements Filterable {
    Context context;
    ArrayList<ImagingCategory> imagingCategories;
    MenuItemsInterface menuItemsInterface;

    public ImagingCategoryAdapter(Context context, ArrayList<ImagingCategory> imagingCategories, MenuItemsInterface menuItemsInterface) {
        this.context = context;
        this.imagingCategories = imagingCategories;
        this.menuItemsInterface = menuItemsInterface;
    }

    public ImagingCategoryAdapter(Context context, ArrayList<ImagingCategory> imagingCategories) {
        this.context = context;
        this.imagingCategories = imagingCategories;
    }

    @Override
    public int getCount() {
        return imagingCategories.size();
    }

    @Override
    public Object getItem(int position) {
        return imagingCategories.get(position);
    }

    @Override
    public long getItemId(int position) {
        return imagingCategories.get(position).getCityId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        convertView = LayoutInflater.from(context).inflate(R.layout.radio_logy_category_item_layout, null);
        TextView txt_radiology_category = (TextView) convertView.findViewById(R.id.txt_radiology_category);
        ImageView img_radiology_category = (ImageView) convertView.findViewById(R.id.img_radiology_category);
        txt_radiology_category.setText(imagingCategories.get(position).getName());
        if (imagingCategories.get(position).getName().equalsIgnoreCase("X-Ray")) {
            img_radiology_category.setImageDrawable(context.getResources().getDrawable(R.drawable.x_ray));
        } else if (imagingCategories.get(position).getName().equalsIgnoreCase("Ultrasound")) {
            img_radiology_category.setImageDrawable(context.getResources().getDrawable(R.drawable.ultrasound));
        } else if (imagingCategories.get(position).getName().equalsIgnoreCase("CT Scans")) {
            img_radiology_category.setImageDrawable(context.getResources().getDrawable(R.drawable.ct_scan));
        } else if (imagingCategories.get(position).getName().equalsIgnoreCase("MRI")) {
            img_radiology_category.setImageDrawable(context.getResources().getDrawable(R.drawable.mri));
        } else if (imagingCategories.get(position).getName().equalsIgnoreCase("Cardiac Imaging")) {
            img_radiology_category.setImageDrawable(context.getResources().getDrawable(R.drawable.cardiac_imaging));
        } else if (imagingCategories.get(position).getName().equalsIgnoreCase("Mammography")) {
            img_radiology_category.setImageDrawable(context.getResources().getDrawable(R.drawable.mammography));
        } else if (imagingCategories.get(position).getName().equalsIgnoreCase("Flouroscopy")) {
            img_radiology_category.setImageDrawable(context.getResources().getDrawable(R.drawable.fluoroscopy));
        } else if (imagingCategories.get(position).getName().equalsIgnoreCase("DEXA Scan")) {
            img_radiology_category.setImageDrawable(context.getResources().getDrawable(R.drawable.dexascan));
        } else if (imagingCategories.get(position).getName().equalsIgnoreCase("Stress Test")) {
            img_radiology_category.setImageDrawable(context.getResources().getDrawable(R.drawable.stress_test));
        } else if (imagingCategories.get(position).getName().equalsIgnoreCase("Nuclear Medicine & PET Scan")) {
            img_radiology_category.setImageDrawable(context.getResources().getDrawable(R.drawable.nuclear_medicine));
        }
        if (menuItemsInterface != null) {
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    menuItemsInterface.onItemClick(position, imagingCategories.get(position));
                }
            });
        }


        return convertView;
    }

    @Nullable
    @Override
    public CharSequence[] getAutofillOptions() {
        return new CharSequence[0];
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            return null;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            String str = ((ImagingCategory) resultValue).getName();
            return str;
        }

//        @Override
//        protected FilterResults performFiltering(CharSequence constraint) {
//            if (constraint != null) {
//                suggestions.clear();
//                for (CountryModel countryModel : tempItems) {
//                    if (countryModel.getCountryName().toLowerCase().contains(constraint.toString().toLowerCase())) {
//                        suggestions.add(countryModel);
//                    }
//                }
//                FilterResults filterResults = new FilterResults();
//                filterResults.values = suggestions;
//                filterResults.count = suggestions.size();
//                return filterResults;
//            } else {
//                return new FilterResults();
//            }
//        }
//
//        @Override
//        protected void publishResults(CharSequence constraint, FilterResults results) {
//            ArrayList<CountryModel> filterList = (ArrayList<CountryModel>) results.values;
//            if (results != null && results.count > 0) {
//                filterList.clear();
//                for (CountryModel countryModel :  filterList) {
//                    add(countryModel);
//                    notifyDataSetChanged();
//                }
//            }
//        }
    };
}
