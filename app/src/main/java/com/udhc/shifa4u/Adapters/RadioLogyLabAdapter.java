package com.udhc.shifa4u.Adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.udhc.shifa4u.Interfaces.MenuItemsInterface;
import com.udhc.shifa4u.Models.LabDetailModel;
import com.udhc.shifa4u.Models.RadioLogyDetail.RadiologyLabsComparison;
import com.udhc.shifa4u.R;

import java.util.ArrayList;

/**
 * Created by Ali Imran Bangash on 6/27/2018.
 */

public class RadioLogyLabAdapter extends BaseAdapter implements Filterable {
    Context context;
    ArrayList<RadiologyLabsComparison> radiologyLabsComparisons;
    MenuItemsInterface menuItemsInterface;
    public RadioLogyLabAdapter(Context context, ArrayList<RadiologyLabsComparison> radiologyLabsComparisons, MenuItemsInterface menuItemsInterface)
    {
        this.context=context;
        this.radiologyLabsComparisons =radiologyLabsComparisons;
        this.menuItemsInterface=menuItemsInterface;
    }
    @Override
    public int getCount() {
        return radiologyLabsComparisons.size();
    }

    @Override
    public Object getItem(int position) {
        return radiologyLabsComparisons.get(position);
    }

    @Override
    public long getItemId(int position) {
        return radiologyLabsComparisons.get(position).getLabId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        convertView= LayoutInflater.from(context).inflate(R.layout.spinner_item,null);
        TextView textViewCountryName=(TextView)convertView.findViewById(R.id.textView);
        textViewCountryName.setText(radiologyLabsComparisons.get(position).getLabName());


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuItemsInterface.onItemClick(position, radiologyLabsComparisons.get(position));
            }
        });

        return convertView;
    }

    @Nullable
    @Override
    public CharSequence[] getAutofillOptions() {
        return new CharSequence[0];
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            return null;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            String str = ((RadiologyLabsComparison) resultValue).getLabName();
            return str;
        }

//        @Override
//        protected FilterResults performFiltering(CharSequence constraint) {
//            if (constraint != null) {
//                suggestions.clear();
//                for (CountryModel countryModel : tempItems) {
//                    if (countryModel.getCountryName().toLowerCase().contains(constraint.toString().toLowerCase())) {
//                        suggestions.add(countryModel);
//                    }
//                }
//                FilterResults filterResults = new FilterResults();
//                filterResults.values = suggestions;
//                filterResults.count = suggestions.size();
//                return filterResults;
//            } else {
//                return new FilterResults();
//            }
//        }
//
//        @Override
//        protected void publishResults(CharSequence constraint, FilterResults results) {
//            ArrayList<CountryModel> filterList = (ArrayList<CountryModel>) results.values;
//            if (results != null && results.count > 0) {
//                filterList.clear();
//                for (CountryModel countryModel :  filterList) {
//                    add(countryModel);
//                    notifyDataSetChanged();
//                }
//            }
//        }
    };
}
