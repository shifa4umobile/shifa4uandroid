package com.udhc.shifa4u.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.squareup.picasso.Picasso;
import com.udhc.shifa4u.Models.StaticHealthCareModel;
import com.udhc.shifa4u.R;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import okhttp3.OkHttpClient;
import okhttp3.Protocol;

/**
 * Created by Ali Imran Bangash on 7/11/2018.
 */

public class StaticHealthCareAdapter extends BaseAdapter {

    private final DisplayImageOptions options;
    Context context;
    ArrayList<StaticHealthCareModel> staticHealthCareModels;
    public StaticHealthCareAdapter(Context context, ArrayList<StaticHealthCareModel> staticHealthCareModels)
    {
        this.context=context;
        this.staticHealthCareModels=staticHealthCareModels;

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.logo)
                .showImageForEmptyUri(R.drawable.logo)
                .showImageOnFail(R.drawable.logo)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .displayer(new RoundedBitmapDisplayer(20))
                .build();


    }

    @Override
    public int getCount() {
        return staticHealthCareModels.size();
    }

    @Override
    public Object getItem(int position) {
        return staticHealthCareModels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.health_care_static,null);

        ImageView img_health_care=(ImageView)convertView.findViewById(R.id.img_health_care);
        TextView txt_title_health_care=(TextView) convertView.findViewById(R.id.txt_title_health_care);
        TextView txt_des_health_care=(TextView)convertView.findViewById(R.id.txt_des_health_care);

        final OkHttpClient client = new OkHttpClient.Builder()
                .protocols(Collections.singletonList(Protocol.HTTP_1_1))
                .build();

        final Picasso picasso = new Picasso.Builder(context)
                .downloader(new OkHttp3Downloader(client))
                .build();

        picasso.with(context).load(staticHealthCareModels.get(position).getImage()).into(img_health_care);

        //ImageLoader.getInstance().displayImage(staticHealthCareModels.get(position).getImage(), img_health_care, options);
        txt_title_health_care.setText(staticHealthCareModels.get(position).getTitle());
        txt_des_health_care.setText(staticHealthCareModels.get(position).getSortDesc());


        return convertView;
    }
}
