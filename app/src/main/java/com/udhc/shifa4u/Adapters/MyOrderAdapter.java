package com.udhc.shifa4u.Adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.udhc.shifa4u.Activities.Shifa4U;
import com.udhc.shifa4u.Interfaces.MenuItemsInterface;
import com.udhc.shifa4u.Models.LabtestResultModel;
import com.udhc.shifa4u.Models.MyOrdersMode.AllOrdersModel;
import com.udhc.shifa4u.Models.MyOrdersMode.CanceledOrder;
import com.udhc.shifa4u.Models.MyOrdersMode.ClosedOrders;
import com.udhc.shifa4u.Models.MyOrdersMode.OpenOrder;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.api.RestAPIFactory;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ali Imran Bangash on 7/8/2018.
 */

public class MyOrderAdapter extends BaseAdapter {

    Context context;
    AllOrdersModel allOrdersModel;
    private ProgressDialog progressDialog;

    ArrayList <OpenOrder>openOrders;

    ArrayList <ClosedOrders> closedOrders;

    ArrayList <CanceledOrder>  canceledOrders;

    MenuItemsInterface menuItemsInterface;

    int tabType;
    public MyOrderAdapter(Context context, AllOrdersModel allOrdersModel, int tabType, MenuItemsInterface menuItemsInterface)
    {
        this.menuItemsInterface=menuItemsInterface;
        this.context=context;
        this.allOrdersModel=allOrdersModel;
        this.tabType=tabType;
       /* if(tabType==1)
        {*/
            openOrders= (ArrayList<OpenOrder>) allOrdersModel.getOpenOrders();
        //}
        //else if(tabType==2)
        //{
            closedOrders=(ArrayList<ClosedOrders>) allOrdersModel.getClosedOrders();
        //}else if(tabType==3)
        //{
            canceledOrders=(ArrayList<CanceledOrder>) allOrdersModel.getCanceledOrders();
        //}
    }

    @Override
    public int getCount() {
        if(tabType==1)
        {
            if (openOrders != null)
            return openOrders.size();
        }else if(tabType==2)
        {
            if (closedOrders != null)
            return closedOrders.size();
        }else if(tabType==3)
        {
            if (canceledOrders != null)
            return canceledOrders.size();
        }

        return 0;
    }

    @Override
    public Object getItem(int position) {

        if(tabType==1)
        {
            if (openOrders != null)
            return openOrders.get(position);
        }else if(tabType==2)
        {
            if (closedOrders != null)
            return closedOrders.get(position);
        }else if(tabType==3)
        {
            if (canceledOrders != null)
            return canceledOrders.get(position);
        }

        return 0;
    }

    @Override
    public long getItemId(int position) {

        if(tabType==1)
        {
            if (openOrders != null)
            return openOrders.get(position).getItemId();
        }else if(tabType==2)
        {
            if (closedOrders != null)
            return closedOrders.get(position).getItemId();
        }else if(tabType==3)
        {
            if (canceledOrders != null)
            return canceledOrders.get(position).getItemId();
        }

        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView= LayoutInflater.from(context).inflate(R.layout.my_order_list_item,null);
        TextView txt_product_name=(TextView)convertView.findViewById(R.id.txt_product_name);
        TextView txt_order_number=(TextView)convertView.findViewById(R.id.txt_order_number);
        TextView txt_order_date=(TextView)convertView.findViewById(R.id.txt_order_date);
        TextView txt_payment_mode=(TextView)convertView.findViewById(R.id.txt_payment_mode);
        TextView txt_payment_status=(TextView)convertView.findViewById(R.id.txt_payment_status);
        TextView txt_order_price=(TextView)convertView.findViewById(R.id.txt_order_price);


        String other="";

        if (openOrders != null) {

            if (openOrders.get(position).getOrderDetailsVM() != null && openOrders.get(position).getOrderDetailsVM().size() > 0) {
                if (openOrders.get(position).getOrderDetailsVM().size() > 1) {
                    other = " & Other";
                }
            }


            if (tabType == 1) {
                if (openOrders != null) {
                    txt_product_name.setText(openOrders.get(position).getItemName() + other);
                }
                txt_order_number.setText(Html.fromHtml("<font color='#242424'><b>Order Ref Number</b> :  </font><font color='#a1a1a1'>" + openOrders.get(position).getOrderId() + "</font>"));
                txt_order_date.setText(Html.fromHtml("<font color='#242424'><b>Order Date</b> :  </font><font color='#a1a1a1'>" + openOrders.get(position).getOrderDate() + "</font>"));
                txt_payment_mode.setText(Html.fromHtml("<font color='#242424'><b>Payment Mode</b> :  </font><font color='#a1a1a1'>" + openOrders.get(position).getPaymentMethod() + "</font>"));
                txt_payment_status.setText(Html.fromHtml("<font color='#242424'><b>Payment Status</b> :  </font><font color='#a1a1a1'>" + openOrders.get(position).getPaymentStatus() + "</font>"));
                txt_order_price.setText(Html.fromHtml("<font color='#242424'><b>Rs:</b>  </font><font color='#2a9f40'>" + openOrders.get(position).getTotalAmount() + "</font>"));

            } else if (tabType == 2) {
                if (closedOrders != null) {
                    txt_product_name.setText(closedOrders.get(position).getItemName() + other);
                }
                txt_order_number.setText(Html.fromHtml("<font color='#242424'><b>Order Ref Number</b> :  </font><font color='#a1a1a1'>" + closedOrders.get(position).getOrderId() + "</font>"));
                txt_order_date.setText(Html.fromHtml("<font color='#242424'><b>Order Date</b> :  </font><font color='#a1a1a1'>" + closedOrders.get(position).getOrderDate() + "</font>"));
                txt_payment_mode.setText(Html.fromHtml("<font color='#242424'><b>Payment Mode</b> :  </font><font color='#a1a1a1'>" + closedOrders.get(position).getPaymentMethod() + "</font>"));
                txt_payment_status.setText(Html.fromHtml("<font color='#242424'><b>Payment Status</b> :  </font><font color='#a1a1a1'>" + closedOrders.get(position).getPaymentStatus() + "</font>"));
                txt_order_price.setText(Html.fromHtml("<font color='#242424'><b>Rs:</b>  </font><font color='#2a9f40'>" + closedOrders.get(position).getTotalAmount() + "</font>"));


            } else if (tabType == 3) {
                if (canceledOrders != null) {
                    txt_product_name.setText(canceledOrders.get(position).getItemName() + other);
                }
                txt_order_number.setText(Html.fromHtml("<font color='#242424'><b>Order Ref Number</b> :  </font><font color='#a1a1a1'>" + canceledOrders.get(position).getOrderId() + "</font>"));
                txt_order_date.setText(Html.fromHtml("<font color='#242424'><b>Order Date</b> :  </font><font color='#a1a1a1'>" + canceledOrders.get(position).getOrderDate() + "</font>"));
                txt_payment_mode.setText(Html.fromHtml("<font color='#242424'><b>Payment Mode</b> :  </font><font color='#a1a1a1'>" + canceledOrders.get(position).getPaymentMethod() + "</font>"));
                txt_payment_status.setText(Html.fromHtml("<font color='#242424'><b>Payment Status</b> :  </font><font color='#a1a1a1'>" + canceledOrders.get(position).getPaymentStatus() + "</font>"));
                txt_order_price.setText(Html.fromHtml("<font color='#242424'><b>Rs:</b>  </font><font color='#2a9f40'>" + canceledOrders.get(position).getTotalAmount() + "</font>"));


            }
        }


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tabType==1)
                {
                    if(openOrders != null) {
                        if (openOrders.size() >0)
                        menuItemsInterface.onItemClick(position, openOrders.get(position));
                    }
                }else if(tabType==2)
                {
                    if  (closedOrders != null) {
                        if (closedOrders.size() >0)
                        menuItemsInterface.onItemClick(position, closedOrders.get(position));
                    }

                }else if(tabType==3)
                {
                    if (canceledOrders != null) {
                        if (canceledOrders.size() >0)
                        menuItemsInterface.onItemClick(position, canceledOrders.get(position));
                    }

                }

            }
        });


//        AbsListView.LayoutParams layoutParams=new AbsListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        layoutParams.setMargins(0,0,0,10);
//        convertView.setLayoutParams(layoutParams);
        return convertView;
    }




}
