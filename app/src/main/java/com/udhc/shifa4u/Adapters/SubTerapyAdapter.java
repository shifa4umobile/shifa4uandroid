package com.udhc.shifa4u.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.chrynan.picassobase64.Picasso;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.udhc.shifa4u.Interfaces.MenuItemsInterface;
import com.udhc.shifa4u.Models.Procedure;
import com.udhc.shifa4u.R;
import com.udhc.shifa4u.Utilities.MyUtils;

import java.util.ArrayList;

/**
 * Created by Ali Imran Bangash on 7/11/2018.
 */

public class SubTerapyAdapter extends BaseAdapter {

    private final DisplayImageOptions options;
    Context context;
    ArrayList<Procedure> Procedure;
MenuItemsInterface menuItemsInterface;
    public SubTerapyAdapter(Context context, ArrayList<Procedure> Procedure,MenuItemsInterface menuItemsInterface) {
        this.context = context;
        this.Procedure = Procedure;
this.menuItemsInterface=menuItemsInterface;
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.logo)
                .showImageForEmptyUri(R.drawable.logo)
                .showImageOnFail(R.drawable.logo)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .displayer(new RoundedBitmapDisplayer(20))
                .build();


    }

    @Override
    public int getCount() {
        return Procedure.size();
    }

    @Override
    public Object getItem(int position) {
        return Procedure.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.sub_therapy_list, null);

        ImageView img_sub_therapy = (ImageView) convertView.findViewById(R.id.img_sub_therapy);
        TextView txt_sub_therapy_name = (TextView) convertView.findViewById(R.id.txt_sub_therapy_name);
        TextView txt_sub_therapy_other_name = (TextView) convertView.findViewById(R.id.txt_sub_therapy_other_name);
        TextView txt_sub_therapy_price = (TextView) convertView.findViewById(R.id.txt_sub_therapy_price);
        Button btn_order = (Button) convertView.findViewById(R.id.btn_order);

        btn_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuItemsInterface.onButtonClick(position,Procedure.get(position),v);
            }
        });


        try {
//            ImageLoader.getInstance().displayImage("http://www.33junaid.xyz/api/common/v1/displayfilebyfileid?id="+Procedure.get(position).getMedicalProductImageUrlMob(), img_sub_therapy, options);
            Picasso.with(context).load("https://api.shifa4u.com/api/common/v1/displayfilebyfileid?id="+Procedure.get(position).getMedicalProductImageUrlWeb()).into(img_sub_therapy);
        } catch (Exception e) {
            e.printStackTrace();
        }

        txt_sub_therapy_name.setText(Procedure.get(position).getMedicalProductName());
        txt_sub_therapy_price.setText("Price: "+ MyUtils.formateCurrency(Procedure.get(position).getProductLineDiscountedPrice()));
        txt_sub_therapy_other_name.setText("Other Name: "+Procedure.get(position).getMedicalProductUrduName());

        return convertView;
    }
}
